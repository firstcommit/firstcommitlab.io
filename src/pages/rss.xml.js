import rss from '@astrojs/rss';
import { getCollection } from 'astro:content';

const item = {
  title: 'Alpha Centauri: so close you can touch it',
  link: '/blog/alpha-centuari',
  pubDate: new Date('2023-06-04'),
  description:
    'Alpha Centauri is a triple star system, containing Proxima Centauri, the closest star to our sun at only 4.24 light-years away.',
  enclosure: {
    url: '/media/alpha-centauri.aac',
    length: 124568,
    type: 'audio/aac',
  },
};

export async function GET(context) {
  const blog = await getCollection('posts');
  return rss({
    title: 'First Commit',
    description: 'El blog de Yayitazale y Mifulapirus',
    site: "https://first-commit-astro-firstcommit-c2f31a45d5ff63013e3a4a59e7e45c4c.gitlab.io", //context.site,
    items: item,
  })
};