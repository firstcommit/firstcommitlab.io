---
title: Nextcloud, la alternativa de software libre a Google Drive
authors: [yayitazale]
date: 2021-09-26T20:00:00Z
image: /images/posts/nextcloud/clients.jpg
categories:
  - selfhosted
tags:
  - cloud
  - selfhosted
  - guia
  - servidores
  - nomoregoogle
  - google
  - drive

---

Después de una __parón veraniego__ necesario para descansar las mentes, volvemos de nuevo a retomar el blog. Haciendo caso a [la encuesta que os realizamos](https://t.me/firstcommit/29) vamos a empezar el curso con una serie sobre __alternativas a Google__ que completen las que ya hemos ido viendo en otros post (como [Vaultwarden](https://firstcommit.dev/2021/05/09/vaultwarden/)).

Las alternativas que os voy a plantear son todas de __software libre__ que he encontrado tanto en [nomoregoogle.com](https://nomoregoogle.com/) como en [ethical.net](https://ethical.net/), dos páginas que os animo a visitar antes de emprender cualquier __proyecto de selfhosted__. 

<a href="https://nomoregoogle.com/"><img src="/images/posts/nextcloud/intro2.png" alt="nomoregoogle" /></a>

En los __tres post que completarán esta serie__, os dejaré enlaces a otras alternativas que pueden ser de utilidad si se ajustan mejor a vuestras necesidades y que podéis probar para compararlos con los que yo os voy a enseñar a instalar y utilizar. Como siempre, os animo a que __compartáis en los comentarios los resultados__ para que todos podamos aprender juntos.

#### TOC

---
# Nextcloud Vs. OwnCloud

Las alternativas más conocidas y fiables para prescindir de [Dropbox](https://www.dropbox.com/es), [Google Drive](https://drive.google.com/) o cualquier otro software comercial. 

[OwnCloud](https://owncloud.com/) nació allá por 2010 y ha ido creciendo de forma constante hasta hoy. Durante los primeros años de vida daba __algunos problemas de sincronización__ y se generó bastante debate en los foros de la aplicación, lo que conllevó en 2016 a que naciera un _fork_ llamado [Nextcloud](https://nextcloud.com/) que en estos 5 años se ha convertido en __el rey del selfhosted cloud__. 

La interfaz es prácticamente igual, tienen prácticamente las mismas funciones e integraciones con terceros por lo que ambas alternativas son igualmente válidas. Esto es como la _coca-cola_ o la _pepsy_, ambas sirven para lo mismo, parecen iguales pero a mi me gusta más la _coca-cola_. 

<img src="/images/posts/nextcloud/cocapepsy.jpg" alt="intro" class="sombra" />

# Instalando Nextcloud sobre unRaid con Docker

Como vimos el año pasado, [el OS de nuestro servidor es UnRaid](https://firstcommit.dev/2021/01/31/Unraid/). La instalación con cualquier otro OS como __Ubuntu Server__ es prácticamente igual, la única diferencia es que nosotros usaremos la interfaz de instalación de UnRaid para levantar el micro-servicio con Docker y no tendremos que usar la línea de comandos.

El único requisito para instalar Nextcloud es disponer de __una base de datos tipo SQL__, por lo que lo primero que vamos a hacer es instalar la BBDD.

Como siempre, nos logeamos en nuestro servidor y vamos al apartado de Apps y buscamos _MariaDB_. Vamos a utilizar la platilla publicada por el usuario _linuxservers_ que es la que tengo yo instalada:

<img src="/images/posts/nextcloud/mariadb1.png" alt="Mariadb1" class="sombra" />

En la plantilla solo debemos añadir __una contraseña para el administrador__ de la base de datos que podemos generar y guardar usando el [generador de contraseñas de vaultwarden que ya tenemos instalado](https://firstcommit.dev/2021/05/09/vaultwarden/):

<img src="/images/posts/nextcloud/mariadb2.png" alt="Mariadb2" class="sombra" />

Aceptamos y esperamos a que la MariaDB se instale.

Ahora, vamos a abrir la consola del contenedor de MariaDB:

<img src="/images/posts/nextcloud/mariadb3.png" alt="Mariadb3" class="sombra" />

Accedemos a la base de datos con el siguiente comando:

```bash
mysql -u root -p
```

<img src="/images/posts/nextcloud/mariadb4.png" alt="Mariadb4" class="sombra" />

Nos pedirá la contraseña. La copiamos del vault y la pegamos con clic derecho de ratón y le damos a enter.

Ahora ya estamos conectados a la base de datos. Creamos el usuario nextcloud con una nueva contraseña diferente a la de root y damos enter:

```bash
CREATE USER ‘nextcloud’ IDENTIFIED by ‘contraseña’;
```

Creamos la base de datos para nextcloud:

```bash
CREATE DATABASE nextcloud;
```

Y le damos todos los permisos de escritura y lectura al usuario nextcloud usando la contraseña del usuario:

```bash
GRANT ALL PRIVILEGES ON nextcloud.* TO ‘nextcloud’ IDENTIFIED BY ‘contraseña’;
```

Ya tenemos todo listo, guardamos eso si, __la contraseña del usuario nextcloud__ ya que la necesitaremos durante la instalación de la la aplicación.

Cerramos la consola y volvemos a la pestaña APPs de UnRaid y buscamos _nextcloud_. De nuevo, instalamos la plantilla de _linuxservers_:

<img src="/images/posts/nextcloud/nextcloud1.png" alt="Nextcloud1" class="sombra" />

Debemos revisar la ruta donde vamos a guardar todos los documentos del cloud, en este caso voy a usar la ruta _/mnt/user/Media/nextcloud/_ ya que _/Media_ es un [share que vive en el array de discos duros](https://firstcommit.dev/2021/02/20/Unraid_config/#Shares). De esta forma, tendremos todos nuestros archivos protegidos contra pérdida si falla uno de los discos duros del servidor.

También debemos revisar el puerto, ya que es posible que lo tengas ya en uso por alguna otra app. Lo comprobamos abriendo la opción de  _Show docker allocations ..._. Si tenemos algún conflicto, cambiamos el puerto a uno que esté libre.

<img src="/images/posts/nextcloud/nextcloud2.png" alt="Nextcloud2" class="sombra" />

Aceptamos y lanzamos la instalación. 

Al finalizar, vamos al la web que general el contenedor:

<img src="/images/posts/nextcloud/nextcloud3.png" alt="Nextcloud3" class="sombra" />

Aceptamos el aviso de seguridad, esto se debe a que no hemos configurado todavía el SSL:

<img src="/images/posts/nextcloud/nextcloud4.png" alt="Nextcloud4" class="sombra" />

Y ahora rellenamos la configuración de la base de datos. Para hacer pruebas podemos saltarnos el paso de crear una base de datos SQL y utilizar una BBDD embebida SQLite, pero como la idea es utilizarlo en _productivo_, seleccionamos como __base de datos el tipo MySQL__ y metemos los datos de la configuración que hayamos utilizado. Además, debemos crear un usuario administrador del sistema, de nuevo, podemos usar el generador de contraseñas de [vaultwarden](https://firstcommit.dev/2021/05/09/vaultwarden/) para crear una contraseña segura:

<img src="/images/posts/nextcloud/nextcloud5.png" alt="Nextcloud5" class="sombra" />

Cuando tengamos todo rellenado, aceptamos y esperamos a que finalice la instalación:

<img src="/images/posts/nextcloud/nextcloud6.png" alt="Nextcloud6" class="sombra" />

Listo, ya tenemos nuestro cloud privado funcionando.

# Configurar Nextcloud para acceso externo

Lo primero que vamos a hacer es configurar nextcloud para que __pueda ser llamado desde un dominio__ diferente a la IP por defecto. Para ello, abrimos la consola del contenedor de nextcloud y hacemos:

```bash
sudo nano \config\www\nextcloud\config\config.php 
```

y añadimos el dominio que vayamos a usar a la lista de dominios permitidos:

<img src="/images/posts/nextcloud/nextcloud8.png" alt="Nextcloud8" class="sombra" />

Guardamos con _ctrl+o_ y reiniciamos nextcloud para que coja la configuración cambiada y vamos a nuestro [Ngix Proxy Manager](https://firstcommit.dev/2021/04/19/saas/). Agregamos la entrada con el dominio que hemos usado y __OJO__, apuntando al puerto __https__:

<img src="/images/posts/nextcloud/proxy1.png" alt="Proxy1" class="sombra" />
<img src="/images/posts/nextcloud/proxy2.png" alt="Proxy2" class="sombra" />

Si lo hemos hecho todo bien, accedemos al link que acabmaos de crear https://nextloud.tudominio.com y debe saltarnos la pantalla de login sin ningún aviso de certificados:

<img src="/images/posts/nextcloud/proxy3.png" alt="Proxy3" class="sombra" />

# Securizar nuesto Cloud y resolver cualquier problema

Ahora que __tenemos el cloud accesible desde cualquier lugar del mundo__, debemos asegurarnos que tenemos toda la configuración segura. Para ver si todo es correcto vamos a _configuración > Vista General (dentro de administración)_. Aquí apareceran los posibles errores que tengamos, en nuestro caso, un error por que falta la región por defecto:

<img src="/images/posts/nextcloud/seguridad1.png" alt="Seguridad1" class="sombra" />

<img src="/images/posts/nextcloud/seguridad2.png" alt="Seguridad2" class="sombra" />

Volvemos a la consola, lanzamos de nuevo el comando:

```bash
sudo nano \config\www\nextcloud\config\config.php 
```

Y en el último bloque, tras la última línea del array añadimos la siguiente línea con el código ISO de nuestro país:

```php
default_phone_region => ES,
```

<img src="/images/posts/nextcloud/seguridad3.png" alt="Seguridad3" class="sombra" />

Guardamos de nuevo y reiniciamos Nextcloud. Ahora si volvemos a la misma página no tendremos ningún error:

<img src="/images/posts/nextcloud/seguridad4.png" alt="Seguridad4" class="sombra" />

En caso de que tengas algún error, déjalo en los comentarios y te ayudaré a solucionarlo (es común que haya algún error de cabeceras de https etc ya que estamos accediendo a través de un proxy reverso).

Ahora es momento de activar algunos elementos de seguridad adicional. Lo primero de todo debemos activar un __servidor de correo SMPT__ para que Nextcloud nos pueda enviar correos para recuperar la cuenta o avisarnos de cambios de contraseñas. Para esto vamos a _Ajustes Básicos_ y rellenamos la configuración tal que así usando un buzón de correo de GMAIL (puedes usar cualquier alternativa como [protonmail](https://protonmail.com/)...):

<img src="/images/posts/nextcloud/seguridad5.png" alt="Seguridad5" class="sombra" />

Y en la pestaña de tu información personal añades tu correo __al que quieres que lleguen las notificaciones__:

<img src="/images/posts/nextcloud/seguridad6.png" alt="Seguridad6" class="sombra" />

Finalmente en la pestaña de _Seguridad (bajo administración)_ deberíamos modificar los ajustes por defecto de longitud y complejidad de contraseñas, así como activar la __verificación en dos pasos obligatoria para todos__ (o al menos los admins):

<img src="/images/posts/nextcloud/seguridad7.png" alt="Seguridad7" class="sombra" />

# Instalar apps y plugins de utilidad en Nextcloud

Para instalar aplicaciones o plugins en Nextcloud desplegamos la lista de acciones en el usuario y vamos a _Aplicaciones_. 

<img src="/images/posts/nextcloud/apps1.png" alt="Apps1" class="sombra" />

Las Apps más interesantes para instalar desde mi punto de vista son las siguientes:

<img src="/images/posts/nextcloud/apps2.png" alt="Apps2" class="sombra" />
1. Two-Factor TOTP Provider: su función es habilitar la doble autenticación con códigos de tiempo que podemos generar en nuestro vault o aplicaciones móviles como Authy que ya hemos usado.

<img src="/images/posts/nextcloud/apps4.png" alt="Apps4" class="sombra" />
2. Registration: Si planteamos usar nuestro cloud con usuarios (familia, amigos etc) y queremos que ellos mismos puedan crearse la cuenta este plugin nos lo permite

<img src="/images/posts/nextcloud/apps5.png" alt="Apps5" class="sombra" />
3. Splash: habilitamos una imagen de aleatoria de fondo en la pantalla de login

<img src="/images/posts/nextcloud/apps6.png" alt="Apps6" class="sombra" />
4. Theming: si queremos cambiar el logo y/o los colores de la app

<img src="/images/posts/nextcloud/apps7.png" alt="Apps7" class="sombra" />
5. Draw.io: una plataforma de dibujo vectorial para hacer esquemas, planos, etc.

# Crear usuarios para nuestros amigos y familiares

Para crear usuarios el primer paso es crear grupos de usuarios. Si no vamos a complicarnos mucho, simplemente podemos crear un grupo de usuarios:

<img src="/images/posts/nextcloud/usuarios1.png" alt="Usuarios1" class="sombra" />

Nos añadimos nosotros mismos como administradores del grupo:

<img src="/images/posts/nextcloud/usuarios2.png" alt="Usuarios2" class="sombra" />

Si queremos limitar el espacio de almacenamiento de los usuarios de este grupo que por defecto es ilimitado, abajo a la izquierda hacemos clic en _configuración_ y lo modificamos por ejemplo escribiendo 10GB:

<img src="/images/posts/nextcloud/usuarios3.png" alt="Usuarios3" class="sombra" />

Para crear usuarios a mano, hacemos clic en _Nuevo Usuario_ y rellenamos sus datos:

<img src="/images/posts/nextcloud/usuarios6.png" alt="Usuarios6" class="sombra" />

Si hemos instalado y activado el registro de usuarios por su cuenta, conviene que en la _configuración > registro_ habilitemos lo siguiente para __evitar que cualquier persona se pueda crear un usuario__ y utilizar nuestro Cloud:

<img src="/images/posts/nextcloud/usuarios4.png" alt="Usuarios4" class="sombra" />

Finalmente, si queremos que todos los usuarios utilicen el doble factor debemos ir de nuevo a _configuración > seguridad_ y habilitar lo siguiente:

<img src="/images/posts/nextcloud/usuarios5.png" alt="Usuarios5" class="sombra" />

# Uso de Nextcloud

Al igual que las alternativas comerciales, Nextcloud nos permite utilizar nuestro espacio personal directamente desde la __web__ o a través de aplicaciones de __escritorio__ o __móvil__ que sincronizarán de forma automática los archivos con el servidor.

<img src="/images/posts/nextcloud/linux.png" alt="Linux" class="sombra" />

Solo debemos descargas los clientes y utilizar el enlace https://nextloud.tudominio.com al configurarlos. Añadiremos nuestra cuenta con el usuario/contraseña/TOTP y seleccionaremos las carpetas que queramos sincronizar.

Puedes leer más sobre esto en la [propia página de nextcloud](https://nextcloud.com/clients/), pero lo veremos con más profundidad en el siguiente post ya que utilizaremos Nextcloud para hacer las copias de las fotos de nuestro móvil para crear una [alternativa de Google Fotos](https://firstcommit.dev/2021/10/30/photoview/).

---
# EXTRA: Actualizaciones

Para actualizar nuestro servidor de Nextcloud no solo debemos actualizar el contenedor con las nuevas imágenes que vayan saliendo, sino que debemos realizar una tarea manual dentro del propio servidor. 

Cuando haya actualizaciones disponibles nos llegará una notificación ya que somos administradores del sistemas. Cuando eso ocurra debemos ir a _configuración > Vista General (dentro de administración)_ como en los pasos anteriores y en la parte inferior le damos a _Abrir el actualizador_:

<img src="/images/posts/nextcloud/updates1.png" alt="Update1" class="sombra" />

El actualizador no es más que un proceso interno que dejará el sistema en un modo mantenimiento para que en caso de cualquier fallo, podamos volver atrás. Una vez dentro del actualizador le damos al botón de _Start update_:

<img src="/images/posts/nextcloud/updates2.png" alt="Update2" class="sombra" />

Descargará la actualización y comprobará que no hay ningún problema de compatibilidad. Si todo va bien, nos aparecerá otro botón de _Disable maintenance mode and continue in the web based updater_, hacemos clic y nos llevará al último paso en el que actualizaremos el sistema:

<img src="/images/posts/nextcloud/updates3.png" alt="Update3" class="sombra" />

Le damos a _Iniciar actualización_ y esperamos a que termine:

<img src="/images/posts/nextcloud/updates4.png" alt="Update4" class="sombra" />

Finalmente, tras la actualización conviene que volvamos a _configuración > Vista General (dentro de administración)_ para comprobar que no hay errores de configuración (por cambios o nuevas funciones que no tenemos aún configuradas).

---
<a href="https://t.me/firstcommit" class="btn btn-primary text-white py-2">Suscríbete, que es gratis</a>

*Nota: algunos de los enlaces a productos o servicios pueden ser enlaces referidos con los que podemos obtener una comisión de venta.*

<style> 
img.sombra {
  display: block;  
  margin-left: auto;  
  margin-right: auto; 
  box-shadow: 5px 5px 8px grey;
}
img.sombra-inline {
  display: inline-block; 
  vertical-align: middle; 
  margin-right: 0px; 
  width:auto;
  box-shadow: 3px 3px 8px grey;
}
img.zoom {
  transition: transform .2s;
}
img.zoom:hover {
  -ms-transform: scale(1.2); /* IE 9 */
  -webkit-transform: scale(1.2); /* Safari 3-8 */
  transform: scale(1.2); 
}
</style>
