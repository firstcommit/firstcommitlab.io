---

title: Light bell
authors: [mifulapirus]
date: 2021-02-13T19:40:00Z
image: /images/posts/lightbell/intro.jpg
categories:
  - diy
tags:
  - electronica
  - esp8266
  - wemos d1
  - home automation

---
Todos tenemos alguna persona mayor a la que le cuesta un poco más escuchar el timbre de la puerta o el teléfono, ¿verdad?

He querido estrenarme como Autor en **First Commit** haciendo un proyecto que os anime a fabricar algo para ayudar a quien podáis.

_Light Bell_ es sencillo. Cuando el timbre suena, se enciende un led bien visible que podéis poner en cualquier lado de la casa.

(Foto de portada <a href="https://unsplash.com/es/@etiennegirardet?utm_content=creditCopyText&utm_medium=referral&utm_source=unsplash">Etienne Girardet</a> en <a href="https://unsplash.com/es/fotos/frasco-de-vidrio-transparente-con-liquido-naranja-Ea4OPjLDt5k?utm_content=creditCopyText&utm_medium=referral&utm_source=unsplash">Unsplash</a>)

#### TOC

# Descripción del sistema

El sistema se compone de dos partes:
- **Detector:** Es quien lleva la batuta. Un ESP8266 con un micrófono colocado cerca del timbre que queremos detectar va a ser el que mande la alerta a la base con el LED.
- **LED:** El protagonista de esta parte es un LED que se encenderá cuando el Detector envíe la alerta del timbre. En este caso, un ESP8266 que se conecta a la red del _Detector_ simplemente está a la escucha por UDP hasta que le llegue el comando de encendido.

# Código

Podéis encontrar todo el cógido para este proyecto [aquí](https://github.com/Mifulapirus/LightBell)

# Qué necesitas

1. **[Wemos D1](https://amzn.to/3sfQoSN) x 2:** Te vale cualquier ESP8266. A mi me gusta usar los _Wemos D1_ porque son baratos y ya tienen todos los compònentes que necesitas para programar sin ser tan grandes como un NodeMCU. Yo suelo comprarlos [aquí](https://amzn.to/3cJPUAq).
2. **[Micrófono](https://amzn.to/3g8Cqjn) x 1:** Hay miles por ahí, pero este es el más sencillo de usar.
3. **[LED](https://amzn.to/3ac1MJn) x 1:** Cualquier LED os vale, pero estos son sencillos y los podéis poner de cualquier color que queráis.

# Carcasas

[Aquí puedes descargarte](https://cad.onshape.com/documents/1e1355ad25f4a99da328ce89/w/8b14ae8cd1bbaf309f14ff3e/e/f96d349cdaad7a1031845bd3) los diseños para imprimirte las carcasas del _LightBell_. Está pensado para imprimirlo en 3D, si no tienes una impresora y quieres montarlo, avísame e intentaré ayudarte con eso.

# Paso a Paso

**1. Electrónica:** Monta los componentes siguiendo estos esquemas.
**2. Configura el Detector:** Abre el proyecto con PlatformIO y edita el archivo _/data/config.json_ con los siguientes datos:

```json
    {
      "is_client":false,
      "ap_ssid":"LightBell Server",

      "using_congfig_file":true,
      "device_id":0,
      "device_name":"LightBell",
      "version":"0.1",
      "ap_pass":"lightbell",
      "ap_ip":"10.0.1.1",
      "ap_gateway":"10.0.1.1",
      "ap_mask":"255.255.255.0",
      "udp_port":4210
    }
```

**3. Carga el programa en el Detector:** Si usas _PlatformIO_ en _Visual Studio_, sólo tienes que ir a la _Extensión de PlatformIO --> Project Tasks --> Default/General --> Upload_
**4. Sube la imagen del sistema:** En _PlatformIO_ con _Visual Studio_ ve a la _Extensión de PlatformIO --> Project Tasks --> env:nodemcuv2 --> Platform  --> Upload File Image_

Ahora ya tienes el Detector listo, vamos a por la base!
**5. Configura la base:** Abre el proyecto con PlatformIO y edita el archivo _/data/config.json_ con los siguientes datos:

```json
    {
      "is_client":true,
      "ap_ssid":"LightBell Client",

      "using_congfig_file":true,
      "device_id":0,
      "device_name":"LightBell",
      "version":"0.1",
      "ap_pass":"lightbell",
      "ap_ip":"10.0.1.1",
      "ap_gateway":"10.0.1.1",
      "ap_mask":"255.255.255.0",
      "udp_port":4210
    }
```

**6. Carga el programa en la base:** Si usas _PlatformIO_ en _Visual Studio_, sólo tienes que ir a la _Extensión de PlatformIO --> Project Tasks --> Default/General --> Upload_

**7. Sube la imagen del sistema:** En _PlatformIO_ con _Visual Studio_ ve a la _Extensión de PlatformIO --> Project Tasks --> env:nodemcuv2 --> Platform  --> Upload File Image_

**8. Enciende el Detector y la Base:** Al encenderse el _Detector_, va a crear una red WiFi con su nombre a la que se conectara la base cuando esté lista. Verás que el led del Wemos D1 parpadeará unas cuantas veces en los dos. Cuando pare, sabrás que está todo listo.

**9. Ajusta la ganancia del micrófono:** Pon el _Detector_ cerca del timbre y, con un destornillador pequeño, ajusta el tornillo del micrófono hasta que se encienda sólo cuando suene el timbre.

**10. Dáselo a alguien que lo necesite:** Seguramente hayas hecho este proyecto para alguien que lo necesita más que tu. Pues dáselo y cuentame qué tal ha ido.

---
<a href="https://t.me/firstcommit" class="btn btn-primary text-white py-2">Suscríbete, que es gratis</a>

*Nota: algunos de los enlaces a productos o servicios pueden ser enlaces referidos con los que podemos obtener una comisión de venta.*