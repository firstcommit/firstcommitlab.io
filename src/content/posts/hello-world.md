---

title: It's alive.
authors: [yayitazale, mifulapirus]
date: 2021-01-29T10:00:00Z
image: /images/posts/hello-world/pasted-0.png
categories:
  - blog
tags:
  - blog

---
Hola mundo.

La intención de este blog es crear un espacio donde compartir nuestras experiencias y proyectos DIY y selfhosted que vamos desarrollando y adoptando.

Esperamos ir creando contenido poco a poco.

---
<a href="https://t.me/firstcommit" class="btn btn-primary text-white py-2">Suscríbete, que es gratis</a>
*Nota: algunos de los enlaces a productos o servicios pueden ser enlaces referidos con los que podemos obtener una comisión de venta.*