---
title: Monitorización de servidores con Telegraf, InfluxDB y Grafana
authors: [yayitazale]
date: 2021-06-07T13:00:00Z
image: /images/posts/monitorizacion/banner.jpg
categories:
  - selfhosted
tags:
  - monitoring
  - timeseries
  - influx
  - grafana

---

Ahora que empezamos a tener algo de carga de servicios en [nuestro servidor Unraid](https://firstcommit.dev/2021/01/31/Unraid/), es el momento de añadir una __monitorización de los recursos__ para saber cómo afectan al rendimiento estos despliegues que hemos hecho y los futuros que vayamos a hacer.

Esta monitorización se puede hacer de muchas maneras pero yo os voy a explicar cómo hacerlo utilizando las herramientas open source de [Influx](https://www.influxdata.com/) para __recolectar__ la información del rendimiento e __historizar__ estos datos en una base de datos “especial”, y otra herramienta de software libre llamado Grafana para __graficarlos__. Además, haremos que estas gráficas se vean como otro dashboard más dentro de [Home Assistant](https://firstcommit.dev/2021/03/11/Hass/).

<img src="/images/posts/monitorizacion/intro1.webp" alt="Intro 1" class="sombra" />

Antes de empezar, quería comentar que existe __un stack ya precocinado__ y fácil de instalar llamado [Grafana Unraid Stack](https://forums.unraid.net/topic/96233-support-testdasi-repo/), pero la idea es que en un futuro, podamos utilizar este mismo sistema para graficar valores de sensores de __Home Assistant__ con un formato más bonito y funcional que las gráficas embebidas que traer por defecto, por lo que te recomiendo que sigas los pasos de esta guía y así aprendas cómo funciona, que es de lo que se trata este blog.

(Foto de portada<a href="https://unsplash.com/es/@tvick?utm_content=creditCopyText&utm_medium=referral&utm_source=unsplash">Taylor Vick</a> en <a href="https://unsplash.com/es/fotos/cable-de-red-M5tzZtFCOfs?utm_content=creditCopyText&utm_medium=referral&utm_source=unsplash">Unsplash</a>)

#### TOC
---
# Bases de datos de tipo timeseries con InfluxDB

Lo primero que vamos a hacer para poner en marcha nuestro __sistema de historización y monitorización de UnRaid__ es levantar nuestra __base de datos__ donde guardaremos estos datos.

Como las métricas que vamos a capturar van a ser todas de tipo __tiempo - valor__, o mejor dicho, __timestamp - valor__, las [bases de datos de series temporales](https://en.wikipedia.org/wiki/Time_series_database) son las que mejor se adaptan para tener un buen rendimiento de escritura y lectura. Podríamos usar [bases de datos relacionales](https://es.wikipedia.org/wiki/Base_de_datos_relacional), pero a medida que las tablas crezcan tendremos problemas de rendimiento, por lo que vamos a optar por [InfluxDB](https://www.influxdata.com/products/influxdb/), una BBDD open source muy potente y fácil de instalar y gestionar.

Antes de instalar nada, vamos a ver los __conceptos básicos de InfluxDB__:

  - __InfluxDB 1 Vs. InfluxDB 2__: la primera versión de esta BBDD se publicó allá por el 2013. Desde entonces, Influx ha crecido como organización hasta convertirse en uno de los actores más importantes en cuanto a bases de datos de series temporales (muy útil para el IoT). A finales del año pasado, [se publicó un “mayor change”](https://docs.influxdata.com/influxdb/v2.0/reference/release-notes/influxdb/), InfluxDB 2.0, con uno de los cambios más importantes que es el paso del lenguaje InfluxQL a Flux. Aunque para muchos, el uso de InfluxDB 1.X siga siendo más sencilla, voy a aventurarme a explicaros la instalación y uso de __InfluxDB 2.0__ y __Flux__.
  - __Flux__: es el lenguaje que vamos a utilizar a la hora de hacer las “querys” a la BBDD, ya que la escritura de datos lo hará Telegraf por nosotros.
  - __Organizations__: influx está pensado para un posible entorno empresarial, por lo que nos obliga a crear organizaciones, que serán en el fondo, una especie de “armarios” de datos estancos donde guardaremos los diferentes cubos de cada organización por separado. Sirve sobre todo para dividir los accesos ya que como veremos, los tokens de acceso serán siempre dirigidos a una organización/cubo en concreto. Si vienes de SQL, esto sería una BBDD como tal.
  - __Bucket__: los cubos son las tablas donde almacenaremos los datos. Podremos crear tantos cubos como queramos para cada organización.
  - __Retention policy__: a cada cubo le especificaremos por cuanto tiempo queremos guardar los datos. Esto es muy útil ya que podremos almacenar por periodos diferentes los datos con diferentes necesidades.
  - __Tasks__: otra función de Influx es la posibilidad de crear tareas para jugar con los datos. _*Ejemplos: disminuir la resolución de unos datos a medias horarias y guardarlas en otro bucket diferente, crear datos calculados sumando dos señales, etc.*_
  - __Kapacitor__: es el servicio embebido que realiza las tareas.
  - __Chronograf__: es la interfaz web para gestionar influxdb y kapacitor.
  - __Authentication token__: los permisos de lectura y escritura a diferentes organizaciones y buckets se crean en Chronograf y en vez de ser los clásicos usuario/contraseña, son tokens únicos.

Bien, ahora que tenemos los conceptos básicos claros, vamos a nuestra App store de UnRaid, buscamos por “Influxdb” e instalamos la versión oficial:

<img src="/images/posts/monitorizacion/influx1.png" alt="Influx 1" class="sombra" />

La plantilla ya viene lista y no hace falta cambiar nada por lo que aceptamos y esperamos que se baje la imagen y se levante el contenedor:

<img src="/images/posts/monitorizacion/influx2.png" alt="Influx 2" class="sombra" />

<img src="/images/posts/monitorizacion/influx3.png" alt="Influx 3" class="sombra" />

Ahora como siempre, vamos a la pestaña Docker y en el contenedor de InfluxDB hacemos click en el logo y vamos _*WebUI*_ para que nos lleve a __Chronograf__:

<img src="/images/posts/monitorizacion/influx4.png" alt="Influx 4" class="sombra" />

Nos encontraremos con un clásico _*Welcome Aboard*_ en el que tendremos que crear una cuenta de Administrador, una organización y un cubo. Las credenciales las podemos crear y guardar en [nuestro gestor de contraseñas](https://firstcommit.dev/2021/05/09/vaultwarden/) para que no se nos olviden. En mi caso, a la organización le he llamado __Home__ y el primer bucket se llamará __Unraid__ ya que es donde guardaremos las métricas del propio server:

<img src="/images/posts/monitorizacion/influx5.png" alt="Influx 5" class="sombra" />

Hacemos click en _*Advanced*_ (tranquilo, yo te voy a guiar para que no te pierdas) que nos llevará directo a la página de configuración de los buckets. Ya que las métricas que vamos a guardar no van a ser muy interesantes para analizar cuando pase el tiempo, vamos a cambiar la _*retention policy*_ a 7 días, de forma que se vayan limpiando los datos antiguos de forma automática. Para esto, hacemos click en el boton _*settings*_ del bucket Unraid y seleccionamos _*7 days*_ (o el tiempo que queráis vosotros):

<img src="/images/posts/monitorizacion/influx6.png" alt="Influx 6" class="sombra" />

Ahora vamos a la pestaña _*Telegraf*_ y vamos a crear una plantilla preconfigurada de Telegraf, de forma que cuando vamos a instalar el colector de datos simplemente usemos dicha plantilla __sin tener que tocar nada__. Le damos a _*Create*_ y seleccionamos _*System*_ para que recoja los datos de rendimiento del sistema y _*Docker*_ para que nos monitorice los contenedores y su impacto:

<img src="/images/posts/monitorizacion/influx7.png" alt="Influx 7" class="sombra" />

Le ponemos un nombre a la configuración y vemos que _*Docker*_ aparece en gris en la lista de la izquierda de los componentes a monitorizar. Si hacemos clic sobre él, nos lleva a la configuración que falta, que es la de definir el __endpoint de docker__. En UnRaid, ese endpoint es el siguiente:

```
unix:///var/run/docker.sock
```

Añadimos esto, aceptamos y finalizamos la configuración. Ahora tenemos que copiar el __token de acceso__ que acabamos de generar en un block de notas. Aceptamos y hacemos click sobre _*Unraid telegraf*_, lo que nos abrirá el propio archivo ya generado. Los descargamos y abrimos con [nuestro editor VsCode](https://firstcommit.dev/2021/03/29/VScode-01-instalacion/), vamos a la línea donde pone token y quitamos la variable y lo metemos directamente:

<img src="/images/posts/monitorizacion/influx8.png" alt="Influx 8" class="sombra" />

Sin cerrar esta pestaña del navegador, vamos al siguiente paso de instalar telegraf.

---
# Recolección de datos con Telegraf

[Telegraf](https://www.influxdata.com/time-series-platform/telegraf/) es un agente liviano que podemos instalar y ejecutar prácticamente __en cualquier tipo de entorno__ (Windows, Linux etc) y que no solo permite capturar métricas de rendimiento de servidores, sino que sirve también como conector entre cientos de plataformas como [Nagios](https://www.nagios.org/syslog), [Syslog](https://es.wikipedia.org/wiki/Syslog), [Kubernetes](https://kubernetes.io/es/), [Collectd](https://collectd.org/), [JSON](https://es.wikipedia.org/wiki/JSON), [MQTT](https://firstcommit.dev/2021/03/14/mqtt/), [CSV](https://es.wikipedia.org/wiki/Valores_separados_por_comas) etc. 

Está pensado para funcionar directamente con InfluxDB como base de datos donde historizar los datos, por lo que su integración es muy sencilla. Como ya tenemos instalado InfluxDB y creado un token de acceso y un archivo de configuración, antes de instalar tenemos que crear la ruta y copiar este archivo a dicha ruta.

Para hacer esto, vamos al explorador de archivos de windows y vamos a la ruta de _*\\IP-DE-UNRAID\appdata*_, [suponiendo que tienes el share accesible](https://firstcommit.dev/2021/02/20/Unraid_config/), y creamos la carpeta _*telegraf*_.

Copiamos el archivo _*unraid_telegraf.conf*_ que nos hemos descargado a esta carpeta y lo renombramos por _*telegraf.conf*_

Ahora ya si, __vamos a instalar el agente de telegraf__. Para ello, vamos a la app store de UnRaid y buscamos pos telegraf:

<img src="/images/posts/monitorizacion/telegraf1.png" alt="Telegraf 1" class="sombra" />

Lo instalamos sin cambiar nada ya que la plantilla ya tiene la ruta del endpoint de docker y la ruta del archivo de configuración:

<img src="/images/posts/monitorizacion/telegraf2.png" alt="Telegraf 2" class="sombra" />

Al arrancar el contenedor, si hemos hecho todo bien en el log deberemos ver algo tal que así (sin errores en rojo):

<img src="/images/posts/monitorizacion/telegraf3.png" alt="Telegraf 3" class="sombra" />

Bien, ya estamos historizando los datos de nuestro sistema. Antes de pasar al siguiente punto, visualización, vamos a __crear un token solo de lectura__ que utilizaremos para leer los datos. Para ello, volvemos a la pestaña del navegador donde tenemos Chronograf, vamos a _*Token*_ -> _*Generate Token*_ y lo configuramos con permisos solo de lectura en el bucket Unraid tal que así:

<img src="/images/posts/monitorizacion/telegraf4.png" alt="Telegraf 4" class="sombra" />

Y después, hacemos click en el nombre del token y lo copiamos en un block de notas para usarlo después.

---
# Gráficas y más gráficas con Grafana


[Grafana](https://grafana.com/) se ha convertido en uno de los motores de visualización de datos más utilizados, sobre todo en el mundo de la __monitorización de activos informáticos__, pero cada vez más está cogiendo fuerza su uso en otro tipo de entornos como la __monitorización de procesos industriales__. No puedo dejar de mencionar otras plataformas muy interesantes de análisis de datos, como [ThingsBoard](https://thingsboard.io/), [Kibana](https://www.elastic.co/es/kibana) o [Plotly](https://plotly.com/), todos ellos muy potentes en su área pero no idóneos para esta tarea.

En Grafana, existe una [versión de pago en su cloud](https://grafana.com/products/cloud/), pero en nuestro caso vamos a instalar la [versión completamente libre](https://grafana.com/oss/). Esto, de nuevo, lo haremos desde la app store de UnRaid:

<img src="/images/posts/monitorizacion/grafana1.png" alt="Grafana 1" class="sombra" />

<img src="/images/posts/monitorizacion/grafana2.png" alt="Grafana 2" class="sombra" />

En la configuración del contenedor no tocamos nada y arrancamos el servicio. Vamos a la _*WebUI*_ y entramos con el usuario y contraseña __admin/admin__, tras lo cual, nos pedirá una nueva contraseña que guardaremos en [nuestro vault](https://firstcommit.dev/2021/05/09/vaultwarden/):

<img src="/images/posts/monitorizacion/grafana3.png" alt="Grafana 3" class="sombra" />

Grafana tiene muchas posibilidades de configuración que no voy a comentar en este manual ya que nos podríamos eternizar. Os dejo el [enlace a su documentación](https://grafana.com/docs/) para que le echéis un ojo ya que es muy completa.

Nosotros lo que vamos a hacer es añadir como __fuente de datos__ el bucket de Unraid que estamos alimentando con datos en InfluxDB, y después crear un __dashboard__ para visualizar estos datos. Lo primero entonces, es ir a _*Configuration*_ -> _*Data Sources*_ y vamos a _*Add Data Source*_:

<img src="/images/posts/monitorizacion/grafana4.png" alt="Grafana 4" class="sombra" />

Entre los orígenes de datos vemos InfluxDB, lo seleccionamos:

<img src="/images/posts/monitorizacion/grafana5.png" alt="Grafana 5" class="sombra" />

Ahora debemos rellenar los siguientes datos:

<img src="/images/posts/monitorizacion/grafana6.png" alt="Grafana 6" class="sombra" />

  - __Query Language__: Flux
  - __URL__: http://ip-de-unraid:8086
  - __Basic Auth__: lo deselecionamos 
  - __Organization__: el nombre de organización de influx, en este caso, _*Home*_
  - __Token__: aquí metemos el token de solo lectura que hemos creado antes
  - __Main Bucket__: el que hayamos puesto, en mi caso _*Unraid*_

Le damos a _*Save & Test*_ y debería de decir que todo ha ido correctamente y ha encontrado el bucket.

<img src="/images/posts/monitorizacion/grafana7.png" alt="Grafana 7" class="sombra" />

Por fin, ya tenemos todo listo para __graficar nuestras señales__. Vamos _*Create*_ -> _*Dashboard*_. Ahora, vamos a crear un panel, seleccionando el botón de _*Add empty panel*_. Nos abrirá la pantalla de creación de paneles donde tendremos tres bloques:

<img src="/images/posts/monitorizacion/grafana8.png" alt="Grafana 8" class="sombra" />

<img src="/images/posts/monitorizacion/grafana9.png" alt="Grafana 9" class="sombra" />

  1. La visualización de cómo va a quedar en panel
  2. El campo donde añadir la query de los datos a mostrar
  3. Donde configuraremos el tipo de visualización y características del panel.

<img src="/images/posts/monitorizacion/grafana10.png" alt="Grafana 10" class="sombra" />

Para los que ya sabéis programar, os voy a explicar muy brevemente cómo se construye una query con Flux. En la imagen anterior podéis ver un ejemplo de query simple. Vamos a ver una query algo más completa:

```flux
from(bucket: "db/rp")
  |> range(start: v.timeRangeStart, stop:v.timeRangeStop)
  |> filter(fn: (r) =>
    r._measurement == "example-measurement" and
    r._field == "example-field"
  )
  |> aggregateWindow(every: v.windowPeriod, fn: mean, createEmpty: false)
  |> map(fn: (r) => ({
        _value: r._value, 
        _time: r._time, 
        name: "Value of example field"
    }))
  |> yield(name: "mean")
```

Lo primero es definir el bucket de donde sacamos el dato. Luego, metemos las cláusulas de filtrado y finalmente hacemos las transformaciones que queramos como agrupar datos o meter aliases.

Al ser bases de datos de series temporales, lo más habitual es que el __primer filtrado sea el rango de tiempo__ que queremos obtener. En grafana, en la zona 1 de la pantalla de creación del panel verás que existe un __time-picker__, con el cual podemos jugar a cambiar este rango. Para que esto funcione, en vez de pasar el rango de forma explícita en la query lo que hacemos es pasar la __variable que genera grafana__ del inicio del rango de tiempo _*v.timeRangeStart*_ y el del final, _*v.timeRangeStop*_.

Además, para hacer que el motor de visualización de grafana trabaje más rápido, podemos añadir la transformación de reescalado de la resolución, usando la función _*aggregateWindow*_. Aquí, la ventaja es que el propio motor de grafana es capaz de adaptar la resolución de forma automática a la escala de tiempo que le pidamos. 

A modo de ejemplo, si le pedimos una escala muy pequeña, la última hora, nos podría una resolución de puntos de la media de cada 10 segundos. Sin embargo, si le pedimos los datos la la última semana, nos sacará una media de cada 3 horas. Esto es muy interesante ya que nosotros vamos a recoger el estado del servidor cada 10 segundos (lo puedes cambiar en el telegraf.conf) y al hacer zoom out no nos interesa tener tanto detalle sino ver las tendencias.

Para poder hacer esto, en la función de datos agregados metemos como resolución la variable de grafana _*v.windowPeriod*_ con la función agregada _*mean*_, que será la media.

En los filtrados por campo, lo que haremos es simplemente filtrar por el nombre de la variable que queremos graficar (% de uso) y el tag correspondiente al elemento (disco duro 1, cpu 2, etc).

El mapeo final es la forma de pasar la serie temporal obtenida a grafana con un alias (_*name*_).

Para empezar, vamos a graficar el porcentaje de uso la RAM, con la siguiente query:

```flux
from(bucket: "unraid")
  |> range(start: v.timeRangeStart, stop: v.timeRangeStop)
  |> filter(fn: (r) => r["_measurement"] == "mem")
  |> filter(fn: (r) => r["_field"] == "used_percent")
  |> filter(fn: (r) => r["host"] == "Tower")
  |> aggregateWindow(every: v.windowPeriod, fn: mean, createEmpty: false)
  |> map(fn: (r) => ({
        _value: r._value, 
        _time: r._time, 
        name: "RAM"
    }))
  |> yield(name: "mean")
```

Ahora deberías ver una línea con la gráfica:

<img src="/images/posts/monitorizacion/grafana11.png" alt="Grafana 11" class="sombra" />

Para el uso de CPU, el % de uso viene separado por _*system*_, _*user*_ etc, por lo que lo más fácil es transformar el dato siendo el % usado = 100 - % sin uso (_*iddle*_). La query sería la siguiente:

```flux
from(bucket: "unraid")
  |> range(start: v.timeRangeStart, stop: v.timeRangeStop)
  |> filter(fn: (r) => r["_measurement"] == "cpu")
  |> filter(fn: (r) => r["_field"] == "usage_idle")
  |> filter(fn: (r) => r["cpu"] == "cpu-total")
  |> filter(fn: (r) => r["host"] == "Tower")
  |> aggregateWindow(every: v.windowPeriod, fn: mean, createEmpty: false)
  |> map(fn: (r) => ({ 
      _value: 100.0 - r._value, 
      _time: r._time, 
      name: "CPU"
    }))
  |> yield(name: "mean")
```

Puedes añadir esta query en la misma gráfica o bien crear otra nueva:

<img src="/images/posts/monitorizacion/grafana12.png" alt="Grafana 12" class="sombra" />

Te invito a que ahora pruebes a cambiar el modo de visualización, colores, juegues con el timepicker etc. Como podrás comprobar, __el potencial de Grafana es tremendo__ y se pueden hacer dashboards a gusto de cada uno. Para ayudarte un poco en la tarea, te dejo [este archivo](https://gitlab.com/firstcommit/firstcommit.gitlab.io/-/raw/master/source/images/posts/monitorizacion/unRAID.json?inline=false) con una copia de mi dashboard de Unraid. 

<img src="/images/posts/monitorizacion/grafana13.png" alt="Grafana 13" class="sombra" />

Para usarlo, debes ir _*Create*_ -> _*Import*_, seleccionar el fichero, y una vez cargado, tendrás que apuntar a tu bucket de Unraid. Como puedes ver en la imagen, __he añadido una variable__ que detectará automáticamente los servicios docker que tengas levantados y te permite filtrar entre los contenedores que quieras monitorizar.

<img src="/images/posts/monitorizacion/grafana14.png" alt="Grafana 14" class="sombra" />

# Publicar el microservicio en Nginx Proxy Manager

Aunque esto [ya lo hemos visto anteriormente](https://firstcommit.dev/2021/04/19/saas/), vamos a repasarlo. En Nginx proxy manager rellenamos los datos de acceso del contenedor y generamos un nuevo certificado.:

<img src="/images/posts/monitorizacion/nginx1.png" alt="Nginx 1" class="sombra" />
<img src="/images/posts/monitorizacion/nginx2.png" alt="Nginx 2" class="sombra" />

Ahora, una vez creado el servicio, tenemos que dar un paso en medio para que Grafana funcione correctamente al acceder desde el exterior. Vamos a editar la configuración del contenedor de grafana, y dentro, le damos a _*Add another Path, Port, Variable, Label or Device*_ y __creamos una variable__ con la URL que acabamos de crear en Nginx:

<img src="/images/posts/monitorizacion/nginx3.png" alt="Nginx 3" class="sombra" />

Guardamos y aplicamos. Ahora, si accedemos desde la URL creada debería de aparecernos la pantalla de login de Grafana.

# Dashboards embebidos en Home Assistant

Una de las cosas que podemos hacer en [nuestro centro domótico](https://firstcommit.dev/2021/03/11/Hass/) es incrustar, mediante [iframes](https://www.home-assistant.io/integrations/panel_iframe/), otros servicios web ajenos al propio Home Assistant. Esto es muy útil si queremos acceder a todo desde una sola URL y pestaña de navegador (y aplicación móvil, si usamos la [app nativa de Hass](https://www.home-assistant.io/integrations/mobile_app/)).

Para que este iframe nos quede aún mejor, Grafana tiene la opción de visualizar los dashboard en modo Kiosko escondiendo toda la interfaz salvo los paneles. Para obtener la URL del modo kiosko vamos a nuestro dashboard de Grafana y arriba a la derecha hacemos click en el icono de la pantalla. Si haces click dos veces verás que solo quedan los paneles a la vista:

<img src="/images/posts/monitorizacion/hass1.png" alt="Hass 1" class="sombra" />

Para embeber el dashboard dentro de grafana, lo tendremos que hacer modificando el archivo principal de configuración que tendremos en la ruta/share _*\\ip-de-UnRaid\appdata\Home-Assistant-Core\configuration.yaml*_

Abrimos este archivo con [nuestro editor de código](https://firstcommit.dev/2021/03/29/VScode-01-instalacion/) añadimos lo siguiente:

```yml
panel_iframe:
  unraid:
    title: "Unraid"
    url: "URL-de-grafana-kiosko"
    icon: mdi:server
    require_admin: true # Solo si queremos que lo vean los usuarios admin de Hass
```

Guardamos el archivo y reiniciamos el servidor. Ya debería aparecer el icono de un servidor en la parte izquierda de tu Home Assistant:

<img src="/images/posts/monitorizacion/hass2.png" alt="Hass 2" class="sombra" />

---

Ahora ya queda en tus manos empezar a jugar con las opciones que tiene Grafana para hacer visualizaciones y dejar las cosas a tu gusto. En el siguiente post, vamos a sensorizar nuestras plantas y conectando Home Assistant e InfluxDB, vamos a monitorizarlas con Grafana para que ellas mismas nos pidan que las reguemos cuando les haga falta.

Si tienes cualquier duda no dudes en dejarlo en los comentarios y te ayudaré en lo que pueda.

---
<a href="https://t.me/firstcommit" class="btn btn-primary text-white py-2">Suscríbete, que es gratis</a>

*Nota: algunos de los enlaces a productos o servicios pueden ser enlaces referidos con los que podemos obtener una comisión de venta.*

<style> 
img.sombra {
  display: block;  
  margin-left: auto;  
  margin-right: auto; 
  box-shadow: 5px 5px 8px grey;
}
img.sombra-inline {
  display: inline-block; 
  vertical-align: middle; 
  margin-right: 0px; 
  width:auto;
  box-shadow: 3px 3px 8px grey;
}
img.zoom {
  transition: transform .2s;
}
img.zoom:hover {
  -ms-transform: scale(1.2); /* IE 9 */
  -webkit-transform: scale(1.2); /* Safari 3-8 */
  transform: scale(1.2); 
}
</style>
