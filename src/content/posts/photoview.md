---
title: Cómo crear tu propio Google Photos en tu servidor con PhotoView
authors: [yayitazale]
date: 2021-10-30T20:00:00Z
image: /images/posts/photoview/intro.jpg
categories:
  - selfhosted
tags:
  - cloud
  - selfhosted
  - guia
  - servidores
  - nomoregoogle
  - google
  - drive
  - photos
  - fotos

---

Para seguir con la serie de [alternativas seflhosted a Google](https://firstcommit.dev/tags/nomoregoogle/) que comenzamos con la [alternativa a Google Drive](https://firstcommit.dev/2021/09/26/nextcloud/), en este post vamos a poner en marcha __nuestro propio Google Photos__. 

Como ya os comenté en el anterior post, las alternativas que os voy a plantear son todas de __software libre__ que he encontrado tanto en [nomoregoogle.com](https://nomoregoogle.com/) como en [ethical.net](https://ethical.net/), dos páginas que os animo a visitar antes de emprender cualquier __proyecto de selfhosted__. 

<a href="https://nomoregoogle.com/"><img src="/images/nextcloud/intro2.png" alt="nomoregoogle" /></a>

En los __tres post que completarán esta serie__, os dejaré enlaces a otras alternativas que pueden ser de utilidad si se ajustan mejor a vuestras necesidades y que podéis probar para compararlos con los que yo os voy a enseñar a instalar y utilizar. Como siempre, os animo a que __compartáis en los comentarios los resultados__ para que todos podamos aprender juntos.

#### TOC

---
<img src="/images/posts/photoview/funciones1.jpg" alt="Funciones1" class="sombra" />

# Funciones de Google Photos y alternativas

Antes de empezar a plantear el listado de __alternativas de Software Libre__ que hay disponibles para instalar en nuestro servidor, creo que lo primero es hacer un listado de las __funciones más interesantes que tiene Google Photos__. Con esta lista, cada uno deberá valorar las que más le interesa tener ya que no hay ninguna alternativa que sea 100% idéntica y contenga todas las funciones.

- Reconocimiento de caras: es una de las funciones más interesantes, ya que nos permite buscar fotos por personas
- Reconocimiento de objetos: por ejemplo, nos permite buscar fotos de nuestro gato o perro. También podemos buscar cosas como atardecers, playas etc.
- Fotos geolocalizadas, para poder rememorar de forma rápida aquel viaje a la otra punta del mundo
- Álbumes, para organizar las fotos a nuestro gusto
- Recordatorio de fotos importantes el mismo día hace años
- Creación de animaciones/vídeos/collage de las fotos del año, de un viaje etc.
- Compartir fotos, álbumes y creaciones del punto anterior con otras personas.
- Edición básica de fotos y videos
- Copia automática de las fotos del móvil

Es posible que me olvide de alguna función, pero a esta larga lista solo queda añadir la __limitación en espacio que nos imponen si no queremos pagar__. Y lo más importante, debemos que tener en cuenta que las fotos subidas a Google Photos (y sus metadatos) [pueden ser utilizadas por Google](https://policies.google.com/terms?hl=es#toc-content) (y lo harán) tanto para promoción, venta a 3º para publicidad, creación de perfiles, desarrollo etc. Se mantiene solo el derecho de propiedad intelectual pero les estamos dando la posibilidad de utilizar nuestros datos y fotos para objetivos ajenos al mero almacenamiento.
 
Las alternativas que he encontrado en la red son estas las siguientes. Os dejo los enlaces para que podáis echar un vistazo a lo que ofrece cada una de ellas, pero en general todas tienen puntos fuertes y puntos débiles. Todo dependerá en cada caso el uso que quieras darle, pero añado una puntuación subjetiva del 1 al 10:

- [Nextcloud](https://nextcloud.com/): si seguiste los pasos del post anterior, habrás visto que el propio nextcloud tiene un apartado de galería de fotos, pero solo por ahora es muy muy simple y además no está muy optimizado. Si no te quieres liar más, es tu opción. __5/10__
- [Piwigo](https://piwigo.org/): probablemente la que más tiempo lleva en desarrollo. Estéticamente se ha quedado un poco atrás y no tiene muchas funciones. __6/10__
- [Lychee](https://lycheeorg.github.io/): muy simple pero muy pocas funciones. Estéticamente mejorable. __5/10__
- [LibrePhotos](https://github.com/LibrePhotos/librephotos): la primera alternativa (bueno, técnicamente la segunda ya que es un fork de [OwnPhotos](https://github.com/hooram/ownphotos)) con funciones parecidas como el reconocimiento de caras. __5/10__
- [PhotoPrism](https://photoprism.app/): muchas funciones, fácil de instalar. La única pega que puedo poner es que el desarrollo se ha ido encaminando a la creación de una empresa y las prioridades han cambiado. Por ahora no tiene opción de multi-usuario. __7/10__
- [Photo-Stream](https://github.com/waschinski/photo-stream): es una especie de foto-libro digital. Muy útil para crear un álbum compartido tipo bodas&bautizos pero sin ninguna función. Estéticamente muy logrado. __6/10__
- [Photoview](https://photoview.github.io/): un fork de librephotos que ha ido creciendo en paralelo para convertirse, en mi opinión, en la opción más completa. Estéticamente creo que PhotoPrism es algo mejor, pero el ambiente que se respira en la comunidad de desarrollo es mucho más sano. Poco a poco van añadiendo funciones y están muy abiertos a sugerencias. Multiusuario. __7/10__

Si revisáis las diferentes funciones que aporta cada una de las alternativas veréis que al final, __deberemos hacer alguna concesión__ ya que ninguna de las opciones tiene todas las funciones que hemos comentado. En mi caso, doy mucha más importancia a la opción de que sea __multiusuario__ por lo que la guía consistirá en instalar la última opción, [PhotoView](https://photoview.github.io/).

<img src="/images/posts/photoview/instalacion1.jpg" alt="Instalacion1" class="sombra" />

# Instalando Photoview sobre unRaid con Docker

La instalación la haremos sobre [nuestro servidor es UnRaid](https://firstcommit.dev/2021/01/31/Unraid/), pero con cualquier otro OS como Ubuntu Server o una Raspberry-pi es prácticamente igual (sí, PhotoView funciona en ARM). La única diferencia es que nosotros usaremos la interfaz de instalación de UnRaid para levantar el micro-servicio con Docker y no tendremos que usar la línea de comandos.

Los requisitos para instalar PhotoView son:

- Disponer de una base de datos tipo __mySQL__
- Para visualizar los mapas, un token de __MapBox__
- Un __subdominio__ que usaremos para acceder desde internet


Para empezar, vamos a crear la BBDD. Si seguiste los pasos del [post de instalación de NextCloud](https://firstcommit.dev/2021/09/26/nextcloud/#Instalando-Nextcloud-sobre-unRaid-con-Docker), ya tendrás __MariaDB__ instalado por lo que solo deberemos crear la base de datos siguiendo exactamente los mismos pasos pero para PhotoView. Vamos a abrir la consola del contenedor de MariaDB:

Accedemos a la base de datos con el siguiente comando:

```bash
mysql -u root -p
```
Nos pedirá la contraseña de root que deberemos tener guardada en [nuestro vault](https://firstcommit.dev/2021/05/09/vaultwarden/). La copiamos de ahí y la pegamos con clic derecho de ratón y le damos a enter.

Ahora ya estamos conectados a la base de datos. Creamos el usuario photoview con una nueva contraseña diferente a la de root y damos enter:

```bash
CREATE USER ‘photoview’ IDENTIFIED by ‘contraseña’;
```

Creamos la base de datos para photoview:

```bash
CREATE DATABASE photoview;
```

Y le damos todos los derechos de escritura y lectura al usuario photoview usando la contraseña del usuario:

```bash
GRANT ALL PRIVILEGES ON photoview.* TO ‘photoview’ IDENTIFIED BY ‘contraseña’;
```

Ya tenemos la base de datos preparada. Guardamos eso si, __la contraseña del usuario photoview__ ya que la necesitaremos durante la instalación de la la aplicación.

Ahora vamos a crearnos una cuenta en [MapBox](https://account.mapbox.com) y una vez creada, en el apartado _Tokens_ veremos que tenemos un token llamado _Default public token_. La dejamos a mano para la instalación.

<img src="/images/posts/photoview/mapbox.png" alt="Mapbox" class="sombra" />

Para terminar con los requisitos, vamos a crear un registro en [nuestro Ngix Proxy Manager](https://firstcommit.dev/2021/04/19/saas/). Agregamos la entrada con el __subdominio__ que queramos usar para el acceso externo (tipo https://photoview.tudominio.com) y apuntando al __puerto http__ que vayamos a utilizar en la instalación, por ejemplo el 8694:

<div id="banner" style="overflow: hidden;justify-content:space-around;">
    <div class="" style="max-width: 49%;max-height: 50%;display: inline-block;">
        <img src="/images/posts/photoview/proxy1.png">
    </div>
    <div class="" style="max-width: 49%;max-height: 50%;display: inline-block;">
        <img src="/images/posts/photoview/proxy2.png">
    </div>
</div>

Ahora ya solo nos queda lanzar el __docker__ con la app. Volvemos a nuestro dashboard de UnRaid, a la pestaña APPs y buscamos _photoview_. Instalamos la plantilla de _CorneliousJD_:

<img src="/images/posts/photoview/unraid1.png" alt="unraid1" class="sombra" />

Los datos que tenemos que rellenar son:

- __*Web Interface*__: el puerto que hayamos puesto en Nginx, en mi caso el 8694
- __*Photos Share*__: lo que vamos a hacer aquí es mapear la carpeta de nextcloud completa ya que luego en la propia APP deberemos mapear una subcarpeta a cada usuario. De esta forma, podremos utilizar la subida automática de fotos de nextcloud con su app móvil que funciona a las mil maravillas y PhotoView solo como galería de visualización.
- __*PHOTOVIEW_MYSQL_URL*__: aquí debemos mere un string del tipo _usuario:contraseña@tcp(IP-Unraid:puerto-maria-db)/base-de-datos_ -> en mi caso __photoview:contraseña@tcp(ip.unraid:3306)/photoview__.
- __*PHOTOVIEW_PUBLIC_ENDPOINT*__: el subdominio que hayamos configurado en Ngix.
- __*MAPBOX_TOKEN*__: el token que hemos creado en MapBox.

Aceptamos y lanzamos la instalación. 

Al finalizar, vamos subdominio que acabamos de crear (por ejemplo https://photoview.tudominio.com) y debería aparecer la ventana de configuración inicial:

<img src="/images/posts/photoview/initial-setup.png" alt="initial-setup" class="sombra" />

Aquí crearemos nuestro propio usuario que será el administrador, con el user/pass que queramos y que podemos almacenar en el vault, y en la ruta deberemos meter la __ruta relativa__ de la carpeta de fotos de nuestro usuario de nextcloud. 

El mapeo que hemos creado al lanzar el contenedor es que en la ruta _/photos_ de PhotoView es la ruta de UnRaid _/mnt/user/Media/nextcloud_.

Por tanto, si nuestro usuario de NextCloud es _admin_, la ruta relativa a nuestras fotos será _/photos/admin/files/Fotos_ ya que la ruta absoluta en UnRaid es _/mnt/user/Media/nextcloud/admin/files/Fotos_

Guardamos y ya tenemos la APP configurada de forma básica. Ahora deberás esperar a que finalice el proceso de escaneado de la biblioteca (ten paciencia sobretodo si tienes muchas fotos de gatos).

<img src="/images/posts/photoview/instalacion2.jpg" alt="Instalacion2" class="sombra" />

# Configuración y uso de Photoview

La configuración de PhotoView no permite hacer grandes cambios. Las únicas cosas que deberemos configurar son:

- __Idioma__: podemos ponerlo en castellano o en el idioma que prefiramos
- __Escaneo periódico__: aquí configuraremos cada cuanto queremos que se escaneen las bibliotecas de fotos y cuantas tareas concurrentes queremos que se lancen a la vez. Yo lo tengo configurado con 3 tareas y 1 vez al día, pero para esto lo mejor es que hagas pruebas de rendimiento empezando por 1 tarea al día ya que consume bastante CPU (aunque no por mucho tiempo).
- __Creación de usuarios__: podremos crear tantos usuarios como queramos. La lógica es la misma que en la creación del usuario administrador, un user/pass y una ruta relativa a la carpeta de fotos de su usuario de NextCloud. Podremos hacer que otros usuarios sean Admins si así lo decidimos.

El uso de la App es bastante simple pero creo que conviene comentar la funcionalidad de __Personas__. El escaner de biblioteca utiliza una __inteligencia artificial__ para identificar caras y agrupar las fotos de esas personas. Esto se hace con un modelo pre-entrenado con caras de cientos de miles de personas diferentes, pero no es infalible. En el apartado de personas, podremos poner nombre a las agrupaciones de caras que identifica como la misma persona:

<img src="/images/posts/photoview/cara1.png" alt="cara1" class="sombra" />

También podremos __fusionar dos agrupaciones diferentes__ si la IA las ha considerado personas diferentes pero realmente se trata de la __misma persona__. Esto suele ocurrir sobre todo cuando las resoluciones de las fotos son muy diferentes, o cuando la luz, peinados, sombreros etc hacen que sea más complicado para el modelo la correcta identificación:

<img src="/images/posts/photoview/cara2.png" alt="cara2" class="sombra" />

Y en el caso contrario, si tenemos una __identificación incorrecta__ de una foto con una cara que no corresponde, podremos __separar esa foto de esa agrupación__ para proceder después a un etiquetado correcto. Una vez etiquetados algunos de los grupo, podemos __re-entrenar el modelo__ usando el botón _Reconocer caras sin etiquetar_.

En el apartado de lugares, el escaner utilizará la __geolocalización de las fotos__ que extrae de los __metadatos__ para posicionarlos en el mapa. En la vista global inicial, nos aparecerán las fotos agrupadas por zonas geográficas grandes y a medida que vayamos haciendo zoom, nos irá mostrando agrupaciones cada vez más pequeñas de zonas más pequeñas (como booking):

<img src="/images/posts/photoview/mapa.png" alt="mapa" class="sombra" />

El apartado álbumes será la visualización de las carpetas que tengamos creadas dentro del directorio de fotos de NextCloud. 

<img src="/images/posts/photoview/instalacion4.jpg" alt="Instalacion4" class="sombra" />

# Carga automática de fotos desde el móvil con NextCloud

Una de las funciones más interesantes que tiene Google Photos es que nos realiza __subidas automáticas de las fotos del teléfono__. Esto hace que tengamos siempre un respaldo de nuestras fotos y que aunque cambiemos de terminal móvil, todas __nuestras fotos estén siempre a salvo__ en un mismo lugar. 

Para tener esta misma funcionalidad, nosotros vamos a utilizar la __función de carga automática del cliente móvil de NextCloud__. Lo que tenemos que hacer primero es descargar la app de [f-droid](https://f-droid.org/packages/com.nextcloud.client/), [googleplay](https://play.google.com/store/apps/details?id=com.nextcloud.client) o [appstore](https://geo.itunes.apple.com/us/app/nextcloud/id1125420102?mt=8):

Nos logeamos usando nuestro subdominio de NextCloud y nuestro user/pass + el doble factor:

<div id="banner" style="overflow: hidden;justify-content:space-around;">
    <div class="" style="max-width: 49%;max-height: 50%;display: inline-block;">
        <img src="/images/posts/photoview/android1.png">
    </div>
    <div class="" style="max-width: 49%;max-height: 50%;display: inline-block;">
        <img src="/images/posts/photoview/android2.png">
    </div>
</div>

En configuración vamos a _carga automática_ y aquí __podremos seleccionar las carpetas que queremos que se sincronicen con nuestro cloud__. Podemos simplemente hacer click en la nubecita para empezar a sincronizar a la carpeta por defecto (_/SubidaInstantanea_) o bien, entrando en los 3 puntos, podremos decidir nosotros la carpeta en la que se subirán las fotos, que podría ser una ruta tipo _/fotos/movil/telegram_, _/fotos/movil/whatsapp_, _/fotos/movil/DCIM_ etc, dependiendo de la carpeta del móvil que vayamos a sincronizar:

<div id="banner" style="overflow: hidden;justify-content:space-around;">
    <div class="" style="max-width: 49%;max-height: 50%;display: inline-block;">
        <img src="/images/posts/photoview/android3.png">
    </div>
    <div class="" style="max-width: 49%;max-height: 50%;display: inline-block;">
        <img src="/images/posts/photoview/android4.png">
    </div>
</div>

Si hemos configurado el escaner de PhotoView para que se active una vez al día, las fotos que se vayan subiendo a NextCloud aparecerán en la galería el día siguiente.

<img src="/images/posts/photoview/instalacion3.jpg" alt="Instalacion3" class="sombra" />

# Migración de tus fotos de Google a PhotoView

Si estas realmente convencido de migrar tus fotos a esta alternativa, puedes __descargar todas tus fotos de Google Photos__ simplemente accediendo a [Google Takeout](https://takeout.google.com), seleccionando solo la parte de Google Fotos y descargando un ZIP.

Después de descomprimir los ficheros, puedes sincronizar la carpeta raíz desde el PC a tu NextCloud bien subiendo la carpeta completa desde el navegador o bien configurando un cliente de escritorio como hemos hecho con el teléfono.

<img src="/images/posts/photoview/instalacion5.jpg" alt="Instalacion5" class="sombra" />

---
Y esto es todo, ya puedes echarte la siestecita que te mereces.

Esperemos que PhotoView vaya creciendo en funcionalidades extra para que se convierta de verdad una alternativa 10/10 a Google. Por ahora puede seguir el [Roadmap que hay en el Github del proyecto](https://github.com/photoview/photoview/projects/1), puedes __colaborar en el desarrollo aportando código__ para esas funciones que faltan y también puedes __probar alguna de las otras alternativas__ que he listado al principio del artículo. Si lo haces, por favor __cuéntanos tu experiencia en los comentarios__.

Nos vemos en el siguiente post que cerrará esta mini-serie de alternativas a servicios de Google en el que instalaremos una suite de ofimática de software libre muy completa.

PD: todas las fotos de relleno son de mi autoría y con el consentimiento de los modelos, por supuesto :)

---
<a href="https://t.me/firstcommit" class="btn btn-primary text-white py-2">Suscríbete, que es gratis</a>

*Nota: algunos de los enlaces a productos o servicios pueden ser enlaces referidos con los que podemos obtener una comisión de venta.*

<style> 
img.sombra {
  display: block;  
  margin-left: auto;  
  margin-right: auto; 
  box-shadow: 5px 5px 8px grey;
}
img.sombra-inline {
  display: inline-block; 
  vertical-align: middle; 
  margin-right: 0px; 
  box-shadow: 3px 3px 8px grey;
}
img.zoom {
  transition: transform .2s;
}
img.zoom:hover {
  -ms-transform: scale(1.2); /* IE 9 */
  -webkit-transform: scale(1.2); /* Safari 3-8 */
  transform: scale(1.2); 
}
</style>
