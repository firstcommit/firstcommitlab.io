---
title: Cómo crear un blog como este sin pagar hosting usando GitLab Pages y Software Libre
authors: [yayitazale]
date: 2022-01-29T21:00:00Z
image: /images/posts/gitlab/intro.jpg
categories:
  - blog
tags:
  - gitlab
  - guia
  - pages
  - bloggin
  - aniversario

---
Este es un post muy especial para nosotros ya que __¡Cumplimos un año!__ Como lo oyes, ya ha pasado un año de aquél primer [It's alive](https://firstcommit.dev/2021/01/29/hello-world/).

Para celebrar este aniversario os traemos un __post especial__ y además, una __sorpresa en forma de sorteo__. En este año hemos ido creciendo en seguidores y visitas poco a poco. De apenas una docena de visitas al mes hemos alcanzado la cifra de más de __700 visitas mensuales__, más de 20 visitas al día, con cada vez un mayor porcentaje de personas que vuelven y pasan más tiempo consumiendo el contenido que vamos creando. Además hemos llegado a pasar de 35 suscriptores en nuestro [canal de telegram](https://t.me/firstcommit) al que, si no lo estas ya, te aconsejamos que [te suscribas](https://firstcommit.dev/Subscribe/) para no perderte ni un solo post, ¡es gratis!

<img src="/images/posts/gitlab/visitas.png" alt="visitas" class="sombra" />

No aspiramos a ser un blog grande ya que nuestro contenido es muy específico, pero estamos muy contentos y para agradecer vuestra fidelidad y animar a más personas a suscribirse al canal vamos a llevar a cabo un sorteo de un __kit de iniciación al mundo de la domótica y el ZigBee__ entre todos los suscriptores que quieran participar durante el mes de febrero.

Las condiciones requeridas para participar son:

- Residir en España, ya que de lo contrario el coste del envío se nos va del presupuesto
- Estar suscrito a nuestro [canal de telegram](https://t.me/firstcommit)
- Participar en el sorteo haciendo click en [Participar](https://t.me/firstcommit/38), solo lo tienes que hacer una vez para que el bot que coordinará el sorteo anote tu participación
- Enviarnos unas pocas fotos y explicarnos tu experiencia al montar el sistema que incluya los productos sorteados, de forma que podamos hacer un pequeño post

<img src="/images/posts/gitlab/sorteo.png" alt="sorteo"  style="max-width: 70%"/>

El sorteo lo realizará un bot automático de Telegram y __finalizará el 28 de febrero a las 12 del mediodía hora peninsular__. El sorteo contiene los siguientes elementos (enlaces referidos) __valorados en unos 100€__:

<div id="banner" style="overflow: hidden;justify-content:space-around;">
    <div class="" style="max-width: 49%;max-height: 50%;display: inline-block;">
        <a href="https://amzn.to/3Gjl2SA"><img src="/images/posts/gitlab/dongle.jpg" alt="dongle" class="zoom" /></a>
    </div>
    <div class="" style="max-width: 49%;max-height: 50%;display: inline-block;">
        <a href="https://amzn.to/3rVXMom"><img src="/images/posts/gitlab/persona.jpg" alt="dongle" class="zoom" /></a>
    </div>
    <div class="" style="max-width: 49%;max-height: 50%;display: inline-block;">
        <a href="https://amzn.to/32GL4kH"><img src="/images/posts/gitlab/puerta.jpg" alt="dongle" class="zoom" /></a>
    </div>
    <div class="" style="max-width: 49%;max-height: 50%;display: inline-block;">
        <a href="https://amzn.to/3KX7b7Z"><img src="/images/posts/gitlab/bombilla.jpg" alt="dongle" class="zoom" /></a>
    </div>
    <div class="" style="max-width: 49%;max-height: 50%;display: inline-block;">
        <a href="https://amzn.to/34kO6vj"><img src="/images/posts/gitlab/enchufe.jpg" alt="dongle" class="zoom" /></a>
    </div>
</div>

(habrá un único ganador y los datos personales para el envío se solicitarán solo a dicha persona)

Os animamos a todos a participar, mucha suerte y vamos con el post.

(Foto de portada<a href="https://unsplash.com/es/@pankajpatel?utm_content=creditCopyText&utm_medium=referral&utm_source=unsplash">Pankaj Patel</a> en <a href="https://unsplash.com/es/fotos/captura-de-pantalla-de-la-aplicacion-gitlab-ZV_64LdGoao?utm_content=creditCopyText&utm_medium=referral&utm_source=unsplash">Unsplash</a>)

#### TOC

# GitLab Pages

Es la [herramienta que nos ofrece GitLab](https://docs.gitlab.com/ee/user/project/pages/) a todos los usuarios de forma __gratuita__ para crear pequeñas páginas dentro de un repositorio. La idea inicial era la de poder implementar una forma más elegante de documentar proyectos que los archivos [Markdown](https://es.wikipedia.org/wiki/Markdown) distribuidos en las carpetas de un proyecto, pero la propia naturaleza de la herramienta nos permite simplemente crear un repositorio únicamente con el fin de __publicar una página web__.

Las funciones principales que nos ofrece son:

-	Utilizarlo para cualquier sitio web personal o de negocios.
-	Utilizar cualquier generador de sitios estáticos (SSG) o HTML plano.
-	Crear sitios web para tus proyectos, grupos o cuentas de usuario.
-	Alojar tu sitio en tu propia instancia de GitLab o en GitLab.com de forma gratuita.
-	Conecta tus dominios personalizados y certificados TLS.
-	Atribuye cualquier licencia a tu contenido. 

Por tanto, podemos utilizar esta herramienta para crear cualquier tipo de página web (que cumpla la legalidad vigente, claro está), tanto en un servidor GitLab privado como el público de GitLab.com y cambiar el enlace de la web para que utilizar el dominio que nosotros queramos.

<img src="/images/posts/gitlab/pages.png" alt="pages" class="sombra" />

Funciona de la siguiente manera: 

-	Utilizaremos alguno de los motores de creación de páginas web estáticas que podemos encontrar de forma gratuita gracias a proyectos de software libre en el propio GitLab
-	La herramienta de [integración y despliegue continuo de GitLab CI/CD](https://docs.gitlab.com/ee/ci/index.html) laanzará los scripts necesarios para generar los archivos html de la página web
-	GitLab publicará estos ficheros en una carpeta navegable bajo el dominio XXXX.gitlab.io

Cabe destacar que [GitHub también ofrece este servicio](https://pages.github.com/), pero [desde que en 2018 fue adquirida por Microsoft](https://es.wikipedia.org/wiki/github) preferimos trabajar con [GitLab ya que es completamente libre](https://es.wikipedia.org/wiki/gitlab).

En nuestro caso lo único que debemos hacer una vez tengamos una cuenta de GitLab (¿no la tenías aún?). Una vez tengamos una cuenta solo tenemos que clonar el repositorio del motor web que más nos guste. Lo más fácil es visitar las [páginas de ejemplo de todos los motores más conocidos que nos ha creado GitLab](https://gitlab.com/pages) para mayor comodidad.

Nosotros utilizamos [Hexo.io](https://hexo.io/) pero podéis comprobar que hay una gran variedad de formatos utilizables. Los más destacados desde mi punto de vista son:

-	[Docusaurus](https://gitlab.com/pages/docusaurus): documentar proyectos
-	[Hugo](https://gitlab.com/pages/hugo): blogs
-	[Gatsby](https://gitlab.com/pages/gatsby): blogs y páginas de cualquier tipo
-	[jupyterbook](https://gitlab.com/pages/jupyterbook): documetnar proyectos de jupyter

<img src="/images/posts/gitlab/docusaurus.svg" alt="docusaurus"  style="max-width: 70%"/>


# Hexo

La decisión de usar __Hexo__ sobre el resto posiblemente surge del desconocimiento inicial que teníamos de la existencia de Gatsby o Hugo, pero en cualquier caso es un motor muy funcional y con bastantes posibilidades de personalización. La mayor pega que podemos poner es que la mayoría de la comunidad que utiliza Hexo es asiática, por lo que en muchos casos la documentación estará en Coreano.

Os animo a probar cualquiera de las opciones posibles que hemos visto en el listado anterior, pero yo me centraré en la instalación y personalización de Hexo. La parte de configuración del dominio y del propio GitLab si será común en cualquiera de las opciones.

En cuanto al acceso a la web, lo podremos hacer bien con un subdominio autogenerado por GitLab del tipo *yayitazale.gitlab.io* o bien con un dominio propio como hemos visto antes. Para utilizar un subdominio de GitLab podemos bien creando un proyecto propio o uno grupal.

<img src="/images/posts/gitlab/hexo.png" alt="hexo" />

En caso de que vayamos a crear una página con múltiples autores, lo mejor es que primero crees un [grupo en GitLab](https://gitlab.com/dashboard/groups) e invites a dicho grupo a los diferentes autores (que deberán tener a su vez cuenta en GitLab). Deberemos crear el proyecto con un nombre del grupo con el formato que he comentado (*xxx.gitlab.io*). Esto es, si el grupo se llama *unblogdecocina*, el proyecto deberá llamarse *unblogdecocina.gitlab.io*. Esto hará que GitLab pages se configure de forma automática con ese subdominio. 

En caso de que vayamos a crear una página con múltiples autores, lo mejor es que primero crees un [grupo en GitLab](https://gitlab.com/dashboard/groups) e invites a dicho grupo a los diferentes autores (que deberán tener a su vez cuenta en GitLab). Si la página la vas a crear solo para ti, puedes saltarte este paso y utilizar como proyecto el nombre de tu propio usuario tipo *usuario.gitlab.io*.

Para arrancar con la “instalación”, solo tienes que ir al proyecto de [Hexo](https://gitlab.com/pages/hexo) y hacer click en “fork”:

<img src="/images/posts/gitlab/hexo1.png" alt="hexo1" class="sombra" />

Rellenamos los datos como si de un repositorio de software se tratase, seleccionando en *namespace* el grupo que hemos creado o nuestro repo personal. Lo vamos a hacer todo en modo público aunque teóricamente podríamos hacerlo privado. El nombre que le vamos a poner al repositorio es el del enlace que queramos que tenga como subdominio de GitLab, como por ejemplo *yayitazale.gitlab.io*:

<img src="/images/posts/gitlab/hexo2.png" alt="hexo2" class="sombra" />

Lo primero que vamos a hacer es eliminar la relación del fork que hemos hecho para que no conste como un fork en los listados GitLab ya que no vamos a aportar ni modificar el software con intención de publicarlo. Para ello vamos a *Settings* > *General* > *Advanced* y abajo del todo hacemos click en *Remove fork relationship*:

<img src="/images/posts/gitlab/hexo3.png" alt="hexo3" class="sombra" />
<img src="/images/posts/gitlab/hexo4.png" alt="hexo4" class="sombra" />

# Git Clone y edición en local

Para trabajar de forma sencilla ahora vamos a bajarnos el repositorio a nuestro PC. Como depende del sistema operativo que tengas los pasos a seguir son distintos, aquí tendrás que instalar los requisitos para que todo funcione por tu cuenta:

-	Instalar Git
-	Instalar Node.js

La documentación para instalar estos dos elementos dependiendo de la plataforma las tienes en la [documentación de Hexo](https://hexo.io/docs/index.html#Installation).

Una vez instalados ambos, abrimos una consola de comandos e instalamos *Hexo* con el siguiente comando:

```bash
npm install -g hexo-cli
```

Y cuando finalice instalamos *hexo-server*:

```bash
npm install hexo-server --save
```

Cuando finalice, vamos a añadir nuestra cuenta de Git usando el mismo email que hemos usado en GitLab para crear la cuenta:

```bash
git config --global user.name "John Doe"
```
```bash
git config --global user.email johndoe@example.com
```

EN GitLab vamos a nuestro perfil y bajo *Access Tokens* creamos un token de acceso con permisos de lectura y escritura de repositorios. Copiamos el token que nos genera.

<img src="/images/posts/gitlab/git1.png" alt="git1" class="sombra" />
<img src="/images/posts/gitlab/git2.png" alt="git2" class="sombra" />

Ahora usando Git vamos a clonar el repositorio a nuestro PC, para ello, sin salir de la consola de comandos escribimos:

```bash
git clone https://gitlab.com/yayitazale/yayitazale.gitlab.io
```
<img src="/images/posts/gitlab/hexo5.png" alt="hexo5" class="sombra" />

Nos pedirá la contraseña donde pegamos el token para logearnos. Esto solo lo haremos una vez.

Este comando nos traerá todos los archivos del proyecto a nuestro PC. En Windows podremos navegar a la carpeta en *C:\Users\tuusuario\repositorio*

Lo primero que vamos a hacer es crear un archivo en la carpeta raíz llamado *.gitignore* y lo vamos a editar usando nuestro editor de código preferido. Añadimos estas líneas para que Git ignore ciertas carpetas y tipos de archivo:

```
public/
node_modules/
.vscode/
settings.json*
.psd
```

Guardamos el archivo. Ahora vamos a probar que todo funciona. Una de las ventajas principales de Hexo es que podemos levantar la página web de forma local para ver cómo quedan los cambios y solo hacer un commit (subir a GitLab los cambios) cuando estemos contentos con el resultado.

Para probarlo en la linea de comandos vamos a la carpeta raíz del repositorio, en mi caso haciendo:

```bash
cd yayitazale.gitlab.io
```

(tip, si pones las primeras letras del repositorio y le das al tabulador se auto completará solo)

y hacemos:

```bash
hexo server
```

Si todo va bien nos dirá que en http://localhost:4000 podemos ver la web, lo abrimos en el navegador y lo podemos comprobar.

<img src="/images/posts/gitlab/hexo6.png" alt="hexo6" class="sombra" />

Con Ctrl+C paramos el servidor.

# Configuración básica de Hexo

La configuración de Hexo la vamos a realizar editando los archivos *_config.yml* utilizando nuestro [editor de código preferido](https://firstcommit.dev/2021/03/29/VScode-01-instalacion/).

Tendrás que editar las cosas típicas como el título de la página, el autor, la forma de ordenar los post etc. Aquí lo más importante es quedarnos con que la URL de la página la deberos cambiar por ahora por la URL que del dominio que vayamos a comprar en la URL del dominio de GitLab, en mi caso de este ejemplo, *yayitazale.gitlab.io*.

El resto de cambios dependerán de cómo quieras que se muestre la información de tu blog, por lo que puedes ver cómo lo tenemos nosotros aquí. Una de las funciones que puedes utilizar es en “one command deployment” de Hexo. Para ello en la parte baja rellenas esto:

```yml
# Deployment
## Docs: https://hexo.io/docs/one-command-deployment
deploy:
  type: git
  repo: **nombredelrepo**
```

De esta manera, cuando hayamos terminado de editar los archivos podremos hacer que el commit sea automático con un solo comando (esto lo vemos después).

# Configurar nuestro dominio personalizado con NameCheap

Como he comentado, la web la podemos dejar accesible desde el subdoiminio que nos hemos creado en GitLab pero también podremos comprar nuestro pripio dominio propio y acceder a la web desde el. Nosotros lo tenemos en [NameCheap](https://www.shareasale.com/r.cfm?u=2804077&m=46483&b=518798) ya que tiene unos precios muy competitivos y el servicio que ofrecen es impecable. 

Una vez comprado el dominio que queramos lo que debemos hacer en GitLab es ir a *Settings* > *Pages* y *New Domain*:

<img src="/images/posts/gitlab/dominio1.png" alt="dominio1" class="sombra" />

Añadimos el nombre de dominio que hemos comprado tipo *misuperdominio.com* y seleccionamos que nos genera un certificado de forma automática usando [Let’s Encrypt](https://letsencrypt.org/es/):

<img src="/images/posts/gitlab/dominio2.png" alt="dominio2" class="sombra" />

Ahora nos aparecerán dos registros, uno DNS y otro TXT:

<img src="/images/posts/gitlab/dominio3.png" alt="dominio3" class="sombra" />

En NameCheap vamos a la pantalla de configuración de nuestro dominio y bajo *Advanced DNS* > *Host Records* añadimos uno de tipo ALIAS y otro de tipo TXT:

<img src="/images/posts/gitlab/dominio4.png" alt="dominio4" class="sombra" />

Ahora tendremos que esperar un ratito para que estos cambios se propagen por la red de servidores DNS. Si somos un poco impacientes podemos ir haciendo click periodicamente en el botón de verificar dominio. Una vez tengamos el dominio verificado ya tenemos todo listo. Solo quedará editar el archivo de configuración de Hexo como hemos visto en el punto anterior para que cuando hagamos el commit todo funcione bien en el servidor de GitLab.

# First Commit

Ahora vamos a crear nuestro primer post. En la carpeta */source/_posts* tenemos un post de ejemplo llamado *hello-world.md*. Lo abrimos y editamos el texto usando la nomenclatura [Markdown](https://es.wikipedia.org/wiki/Markdown). Una vez hechos los cambios guardamos el archivo.

Para subir los cambios a GitLab tenemos dos opciones, la clásica de hacer los commits de forma manual y bien utilizar el *one-command-deploy* de Hexo. Los pasos para hacerlo de forma clásica usando la consola de comandos es:

```bash
git add --all
```
```bash
git commit –a –m “First Commit”
``` 
(entre parentesis es el titulo o texto que vamos a dar al commit, normalmente se utiliza una descripción breve de los cambios realizados)
```bash
git push
```

Si usamos en *one-command-deploy* solo debemos hacer:
```bash
hexo clean && hexo deploy
```

Una vez hayamos hecho esto en GitLab veremos que hay una tarea en curso en el CI/CD:

<img src="/images/posts/gitlab/cicd.png" alt="cicd" class="sombra" />

Cuando finalice vamos a abrir a acceder al dominio que hemos comprado desde el navegador y veremos que ya podemos acceder a la web y tenemos un primer post editado:

<img src="/images/posts/gitlab/hello.png" alt="hello" class="sombra" />

# Overdose theme

Para cambiar la apariencia del blog podemos buscar entre los [cientos de themes que hay disponibles](https://hexo.io/themes/index.html)

<img src="/images/posts/gitlab/mock-up.png" alt="mock-up" class="sombra"/>

En nuestro caso usamos [Overdose](https://github.com/HyunSeob/hexo-theme-overdose) pero la instalación es exactamente igual en todos los casos. Usando la consola de comandos desde la carpeta raiz del repositorio del blog hacemos:

```bash
git clone https://github.com/HyunSeob/hexo-theme-overdose.git themes/overdose
```

Para que funcione, este theme tiene un requisito adiccional. Debemos instalar un renderizador externo. Para instalarlo en local hacemos:

```bash
npm install --save hexo-renderer-jade
```

Esto solo hará que se instale en nuestro PC. Para que el Theme funcione en GitLab debemos decirle al CI/CD que lo instale también. Para ello editamos el archivo *.gitlab-ci.yml* y añadimos:
```bash
npm install --save hexo-renderer-jade
```
tras la línea install hexo-cli para que quede tal que así:

<img src="/images/posts/gitlab/theme1.png" alt="theme1" class="sombra" style="max-width: 60%"/>

Ahora para activar el theme debemos editar el archivo de configuración de la carpeta raíz que hemos editado antes *_config.yml* y en “theme” ponemos el nombre del theme que hemos instalado:

```yml
theme: overdose
```

La mayoría de themes tienen también un archivo de configuración propio dentro de su carpeta, en este caso  */themes/overdose* donde deberemos configurar aspectos específicos del theme. En caso de overdose, debemos primero duplicar el archivo de ejemplo llamado *_config.yml.example* y renombralo como *_config.yml*. 

Además deberas tener en cuenta que los themes suelen además requerir que el título, autor etc se añadan de una forma concreta en cada post. Para ello te recomiendo siempre que sigas los pasos de la documentación del autor, y si tienes dudas, nos dejes un comentario para que te ayudemos.

---

Mucha suerte en el sorteo y nos vemos en el siguiente post.

---
<a href="https://t.me/firstcommit" class="btn btn-primary text-white py-2">Suscríbete, que es gratis</a>

*Nota: algunos de los enlaces a productos o servicios pueden ser enlaces referidos con los que podemos obtener una comisión de venta.*

<style> 
img.sombra {
  display: block;  
  margin-left: auto;  
  margin-right: auto; 
  box-shadow: 5px 5px 8px grey;
}
img.sombra-small {
  display: block;  
  margin-left: auto;  
  margin-right: auto; 
  box-shadow: 5px 5px 8px grey;
  max-width: 20%;
}
img.sombra-inline {
  display: inline-block; 
  vertical-align: middle; 
  margin-right: 0px; 
  box-shadow: 3px 3px 8px grey;
}
img.zoom {
  transition: transform .2s;
}
img.zoom:hover {
  -ms-transform: scale(1.2); /* IE 9 */
  -webkit-transform: scale(1.2); /* Safari 3-8 */
  transform: scale(1.2); 
}
</style>
