---
title: Programando un Arduino con VS Code
authors: [mifulapirus]
date: 2021-03-31T20:30:00Z
image: /images/posts/VScode/banner2.jpg
categories:
  - diy
tags:
  - diy
  - diwo
  - esp8266
  - arduino
  - programación
---
Bien, supongo que ya has instalado _VS Code_ y _Platform IO_, así que ahora empieza lo divertido. Vamos a programar un Arduino. De momento va a ser algo sencillo, un Arduino normalito conectado por USB igual que lo harías en el IDE de Arduino.

(Foto de portada <a href="https://unsplash.com/es/@sahandbabali?utm_content=creditCopyText&utm_medium=referral&utm_source=unsplash">Sahand Babali</a> en <a href="https://unsplash.com/es/fotos/placa-de-circuito-azul-y-negro-gavODTHG36Y?utm_content=creditCopyText&utm_medium=referral&utm_source=unsplash">Unsplash</a>)

¡Empezamos!

Programemos un Arduino
======================
1. Abre _VS Code_ y ve a _PlatformIO_ --> _PIO Home_ --> _Open_,  en caso de que no esté ya abierto. Aprovecha para hacer que _Platform IO_ no se abra al inicio, que es un incordio. Después, selecciona _New Project_ 
<img src="/images/posts/VScode/pio-new_project.jpg" alt="Platform IO New Project" class="sombra" />

2. Ponle un nombre, selecciona tu Microcontrolador y asegúrate de estar poniendo el proyecto en el directorio adecuado. Si no cambias el directorio, estarás usando el que _PlatformIO_ crea por defecto y, si eres una persona ordenada, es muy probable que no sea la carpeta adecuada.
<img src="/images/posts/VScode/pio-new_project_settings.jpg" alt="Platform IO New Project" class="sombra" />

3. Verás que _PlatformIO_ ha creado unos cuantos directorios y archivos. Iremos viendo más adelante que hay en cada uno. De momento abre el archivo _main.c_ que se encuentra en la carpeta _scr_. <img src="/images/posts/VScode/pio-file_tree.jpg" alt="pio-file_tree" class="sombra zoom"/> _main.c_ es tu archivo principal donde tienes la típica rutina de _setup_ y el _loop_ principal.

4. ¿Ves la línea 1 que dice _#include <Arduino.h>_? No la pierdas de vista. Siempre que trabajes con Arduinos, te hará falta puesto que es quien contiene los encabezados necesarios.
<img src="/images/posts/VScode/pio-main_arduino_line.jpg" alt="pio-file_tree" class="sombra" />

5. Sustituye las funciones de setup y loop por el siguiente código incluyendo los comentarios, que aparecerán como parte del tool-tip de la función.

```c++
/* Configuración general */
void setup() {
  pinMode(LED_BUILTIN, OUTPUT); //Configura el LED como SALIDA
}

/* Loop principal
 * Hace que parpadee el LED cada segundo
 */
void loop() {
  digitalWrite(LED_BUILTIN, HIGH);
  delay(1000);                    
  digitalWrite(LED_BUILTIN, LOW); 
  delay(1000);                    
}
```

 6. Conecta tu Arduino y sube el código desde `PlatformIO --> General --> Upload` o dándole al botón de la flecha <img src="/images/posts/VScode/pio-upload-single.jpg" alt="pio-upload" class="sombra-inline" style="display: inline-block; "/> 
<img src="/images/posts/VScode/pio-upload-menu.jpg" alt="pio-upload-menu" class="sombra"/>

Y ya está... no tiene mucho más misterio que saber dónde están las herramientas e ir acostumbrándote poco a poco.
[<img src="/images/posts/VScode/platipus.png" alt="ya esta"/>](https://t.me/addstickers/MyNiffler)

---
<a href="https://t.me/firstcommit" class="btn btn-primary text-white py-2">Suscríbete, que es gratis</a>

*Nota: algunos de los enlaces a productos o servicios pueden ser enlaces referidos con los que podemos obtener una comisión de venta.*

<style> 
img.sombra {
  display: block;  
  margin-left: auto;  
  margin-right: auto; 
  box-shadow: 5px 5px 8px grey;
}
img.sombra-inline {
  display: inline-block; 
  vertical-align: middle; 
  margin-right: 0px; 
  width:auto;
  box-shadow: 3px 3px 8px grey;
}
img.zoom {
  transition: transform .2s;
}
img.zoom:hover {
  -ms-transform: scale(1.2); /* IE 9 */
  -webkit-transform: scale(1.2); /* Safari 3-8 */
  transform: scale(1.2); 
}
</style>