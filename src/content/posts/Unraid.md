---

title: UnRaid, el sistema operativo para servidores domésticos
authors: [yayitazale]
date: 2021-01-31T20:00:00Z
image: /images/posts/unraid/pasted-2.png
categories:
  - selfhosted
tags:
  - selfhosted
  - unraid
  - guia
  - servidores

---
En este primer post del blog quiero empezar hablando del sistema operativo que tengo instalado en mi servidor, ya que es la base sobre la que voy a trabajar muchos de los siguientes proyectos que vaya subiendo.

Llevo aproximadamente dos años con este sistema y no puedo estar más contento: es un sistema robusto, con una empresa seria detrás y hay una comunidad muy amplia y activa. La calidad/precio de lo que ofrece es inigualable al menos desde mi punto de vista, ya que tiene todas las bondades de basarse en el software libre (linux) y los aportes de la comunidad, pero con el desarrollo y mantenimiento principal a manos de la empresa [Limetech](https://unraid.net/about) con un coste de licenciamiento muy bajo (y además perpetuo).  Por suerte para nosotros podemos probar el sistema con la [licencia gratuita](https://unraid.net/pricing) que ofrecen para un mes.

Cuando arranqué con Unraid venía de un mundo caótico basado en un NAS synology más lento que una tortuga boba y varias raspberry-pis *cometarjetas* que arruinaban en cualquier momento todo lo que no tuvieras con un backup reciente.

Antes de explicar qué hardware necesitamos y cómo podemos poner en marcha un servidor con unraid, quiero explicar algunos conceptos que puede que no conozcas y te serán de gran ayuda si finalmente te decides por Unraid como tu SO doméstico.

### TOC

# Principios básicos

## Host
Es como se denomina a tu equipo, el servidor que vas a tener en casa montado. En castellano sería “hospedador”.

## Dispositivo USB para BOOT
El sistema operativo vive en un USB desde donde arrancaremos el servidor. Este USB solo guarda algunas configuraciones de nuestro sistema, pero debe ser un USB flash de buena calidad. Esto nos permite utilizar los discos duros y SSD para usarlos en nuestros servicios y guardar datos sin necesidad de instalar nada sobre ellos.

## Paridad
Cuando buscamos tener los datos lo más seguros posibles a prueba de fallos de discos duros, lo ideal es tener los datos redundantes, esto es, en varios sitios “a la vez”. Imaginamos que tenemos. Si tenemos dos discos duros, usamos uno para guardas datos y el otro sería una copia, de forma que, si uno de los dos discos falla, seguimos teniendo el dato asegurado.


![sin paridad](/images/posts/unraid/pasted-3.png)

Esto con dos discos funciona, pero si tenemos 6 discos, no es muy eficiente usar 3 para duplicar los datos de los otros tres, por lo que utilizamos solo uno de ellos como disco de paridad y el resto sirve para guardar datos.
Los datos de los discos se guardan en ceros y unos, y la paridad será el resultado de sumar en binario todos esos bits de datos de cada disco. Ejemplo de un bloque de bits:

-	Disco 1: 1
-	Disco 2: 0
-	Disco 3: 0
-	Paridad = 1 + 0 + 0 = 1


![Paridad](/images/posts/unraid/pasted-4.png)

En caso de que uno de los discos falle, es fácil recuperar el dato:

-	Disco 1: 1
-	Disco 2: 0
-	Disco 3: ?
-	Paridad = 1 + 0 + ? = 1 -> ? = 0


![Cálculo de paridad](/images/posts/unraid/pasted-5.png)

Podríamos tener problemas si fallan dos discos de forma simultánea, por lo que podemos añadir un segundo disco de paridad si tenemos un array grande, como con 6 discos, por ejemplo. En mi caso, Al tener un total de 3 discos, solo tengo de un disco de paridad. En caso de que falle un disco, tengo otro físicamente disponible en casa para cambiar cuanto antes el disco fallido y regenerarlo.

## Array de discos
Es un sistema RAID en el que la información de los discos duros se almacena de forma redundante. Existen [varios tipos](https://es.wikipedia.org/wiki/RAID), en este caso es un [sistema propio tipo MAID](https://wiki.unraid.net/UnRAID_6/Overview#Parity-Protected_Array) que se queda a medio camino entre un raid 3 y un raid 4. Por explicarlo de forma sencilla, de todos los discos duros que añadamos, uno de ellos será siempre el de Paridad. Este disco deberá ser igual o superior en tamaño de almacenamiento al disco de mayor capacidad donde guardaremos información. Por ejemplo:
-	Si tenemos un disco de 4TB y dos discos de 3TB, tendremos 6TB de almacenamiento útil siendo el disco de 4TB el dedicado a la paridad.
-	Si tenemos un disco de 3TB y dos discos de 5TB, tendremos 8TB de espacio de almacenamiento útil, siendo uno de los dos discos de 5TB el dedicado a la paridad.

Una vez creado y puesto en marcha nuestro array, la totalidad del espacio de almacenamiento útil será un único volumen para nosotros, aunque podremos configurar en qué forma queremos que se distribuya la información sobre los discos físicos.

## Disco cache
La cache es una unidad (o varias) de almacenamiento intermedio que pondremos en el sistema. Esta unidad, normalmente un disco SSD de alta velocidad, pero no gran capacidad de almacenamiento es en el que escribiremos los datos inicialmente y donde vivirán nuestros servicios que vayamos poniendo en marcha. Con esto conseguimos recudir drásticamente la cantidad de escrituras y lecturas del array de HDD, aumentando su esperanza de vida en el tiempo.

## Mover
Es el servicio que se dedica a traspasar de forma periódica los datos que tengamos en la cache y que queramos que vivan en el array. Por ejemplo, si nos descargamos una película o guardamos un documento, inicialmente se almacenará en la cache. Cuando se active el *mover*, este será traspasado definitivamente al array.
![Ejemplo mover](/images/posts/unraid/pasted-7.png)

## S.m.a.r.t.
Es una tecnología que consiste en la capacidad de detección de fallos de discos duros. Este sistema nos permite anticiparnos a la muerte total de un disco duro y cambiarlo antes de que ocurra dicho fallo, de forma que el riesgo de perder datos es menor. Es un sistema que viene preinstalado en Unraid.

## Docker
Es un sistema muy de moda en la informática en el que [los distintos servicios o programas se ejecutan dentro de un contenedor](https://es.wikipedia.org/wiki/Docker_(software)). La idea es que el desarrollador encapsule dentro de una caja (el contenedor) su aplicación con todas las dependencias necesarias para funcionar, pero sin incluir un sistema operativo completo. Después el usuario pone en marcha el contenedor pasándole los recursos que este necesite, tales como carpetas, puertos, periféricos etc. Es un sistema muy eficiente y que además permite en teoría que cualquier contenedor se pueda ejecutar en cualquier sistema operativo host.

![esquema docker](/images/posts/unraid/pasted-8.png)

## VM o máquina virtual
Es un ordenador normal y corriente, pero virtualizado dentro de tu host. Unraid nos permite levantar en cosa de minutos ordenadores completos, Linux, Windows etc. Esto nos permite poder levantar VM como servidores virtuales dedicados o tener nuestro ordenador alojado en nuestro servidor y conectarnos a el utilizando una raspberry-pi o un thin-client. Este sistema consume muchos más recursos que un contenedor y además requiere que la CPU del host y tu placa base lo permitan.

![esquema VM](/images/posts/unraid/pasted-9.png)

## Shares
Como hemos visto, unraid tiene como sistema de archivos la conjunción de un raid con un disco cache. Nosotros como usuarios no accederemos directamente a los discos ni a la cache como lo haríamos en un sistema operativo habitual como Ubuntu o Windows, sino que definiremos una serie de carpetas llamados *shares* que se distribuirán según su configuración y necesidad por nuestro array y cache.

![esquema de shares](/images/posts/unraid/pasted-10.png)

# Hardware necesario

El equipo básico necesario para empezar es lo que lleva un PC de sobremesa:

-	Placa base
-	Fuente de alimentación
-	CPU
-	Memoria RAM
-	Disco USB para BOOT
-	Disco (o discos) SSD cache
-	Discos duros para almacenamiento

Aquí es complicado definir un sistema concreto ya que deberás optar por los elementos que se ajusten a tus necesidades y presupuesto, por lo que lo mejor es que [te pases por la propia página de Unraid](https://wiki.unraid.net/UnRAID_6/Overview) donde dan unas pinceladas de los requisitos mínimos según la cantidad de servicios que vayas a poner en marcha y si vas o no a tener VMs.
Si tu presupuesto es ajustado, siempre puedes empezar reutilizando un PC de sobremesa que se haya quedado viejo sin gastar mucho dinero (solo en los discos duros).

En mi caso, tengo el siguiente build:

-	Placa base: Asus TUF H310-PLUS
-	Fuente de alimentación: Corsair VS450 450W
-	CPU: Intel Pentium Gold G5600 3.9GHz
-	Memoria RAM: Corsair Vengeance LPX 32 GB DDR4, 3000 MHz, CL16
-	Disco USB para BOOT: Toshiba Hayabusa - USB 2.0 16 GB
-	Disco SSD cache 1: Samsung MZ-V7S250BW M.2 250Gb
- Disco SSD cache 2: Samsung SSD 860 EVO 250GB
-	Discos duros para almacenamiento: 3x WD NAS Red 3TB SATA3
-	Caja: Corsair Carbide Air 240

# Guía de instalación
A partir de aquí, teniendo ya nuestro sistema listo y montado os voy a explicar cómo realizar la instalación de unraid y unas configuraciones básicas iniciales.

## Prepara el USB
- Inserta el USB para BOOT en tu PC
- Formatealo usando el formato FAT (or FAT32)
- Asigna el nombre al USB ‘UNRAID’ (en mayúsculas y sin comillas)
- [Descarga la última versión de la página oficial](https://unraid.net/download)
- Selecciona la versión más reciente de la versión stable, el usb llamado UNRAID y dale a *write*

![Preparación USB](/images/posts/unraid/pasted-11.png)

## Prepara la BIOS
- Conecta el PC a tu red local por cable
- Inserta el USB en el servidor y enciéndelo (siempre en los slots USB del panel trasero del equipo, evita usar los delanteros)
- Durante el arranque deberás pulsar la tecla *supr* o alguna de las teclas de función, la primera pantalla del encendido te dirá que tecla debes pulsar para entrar a la configuración de la bios
- Debes configurar el USB como disco principal de arranque
- Debes configurar tus puertos SATA en modo HBA Estándar (nunca en modo RAID)
- Si tu placa base lo soporta, habilita la virtualización para poder crear máquinas virtuales en Unraid (Deberás habilitar algo parecido a esto: Intel VT-x / AMD-V)
- Finalmente guarda esta configuración y reinicia el equipo

## Selector de modo BOOT
Existen varias formas de arrancar el sistema, nosotros lo vamos a hacer en modo Unraid OS (Headless), de forma que no vamos a conectar nada (ni pantalla ni teclado y ratón). Esperamos a que arranque en este modo ya que es el de por defecto y cuando veamos que en nuestro router aparece un nuevo equipo llamado tower, accedemos al siguiente enlace desde nuestro navegador:
[http://tower](http://tower) (o [http://tower.local](http://tower.local) en mac)
Si no conseguimos acceder prueba a entrar directamente con la IP que aparezca en el router, tipo: [http://192.168.1.30](http://192.168.1.30)
(username = root, sin contraseña)

## Registra el sistema
Una vez arrancados nos llevará a la página de registro, donde tendremos que registrarlo bien con la licencia gratuita de un mes o cualquiera de las licencias de pago. (Al final de este proceso, la *key* se guardará en la carpeta *config* del USB, por lo que, si el registro es de una licencia de pago, conviene que hagas una copia de este archivo en otro lugar, como tu pc personal)

## Asignando los discos al array y cache
Después de registrar el sistema, estamos listos para comenzar a asignar dispositivos para que los administre Unraid. Haz clic en la pestaña *Main* de Unraid y sigue estas pautas al asignar discos:

- Elige siempre el disco duro con mayor capacidad disponible para que actúe como dispositivo(s) de paridad. Si en futuro quieres ampliar tu array, estos nuevos discos nunca podrán ser más grande que el disco de paridad. Siempre podrás crear un disco nuevo de paridad más grande en el futuro, pero es mejor empezar ya con un disco grande.
- Asigna tu disco o discos SSD al array de cache. Ten en cuenta que si solo dispones de un disco cache, durante el tiempo en el que el dato está en cache y no ha sido movido al array de discos duros, no estará protegido frente a un fallo y hay un potencial peligro de pérdida de datos.

![asignacion de discos](/images/posts/unraid/pasted-12.png)

## Iniciando el array y formateo de discos
Una vez que tengas todos sus discos asignados, haz clic en el botón *Start* en la zona de *Array Operation*. Esto montará sus discos e iniciará la matriz. Los nuevos discos agregados a aparecerán como *Unformatted* y no los podrás usar todavía para almacenar archivos hasta que los formatees. Para formatear tus discos, haz clic en la casilla de *Format*, aparecerá un mesage de advertencia y la das de nuevo a *Format*.  Una vez formateados los discos, se hará la primera sincronización de paridad. Hasta que finalice esta sincronización, no tendremos aún redundancia de datos por lo que debemos esperar un rato a que finalice para poder empezar a guardad datos en el servidor.


# Primeros pasos
Lo primero que vamos a hacer en nuestro sistema es habilitar una contraseña para el usuario. Para ello vamos a __*users*__, elegimos root y escribimos una contraseña.

![root_password](/images/posts/unraid/pasted-13.png)

El segundo paso será configurar los sistemas periódicos (mover y parity check). Para ello vamos a __*settings*__ y __*scheduler*__. Aquí la configuración será la que más te guste, pero para empezar vamos a poner un parity check mensual y el mover diario.

![scheduler](/images/posts/unraid/pasted-14.png)

Finalmente vamos a activar Docker, para ello vamos __*settings*__ -> __*Docker*__ y vamos a decir que __*Enable Docker*__ -> __*yes*__


![Enable docker](/images/posts/unraid/pasted-15.png)

El último paso de esta guía será instalar el plugin de aplicaciones, que es de donde podremos configurar de forma sencilla los distintos servicios que queramos activar. Para ello vamos a __*plugins*__ -> __*install plugin*__, metemos esta url:

`https://raw.githubusercontent.com/Squidly271/community.applications/master/plugins/community.applications.plg`

y le damos a install:

![Plugin install](/images/posts/unraid/pasted-16.png)


# Estamos listos
A partir de aquí estaremos listos para empezar a poner en marcha todo lo que queramos. En próximos posts explicaré algunas configuraciones más avanzadas, plugins básicos más recomendados y también iré explicando los distintos servicios que tengo yo en marcha.

---
<a href="https://t.me/firstcommit" class="btn btn-primary text-white py-2">Suscríbete, que es gratis</a>

*Nota: algunos de los enlaces a productos o servicios pueden ser enlaces referidos con los que podemos obtener una comisión de venta.*