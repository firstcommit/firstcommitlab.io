---
title: Eginzaleak, presentación de proyectos
authors: [yayitazale]
date: 2021-02-03T12:30:00Z
image: /images/posts/eginzaleak/eginzaleak.png
categories:
  - diy
tags:
  - selfhosted
  - diy
  - diwo
  - impresión 3D
  - charla
  - presentación
  - covid19
---

En esta ocasión os traigo un video de la charla/presentación que di junto a Lizar Azkune en las jornadas [Eginzaleak 2020](https://www.tabakalera.eus/es/eginzaleak-2020) en [Medialab Tabakalera](https://www.tabakalera.eus/es/medialab). Os invito a ver la jornada completa ya que el resto de proyectos presentados, [como suele ser habitual Medialab](https://www.tabakalera.eus/es/medialab/proyectos), son muy interesantes.

Os dejo también el enlace a las [diapositivas que utilizo en la presentación](https://docs.google.com/presentation/d/1-zC-sjmVR7mipSjOaXqPLEL7Yix8gXgjeDGq8ZajIsI/edit?usp=sharing).

<div style="padding:56.25% 0 0 0;position:relative;"><iframe src="https://player.vimeo.com/video/486825499?title=0&byline=0&portrait=0" style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe></div><script src="https://player.vimeo.com/api/player.js"></script>
<p><a href="https://vimeo.com/486825499">Eginzaleak 2020 Proiektuen aurkezpena /  Presentaci&oacute;n de proyectos</a> from <a href="https://vimeo.com/tabakalera">Tabakalera</a> on <a href="https://vimeo.com">Vimeo</a>.</p>
<p></p>

---
<a href="https://t.me/firstcommit" class="btn btn-primary text-white py-2">Suscríbete, que es gratis</a>

*Nota: algunos de los enlaces a productos o servicios pueden ser enlaces referidos con los que podemos obtener una comisión de venta.*