---
title: "Apache Guacamole, escritorios remotos seguros en el navegador"
authors: [yayitazale]
date: 2022-04-02T20:00:00Z
image: /images/posts/guacamole/intro.jpg
categories:
  - selfhosted
tags:
  - guacamole
  - guia
  - remote
  - vnc
  - rdp
  - ssh
  - html5

---
No, este no es un post de la receta culinaria del mejor guacamole que hayas probado, __es otro post sobre informática selfhosted__. ¡Lo siento!

Desde la pandemia hemos visto como __los accesos remotos se han multiplicado debido al teletrabajo__ y no siempre se han puesto en marcha con una __seguridad mínima__. Con Guacamole evitamos el clásico esquema de conexión donde cada usuario tiene un acceso por VPN a la red por ejemplo de la empresa y a su vez acceso a las máquinas X que le correspondan. Esto también puede servir para accesos de proveedores y terceros, a los que habitualmente se les debe configurar una VPN con la incertidumbre de __cómo de limpios tendrán los equipos origen desde donde se van a conectar__.

<img src="/images/posts/guacamole/arquitectura_vpn.png" alt="vpn" style="max-width: 70%"/>

Guacamole se trata de un __servidor [HTML5](https://desarrolloweb.com/articulos/que-es-html5.html)__ que se encarga de realizar la conexión dentro de la red doméstica o de la empresa por Remote Desktop, SSH, Telnet, VNC o Kubernetes y __servir esa conexión al exterior vía WEB__. Esto hace que podamos controlar de forma detallada quién se conecta a que servicio sin tener que crear complejas reglas en el firewall, sin que haya conexión de equipos ajenos a nuestro control (proveedores etc.) y además con la posibilidad de implementar medidas de seguridad extra como:

- Acceso con doble factor
- Permisos de acceso con horarios
-	Caducidad de los permisos
-	Grabación de las sesiones
-	sFTP para los accesos SSH (poder subir y bajar ficheros sin necesidad de otra conexión)
-	Limites de conexiones concurrentes a máquinas
-	Wake-on-LAN (encender los equipos al levantar la sesión)
-	Usuarios y grupos de usuarios conectados a nuestro Active Directory, LDAP, openID, etc
- Balanceo de carga de usuarios entre grupos de máquinas

Además al ser un __software completamente libre y gratuito__, podemos utilizar extensiones de terceros por ejemplo para [modificar el portal de login con nuestro logo](https://github.com/Zer0CoolX/guacamole-customize-loginscreen-extension), o podemos crear nuestras propias extensiones si hay algo que no nos encaja.

<img src="/images/posts/guacamole/arquitectura_guacamole.png" alt="guacamole"  style="max-width: 70%"/>

Este post es una mera introducción a la herramienta ya que dependiendo de las necesidades de cada uno existen diferentes opciones de arquitectura de instalación y uso. En este caso lo voy a enfocar más al ámbito doméstico, realizando la instalación sobre [nuestro server unRaid](https://firstcommit.dev/2021/01/31/Unraid/) con Docker (versión no oficial) y configurando el acceso los equipos de casa (el propio servidor unRaid por ejemplo).

(Foto de portada<a href="https://unsplash.com/es/@t_rampersad?utm_content=creditCopyText&utm_medium=referral&utm_source=unsplash">Tessa Rampersad</a> en <a href="https://unsplash.com/es/fotos/sopa-verde-en-tazon-de-ceramica-blanca-9ND-qkGs1_8?utm_content=creditCopyText&utm_medium=referral&utm_source=unsplash">Unsplash</a>)

#### TOC

# Instalación de Apache Guacamole en unRaid

La instalación que vamos a llevar a cabo será como lo hemos hecho hasta ahora, desde la *tienda de aplicaciones* de la comunidad de unRaid. Buscamos por *guacamole* y veremos que ya existe una plantilla de instalación de la versión no oficial del [repositorio de jasonbean](https://hub.docker.com/r/jasonbean/guacamole), donde tiene publicada una versión completa que incluye todo lo necesario para hacer correr la aplicación en un solo contenedor:

<img src="/images/posts/guacamole/install1.png" alt="pages" class="sombra" />

Para una instalación profesional, la forma de instalarlo sería diferente ya que deberíamos instalar los contenedores oficiales de guacamole: [el front-end](https://hub.docker.com/r/guacamole/guacamole), [el backend o guacd](https://hub.docker.com/r/guacamole/guacd) y una base de datos tipo SQL, por ejemplo [MadriaDB](https://hub.docker.com/r/linuxserver/mariadb/).

<img src="/images/posts/guacamole/install2.png" alt="pages" class="sombra" />  

En la pantalla de configuración de la plantilla, y está es la configuración que yo tengo. Tengo cambiado __el puerto por defecto que es el 8080__, que como es probable que lo tengamos ocupado por otro servicio. Yo lo tengo cambiado al 8180. Además __he cambiado la opción de TOTP (doble factor) a *Y* (yes)__ ya que vamos a activarlo en todos los usuarios [por motivos de seguridad](https://privacy.eus/es/doble-factor-de-autenticacion-que-es-y-por-que-es-recomendable-usarlo/).  

Una vez instalado desde la pestaña docker de unRaid hacemos click en el logo y vamos al la web-ui de Guacamole: 

<img src="/images/posts/guacamole/install3.png" alt="pages" class="sombra" />

Para entrar por primera vez debemos utilizar las siguientes credenciales:

- Usuario: *guacadmin*
- Password: *guacadmin*

<img src="/images/posts/guacamole/install4.png" alt="pages" class="sombra" />

Lo primero que vamos a hacer es crear un nuevo usuario para nosotros con permisos de administrador y borraremos el usuario *guacadmin*. Para ello, hacemos click arriba en la derecha donde pone *guacadmin* y vamos a *configuración* > *usuarios*:

<img src="/images/posts/guacamole/install5.png" alt="pages" class="sombra" />

Creamos el usuario seleccionando todos los permisos de administrador. Como puedes ver, en la creación de usuarios podemos configurar unas restricciones para la cuenta, como horarios, caducidad etc:

<img src="/images/posts/guacamole/install6.png" alt="pages" class="sombra" />

Estas credenciales las podemos guardar en [nuestro gestor de contraseñas](https://firstcommit.dev/2021/05/09/vaultwarden/). Ahora, salimos de la cuenta guacadmin y entramos con las nuevas credenciales que acabamos de crear. Nos pedirá configurar el doble factor:

<img src="/images/posts/guacamole/install7.png" alt="pages" class="sombra" />

La activación la podemos hacer utilizando, como ya vimos, nuestro propio gestor de contraseñas, o bien utilizando una aplicación móvil tipo [Authy](https://authy.com/). Ya vez confirmado este paso, volveremos al listado de usuarios, entramos en el usuario *guacadmin* y procedemos a borrarlo.

# Configuración de grupos de usuarios

Una de las formas más cómodas para gestionar los usuarios y sus permisos (en caso de que vayamos a tener más de un usuario y con diferentes permisos, en una pyme por ejmplo) son los grupos:

<img src="/images/posts/guacamole/install8.png" alt="pages" class="sombra" />

En los grupos podremos administrar los mismos permisos que a nivel de usuario, incluyendo los permisos de accesos a los equipos:

<img src="/images/posts/guacamole/install9.png" alt="pages" class="sombra" />

# Conexiones VNC, RDP y SSH

La creación de conexiones es muy sencilla. Para aprender a realizar una conexión voy a poneros un ejemplo concreto de forma que lo veamos su usabilidad en la práctica real. Vamos a empezar con las conexiones VNC, que las podemos usar para conectarnos nativamente a los VM de unRaid. El motor de virtualización de unraid es el software libre [QEMU](https://www.qemu.org), el cual integra un servidor VNC en cada instancia de virtualización (independiente del sistema operativo, es como una conexión VNC a la placa base).

En mi caso tengo este ejemplo de un [Kubuntu](https://kubuntu.org/) virtual el cual tiene como puerto VNC el 5900:

<img src="/images/posts/guacamole/vnc1.png" alt="pages" class="sombra" />

En guacamole vamos a *configuración* > *conexiones* > *nueva conexión*. 

<img src="/images/posts/guacamole/vnc2.png" alt="pages" class="sombra" />

Aquí configuraremos la conexión de tipo VNC, el nombre, el puerto, la contraseña VNC si tiene, etc. Como podemos ver, podremos habilitar la grabación de la sesión que creará un vídeo por cada acceso que se realice a esta conexión. Esto puede ser interesante en pymes si queremos dar acceso remoto a proveedores y queremos que todo lo que se lleve a cabo en dicha máquina quede registrado:

<img src="/images/posts/guacamole/vnc3.png" alt="pages" class="sombra" />

Ahora que hemos creado la conexión, como somos administradores deberíamos tener el acceso ya permitido. Si queremos dar acceso a un grupo de usuarios o a un usuario específico a esta conexión, iremos a *configuraciones* > *usuarios/grupos* y daremos el permiso de acceso a dicha máquina dentro del perfil de usuario/grupo:

<img src="/images/posts/guacamole/vnc4.png" alt="pages" class="sombra" />

Ahora hacemos click en nuestro usuario (arriba a la derecha) y vamos a Inicio y veremos que en el listado inferior tenemos la máquina Kubuntu disponible. Hacemos click y automáticamente nos abrirá la conexión:

<img src="/images/posts/guacamole/vnc5.png" alt="pages" class="sombra" />
<img src="/images/posts/guacamole/vnc6.png" alt="pages" class="sombra" />

Si hacemos ctrl+alt+mayus nos abrirá una barra la lateral izquierda donde podremos cambiar el método de entrada (podemos poner un teclado en pantalla, muy cómodo si estamos accediendo desde una tablet), activar el ajuste automático de la resolución etc. Desde aquí también podremos cambiar de forma rápida entre conexiones, desconectarnos de la sesión o volver al inicio de guacamole manteniendo la sesión abierta:

<img src="/images/posts/guacamole/vnc7.png" alt="pages" class="sombra" />

En caso de estar en una empresa o tener servidores o equipos windows pro, también __podremos configurar conexiones RDP a servidores y máquinas virtuales__, la configuración es prácticamente igual. Solo tendremos que configurar el la conexión como RDP, el puerto 3389, dominio, usuario y contraseña etc.

En cuanto a __conexiones SSH__, podemos utilizar tanto login con user/pass como con claves compartidas. Además, en SSH la configuración de SFTP es muy sencilla ya que la transferencia de ficheros irá también a través del propio SSH. Aquí un ejemplo de conexión a una Raspberry Pi:

<img src="/images/posts/guacamole/ssh0.png" alt="pages" class="sombra" />

Y así es como veremos la terminal de UnRaid por ejemplo y el SFTP integrado:

<img src="/images/posts/guacamole/ssh1.png" alt="pages" class="sombra" />
<img src="/images/posts/guacamole/ssh2.png" alt="pages" class="sombra" />

Como probablemente ya te habrás dado cuenta, todas estas conexiones las podremos agrupar en grupos, de los cuales existen dos tipos: 

- __Grupos organizativos__: funcionan como si de una carpeta se tratase, donde agrupamos las conexiones por ejemplo de un mismo proyecto o departamento y así podemos definir los permisos de un grupo de usuarios contra un grupo organizativo de conexiones, independientemente del tipo de conexión.
- __Grupos de balanceo__: sirven para crear un sistema de tipo Pool, al hacer click en un grupo organizativo guacamole mirará cual de las conexiones tiene el menor número de usuarios activos y automáticamente abrirá una sesión contra ella. Esto nos permite por ejemplo crear un grupo de escritorios remotos que se irán asignando a usuarios según entren al sistema de forma dinámica.

# Acceso remoto con Nginx Proxy Manager

Como ya vimos en [este post](https://firstcommit.dev/2021/04/19/saas/), utilizando [Nginx Proxy Manager](https://nginxproxymanager.com/) podemos publicar este servicio web para que podamos acceder a el desde cualquier parte, algo muy útil para poder realizar trabajos en remoto, dar acceso a proveedores a máquinas sin necesidad de VPN, o incluso permitir que los trabajadores de la empresa puedan teletrabajar contra su equipo habitual sea de sobremesa o virtual desde cualquier aparato.

Para publicar el servicio, teniendo el Nginx Proxy Manager instalado es tan sencillo como decidir un nombre para el servicio y apuntar al equipo y puerto donde tenemos nuestro Guacamole. Quiero recalcar de nuevo la importancia de activar el doble factor en caso de que vayamos a publicar este servicio ya que de lo contrario, un robo de una contraseña a un usuario con un keylogger etc. puede convertirse en un vector de ataque muy peligroso.

<div id="banner" style="overflow: hidden;justify-content:space-around;">
    <div class="" style="max-width: 49%;max-height: 50%;display: inline-block;">
        <img src="/images/posts/guacamole/nginx.png" alt="pages" class="sombra" />
    </div>
    <div class="" style="max-width: 49%;max-height: 50%;display: inline-block;">
        <img src="/images/posts/guacamole/nginx2.png" alt="pages" class="sombra" />
    </div>
</div>

---

Espero que esta __introducción a Guacamole__ pueda servir para descubriros un nuevo mundo de conexiones remotas y lo pongáis en práctica en vuestras casas, empresas etc. Si no es así al menos espero haberos abierto el apetito.

Nos vemos en el siguiente post.

---
<a href="https://t.me/firstcommit" class="btn btn-primary text-white py-2">Suscríbete, que es gratis</a>

*Nota: algunos de los enlaces a productos o servicios pueden ser enlaces referidos con los que podemos obtener una comisión de venta.*

<style> 
img.sombra {
  display: block;  
  margin-left: auto;  
  margin-right: auto; 
  box-shadow: 5px 5px 8px grey;
}
img.sombra-small {
  display: block;  
  margin-left: auto;  
  margin-right: auto; 
  box-shadow: 5px 5px 8px grey;
  max-width: 20%;
}
img.sombra-inline {
  display: inline-block; 
  vertical-align: middle; 
  margin-right: 0px; 
  box-shadow: 3px 3px 8px grey;
}
img.zoom {
  transition: transform .2s;
}
img.zoom:hover {
  -ms-transform: scale(1.2); /* IE 9 */
  -webkit-transform: scale(1.2); /* Safari 3-8 */
  transform: scale(1.2); 
}
</style>
