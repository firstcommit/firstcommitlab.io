---
title: "Consulta la ocupación de los parkings de San Sebastian en tiempo real con datos abiertos, Grafana y Node-Red"
authors: [yayitazale]
date: 2024-05-11T21:30:00Z
image: /images/posts/parkings/sven-mieke-lOgR82HSQKM-unsplash.jpg
categories:
  - selfhosted
tags:
  - monitoring
  - guia
  - api
  - grafana
  - node-red
  - json

---
Ahora que viene el verano es habitual que __los parkings de la costa estén más concurridos que en invierno__. A nadie le gusta llegar y tener que hacer cola para esperar a entrar a un parking por lo que sería ideal poder saber la ocupación de estos aparcamientos. En San Sebastian, hace algunos años __el ayuntamiento creó un servicio para informar de la ocupación de los aparcamientos de la ciudad casi en tiempo real__. Este servicio llamado [GEODONOSTIA](https://www.donostia.eus/ataria/es/web/geodonostia) está alojado en la propia página web del ayuntamiento, el cual es [mapa de representación GIS](https://www.donostia.eus/geozerbitzuak/apps/MapasLigeros/index.html?lang=es&webmap=83066106148f437ca1645e90f3a5148a) creado con Javascript que es bastante lento y que además, no ofrece la información a simple vista de cada aparcamiento sino que hay que hacer clic en cada uno de ellos y solo ofrece el nº de plazas totales:

<img src="/images/posts/parkings/gis_donostia.png" alt="Mapa GIS aparcamientos San Sebastian" />

Este tipo de desarrollos son bastante habituales en la administración pública, donde aglutinan toda la información geoespacial de la ciudad en un pequeño monstruo pensado para representar datos estáticos y que solo sirve a los técnicos del ayuntamiento y es muy poco útil para la ciudadanía en general salvo para los casos en los que tienes un par de horas y necesitas saber la ubicación de los mojones que delimitan los límites administrativos de la ciudad:

<img src="/images/posts/parkings/mojones_donostia.png" alt="Mapa GIS mojones San Sebastian" />

Por suerte, la propia web del ayuntamiento dispone de un [apartado de datos abiertos](https://www.donostia.eus/datosabiertos/) en el que está disponible dentro del catalogo de datos que ofrece, los [datos de ocupación actual de los parkings subterráneos en tiempo real](https://www.donostia.eus/datosabiertos/catalogo/parkings_actual). Estos datos están accesibles en forma de [API](https://es.wikipedia.org/wiki/API) [JSON](https://es.wikipedia.org/wiki/JSON) completamente abierto, por lo que vamos a jugar con estos datos y crearnos nuestro propio mapa interactivo que realmente nos sirva para poder consultar esta información de forma rápida desde cualquier lugar, en cualquier momento y con cualquier dispositivo.

(Foto de portada <a href="https://unsplash.com/es/@sxoxm?utm_content=creditCopyText&utm_medium=referral&utm_source=unsplash">Sven Mieke</a> en <a href="https://unsplash.com/es/fotos/piso-de-rayas-amarillas-y-negras-lOgR82HSQKM?utm_content=creditCopyText&utm_medium=referral&utm_source=unsplash">Unsplash</a>)

Si quieres navegar de forma rápida, este es el TOC:

#### TOC
---
# Requisitos

-	Vamos a utilizar [Node-Red](https://nodered.org/) para transformar los datos del JSON del ayuntamiento ya que el formato de alguno de los campos no es el adecuado para nuestra herramienta de visualización. Tienes disponibles una pequeña introducción a Node-Red y la guía para instalarlo en el mi post [Autoriego inteligente de plantas mediante IoT, HomeAssistant, OpenWeatherMap y NodeRed](https://firstcommit.dev/plantas/#node-red-la-aplicaci%C3%B3n-definitiva-para-automatizaciones).

-	Para la parte de visualización de los datos, vamos a hacer uso de [Grafana](https://grafana.com/). Tienes una introducción a esta herramienta y la guía de instalación en el mi post sobre [Monitorización de servidores con Telegraf, InfluxDB y Grafana](https://firstcommit.dev/monitorizacion-servidores/#gr%C3%A1ficas-y-m%C3%A1s-gr%C3%A1ficas-con-grafana).

# JSON del servicio del ayuntamiento

Para empezar, vamos a echar un vistazo en el formato del [JSON del servicio web del ayuntamiento](https://www.donostia.eus/info/ciudadano/camaras_trafico.nsf/getParkings.xsp). Como es un poco grande, vamos a ver un ejemplo solo con un parking:

```json
{
    "features": [
        {
            "geometry": {
                "coordinates": [
                    582435.5700000001,
                    4796617.12
                ],
                "type": "Point"
            },
            "type": "Feature",
            "properties": {
                "tipo": "LIB",
                "plazasRotatorias": 300,
                "plazasResidentesLibres": 2,
                "noteId": "A8C4DD6C19C157A2C1257D6B00310619",
                "libres": "104",
                "nombre": "San Martin",
                "precios": [
                    "0,79",
                    "1,32",
                    "2,38",
                    "3,45",
                    "4,46",
                    "6,56",
                    "11,47",
                    "20,68",
                    "23,02",
                    "24,57"
                ],
                "plazasResidentes": 300
            }
        }
            ],
    "success": true,
    "name": "parkings",
    "count": 16,
    "type": "FeatureCollection"
}
```

Podemos ver que los aparcamientos vienen listados dentro del __objeto tipo array__ *features*. Dentro de cada objeto del array, tenemos tres elementos que lo definen: 
- __*type*__: define el tipo de objeto, en este caso es siempre *Feature*, seguramente en otros juegos de datos del ayuntamiento existirán otros.
- __*Geometry*__: donde encontraremos los datos de geolocalización.
- __*Properties*__: donde tenemos los datos propiamente dichos de cada elemento. 

Finalmente tenemos unos pocos campos en los que se define el array de objetos con un tipo *FeatureCollection*, el nº total de elementos *Count*, el nombre de la colección de datos y un *success* que nos indica si nos trae datos de la base de datos.

Cada objeto array, como hemos dicho, es un Parking subterráneo de la ciudad. Dentro del array de cada parking encontramos varios bloques de información.

En el bloque __*Geometry*__ de nuevo tenemos una definición de tipo, en este caso siempre son *Point* (el ayuntamiento localiza cada parking en un punto específico del mapa, si fuera un barrio por ejemplo, sería un área) y sus coordenadas. Como podemos ver, las coordenadas son un poco especiales. En herramientas GIS es habitual encontrarse con un sistema de [coordenadas de tipo UTM, comúnmente conocido como Mercator](https://es.wikipedia.org/wiki/Sistema_de_coordenadas_universal_transversal_de_Mercator). Este __sistema de coordenadas desarrollado por el cuerpo de ingenieros del ejercito de estados unidos__ es una proyección cilíndrica conforme. No voy a entrar en detalles técnicos, pero debemos saber que __este sistema de no está soportado por la herramienta de representación [GEOMAP](https://grafana.com/docs/grafana/latest/panels-visualizations/visualizations/geomap/) de Grafana__, por lo que deberemos hacer una transformación de UTM al sistema clásico de [coordenadas geográficas](https://es.wikipedia.org/wiki/Coordenadas_geogr%C3%A1ficas) antes de poder representarlo en dichos mapas. Esto lo veremos más adelante en Node-Red.

En la parte de __*Properties*__ tenemos bastante información. Por un lado, está disponible el nº total de plazas rotatorias (que son las que nos interesan a nosotros, son las de utilización libre), el nombre del parking, el nº de plazas rotatorias libres y también información sobre las plazas de residentes y precios que en este caso vamos a descartar. 

Por comentar un detalle técnico, que el nº de plazas libres venga con un formato [String](https://es.wikipedia.org/wiki/Cadena_de_caracteres) (Cadena de carácteres) en vez de un [Integer](https://es.wikipedia.org/wiki/Entero_(tipo_de_dato)) (Entero) __es bastante chapucero por parte del desarrollador de esta API__, pero bueno, cosas que pasan hasta en las mejores casas. Además, si las coordenadas se oferecieran en formato geográfico como hemos dicho, podríamos directamente usarlo en Grafana como fuente de datos y nos podríamos saltar todo lo que indico en el siguiente punto.


# Transformado los datos en nodered

Bien vamos a ello. Como he dicho como el sistema UTM no nos vale, lo que vamos a hacer en Node-Red es crear un flujo para extraer los datos de este JSON con una frecuencia fija (por ejemplo, cada minuto), después vamos a hacer la transformación de UTM a Lat/Lon en cada objeto del array de parkings, y ya que no son muchos datos, para evitar usar una base de datos con todo lo que ello implica, lo vamos a guardar en la memoria interna de Node-Red como una variable.

Después, vamos a crear y publicar un servicio web en Node-Red al que llamaremos desde Grafana y el cual devolverá los datos almacenados en dicha variable.

Estamos de suerte (gracias a la comunidad de Node-Red) ya que existe un [pallete preparado ya para pasarle datos de UTM y pasarlos al sistema Lat/Lon](https://flows.nodered.org/node/node-red-contrib-utm). Esto parece algo sencillo, pero créeme que no lo es. [Si tuviéramos que hacerlo con funciones matemáticas sería bastante más complicado](https://stackoverflow.com/questions/2689836/converting-utm-wsg84-coordinates-to-latitude-and-longitude). 

El flujo, que puedes descargarte [aquí](/images/posts/parkings/flow_api_request_parkings.json), hace lo siguiente:

<img src="/images/posts/parkings/flujo_solicitud_api.png" alt="Flujo de Node-Red de solicitud a la API" />

1.	Se acciona cada minuto.
2.	Hace un [http request](https://es.wikipedia.org/wiki/Protocolo_de_transferencia_de_hipertexto) al *endpoint* del servidor donde se encuentra el JSON.
3.	Si la respuesta del *request* es correcta (*statusCode* = 200) sigue adelante, de esto hablaremos más adelante.
4.	Si el campo *succes* = *true* se queda con el array de parkings y descarta todo lo demás.
5.	Divide el array en objetos únicos para tratarlos uno a uno.
6.	De parking en parking, prepara las coordenadas de UTM y los deja en las propiedades que requiere el conversor de UTM a Lat/Lon.
7.	Ejecuta la conversión de UTM a Lat/Lon para cada parking.
8.	Guarda las nuevas coordenadas en dentro de Geometry/Coordinates y borra las UTM de cada parking.
9.	Junta de nuevo todo el array de parkings y lo convierte en un objeto único de tipo array como teníamos al principio.
10.	Guarda el objeto en una [variable de Node-Red de tipo Flow](https://nodered.org/docs/user-guide/context).

Ahora, lanzamos el flujo manualmente (o esperamos un minuto), si vamos a la pestaña de *Información de contexto* de Node-Red y actualizamos la lista de *Variables de Flujo*, podremos ver que se nos ha creado una nueva variable llamada *parking* donde tenemos un objeto muy parecido al JSON del ayuntamiento, pero ya con las coordenadas Lat/Lon incluidas en cada objeto del array de parkings, dentro de geometry/coordinates de cada parking:


<img src="/images/posts/parkings/variable_contexto_nodered.png " alt="Variable de contexto en nodered" />

# Publicando nuestro propia API JSON

Ahora vamos a publicar el servicio. Como hemos visto en la llamada al servicio del ayuntamiento, lo primero que hemos hecho es mirar que tenemos un *statusCode* = 200.

Este 200 es una de las respuestas posibles de [la lista de códigos de estado del estándar http](https://es.wikipedia.org/wiki/Anexo:C%C3%B3digos_de_estado_HTTP). Un servicio como el que vamos a publicar nosotros debería (si queremos cumplir con el estándar) utilizar los códigos necesarios para poder informar al cliente del estado de su petición. Estos códigos son los típicos que nos encontramos cuando intentamos acceder a una web y no funciona que sería un *404 Not Found*.

<img src="/images/posts/parkings/github-404.png " alt="Ejemplo de 404 Not Found de GitHub" class="sombra"/>

Los códigos son de tres dígitos, siendo __el primer digito el que define el tipo de mensaje__ que estamos recibiendo:

- __1xx respuestas informativas__: se ha recibido la solicitud, proceso en curso.
- __2xx Peticiones correctas__: la solicitud ha sido recibida, comprendida y aceptada con éxito.
- __3xx Redirecciones__: es necesario realizar más acciones para completar la solicitud.
- __4xx Errores del cliente__: la solicitud contiene una sintaxis incorrecta o no puede completarse.
- __5xx Errores del servidor__: el servidor no ha podido completar una solicitud aparentemente válida.

En nuestro caso, el servicio que vamos a crear va a ser de consumo en la red local y no lo vamos a publicar a internet, ya que el servidor de Grafana va a estar alojado en el mismo servidor que Node-red. Con esta premisa, el servicio no va a estar encriptado (será un http y no un https) pero si vamos a añadir un sistema de autenticación muy simple al menos para evitar que alguien con malas intenciones que consiga acceder a nuestra red pueda llevar a cabo un por ejemplo, un [ataque DDoS](https://es.wikipedia.org/wiki/Ataque_de_denegaci%C3%B3n_de_servicio) realizando llamadas de forma reiterada hasta hacer caer el servicio por saturación. 

El servicio va a comprobar primero, que la IP del que se realiza la llamada es de la red local (en mi caso, 192.168.1.0/24), vamos a comprobar que nos llega un token (lo vamos a generar a mano por ejemplo usando [este servicio](https://abctoolbox.com/es/token-generator/)), y vamos a implementar un límite de 1 llamada por segundo para evitar que haciendo llamadas al dashboard de Grafana de forma muy repetida se pueda saturar el servicio. Las salidas de estos controles devolverán cada uno su *statusCode* correspondiente. Además, como el método que vamos a publicar es un *GET*, vamos a hacer que devuelva también un error si el *endpoint* es llamado con otro método:

<img src="/images/posts/parkings/flujo_api_propia.png " alt="Flujo de publicación de API en Node-Red" />

El flujo de este servicio lo tienes descargable [aquí](/images/posts/parkings/flow_publish_api.json) para importarlo, solo deberás añadir un token.

Ahora, si hacemos la llamada a http://la-ip-de-nodered:puerto-de-nodered/parking, en mi caso, http://192.168.1.20:3880/parking desde nuestro navegador nos devolverá un 401 ya que no estaremos pasándole el token que espera. Usando el modo depuración del navegador, podemos añadir el token de forma manual y conseguiremos que nos devuelva el JSON tal y como lo hemos preparado.

<img src="/images/posts/parkings/response_api.png " alt="Respuesta API navegador" />

Este sistema de token plano no es para nada lo más seguro ya que es “fácil” de interceptar, pero ya que se trata de un servicio local, vamos a dejarlo así ya que es sencillo de implementar. En caso de querer implementar un sistema más seguro de autenticación, podríamos generar un flujo de token con [JWT](https://jwt.io/) por ejemplo donde el token va encriptado y firmado y solo sabiendo la base de encriptación usada y el secret (que solo el servidor como el cliente conocen) se puede descifrar para compararlo con el que se espera. Algo así sería imprescindible si fuesemos a publicar la API a internet y quisieramos controlar el acceso.

# Dashboard en Grafana

Ahora vamos a crear el dashboard de Grafana. Lo primero que vamos a hacer es ir a *Inicio -> Conexiones -> Orígenes de datos* y vamos a añadir una nueva fuente de datos. Buscamos por JSON y agregamos un nuevo origen de este tipo:

<img src="/images/posts/parkings/fuente_datos_JSON.png " alt="Agregar fuente de datos API JSON en Grafana" />

Rellenamos los datos de nombre, la URL y añadimos el *header* *token* con el token antes creado, le damos a *save&test* y vemos que va todo bien:

<img src="/images/posts/parkings/fuente_datos_JSON_datos.png " alt="Datos para fuente de datos API JSON en Grafana" />

Ahora creamos un nuevo dashboard y añadimos un panel de tipo GEOMAP:

<img src="/images/posts/parkings/add_geomap_panel.png " alt="Añadimos un panel de tipo GEOMAP en Grafana" />

Como query, apuntamos a la fuente de datos que acabamos de crear, lo cual hará que se llame a la API cada vez que carguemos el dashboard. Ahora, creamos *Fields* que será el parseado de los campos de los objetos del JSON a señales de Grafana. Vamos a parsear los campos de libres, nombre, plazas rotatorias, lat y lon:

<img src="/images/posts/parkings/query.png " alt="Query a la API en Grafana" />

Como ves, estoy usando una expresión regular de [JSON Path](https://goessner.net/articles/JsonPath/) para definir que quiero coger los elementos de cada objeto del array:

```json
$[*].properties.libres
$[*].properties.nombre
$[*].properties.plazasRotatorias
$[*].geometry.coordinates.lat
$[*].geometry.coordinates.lon
```

Fíjate que he definido como *Cache Time* en 5 segundos. Esto hará que aunque el dashboard se abra mile de veces por segundo, solo llame al servicio de Node-Red una vez cada 5 segundos. Durante esos 5 segundos se cacheara la respuesta y así limitamos muchísimo el tráfico y el nº de solicitudes.

Con esto podremos calcular el % de ocupación haciendo la regla de tres (libres / plazasRotatorias * 100):

<img src="/images/posts/parkings/porcentaje_ocupacion.png " alt="Cálculo de porcentaje de ocupación en Grafana" />

Ahora, en la configuración del panel de GEOMAP, vamos a decirle que debe representar la query A y que los datos de geolocalización están en formato Auto (lat/lon): 

<img src="/images/posts/parkings/mapear_capas.png " alt="Mapear capas en Grafana GEOMAP" />

Hacemos que los puntos tengan colores en base a su %, aunque también podríamos variar su diámetro en base a este valor:

<img src="/images/posts/parkings/colorear_valores.png
 " alt="Colorear valores de porcentaje en Grafana" />

Además, podemos añadir la etiqueta del valor real de plazas libres al lado del puntito para que tengamos la info más importante a simple vista:

<img src="/images/posts/parkings/etiqueta_plazas_libres.png
 " alt="Etiqueta de plazas libres" />

Finalmente, nos quedará poner a nuestro gusto el mapa base, el centrado y nivel de zoom de la vista etc.

Si queremos tener otro tipo de visualizaciones, como, por ejemplo, un gráfico de barras, podemos aprovechar la query y el cacheo de datos por lo que al crear el nuevo panel, haremos referencia a la query del otro panel:

<img src="/images/posts/parkings/query_reutilizada.png
 " alt="Reutilizar query en Grafana" />

Solo nos quedará transformar un poco esos datos al formato requerido por el panel, descartar los campos de lat y lon y tocar la parte estética del panel:

<img src="/images/posts/parkings/transformar_datos_grafico_barras.png
 " alt="Transformar fatos para la gráfica de barras de Grafana" />

Con un poco de mimo, puede conseguir algo parecido a esto:

<img src="/images/posts/parkings/dashboard_completo.png
 " alt="Dashboard completo en Grafana" />

Si quieres, puedes bajarte el cuadro de mandos [aquí](/images/posts/parkings/Ocupacion_actual_parkings_Donostia-1715451375558.json) para importarlo como base. También puedes [directamente acceder a mi dashboard](https://data.yayitazale.cc/d/e2fbd7bb-22ff-426e-8d71-4934f12cd41c/ocupacion-actual-parkings-donostia?orgId=4&refresh=30s), ya que lo tengo en la parte pública de mi Grafana, y así no tendrás que hacer nada más que guardarlo entre tus páginas favoritas. Para agregarlo como APP a tu teléfono móvil te dejo [aquí una guía publicada por Euskalmet](https://www.euskalmet.euskadi.eus/euskalmet-en-tu-movil-app/) que está muy bien explicada.

PD: algunos parkings llevan meses emitiendo un 0 en la variable de plazas libres...

---

Espero que te haya parecido interesante y hayas aprendido algo nuevo y recuerda que para cualquier comentario, duda o sugerencia tienes a tu disposición [nuestro grupo de telegram](https://t.me/firstcommitchat).

---
<a href="https://t.me/firstcommit" class="btn btn-primary text-white py-2">Suscríbete, que es gratis</a>


*Nota: algunos de los enlaces a productos o servicios pueden ser enlaces referidos con los que podemos obtener una comisión de venta.*


<style> 
img.sombra {
  display: block;  
  margin-left: auto;  
  margin-right: auto; 
  box-shadow: 5px 5px 8px grey;
}
img.sombra-small {
  display: block;  
  margin-left: auto;  
  margin-right: auto; 
  box-shadow: 5px 5px 8px grey;
  max-width: 20%;
}
img.sombra-inline {
  display: inline-block; 
  vertical-align: middle; 
  margin-right: 0px; 
  box-shadow: 3px 3px 8px grey;
}
img.zoom {
  transition: transform .2s;
}
img.zoom:hover {
  -ms-transform: scale(1.2); /* IE 9 */
  -webkit-transform: scale(1.2); /* Safari 3-8 */
  transform: scale(1.2); 
}
</style>
