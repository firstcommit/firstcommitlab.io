---
title: Ofimática en la nube con Software Libre 
authors: [yayitazale]
date: 2021-11-28T19:00:00Z
image: /images/posts/OnlyOffice/intro.jpg
categories:
  - selfhosted
tags:
  - cloud
  - selfhosted
  - guia
  - servidores
  - nomoregoogle
  - google
  - drive
  - docs
  - spreadsheet
  - excel
  - office
  - office365

---

Para terminar con esta serie de [alternativas seflhosted a Google](https://firstcommit.dev/tags/nomoregoogle/), en este tercer y último post os voy a proponer una __alternativa a Google Docs/Drive y Office365__.

Como ya os comenté en los post anteriores, las alternativas que os voy a plantear son todas de __software libre__ que he encontrado tanto en [nomoregoogle.com](https://nomoregoogle.com/) como en [ethical.net](https://ethical.net/), dos páginas que os animo a visitar antes de emprender cualquier __proyecto de selfhosted__. 

<a href="https://nomoregoogle.com/"><img src="/images/nextcloud/intro2.png" alt="nomoregoogle" /></a>

En los __tres post que completan esta serie__, os he dejado enlaces a otras alternativas que pueden ser de utilidad si se ajustan mejor a vuestras necesidades y que podéis probar para compararlos con los que yo os voy a enseñar a instalar y utilizar. Como siempre, os animo a que __compartáis en los comentarios los resultados__ para que todos podamos aprender juntos.

#### TOC

---
# Ofimática en la Nube

Uno de los __grandes cambios__ ocurridos en el ámbito informático laboral en la __última década__ ha sido la de pasar de utilizar una __ofimática offline a una ofimática online y colaborativa__. 

Google lanzó su servicio primitivo allá por [abril de 2012](https://es.wikipedia.org/wiki/Google_Drive) aunque la adopción de [Drive](https://drive.google.com/) fue mayoritariamente en el ámbito doméstico. Microsoft hizo lo propio casi un año antes, en [junio de 2011](https://es.wikipedia.org/wiki/Microsoft_Office_365) lanzando [Office365](https://www.office.com/). Aunque su adopción ha sido más lenta, paulatinamente se ha ido convirtiendo en el estándar de ofimática de facto para la mayoría de empresas (como lo era previamente word, excel y exchange/outlook).

<img src="/images/posts/OnlyOffice/cloud.jpg" alt="cloud" class="sombra" />

Este cambio a la ofimática en la nube ha propiciado un __cambio en la forma de trabajar de muchas empresas__, siendo los principales cambios:

- __Almacenamiento:__ los documentos ya no solo viven en los ordenadores de los trabajadores/usuarios o en las posibles rutas compartidas de un servidor de ficheros. Todo se guarda automáticamente en la nube haciendo el trabajo más seguro frente a pérdidas accidentales.
- __Correo:__ ya nadie se plantea mantener el buzón de correo en sus servidores por el coste de mantenimiento que acarrea. Es muy difícil que una sola persona o un equipo de IT pueda mantener los mismos niveles de servicio y seguridad que te ofrecen Google o Microsoft. También es cierto que estas proveedoras han jugado sucio haciendo que los correos recibidos por entidades no _reconocidas_ fueran directamente a los buzones de spam, alegando que es por la seguridad de sus clientes... En parte tienen razón pero en muchos casos a obligado a que, si quieres seguir utilizando el email como método principal de comunicación interna y externa en la empresa, tengas que pasar a un cloud de pago por servicio.
- __Colaboración:__ hasta la aparición de los servicios de ofimática en cloud la edición de los archivos era posible solo para un usuario por vez. Muchos nos acordamos de cuando queríamos editar un excel de una carpeta compartida había que esperar a que tu compañero de trabajo cerrara el archivo. Con una ofimática en nube, en poco tiempo apareció la posibilidad de que varias personas editaran el mismo archivo de forma simultánea. 

Además de estos cambios, la ofimática en la nube ha permitido gracias al almacenamiento también en la nube, que puedas acceder a tus archivos desde __cualquier dispositivo, cualquier lugar y en cualquier momento__, aunque de eso ya hablamos en el [primer post de la serie](https://firstcommit.dev/2021/09/26/nextcloud/). 

# Instalando OnlyOffice sobre unRaid con Docker

Si seguiste los pasos del [primer post](https://firstcommit.dev/2021/09/26/nextcloud/), ya tenemos nuestra nube privada operativa gracias a [NextCloud](https://nextcloud.com/), por lo que lo ideal es utilizar un paquete de ofimática que se integre en esta nube. Existen dos paquetes con más o menos las mismas funcionalidades, [Collabora Online](https://www.collaboraoffice.com/es/collabora-online/) y [OnlyOffice](https://www.onlyoffice.com/es), ambos de software libre. 

__Collabora es una alternativa fantástica de ofimática online__ basada en [LibreOffice](https://es.libreoffice.org/descarga/libreoffice-online/). __OnlyOffice es más parecido a Office365__ en cuanto a interfaz. En mi caso me decanto por el segundo ya que me parece más completo. En cualquier caso la __instalación es prácticamente idéntica__ en los pasos a seguir, por lo que te invito a que pruebes ambos y te decantes por el que mejor te convenga ya que [las diferencias son muy pequeñas](https://medium.com/onlyoffice/onlyoffice-vs-collabora-a-critical-comparison-18a5ba0dee62).

<img src="/images/posts/OnlyOffice/vs.jpg" alt="logo1" class="sombra" />

La instalación la haremos sobre [nuestro servidor unRaid](https://firstcommit.dev/2021/01/31/Unraid/), pero con cualquier otro OS como __Ubuntu Server__. la única diferencia es que nosotros usaremos la interfaz de instalación de UnRaid para levantar el micro-servicio con Docker y no tendremos que usar la línea de comandos. 

Los requisitos previos son

- [Tener instalado NextCloud](https://firstcommit.dev/2021/09/26/nextcloud/) ya que tanto OnlyOffice como Collabora no funcionan por si solos como otras APPs. Ambas aplicaciones necesitan un gestor de archivos en cloud y simplemente se integran sobre el cloud como una app externa que se carga como un motor de fondo para ofrecernos la funcionalidad de ofimática.
- [Tener instalado NginxProxyManager](https://firstcommit.dev/2021/04/19/saas/) o cualquier alternativa de proxy reverso.

Bueno, vamos a ello. La instalación es simple como os he dicho. Vamos a la __APP store de UnRaid y buscamos OnlyOffice__. Aquí nos aparecerán varias opciones. OnlyOffice aun siendo una suite Open Source está muy enfocado al ámbito empresarial y tiene un licenciamiento opcional de pago por funcionalides especiales y soporte (recuerda, [software libre != gratis](https://www.gnu.org/philosophy/free-sw.es.html)). En este caso, vamos a instalar la versión _Community Edition_ que es el gratuito y, aunque no tenga el 100% de funcionalideades, es suficiente para uso doméstico (también puedes instalar la versión enterprise y añadir la licencia correspondiente).

La versión de la comunidad que tienes que instalar es esta:

<img src="/images/posts/OnlyOffice/install1.png" alt="Instalación 1" class="sombra-small" />

En la configuración del contenedor __debes revisar los puertos__, en mi caso he tenido que cambiar el _8080_ al _8081_ ya que lo tenía ocupado por otro servicio. Además vamos a añadir __dos variables de entorno__ con los cuales vamos a proteger nuestra instalación. Para poder usar OnlyOffice en cualquier lugar deberemos exponerlo a internet mediante proxy reverso y esto hace que cualquier persona pueda hacer uso del servidor. __OJO:__ esto no quiere decir que cualquiera vaya a acceder a nuestros archivos ya que estos están en NextCloud. Lo que estamos instalando, para que me entiendas, son solo los programas de edición que luego usaremos dentro de NextCloud. El peligro aquí reside solo en que otra persona pudiera hacer uso de tu potencia de computación (y/o tu licencia en caso de que compres una).

<img src="/images/posts/OnlyOffice/install2.png" alt="Instalación 2" class="sombra" />

Para proteger la instalación y que solo la usemos nosotros hacemos lo siguiente. En la parte de abajo hacemos click en _Add Another Path, Port, Variable, Label or Device_:

<img src="/images/posts/OnlyOffice/install3.png" alt="Instalación 3" class="sombra" />

En el Pop-Up selecionamos _Variable_ y rellenamos con *JWT_ENABLED* en los campos _NAME_ y _KEY_. En el campo _Value_ escribimos _true_ y aceptamos:

<img src="/images/posts/OnlyOffice/install4.png" alt="Instalación 4" class="sombra-small" />

De nuevo, en la parte de abajo hacemos click en _Add Another Path, Port, Variable, Label or Device_. Esta vez creamos otro registro tipo _Variable_ pero en _NAME_ y _KEY_ escribimos *JWT_SECRET* y en _Value_ ponemos una contraseña larga tipo token (que podemos generar con nuestro gestor de contraseñas o usando una web tipo [randomkeygen.com](https://randomkeygen.com/):

<img src="/images/posts/OnlyOffice/install5.png" alt="Instalación 5" class="sombra" />

Una vez añadidas las dos variables aceptamos y esperamos a que el contenedor se despliegue. Si todo va bien, vamos a nuestro [proxy reverso](https://firstcommit.dev/2021/04/19/saas/) y creamos un __subdominio con certificados__ que apunte al __puerto 8080__ (o el que hayas puesto como _Host Port 1_:

<div id="banner" style="overflow: hidden;justify-content:space-around;">
    <div class="" style="max-width: 49%;max-height: 50%;display: inline-block;">
        <img src="/images/posts/OnlyOffice/install6.png">
    </div>
    <div class="" style="max-width: 49%;max-height: 50%;display: inline-block;">
        <img src="/images/posts/OnlyOffice/install7.png">
    </div>
</div>

Y ahora si accedemos al subdomninio que hemos creado desde nuestro navegador, deberíamos llegar a una página de confirmación como esta:

<img src="/images/posts/OnlyOffice/install8.png" alt="Instalación 8" class="sombra" />

Como ves, no hemos instalado nada más que __el motor de la suite ofimática__, pero no podemos hacer nada ya que no tenemos una interfaz ni un gestor de archivos. Esto lo vamos a tener una vez integremos el motor en nuestro Cloud.

PD: antes de finalizar este bloque quiero comentar que OnlyOffice utiliza una base de datos de fondo para sus cosas, por lo que si vas a utilizarlo en un entorno empresarial te aconsejo que instales un servicio dedicado como MariaDB ([que ya tendrás funcionando para NextCloud](https://firstcommit.dev/2021/09/26/nextcloud/#Instalando-Nextcloud-sobre-unRaid-con-Docker)). En la [documentación de instalación](https://helpcenter.onlyoffice.com/installation/docs-enterprise-install-docker.aspx) de un _Enterprise Server_ están los pasos a seguir.


#  Integración en NextCloud

La integración en nuestro cloud es muy muy sencilla. Entramos como administrador y vamos a _aplicaciones_. En el listado de tipo de aplicación vamos a _Oficina y texto_:

<div id="banner" style="overflow: hidden;justify-content:space-around; ">
    <div class="" style="max-width: 25%;max-height: 20%;display: inline-block;">
        <img src="/images/posts/OnlyOffice/integracion1.png">
    </div>
    <div class="" style="max-width: 25%;max-height: 20%;display: inline-block;">
        <img src="/images/posts/OnlyOffice/integracion2.png">
    </div>
</div>

Y aquí buscamos _OnfyOffice Connector_ e instalamos (en mi caso ya lo tengo instalado):

<img src="/images/posts/OnlyOffice/integracion3.png" alt="integracion3" class="sombra" />

Cuando termine vamos a _Ajustes_ y en el listado de la izquierda buscamos _ONLYOFFICE_:

<div id="banner" style="overflow: hidden;justify-content:space-around;">
    <div class="" style="max-width: 25%;max-height: 20%;display: inline-block;">
        <img src="/images/posts/OnlyOffice/integracion4.png">
    </div>
    <div class="" style="max-width: 25%;max-height: 20%;display: inline-block;">
        <img src="/images/posts/OnlyOffice/integracion5.png">
    </div>
</div>

Y nos llevará a la página de configuriación donde deberemos introducir el subdominio completo _https://office.tudominio.com_ que hayamos creado y la contraseña/token que hemos usado al lanzar el contenedor en la variable *JWT_SECRET*:

<img src="/images/posts/OnlyOffice/integracion6.png" alt="integracion6" class="sombra" />

Guardamos y ya tenemos nuestro cloud integrado con OnlyOffice. Si miramos más abajo, solo nos queda configurar los tipos de archivo que queremos que se abran con OnlyOffice y su comportamiento como editor:

<img src="/images/posts/OnlyOffice/integracion7.png" alt="integracion7" class="sombra" />

Además podemos también cargar plantillas por defecto para los diferentes tipos de archivo (texto, cálculo y presentación) con las plantillas base que tengamos creadas para nuestra organización o empresa.

<img src="/images/posts/OnlyOffice/integracion8.png" alt="integracion8" class="sombra" />

# Editando archivos en cloud

Ahora que ya tenemos todo listo podemos empezar a usar el editor online simplemente seleccionando _+_ y el tipo de archivo deseado en nuestra página principal o en la carpeta que queramos en NextCloud:

<div id="banner" style="overflow: hidden;justify-content:space-around;">
    <div class="" style="max-width: 30%;max-height: 50%;display: inline-block;">
        <img src="/images/posts/OnlyOffice/uso1.png">
    </div>
    <div class="" style="max-width: 30%;max-height: 50%;display: inline-block;">
        <img src="/images/posts/OnlyOffice/uso3.png">
    </div>
    <div class="" style="max-width: 30%;max-height: 50%;display: inline-block;">
        <img src="/images/posts/OnlyOffice/uso5.png">
    </div>
</div>

Vamos a ver tres ejemplo de cada tipo para que podáis ver la interfaz, empezando por un _Documento_:

<img src="/images/posts/OnlyOffice/uso2.png" alt="uso2" class="sombra" />

Una _Hoja de Cálculo_:

<img src="/images/posts/OnlyOffice/uso4.png" alt="uso4" class="sombra" />

Una _Presentación_:

<img src="/images/posts/OnlyOffice/uso6.png" alt="uso6" class="sombra" />

En mi caso tengo la interfáz configurada en __modo oscuro__ ya que me parece más agradable para trabajar, pero puedes cambiar al gris o claro desde el _menú_ -> _Ajustes Avanzados_:


<img src="/images/posts/OnlyOffice/uso7.png" alt="uso7" class="sombra-small"/>

<img src="/images/posts/OnlyOffice/uso8.png" alt="uso8" class="sombra"/>


# Extra: Otras herramientas útiles de software libre para el día a día

Con el final de esta serie vamos a __cerrar el bloque de cloud/ofimática__, pero muchos me diréis que esto no es suficiente para animarse a sustituir el paquete cloud/docs que te ofrece Google o Micosoft, ya que estas ofrecen muchas más cosas que lo tratado en estos tres post. Es algo que no niego, estas empresas trabajan no solo en crear productos y servicios, sino en crear __ecosistemas de productos y servicios interelacionados__, por lo que puede que las alternativas planteadas te parezcan un poco pobres. Me gustaría al menos listar una serie de proyectos o servicios que me parecen interesantes y que pueden ser de utilidad para crear un ecosistema propio basado en software libre que pueda llegar competir al menos como una alternativa real para personas individuales en el ámbito doméstico o para PYMES quieran ahorrar algo de presupuesto anual:

- __Alternativa a GTALK o TEAMS:__ el propio NextCloud tiene [Talk](https://nextcloud.com/talk/) que funciona de forma integrada y también tiene chats integrados. También existe [Jitsi](https://jitsi.org/) aunque instalarlo es un poco PITA (pain-in-the-ass). [Discord](https://discord.com/) también puede ser una alternativa que aunque no sea de software libre, es gratuito y funciona muy bien y tiene funcionalidades de salas/etc que son muy prácticas para trabajar en proyectos/empresa.
- __Alternativa a VISIO:__ si eres de los que hacen diagramas vectoriales como grafcets, bloques de diagramas, topologías de red etc, puedes integrar [Draw.io](https://app.diagrams.net/) en NextCloud en _Aplicaciones -> integración_. En este caso no es necesario instalar un servidor local del motor (para lo bueno y para lo malo). 
- __Alternativa a OUTLOOK o GMAIL:__ aunque no lo recomiendo ya que mantener el spam y phishing a raya es muy costoso en tiempo, NextCloud tiene la posibilidad de integrar nuestro correo de domino en [Mail](https://apps.nextcloud.com/apps/mail). La opción más sensata sería, en todo caso, hacer uso de un servicio de terceros que nos ofrezca esa capa de seguridad y una conexión [IMAP/POP3](https://es.wikipedia.org/wiki/Protocolo_de_acceso_a_mensajes_de_Internet). Mencionar también [poste.io](https://poste.io/), proyecto que quiero seguir de cerca.
- __Alternativa a WeTransfer:__ [youtransfer.io](https://www.youtransfer.io/) lo alojamos en nuestros servidores para que no la información no salga de nuestra empresa, perfecto para enviar archivos grandes. De todas formas, con una buena política de grupos y permisos en NextCloud los usuarios deberías ser capaces de compartir enlaces de sus archivos sin necesidad de una segunda herramienta como esta...

Dicho esto, os animo a que __si conocéis alguna herramienta interesante__ de este tipo que sea de __software libre__ lo dejéis en los __comentarios__ para que podamos conocerlo nosotros también. 


Nos vemos en el siguiente post.

---
<a href="https://t.me/firstcommit" class="btn btn-primary text-white py-2">Suscríbete, que es gratis</a>

*Nota: algunos de los enlaces a productos o servicios pueden ser enlaces referidos con los que podemos obtener una comisión de venta.*

<style> 
img.sombra {
  display: block;  
  margin-left: auto;  
  margin-right: auto; 
  box-shadow: 5px 5px 8px grey;
}
img.sombra-small {
  display: block;  
  margin-left: auto;  
  margin-right: auto; 
  box-shadow: 5px 5px 8px grey;
  max-width: 20%;
}
img.sombra-inline {
  display: inline-block; 
  vertical-align: middle; 
  margin-right: 0px; 
  box-shadow: 3px 3px 8px grey;
}
img.zoom {
  transition: transform .2s;
}
img.zoom:hover {
  -ms-transform: scale(1.2); /* IE 9 */
  -webkit-transform: scale(1.2); /* Safari 3-8 */
  transform: scale(1.2); 
}
</style>
