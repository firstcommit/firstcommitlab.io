---
title: "ZeroTier, conecta de forma segura cualquier dispositivo en cualquier lugar"
authors: [yayitazale]
date: 2023-10-22T21:30:00Z
image: /images/posts/zerotier/zerotier2.webp
categories:
  - networking
tags:
  - zerotier
  - vpn
  - openwrt
  - firewall
  - guia
  - redes
  - IoT
  - router

---

En este mundo hiperconectado en el que vivimos crear __redes seguras de comunicación entre equipos__ que están físicamente a cientos o miles de kilómetros de distancia es un reto complejo. Una de las formas más habituales y conocidas es __utilizar una VPN (red privada virtual)__.

Si quieres navegar de forma rápida, este es el TOC:

#### TOC

# Qué es una VPN

Una [red privada virtual](https://es.wikipedia.org/wiki/Red_privada_virtual) se basa en __extender una red local a través de internet__, normalmente a través uno o varios servidores VPN expuestos hacia el exterior desde la red local a través del cual se enruta el tráfico de los clientes de esa VPN. Este tráfico entre cliente servidor es [cifrado entre estos dos puntos](https://es.wikipedia.org/wiki/Cifrado_de_extremo_a_extremo) para que [ningún usuario de internet pueda interceptar la comunicación](https://es.wikipedia.org/wiki/Ataque_de_intermediario) (por eso es una red privada). Dependiendo de la configuración del servidor VPN y el firewall de la red local, __el cliente VPN podrá acceder a los recursos a los que tenga permiso desde internet__, tal y como lo haría si estuviera en la red local, por lo que virtualmente el cliente está dentro de la red privada.

<img src="/images/posts/zerotier/esquema_vpn.png" alt="Esquema VPN" />

Existen varias desventajas en los sistemas, entre otros:

- __Seguridad__: el servidor VPN debe tener puertos expuestos a internet para que los clientes puedan conectarse, por lo que la seguridad del acceso a los recursos detrás del servidor quedan a expensas de la configuración de la seguridad del propio servidor VPN. 
- __Latencia__: el tráfico a entre el cliente VPN y los recursos de la red local debe pasar por el servidor VPN, por lo que se convierte en un cuello de botella haciendo que la latencia de las comunicaciones aumente.
- __Restricciones de red__: en muchos casos nos podemos encontrar navegando por internet a través de una red local con restricciones de seguridad que no permitan iniciar una comunicación VPN con el servidor de la red local a la que queremos acceder. Esto es algo habitual en redes como hoteles, aeropuertos etc. 
- __Salida a internet desde un punto__: en caso de que queramos utilizar la red de la VPN para salir a internet (por ejemplo, para navegar como si estuviéramos en nuestro país cuando estamos de viaje), la propia arquitectura de un servidor VPN fijo nos ata a que solo podamos hacerlo a través del dicho servidor VPN (que además deberá estar configurado para rutear todo el tráfico).

# La alternativa, ZeroTier

Desde hacer unos años llevo utilizando la herramienta [ZeroTier](https://www.zerotier.com/) de Software Libre ([casi en su totalidad](https://github.com/zerotier/)) que nos permite crear __redes virtuales P2P sin necesidad de un servidor VPN__. ¿Qué quiere decir esto? Pues que evitamos todas las desventajas de un sistema clásico de VPN ya que los equipos se van a conectar punto a punto a través de internet.

## Ventajas principales

Además, ZeroTier combina la practicidad de una [VPN P2P](https://en.wikipedia.org/wiki/Private_peer-to-peer) con [SD-WAN](https://en.wikipedia.org/wiki/SD-WAN), una virtualización por software de una [WAN](https://en.wikipedia.org/wiki/Wide_area_network) (red de internet, para entendernos) que es muy parecida a una virtualización de [LAN](https://en.wikipedia.org/wiki/Local_area_network) (VLAN, que seguramente te suene más), que no es otra cosa que crear redes privadas segmentadas a través de software. Esto normalmente se utiliza mucho en ámbitos empresariales para que tu proveedor de internet (ISP) te proporcione una “VPN” completamente aislada del resto de navegación de internet, algo como por ejemplo una [MPLS](https://es.wikipedia.org/wiki/Multiprotocol_Label_Switching), para conectar sus sedes remotamente. 

Estas VPNs especiales se basan es una segmentación física por lo que a diferencia de una SD-WAN, dependen de que toda la cadena de equipos hardware entre los puntos remotos sea capaz de trabajar con los protocolos necesarios. Las SD-WAN además permiten utilizar sistemas dinámicos de selección de rutas y a través de cualquier tipo de conexión de red (nos permite conectar dos equipos a través de Cable y/o 3-4-5G y que la selección de la ruta sea automática…)

<img src="/images/posts/zerotier/sdwan.webp" alt="ZeroTier" />

## Cómo funciona ZT

Las ventajas de usar ZeroTier no terminan aquí: el despliegue de un esquema de red sencillo se puede hacer en minutos, la latencia es mínima al ser una conexión mesh punto a punto y sobretodo, funciona prácticamente bajo cualquier configuración de firewall por muy estricta que sea, ya que los clientes de ZeroTier utilizar una técnica llamada [UDP Hole punching](https://en.wikipedia.org/wiki/UDP_hole_punching).

<img src="/images/posts/zerotier/esquema_ZT.png" alt="Esquema ZeroTier"/>

La técnica Hole-Punch se puede resumir de forma sencilla: los clientes que quieres hablar entre ellos buscarán un puerto abierto en salida a través de firewall con una conexión UDP, publicarán su IP local, su IP externa y puerto en un servidor intermedio y este se lo suministrará al otro cliente. Tras ello, ambos equipos tratarán de abrir un canal P2P UDP a través de este puerto. Como en la mayoría de los casos el firewall tendrá la salida UDP habilitada en al menos un puerto, los firewall harán un *accept* y *forward* de los paquetes de entrada. Una vez abierto el tunel UDP, los equipos intercambiaran los paquetes de forma directa entre ellos.

Esta técnica es algo muy habitual por ejemplo en videojuegos online multijugador donde la latencia es muy importante.

También debemos saber que ZeroTier emula la [capa 2](https://en.wikipedia.org/wiki/Data_link_layer) permitiendo [multipath](https://en.wikipedia.org/wiki/Multipath_propagation), [multicast](https://en.wikipedia.org/wiki/Multicast) y [bridging](https://en.wikipedia.org/wiki/Network_bridge). Esto es __muy interesante para los administradores de sistemas__ ya que nos permite montar cualquier cosa que se nos pueda ocurrir sobre ZeroTier por que las conexiones son virtualmente idénticas a las que podríamos tener con un red física entre los dos puntos. Veremos algún ejemplo de caso práctico en entornos IoT industriales donde haremos uso de esto.

## Seguridad

Ante todo, __lo que buscamos es una seguridad de las comunicaciones entre dos o más puntos remotos conectados a internet__. ZeroTier utiliza una encriptación P2P de [256-bit](https://es.wikipedia.org/wiki/Advanced_Encryption_Standard) y el sistema de gestión de redes y los clientes nos permite adaptarlo a los requerimientos específicos de cada red local de cada situación. 

Como hemos visto, las comunicaciones son siempre punto a punto, pero para habilitar esta conexión debe existir un servidor controlador accesible por todos los clientes para que estos puedan publicitar su IP y puerto. La comunicación nunca pasará por este servidor y para simplificar los despliegues, vamos a usar el que ZeroTier nos ofrece de forma gratuita (con limitaciones y no de software libre), aunque existe la posibilidad de [desplegar uno por nosotros mismos autoalojado](https://docs.zerotier.com/self-hosting/network-controllers/). Estos controladores son los que gestionan la virtualización de capa 2. 

Además de esto, en caso de que quieras una comunicación 100% segura sin intermediarios deberías de levantar [servidores Root autoalojados](https://docs.zerotier.com/zerotier/moons/), ya que estos son los que gestionan la virtualización de capa 1 y por tanto, al menos de forma puntual, si hay comunicación que pasa a través de dichos servidores (cifrado punto a punto). [Conviene que leas esto](https://docs.zerotier.com/self-hosting/introduction) si quieres montar una __red empresarial con un grado de exigencia de seguridad muy alta__ (sectores estratégicos) o si __quieres evitar controles gubernamentales en tus comunicaciones__.

# Ejemplos

Para que puedas ver el potencial de esta herramienta, he preparado una serie de ejemplos prácticos que te voy a mostrar en un orden creciente en dificultad. En el primer ejemplo entenderemos los conceptos básicos de ZeroTier con un esquema de configuración muy simple, y después iremos subiendo la complejidad.

## Conectar un equipo tras un firewall y otro a través de 3-4-5G

Este es el ejemplo más sencillo de ejecutar y se puede hacer en 5 minutos. Es la base para entender cómo se utiliza ZeroTier en distintos clientes.

Tenemos dos equipos, el primero es una Raspberry Pi alojada en una red local con una IP privada 192.168.1.228 (obtenida por DHCP) y por otro lado tengo un teléfono Android con una IP pública 88.28.19.250 conectado a internet a través de 5G.

<img src="/images/posts/zerotier/ejemplo_1_esquema_1_inicio.png" alt="Esquema Ejemplo 1 Inicio"/>

Estos dos equipos no se ven entre ellos ya que por un lado, la RPI está bajo un firewall y desconocemos su IP pública, y por otro por que la IP pública del teléfono muy probablemente sea compartida entre varios dispositivos de la misma operadora y es imposible acceder a el remotamente.

Lo primero que debemos hacer en entrar en la [página web de ZeroTier](https://my.zerotier.com/) y hacernos una cuenta gratuita. 

Con la cuenta creada, iremos a nuestro panel de administración y vamos a darle a *Create A Network*. Nos creará automáticamente una red virtual preconfigurada con un nombre aleatorio y un ID. Si entramos a editarlo podremos cambiarle el nombre. En la parte avanzada veremos que nos ha creado ya una ruta/direccionamiento automáticamente y además con un direccionamiento 192.168.191.0/24 que ninguno de nuestros equipos tiene. Si coincidiera con algunos de los dos u otro que ya exista en alguna de nuestras redes locales, podemos seleccionar otro direccionamiento entre los predefinidos o 
hacerlo manualmente.

<img src="/images/posts/zerotier/ejemplo_1_pasos_0.png" alt="Ejemplo 1 paso 0" class="sombra" />

Ahora vamos primero a la consola de la RPI y mediante el siguiente comando vamos a instalar el paquete de ZeroTier One (el cliente de ZeroTier):

```bash
curl -s https://install.zerotier.com | sudo bash
```

<img src="/images/posts/zerotier/ejemplo_1_pasos_1.png" alt="Ejemplo 1 paso 1" class="sombra" />

Cuando termine con la instalación, vamos a darlo de alta en la red que acabamos de crear. Para ello, cogemos el ID de la red y ejecutamos el siguiente comando:

```bash
sudo zerotier-cli join ########
```

<img src="/images/posts/zerotier/ejemplo_1_pasos_2.png" alt="Ejemplo 1 paso 2" class="sombra" />

Nos dirá que *200 Join OK* y ahora vamos de nuevo al panel de control de la red de ZeroTier y en clientes vemos que hay un nuevo cliente esperando a ser autorizado, si lo aceptamos veremos que le asigna una IP del direccionamiento definido, en este caso 192.168.191.249. También podemos ver la IP pública del equipo que hasta ahora desconocíamos. 

<img src="/images/posts/zerotier/ejemplo_1_pasos_3.png" alt="Ejemplo 1 paso 3" class="sombra" />
<img src="/images/posts/zerotier/ejemplo_1_pasos_4.png" alt="Ejemplo 1 paso 4" class="sombra" />

Si volvemos a la consola de RPI y hacemos un *ifconfig*, veremos que tenemos una nueva interfaz con un nombre que empieza por *zt* y con la IP asignada:

<img src="/images/posts/zerotier/ejemplo_1_pasos_5.png" alt="Ejemplo 1 paso 5" class="sombra" />

Ahora vamos a hacer lo mismo pero en el teléfono móvil. Vamos a instalar el cliente ZeroTier One desde [Google Play](https://play.google.com/store/apps/details?id=com.zerotier.one) o la [App Store](https://apps.apple.com/us/app/zerotier-one/id1084101492). 


En configuración habilitamos la opcion *Allow Mobile Data*, después de nuevo en la pantalla principal le damos a *Add Network* e introducimos el ID de la red. Ahora en el panel de control de ZeroTier autorizamos el cliente y veremos que le asigna una nueva IP del direccionamiento, en este caso 192.168.191.241:

<img src="/images/posts/zerotier/ejemplo_1_pasos_6.png" alt="Ejemplo 1 paso 6" class="sombra" />

Bien, vamos a ver si ahora los dos equipos se ven entre ellos. Desde la consola de la RPI vamos a ver si vemos el teléfono haciendo ping a la IP de ZeroTier:

<img src="/images/posts/zerotier/ejemplo_1_pasos_7.png" alt="Ejemplo 1 paso 7" class="sombra" />

Efectivamente ahora si llegamos al terminal. Para probar lo contrario, en la RPI tengo un servicio http accesible en el puerto 8080, vamos a ver si accedemos:

<img src="/images/posts/zerotier/ejemplo_1_pasos_8.jpg" alt="Ejemplo 1 paso 8" class="sombra" />

Efectivamente desde el terminal ya puedo acceder a la RPI de forma remota. En este momento y con esta configuración, solo puedo ver la RPI por su interfaz de ZeroTier, por lo que es imposible que el terminal vea ningún otro dispositivo en la red local de la RPI.

<img src="/images/posts/zerotier/ejemplo_1_esquema_1_fin.png" alt="Ejemplo 1 esquema fin" />

## Conexión remota a un equipo sin internet con un NAT

Ahora vamos a tratar de conseguir __acceder a un equipo que no tiene salida a internet__. Para ello voy a utilizar una RPI que configuraremos como [Gateway NAT](https://en.wikipedia.org/wiki/Network_address_translation) de forma que nos permita ver el equipo en una red aislada desde internet con ZeroTier.

El esquema es el siguiente: por un lado, tenemos un PC conectado a internet a través de un terminal móvil que comparte su conexión 5G a través de WiFi ([tethering](https://es.wikipedia.org/wiki/Tethering)), y por otro lado tenemos una RPI conectada por WiFi a una red local y mediante cable punto a punto un PC aislado a internet. El equipo conectado al terminal tiene una IP asiganda por DHCP de red local 192.168.238.214:

<img src="/images/posts/zerotier/ejemplo_2_esquema.png" alt="Ejemplo 2 esquema" />

Si nos conectamos desde la RPI al el equipo aislado por SSH, vemos tiene una IP fija 192.168.191.20 y no tiene salida a internet:

<img src="/images/posts/zerotier/ejemplo_2_pasos_1.png" alt="Ejemplo 2 paso 1" class="sombra" />

La RPI tiene una IP por la interfaz Wifi 192.168.1.196 además vemos que si tiene salida a internet:

<img src="/images/posts/zerotier/ejemplo_2_pasos_2.png" alt="Ejemplo 2 paso 2" class="sombra" />

Lo primero como hemos visto en el anterior paso es __crear la red__. Para poder ejecutar el NAT tal y como queremos, lo que vamos a hacer es configurar un direccionamiento con el rango diferente al 192.168.191.0/24 que es el direccionamiento de la del equipo aislado, en este caso por ejemplo puede ser el 10.147.17.0/24. Después, siguiendo los mismos pasos del ejemplo anterior, debemos instalar el cliente ZeroTier en la RPI, unirlo a la red y autorizarlo. Vemos que Zerotier le asigna la IP 10.147.17.112:

<img src="/images/posts/zerotier/ejemplo_2_pasos_3.png" alt="Ejemplo 2 paso 3" class="sombra" />

En segundo lugar vamos a descargar el [cliente ZeroTier para Windows](https://www.zerotier.com/download/), lo instalamos en el equipo 1 y lo añadimos también a la red. En este caso le asigna la IP 10.147.17.98:

<img src="/images/posts/zerotier/ejemplo_2_pasos_4.png" alt="Ejemplo 2 paso 4" class="sombra" />

Si probamos a hacer ping desde el equipo 1 (10.147.17.98) a la RPI (10.147.17.112) por ZeroTier vemos que llegamos, pero es imposible llegar a la IP del equipo aislado (192.168.191.20) ya que para el equipo 1, no existe:

<img src="/images/posts/zerotier/ejemplo_2_ping_equipo_1_rpi.png" alt="ejemplo 2 ping equipo 1 rpi" class="sombra" />

Para poder efectivamente llegar al equipo aislado tenemos que habilitar el NAT en la RPI. Para ello, nos conectamos por SSH al mismo y tendremos que hacer unos pocos pasos. Lo primero es hacer un ifconfig para ver las interfaces disponibles:

<img src="/images/posts/zerotier/ejemplo_2_pasos_5.png" alt="Ejemplo 2 paso 5" class="sombra" />

Como vemos, tenemos una interfaz física llamada *eth0* conectada al equipo aislado, y otra *ztfp6prrtz* (en tu caso el nombre será diferente pero empezando también por *zt*) de ZeroTier.

Conociendo esto el primer paso es habilitar el *IP forwarding* y crear unas variables de entorno llamadas *PHY_IFACE* (la interfaz física por la que queremos conectarnos al equipo aislado, en nuestro caso el ETH0) y *ZT_IFACE*, el nombre de la interfaz ZeroTier que se nos haya creado:

```bash
sudo sysctl -w net.ipv4.ip_forward=1
PHY_IFACE=eth0; ZT_IFACE=ztfp6prrtz
```

<img src="/images/posts/zerotier/ejemplo_2_pasos_6.png" alt="Ejemplo 2 paso 6" class="sombra" />


En segundo lugar, vamos a instalar *iptables* e *iptables-persistent* en la RPI:

```bash
sudo apt-get update
sudo apt install iptables iptables-persistent -y
```

Nos preguntará si queremos guardar las reglas actuales y le decimos que si tanto en IPv4 como en IPv6. Una vez instalados, vamos a añadir las reglas correspondientes a *iptables* para que el equipo permita pasar paquetes desde la interfaz ZT a la PHY:

```bash
sudo iptables -t nat -A POSTROUTING -o $PHY_IFACE -j MASQUERADE
sudo iptables -A FORWARD -i $PHY_IFACE -o $ZT_IFACE -m state --state RELATED,ESTABLISHED -j ACCEPT
sudo iptables -A FORWARD -i $ZT_IFACE -o $PHY_IFACE -j ACCEPT
```

<img src="/images/posts/zerotier/ejemplo_2_pasos_7.png" alt="Ejemplo 2 paso 7" class="sombra" />

Ahora, volvemos a nuestro panel de control de ZeroTier y en la parte superior, en el apartado *Advanced → Maganed Routes* vamos a añadir la ruta al direccionamiento aislado (192.168.191.0/24) desde la IP de ZeroTier de la RPI (10.147.17.112):

<img src="/images/posts/zerotier/ejemplo_2_pasos_8.png" alt="Ejemplo 2 paso 8" class="sombra" />

Ahora ya podemos probar a hacer ping o conectarnos por SSH a la IP del equipo aislado (192.168.191.20) desde el equipo 1: 

<img src="/images/posts/zerotier/ejemplo_2_ping_ssh_equipo_1_rpi_aislado.png" alt="ejemplo 2 ping shh equipo 1 rpi aislado" class="sombra" />

Finalmente, hacemos lo siguiente para que las reglas de *iptables* se mantengan si reiniciamos la RPI:

```bash
sudo su
bash -c iptables-save > /etc/iptables/rules.v4
exit
```

Con esto vemos la aplicabilidad de una NAT a entornos donde tengamos equipos aislados, como suele ser habitual por ejemplo en la industria. Con esta configuración podemos acceder a equipos remotos que no tengan salida a internet de forma segura, como PLCs, HMIs etc.

## Acceso remoto a redes bajo un firewall OpenWRT con bridge y salida a internet

En este último ejemplo vamos utilizar ZeroTier como una VPN más clásica ya que el cliente será instalado directamente en un Router/Firewall [OpenWRT](https://openwrt.org/start) ([que ya deberías tener si seguiste mis pasos en este post](https://firstcommit.dev/2021/04/11/infraestructura/)). Esto es un poco más complejo que ejemplo anterior pero nos permitirá gestionarlo como una interfaz brigde que a diferencia del NAT, nos permitirá levantar cualquier tipo de conexión ya que se tratará de un bridge de capa 2.

<img src="/images/posts/zerotier/ejemplo_3.png" alt="ejemplo 3 esquema"  />

El primer paso es crear una red en ZeroTier, con un rango específico tipo:

<img src="/images/posts/zerotier/ejemplo_3_pasos_1.png" alt="Ejemplo 3 paso 1" class="sombra" />

Además, vamos a crear las rutas de esta forma:

<img src="/images/posts/zerotier/ejemplo_3_pasos_2.png" alt="Ejemplo 3 paso 2" class="sombra" />

Antes de empezar a tocar el router, vamos a hacer un backup de la configuración actual de tu firewall. Para ello vamos a *System → Backup / Flash firmware* y hacemos un *backup*:

<img src="/images/posts/zerotier/ejemplo_3_pasos_3.png" alt="Ejemplo 3 paso 3" class="sombra" />

Una vez hecho esto, vamos a instalar ZeroTier en OpenWRT, desde *System → Software* (Si no te sale el listado de paquetes disponibles deberás hacer un *Update list*):

<img src="/images/posts/zerotier/ejemplo_3_pasos_4.png" alt="Ejemplo 3 paso 4" class="sombra" />

Al finalizar la instalación vamos a conectarnos por SSH al router y hacemos para ver que está instalado correctamente:

```bash
uci show zerotier
```

<img src="/images/posts/zerotier/ejemplo_3_pasos_5.png" alt="Ejemplo 3 paso 5" class="sombra" />

Ahora vamos a definir una nueva network zerotier en OpenWRT, hacer join a la red ZT que hayas creado como en el paso 1 y habilitarlo:

```bash
uci set zerotier.openwrt_network=zerotier
uci add_list zerotier.openwrt_network.join='La_ID_de_la_red_zerotier'
uci set zerotier.openwrt_network.enabled='1'
uci commit zerotier
```

Tras ello __es necesario reiniciar el router para que tenga efecto__.

```bash
reboot
```


Al reiniciar, en el panel de control de ZeroTier ya nos aparecerá el router, lo tenemos que aceptar, y en su confguración definimos una IP fija y seleccionamos que *Allow Ethernet Bridging* y *Do Not Auto-Assing IPs* ya que no queremos que nos cambie la IP asignada:

<img src="/images/posts/zerotier/ejemplo_3_pasos_6.png" alt="Ejemplo 3 paso 6" class="sombra" />

En la consola del router podemos ver la red conectada y online con:

```bash
zerotier-cli info
```

Ahora, en la web del router vamos a *Network → intefaces* y creamos una nueva *interface* de tipo *unmanaged* usando el *Device* que empiece con zt:

<img src="/images/posts/zerotier/ejemplo_3_pasos_7.png" alt="Ejemplo 3 paso 7" class="sombra" />

Tras eso, vamos a *Network → firewall* y en la parte de abajo creamos una nueva zona de esta forma:

<img src="/images/posts/zerotier/ejemplo_3_pasos_8.png" alt="Ejemplo 3 paso 8" class="sombra" />


Si solo queremos poder acceder a la red local, en destination zones nos vale con poner la *lan*. Si ponemos la *wan* como he hecho yo, los equipos conectados a ZeroTier saldrán a internet a través del router. Podemos si queremos, crear dos redes ZeroTier diferenciadas, una solo con acceso a la red local y otra para salir a internet por ejemplo, siguiendo los mismos pasos de este ejemplo. En cualquier caso, en los clientes también tendremos que configurar la opción de *Route all traffic through ZeroTier* para que la salida a internet sea a través del router.

Guardamos los cambios y aplicamos. Ahora ya tenemos lista una red donde los equipos ZeroTier se ven entre ellos con la red 192.168.2.0/24 y todos ellos también pueden ver los equipos de la red local 192.168.1.0/24.

---

Espero que estos ejemplos puedan servirte como introducción a esta herramienta tan potente y versátil como es ZeroTier. Si tienes cualquier duda no dudes en dejarlo en los comentarios y si te gusta el contenido, suscríbete que es gratis.

---
<a href="https://t.me/firstcommit" class="btn btn-primary text-white py-2">Suscríbete, que es gratis</a>


*Nota: algunos de los enlaces a productos o servicios pueden ser enlaces referidos con los que podemos obtener una comisión de venta.*


<style> 
img.sombra {
  display: block;  
  margin-left: auto;  
  margin-right: auto; 
  box-shadow: 5px 5px 8px grey;
}
img.sombra-small {
  display: block;  
  margin-left: auto;  
  margin-right: auto; 
  box-shadow: 5px 5px 8px grey;
  max-width: 20%;
}
img.sombra-inline {
  display: inline-block; 
  vertical-align: middle; 
  margin-right: 0px; 
  box-shadow: 3px 3px 8px grey;
}
img.zoom {
  transition: transform .2s;
}
img.zoom:hover {
  -ms-transform: scale(1.2); /* IE 9 */
  -webkit-transform: scale(1.2); /* Safari 3-8 */
  transform: scale(1.2); 
}
</style>
