---
title: "Eficiencia energética DIY con software libre, primera parte: consumos eléctricos "
authors: [yayitazale]
date: 2022-12-08T21:00:00Z
image: /images/posts/electricidad/intro.jpg
categories:
  - diy
tags:
  - guia
  - diy
  - energy
  - monitoring
  - efficiency
  - mqtt
  - zigbee
  - domotica
  - home assistant
  - selfhosted
  - node-red

---
En este post, vamos a hablar sobre cómo monitorizar y controlar los __consumos eléctricos__ en un hogar utilizando dispositivos y sistemas de automatización doméstica. En un momento en que el precio de la energía está aumentando a nivel mundial, es más importante que nunca asegurarnos de que nuestro hogar está siendo lo más eficiente posible en términos de consumo de energía. En este sentido, en este post __profundizaremos en el uso de luces, enchufes inteligentes y medidores de energía Zigbee__ para monitorizar y controlar el consumo de energía eléctrica. Descubriremos cómo funcionan estos dispositivos, cómo se pueden utilizar para obtener datos precisos sobre el consumo de energía y cómo se pueden integrar en nuestro sistema domótico para __mejorar la eficiencia energética en el hogar__. Además, para los usuarios con tarifas eléctricas con discriminación horaria, integraremos un sistema para conocer en que tarifación nos encontramos y así poder __visualizar de forma sencilla cómo utilizamos la energía durante el día__.

Los pre-requisitos para poder seguir los pasos de este post serán tener ya en marcha una instancia de [Home Asisstant](https://firstcommit.dev/2021/03/11/Hass/), un [broker MQTT](https://firstcommit.dev/2021/03/14/mqtt/) y también tener una [red Zigbee](https://firstcommit.dev/2021/03/19/zigbee/) con una pasarela [Zigbee2MQTT](https://firstcommit.dev/2021/03/19/zigbee/#Zigbee2MQTT). Además, aunque no sea estrictamente necesario, puede ser de gran ayuda disponer de un [servidor de NodeRed](https://firstcommit.dev/2021/07/14/plantas/#Node-Red-la-aplicacion-definitiva-para-automatizaciones) que nos puede servir para crear medidores virtuales de energía y más adelante, automatizar encendidos y apagados de los dispositivos. Todo esto lo tenemos montado sobre el [sistema operativo Unraid](https://firstcommit.dev/2021/01/31/Unraid/), pero puedes hacerlo sobre una [raspberry Pi con HassOS](https://www.home-assistant.io/installation/raspberrypi) o cualquier equipo con otro sistema operativo.

(Foto de portada <a href="https://unsplash.com/es/@appolinary_kalashnikova?utm_content=creditCopyText&utm_medium=referral&utm_source=unsplash">Appolinary Kalashnikova</a> en <a href="https://unsplash.com/es/fotos/aerogenerador-rodeado-de-hierba-WYGhTLym344?utm_content=creditCopyText&utm_medium=referral&utm_source=unsplash">Unsplash</a>
  )

#### TOC

# Potencia vs. energía

Lo primero que quiero explicar es la __diferencia entre la potencia eléctrica y la energía eléctrica consumida__. Como es un tema tratado ya miles de veces por muchísima gente, os dejo este vídeo de youtube en el que lo explica de forma bastante sencilla.

<iframe width="560" height="315" src="https://www.youtube.com/embed/jn9G6yBrlvE" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Básicamente, lo que debemos tener en cuenta es:

- __Potencia eléctrica__: cuanta electricidad estamos consumiendo en un instante (W o kW)
- __Energía eléctrica__: cuanta potencia estamos utilizando durante un tiempo, normalmente, una hora (Wh o kWh)

Los datos de medición directa de dispositivos como enchufes inteligente o pinzas amperimétricas normalmente nos darán el dato de potencia, que tendremos que ir registrado cada instante para, una vez pasada una hora, podamos calcular la energía consumida que es por lo que vamos a pagar en nuestra factura de la luz.

Sin embargo, si solicitamos datos de nuestro consumos a nuestro distribuidor o terceros a través de Internet, nos darán siempre datos de energía consumida por hora.

# Medición de consumo general

Para empezar con el post, lo primero que vamos a ver cómo podemos medir el consumo general o total de la vivienda. Evidentemente todo dependerá de a que nivel de detalle queramos llegar y que presupuesto tengamos, pero lo ideal para poder hacer un análisis general de nuestros hábitos de consumo es tener una medición global del consumo por un lado, y una medición de lo aparatos de mayor consumo por otro. 

Para obtener los datos de consumo total de vivienda, tenemos dos opciones. La primera, sin coste alguno, depende de si nuestro distribuidora eléctrica es Iberdrola Distribución u otra.

Para saber cual es tu distribuidora de electricidad, este es el mapa actual en España:

<img src="/images/posts/electricidad/mapa.webp" alt="mapa distribuidoras electricas españa" class="sombra" />

A grandes rasgos, el reparto es el siguiente:

- __I-DE__: Navarra, País Vasco, La Rioja, Comunidad Valenciana, Murcia, Madrid, Castilla y León, Extremadura, Castilla la Mancha.
- __E-Distribución__: Aragón, Cataluña, Baleares, Canarias, Andalucía, Extremadura.
- __Unión Fenosa Distribución__: Galicia, Castilla y León, Madrid, Castilla la Mancha
- __E-REDES__: Asturias
- __Viesgo Distribución__: Cantabria, Asturias, Galicia, Castilla y León.

Además, existen más de 300 pequeñas distribuidoras locales, que son propietarias y gestoras de redes de distribución de algunos municipios y comarcas.

Si tienes dudas, puedes consultar la última factura de luz que tengas a mano, busca tu código CUPS, con la parte inicial de la numeración podremos saber si nuestra distribuidora es I-DE u otra:

- __ES0021__: I-DE Distribución

## I-DE Distribución

En caso de que nuestra distribuidora sea I-DE, tendremos que [hacernos una cuenta en su web con perfil avanzado](https://www.i-de.es/consumidores/web/guest/register?start=false), para lo que necesitaremos el CUPS y nos pedirán identificarnos con una copia de nuestro DNI para verificar que la línea a la que queremos acceder es la nuestra. Una vez nos confirmen que estamos dados de alta (puede tardar un par de días), podremos configurar el módulo de lectura de datos de I-DE en Home Asisstant.

<a href="https://www.i-de.es/consumidores/web/guest/register?start=false"><img src="/images/posts/electricidad/webide.png" alt="web ide iberdrola" class="zoom" /></a>

Para ello, vamos al apartado [HACS que ya tenemos instalado](https://firstcommit.dev/2021/04/05/home-automation/#HACS-y-Themes), el cual nos permite instalar paquetes de terceros. Dentro de HACS, vamos a *integraciones*, arriba a la derecha hacemos clic en los tres puntos y vamos a *Repositorios personalizados*. Añadimos la URL del [proyecto de github](https://github.com/ldotlopez/ha-ideenergy) en el campo repositorio y como tipo, ponemos integración:

<img src="/images/posts/electricidad/hacs.png" alt="instalación plugin ide 1" class="sombra" />
<img src="/images/posts/electricidad/hacs2.png" alt="instalación plugin ide 2" class="sombra" />

Una vez instalado vamos de nuevo a integración y hacemos clic en *explorar y añadir repositorios* y buscamos por *I-de Energy Monitor*, hacemos clic y lo instalamos:

<img src="/images/posts/electricidad/hacs3.png" alt="instalación plugin ide 3" class="sombra" />
<img src="/images/posts/electricidad/hacs4.png" alt="instalación plugin ide 4" class="sombra" />
<img src="/images/posts/electricidad/hacs5.png" alt="instalación plugin ide 5" class="sombra" />

Una vez instalado tendremos que reiniciar nuestro Home Asisstant para que se apliquen los cambios y podamos usar esta integración. Una vez reiniciado, podremos ir a *Ajustes* > *Dispositivos y servicios* > *Añadir integración* y buscamos de nuevo por *I-de Energy Monitor*:

<img src="/images/posts/electricidad/hacs6.png" alt="instalación plugin ide 6" class="sombra" />

Al añadir la integración nos pedirá nuestro usuario y contraseña de la cuenta de I-DE Distribución, y una vez conectados nos sacara la lista de CUPS que tengamos contratados. En caso de que tengamos más de un contrato, podremos añadir más de un contrato al mismo Home Asisstant si queremos tenerlo todo en el mismo sitio:

<img src="/images/posts/electricidad/hacs7.png" alt="instalación plugin ide 7" class="sombra" />

Nos generará un dispositivo con 4 sensores: 

- Consumo acumulado de energía
- Consumos históricos
- Generación histórica (para los que tengan autogeneracion solar etc)
- Demanda de potencia histórica

Los datos __tardarán un rato en llenarse__, pero una vez rellenados los datos de consumos horario llegarán más o menos de forma puntual cada hora. De todas formas he de comentar que la estabilidad del sistema no es muy alta ya que cada hora se realizará una llamada a tu contador inteligente por LTE y en ocasiones este no estará disponible, por lo que en la siguiente hora llegará un consumo acumulado por las dos horas y nos dejará huecos en las gráficas. Si quieres saber más sobre la integración, puedes pasar por la [página del proyecto en github](https://github.com/ldotlopez/ha-ideenergy) (no te olvides de darle una estrella para agradecer su trabajo).

## Resto de distribuidoras

Para el resto de distribuidoras existe la opción de utilizar la integración con [Datadis](https://datadis.es/), que es un servicio gratuito y centralizado puesto en marcha por la [Asociación de Empresas Eléctricas](https://www.aseme.org/) que incluye la mayoría de distribuidoras eléctricas de España. La única pega de este servicio gratuito es que __los datos de consumos se recogerán de forma diaria__, por lo que solo nos servirán para analizar datos del pasado pero no para controlar los consumos horarios ni instantáneos.

Eso si, a diferencia de la integración de I-DE Distribución, esta integración si nos ofrece los datos de consumo ya separados en caso de que tengamos una tarifa con discriminación horaria. Lo primero es [crearnos una cuenta identificandonos con nuesto DNI](https://datadis.es/registry):

<a href="https://datadis.es/registry"><img src="/images/posts/electricidad/datadis.png" alt="web datadis" class="sombra" /></a>

Para instalarlo los pasos son muy parecidos. A la hora de instalar el repositorio personalizado en HACS, debemos añadir [este link](https://github.com/uvejota/homeassistant-edata):

<img src="/images/posts/electricidad/hacs8.png" alt="instalación plugin datadis 1" class="sombra" />

Una vez instalado vamos de nuevo a integración y hacemos clic en *explorar y añadir repositorios* y buscamos por *e-data*, hacemo clic y lo instalamos:

<img src="/images/posts/electricidad/hacs9.png" alt="instalación plugin datadis 2" class="sombra" />

Una vez instalado tendremos que reiniciar nuestro Home Asisstant para que se apliquen los cambios y podamos usar esta integración. Una vez reiniciado, podremos ir a *Ajustes* > *Dispositivos y servicios* > *Añadir integración* y buscamos de nuevo por *e-data*: 

<img src="/images/posts/electricidad/hacs10.png" alt="instalación plugin datadis 3" class="sombra" />

En este caso nos pedirá el usuario, contraseña y el CUPS para poder leer los datos:

<img src="/images/posts/electricidad/hacs11.png" alt="instalación plugin datadis 3" class="sombra" />

Esto nos generará un sensor con un motón de propiedades que podremos usar después para graficar nuestras curvas de consumo:

<img src="/images/posts/electricidad/hacs12.png" alt="instalación plugin datadis 4" class="sombra" />

Si quieres sabes un poco más en profundidad cómo funciona cada propiedad y cómo podemos configurar el sensor si tenemos PVPC etc, te recomiendo que [leas la documentación del proyecto en github](https://github.com/uvejota/homeassistant-edata) (y ya que estás déjale una estrella por el trabajazo altruista que están haciendo).

## Medidor en cuadro eléctrico

Si lo que queremos es tener una medición en tiempo real de la potencia instantánea consumida en casa y poder ver datos horarios sin retrasos ni perdidas de datos, te aconsejo que instales un dispositivo de medida en tu cuadro eléctrico. 

__OJO: si no sabes lo que haces corres el riesgo de morir electrocutado, crear un incendio etc. por lo que si no estas seguro de poder instalar un medidor por tu cuenta, habla con tu electricista que seguramente te lo haga por un precio muy razonable y de forma 100% segura. No me hago responsable de cualquier accidente que pudiera ocurrir.__

En mi caso, tengo un cuadro eléctrico con algo de espacio por lo que la instalación es sencilla. Hay varios [aparatos de medida disponibles compatibles con el protocolo Zigbee](https://www.zigbee2mqtt.io/supported-devices/#s=DIN&e=power), pero el que yo os recomiendo es [este de Legrand](https://amzn.to/3uBPrbi) que es el tengo instalado hace ya meses y funciona muy bien por el precio que tiene además de cumplir con las directivas de baja tensión, CE y CEM:

<a href="https://amzn.to/3uBPrbi"><img src="/images/posts/electricidad/legrand.jpg" alt="medidor legran" class="zoom" /></a>

Los medidores de [pinza amperimétrica](https://es.wikipedia.org/wiki/Pinza_amperim%C3%A9trica) como este son muy precisos y la instalación es muy segura ya que el aparato se alimenta por debajo de alguno de los magnetotermicos automáticos es de casa y la pinza se instala en el cable de fase que va desde el automático general al distribuidor de los automáticos zonales de la vivienda con una separación física. La medición se hace de forma indirecta utilizando [los principios del electromagnetismo de Lenz-Maxwell-Faraday](https://www.youtube.com/watch?v=xxZenoBs2Pg).

__OJO: puede que la instalación de tu vivienda sea diferente por lo que tendrías que ver si este medidor es el indicado. Como he dicho, si tienes dudas puedes hablar con tu electricista y pasarle el listado de los aparatos compatibles por Zigbee para que te diga cual de ellos se adapta mejor a tu instalación.__

Las especificaciones de este medidor son los siguientes:

- Potencia máxima medible: 80A o lo que es lo mismo en España unos 18kW. Normalmente en la mayoría de viviendas (sin suelo radiante eléctrico) la acometida estará dimensionada para 5 o 7kW por lo que vamos sobrados. En viviendas con suelo radiante eléctrico suele haber una instalación trifásica de 15kWh la cual complica bastante la medición (no voy a entrar a explicarlo en este post, si particularmente tienes interés escribe un comentario y trataré de ayudarte). 
- Diámetro máximo del cable a medir (hueco de la pinza): Ø10.2 mm

Este es el [esquema de instalación e instrucciones](https://assets.legrand.com/pim/NP-FT-GT/LE12267AE.pdf) (solo válido en instalaciones monofásicas):

<img src="/images/posts/electricidad/medidor1.png" alt="instalación medidor 1" class="sombra" />

Para poder acometer la instalación __es imprescindible que cortes el suministro general de la vivienda bajando el automático general__. Una vez bajado el general podemos proceder a quitar la tapa de plástico para acceder al cuadro. Lo primero que vamos a hacer es comprobar con un polímetro que no hay voltaje entre la fase y el neutro a la salida del automático general. 

<img src="/images/posts/electricidad/cuadro1.jpg" alt="cuadro electrico 1" class="sombra" />

Una vez estemos seguros que no hay corriente, podemos colocar la parte del medidor que encaja en el carril DIN simplemente empujándolo hacia atrás hasta que haga clic. Para conectarlo al automático correspondiente (en mi caso, el automático de enchufes generales) usaremos dos cables de sección 1.5mm² con los cuales conectaremos la bornas del adaptador (fase: cable marrón, gris o negro; neutro: cable azul) a la salida del automático.

<img src="/images/posts/electricidad/medidor2.png" alt="instalación medidor 2" class="sombra" />

Después, tendremos que enchufar el cable que viene de la pinza aperimétrica en la parte superior del aparato. Ahora, soltaremos el cable de fase del automático general, lo pasamos por dentro de la pinza (ojo a la flecha de dirección, debe mirar hacia donde va la energía) y lo volvemos a atornillar al automático general. 

<img src="/images/posts/electricidad/cuadro2.jpg" alt="instalación medidor 2" class="sombra" />
<img src="/images/posts/electricidad/cuadro3.jpg" alt="instalación medidor 3" class="sombra" />

Finalmente toca revisar que todo el cableado está bien sujeto y la instalación la hemos hecho correctamente, repasando que los cables están bien colocados y en su orden correspondiente.

Ahora podemos levantar el automático general. Si tenemos todo correcto el aparato se encenderá y parpadeará en azul.

<img src="/images/posts/electricidad/cuadro4.jpg" alt="instalación medidor 4" class="sombra" />

Como [ya vimos en su día](https://firstcommit.dev/2021/03/19/zigbee/#Emparejar-dispositivos), para añadir el equipo a Zigbee2MQTT, abrimos el portal en el navegador y permitimos unirse a nuevos aparatos. 

<img src="/images/posts/electricidad/medidor3.png" alt="instalación medidor 3" class="sombra" />

En unos segundos nos aparecerá el equipo y podremos cambiarle el nombre al que queramos que tenga en Home Asisstant:

<img src="/images/posts/electricidad/medidor4.png" alt="instalación medidor 4" class="sombra" />

Una vez añadido nos ofrecerá una dato de potencia eléctrica instantánea cada segundo que tendremos que transformar a consumo de energía en Home Asisstant.


# Contadores virtuales con discriminación horaria

En caso de que nos hayamos decantado por Datadis como fuente de datos de consumos generales, este paso no necesario ya que los datos ya nos llegarán separados según la discriminación horaria en los diferentes atributos del sensor. Sin embargo, si hemos colocado un medidor de potencia instantánea, necesitaremos convertir los datos de potencia en energía horaria y a su vez, separar estos datos de energía por los tramos tarifarios. Lo mismo ocurre con los datos de I-DE, que aunque sean datos de energía consumida, nos interesa separarlos para tener el detalle por tarifación.

Para llevar a cabo esta tarea vamos a echar mano de otra integración disponible en Home Assistant, que es el de __PVPC__. Si, puede que tu no tengas PVPC, pero en el 90% de los casos, tu tarifación seguirá el mismo patrón (será una [2.0TD](https://www.energigreen.com/tarifas-electricidad/horario-tarifa-2-0td/)) por lo que podremos utilizar los sensores que genera esta integración para saber cuándo estamos en el periodo pico, cuándo en el periodo valle y cuando en el super-valle. Además, con el 2.0TD podremos tener contratados dos potencias distintas en los periodos valle y pico, algo muy útil para los que disponemos de soluciones de movilidad de propulsión eléctrica, y podremos automatizar la carga de estos en los periodos de menor coste y mayor potencia disponible.

Si tu tarifación es diferente a una 2.0TD, siempre puedes crear un flujo en Node-Red para definir un sensor que cambie según los horarios de tu contrato. Si tienes dudas de cómo hacerlo, déjamelo en los comentarios y te ayudaré a crear el flujo.

Si además estás con un contrato de PVPC, la integración nos ofrecerá el precio por hora de cada día para que puedas automatizar por ejemplo el encendido del lavavajillas o la lavadora a las horas de menor precio.

Para instalar la integración de PVPC en este caso no es necesario hacerlo a través de HACS ya que es un [módulo oficial de Home Assistant](https://www.energigreen.com/tarifas-electricidad/horario-tarifa-2-0td/). Lo único que tenemos que hacer es ir a *Ajustes* > *Dispositivos y servicios* > *Añadir integración* y buscamos de nuevo por *PVPC*:

<img src="/images/posts/electricidad/pvpc.png" alt="Instalacion plugin pvpc 1" class="sombra" />

Al configurarlo nos pedirá definir qué tarifa tenemos y qué potencias tenemos contratadas en pico y en valle:

<img src="/images/posts/electricidad/pvpc2.png" alt="Instalacion plugin pvpc 2" class="sombra" />

Una vez configurado nos creará un sensor con un montón de propiedades, entre los cuales nos interesa el de *Period*:

<img src="/images/posts/electricidad/pvpc3.png" alt="Instalacion plugin pvpc 3" class="sombra" />

Con esta integración ya podemos pasar a configurar medidores virtuales en base al periodo y así podre segregar los consumos por periodos.

Lo primero que vamos a hacer es pasar la medición potencia instantánea en kW a un dato de energía consumida en kWh. Para ello vamos a utilizar una función matemática llamada [Suma de Riemann](https://es.wikipedia.org/wiki/Suma_de_Riemann) que básicamente consiste en separar el área que se genera si juntamos todos los valores de potencia que nos llegan cada segundo en pequeños elementos de forma geométrica más simple y así hacer un calculo aproximado más simple que una integral de la curva. 

<img src="https://upload.wikimedia.org/wikipedia/commons/2/2a/Riemann_sum_convergence.png" alt="Suma de Riemann 1" class="sombra" />

La forma geométrica puede ser diversa pero en nuestro caso vamos a usar la trapezoidal ya que es la más precisa para cuando los cambios entre instantes no son muy grandes:

<img src="https://upload.wikimedia.org/wikipedia/commons/7/76/TrapRiemann2.svg" alt="Suma de Riemann trapezoidal" class="sombra" />

Para poner en marcha esto [Home Assistant ya tiene esta función implementada](https://www.home-assistant.io/integrations/integration/), así que vamos a *Ajustes* > *Dispositivos y servicios*, arriba seleccionamos *Ayudantes* y después seleccionamos *Integración – Sensor de suma integral de Riemann*:

<img src="/images/posts/electricidad/ayudante.png" alt="Creacion ayudante suma riemann 1" class="sombra" />

Aquí vamos a configurar el nuevo sensor virtual de consumo energético con los siguientes datos:

- Nombre: nombre que le vamos a dar al sensor, en mi caso *Casa*.
- Sensor de entrada: el sensor de potencia instantánea que hemos instalado
- Método de integración: Trapezoidal Rule
- Precisión: ya que el sensor nos da datos en Vatios (W) vamos a poner 2 y así tendremos el valor más preciso posible al pasarlo a kWh
- Prefijo métrico: como he dicho, el valor de potencia viene en W y nosotros queremos la energía en kWh, por lo que ponemos como prefijo el k (kilo)
- Unidad de tiempo: de nuevo, si queremos pasar de W a kWh, añadimos como medida de tiempo la hora, para que el valor sea calculado con esa base

<img src="/images/posts/electricidad/ayudante2.png" alt="Creacion ayudante suma riemann 2" class="sombra" />

Una vez creado este sensor de consumo de energía, tendríamos ya una situación igual a si hemos optado por integrar los datos de I-DE. 

Ahora vamos a crear un __Contador virtual__, al que le definiremos las tarifas de consumo posibles para segregar el consumo registrado en el sensor de consumo virtual que acabamos de crear en tres contadores diferentes. Para ello, en la misma ventada de ayudantes, creamos uno nuevo llamado *Contador*:

<img src="/images/posts/electricidad/contador1.png" alt="Creacion ayudante contador 1" class="sombra" />

Y pondremos estas opciones:

- Nombre: Nombre que le daremos al contador
- Sensor de entrada: el sensor de consumo de energía que acabamos de crear
- Ciclo de reinicio: esto ya es a gustos, la idea es que el contador vuelva a 0 cada ciclo. Lo normal es que si tenemos un ciclo de facturación por parte de nuestro comercializadora de electricidad concreto como en mi caso, mensual, pongamos el reinicio con el mismo ciclo para poder sacar un valor equivalente y poder comparar si la factura coincide con nuestras mediciones. Si seleccionamos mensual, podemos mover el inicio del ciclo hacia delante en el tiempo a un día concreto del mes sumando del día 1 los días que queramos desplazarlo.
- Tarifas soportadas: aquí es donde vamos a poner las tres tarifas de nuestro 2.0TD. Ponemos P1 y hacemos clic sobre la barra que nos aparece abajo, luego P2 y luego P3.
- Consumo neto: en nuestro caso el sensor virtual que hemos creado o el sensor de I-DE siempre será un sumatorio, por lo que lo dejamos sin marcar. 
- Valores delta: en este caso lo dejamos también sin marcar ya que tanto el sensor virtual con el I-DE nos proporciona siempre un valor sumatorio, no la diferencia entre consumos de una hora a la siguiente.

<img src="/images/posts/electricidad/contador2.png" alt="Creacion ayudante contador 2" class="sombra" />

Al crear el contador virtual veremos como no solo nos crea uno general, sino que nos ha creado tres contadores adicionales, uno para cada periodo de tarifación que hemos definido:

<img src="/images/posts/electricidad/contador3.png" alt="Creacion ayudante contador 3" class="sombra" />

Si vamos a *herramientas de desarrollador* y estados filtramos por el nombre que hayamos puesto al contador, vemos que nos ha generado además de los tres contadores por periodo un elemento llamado *select.contador*. Este es el elemento que utilizaremos para decirle al contador en cada momento en que periodo estamos. Si te fijas, la lista de opciones permitidas son los tres periodos tarifarios que hemos definido:

<img src="/images/posts/electricidad/contador4.png" alt="Creacion ayudante contador 4" class="sombra" />

Para hacer esta selección tendremos que crear una automatización bien en node-red o bien en Home Assistant que cambie el periodo en el selector según el periodo del atributo del sensor PVPC. Es algo sencillo pero por suerte para ti he creado un blueprint y un flujo de nodered que ya lo hacen.

Para importar el blueprint en Home Asisstant vamos a *Ajustes* > *automatizaciones y escenas* > *planos* y le damos a importar plano, en el bocadillo que nos sale metemos [este enlace](https://github.com/yayitazale/automation-blueprints/blob/main/period_selector.yaml).

Para añadir el flujo a nodered previamente deberás instalar y configurar la librería [node-red-contrib-home-assistant-websocket](https://flows.nodered.org/node/node-red-contrib-home-assistant-websocket) y después importar el [siguiente archivo json](https://gitlab.com/yayitazale/automation-blueprints/-/raw/main/period_selector_flow.json).


# Panel de energía

Ahora que ya tenemos todo listo, nos queda poner en marcha el panel de energía que podemos habilitar en Home Asisstant. Para ello vamos a *Ajustes* > *Paneles de control* y seleccionamos el panel *Energía*. En este panel vamos a configurar primero los tres medidores que hemos creado. Para ello dentro de la tarjeta *Red Eléctrica* hacemos clic en *Añadir consumo*:

<img src="/images/posts/electricidad/dashboard.png" alt="Configuracion panel energia 1" class="sombra" />

Seleccionamos el contador del P1 por ejemplo. Aquí podemos añadir un precio al kWh de forma que el propio Home Assistant nos puede calcular el coste energético por periodos. Es importante recalcar que, al menos por ahora, el precio del coste fijo de precio de potencia y los impuestos no es posible añadirlos, por lo que el coste total de tu factura será siempre la suma de lo que Home Assistant haya calculado + los coste fijos que no podemos añadir. Las opciones de precios son las siguientes dependiendo de tu tarifa:

- Usar una entidad con el precio actual: en caso de tener PVPC, el estado de este sensor nos dice el precio actual, por lo que si añadimos este sensor como precio actual nos irá haciendo el cálculo. 
- Usar un precio estático: para los que estemos en tarifa de mercado libre, aquí puedes añadir el precio por kWh + impuestos por cada periodo.

<img src="/images/posts/electricidad/dashboard2.png" alt="Configuracion panel energia 2" class="sombra" />

Una vez añadido el periodo 1, hacemos lo mismo para tener los tres periodos como tengo yo.

Solo con esto, a medida que vaya pasando el tiempo iremos podremos ver en nuestro cuadro de mando de energía cómo va sumando por periodos:

<img src="/images/posts/electricidad/dashboard22.png" alt="Configuracion panel energia 22" class="sombra" />

Si además queremos tener detallado el consumo concreto de alguno de los aparatos de casa, podemos añadir [enchufes inteligentes Zigbee](https://www.zigbee2mqtt.io/supported-devices/#e=power&s=plug). Mi recomendación es que te compres [estos de Aqara](https://amzn.to/3BknTdY), que son compatibles con dispositivos de hasta 2300W (más que de sobra para la mayoría de electrodomésticos) y además ya nos incluye un sensor de energía. 

<a href="https://amzn.to/3BknTdY"><img src="/images/posts/electricidad/aqara.jpg" alt="Ecnhufe aqara" class="zoom" /></a>

En caso de que optes por sensores que unicamente te den la potencia instantánea, ahora ya sabes crear un sensor virtual usando la función de Suma de Riemann para calcular el consumo de energía. 

Estos dispositivos los añadiremos también al cuadro de mandos en la parte inferior derecha de la pantalla de configuración del panel de energía, en el que simplemente tendremos que indicar el sensor que queremos añadir (debe ser una medición en kWh):

<img src="/images/posts/electricidad/dashboard3.png" alt="Configuracion panel energia 3" class="sombra" />

Esto nos permite tener un detalle de los consumos concretos por separado:

<img src="/images/posts/electricidad/dashboard4.png" alt="Configuracion panel energia 4" class="sombra" />

Como puedes ver, el panel de energía admite añadir consumos de agua y gas, y también el aporte de solar y baterías. Por ahora no dispongo de una instalación de autoconsumo ni baterías, y tampoco es factible realizar mediciones ni obtener datos de agua y gas como hemos hecho en este manual, por lo que al menos por el momento, no voy a entrar a explicar estas partes (que además no controlo).

Finalmente como guinda al pastel, vamos a añadir un sensor virtual de huella de carbono. Gracias a la API de [CO2signal](https://www.co2signal.com/) que nos da acceso información de la fuente de la energía eléctrica en una región específica, cómo se produjo y cuánto dióxido de carbono se emitió para producirla. Todo gratis para uso no comercial. Para poder añadir esto tendremos que crear un token en su web:

<a href="https://www.co2signal.com/"><img src="/images/posts/electricidad/co2.png" alt="Web CO2signal" class="zoom" /></a>

Y en Home Asisstant vamos al cuadro de mando, le damos a *Añadir Cuenta* en bajo *Huella de carbono de la red* e introducimos el token que nos haya llegado a nuestro email. Si tenemos bien configurada la ubicación de nuestra vivienda en Home Assistant, vamos a poder obtener un cálculo aproximado del CO2 generado por la electricidad que vamos consumiendo cada hora, ya que hace un cálculo aproximado de la producción instantánea por regiones. Es algo parecido a lo que podemos consultar en [electricitymaps](). 

<a href="https://app.electricitymaps.com/zone/ES"><img src="/images/posts/electricidad/carbono.png" alt="Ecnhufe aqara" class="zoom" /></a>

Al final, con todo ya puesto en marcha nuestro panel de energía será tal que así:

<img src="/images/posts/electricidad/dashboard5.png" alt="Configuracion panel energia 5" class="sombra" />

<img src="/images/posts/electricidad/dashboard7.png" alt="Configuracion panel energia 7" class="sombra" />

<img src="/images/posts/electricidad/dashboard6.png" alt="Configuracion panel energia 6" class="sombra" />


A partir de aquí ya puedes empezar a monitorizar los consumos, automatizarlos o al menos ser consciente del coste de tus hábitos de consumo de electricidad en casa. También te puede servir para detectar consumos fantasma de aparatos conectados a la red etc.

PD: si ya te has hecho una cuenta en [I-DE Distribución](https://www.i-de.es/), puedes utilizar [este simulador](https://www.simuladorfacturaluz.es/simulador/) descargando el [fichero de consumos](https://www.simuladorfacturaluz.es/como-obtener-el-fichero-csv-de-consumos/) para saber qué comercializadora y tarifa te conviene más.

---

Espero que te sea de ayuda, en el próximo post os hablaré sobre cómo optimizar el consumo del gas en instalaciones de calefacción con caldera individual o comunitaria utilizando termostatos inteligentes y un poco de magia con Node-Red.

Un saludo y nos vemos en el siguiente post.

---
<a href="https://t.me/firstcommit" class="btn btn-primary text-white py-2">Suscríbete, que es gratis</a>


*Nota: algunos de los enlaces a productos o servicios pueden ser enlaces referidos con los que podemos obtener una comisión de venta.*


<style> 
img.sombra {
  display: block;  
  margin-left: auto;  
  margin-right: auto; 
  box-shadow: 5px 5px 8px grey;
}
img.sombra-small {
  display: block;  
  margin-left: auto;  
  margin-right: auto; 
  box-shadow: 5px 5px 8px grey;
  max-width: 20%;
}
img.sombra-inline {
  display: inline-block; 
  vertical-align: middle; 
  margin-right: 0px; 
  box-shadow: 3px 3px 8px grey;
}
img.zoom {
  transition: transform .2s;
}
img.zoom:hover {
  -ms-transform: scale(1.2); /* IE 9 */
  -webkit-transform: scale(1.2); /* Safari 3-8 */
  transform: scale(1.2); 
}
</style>
