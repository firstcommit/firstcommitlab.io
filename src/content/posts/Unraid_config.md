---

title: Configuración básica de UnRaid
authors: [yayitazale]
date: 2021-02-20T14:00:00Z
image: /images/posts/unraid_config/banner.jpg
categories:
  - selfhosted
tags:
  - selfhosted
  - unraid
  - guia
  - servidores

---
Ahora que [ya conocemos los principios básicos de Unraid, lo hemos instalado y hemos configurado los elementos más básicos](https://firstcommit.dev/2021/01/31/Unraid/), nos toca revisar y configurar a nuestro gusto los distintos aspectos del propio SO para que no tengamos que preocuparnos mucho en el día a día y lo tengamos todo listo para poder trabajar en distintos proyectos que iremos explicando más adelante.

Para que la guía sea lo más práctica posible, vamos a ir pasando por todas las pestañas del menú del SO donde vayamos a realizar alguna acción o sea interesante ver las opciones existentes. Unraid viene por defecto con casi todo listo para empezar a funcionar, pero es interesante tener claros algunos aspectos de la configuración para tenerlo todo bajo control y dejarlo todo acorde al uso que le vayamos a dar al servidor. 

(Foto de portada<a href="https://unsplash.com/es/@fennings?utm_content=creditCopyText&utm_medium=referral&utm_source=unsplash">Marc PEZIN</a> en <a href="https://unsplash.com/es/fotos/un-primer-plano-de-las-placas-de-identificacion-de-un-servidor-en-el-lado-de-un-i_JUAdanGH0?utm_content=creditCopyText&utm_medium=referral&utm_source=unsplash">Unsplash</a>)

#### TOC

# Shares

En este apartado vamos a ver qué posibles configuraciones tienen las carpetas del sistema. Tras instalar Unraid, vamos a tener al menos los siguientes shares:

- __*appdata*__: donde se alojará la información de las aplicaciones que instalemos
- __*domains*__: lugar donde se guardan las VMs
- __*isos*__: aquí dejaremos las plantillas ISO de las VMs que queramos crear
- __*Media*__: carpeta dedicada a albergar todos los archivos multimedia como fotos, videos y música
- __*system*__: archivos del propio SO

Si hacemos clic sobre __*appdata*__ por ejemplo nos lleva a la ventana de configuración de esa carpeta. Aquí tenemos 2 apartados, __*share settings*__ y __*SMB security Settings*__:

![appdata](/images/posts/unraid_config/unraid_config_2.png)

Antes de tocar nada, fíjate bien en la parte superior derecha de la pantalla. Aquí tenemos algo muy útil, un copiador de configuraciones. Como puedes ver podemos copiar configuraciones de cualquier otra carpeta del sistema de forma muy rápida y sencilla:

![copy_share_config](/images/posts/unraid_config/unraid_config_3.png)

Esto nos viene muy bien si vamos a crear una serie de carpetas con configuraciones idénticas, por ejemplo, para los usuarios de nuestra pequeña empresa o para nuestros familiares.

Dicho esto, vamos a ver las configuraciones del apartado __*share settings*__ (como puedes ver, si haces clic sobre cualqueira de los puntos aparece una explicación sobre las opciones que tenemos, algo que se repite en todos los menús de unraid):

![share_settings](/images/posts/unraid_config/unraid_config_4.png)

- __*Name*__: nombre que le daremos a la carpeta
- __*Comments*__: una descripción básica para saber lo que guardamos en la carpeta
- __*Allocation method*__: tiene tres opciones, aunque la opción 2 no es muy útil
  1. _High-water_: llenará primero el disco con menos espacio libre y después el siguiente, para minimizar los accesos a cada disco
  2. _Fill up_: escribirá primero en el disco 1 si tiene espacio disponible, sino en el 2, y así sucesivamente
  3. _Most-free_: lo contrario a la opción 1, irá escribiendo siempre en el disco más vacío
- __*Minimum free space*__: espacio mínimo requerido para escribir en el disco
- __*Split level*__: cómo queremos que las subcarpetas y archivos se despliguen en los díscos físicos. Es un poco extenso de explicar por lo que lo mejor es que lo dejéis como está, pero si queréis saber más tenéis una explicación en la [documentación de unraid](https://wiki.unraid.net/index.php/UnRAID_Manual_6#User_Shares_2)
- __*Include & exclude disks*__: si queremos incluir/excluir discos en concreto
- __*Use cache*__: configuraremos la carpeta para que use o no el array de caché. En principio, como todavía no hemos añadido un segundo disco de caché, debemos tener en cuenta que toda la información que se aloje en la caché estará desprotegida hasta que el _mover_ lo mueva al array de discos duros. Las opciones que tenemos son:
  1. _No_: no usar la caché, cualquier archivo nuevo se creará directamente en el array de discos duros
  2. _Yes_: utilizará la caché para crear archivos nuevos siempre que haya espacio disponible, sino lo hará el array de discos duros. El _mover_ cogerá lo que haya en la caché y lo pasará a los discos duros. Este modo es el habitual por ejemplo para la carpeta media o descargas torrent, ya que no sobrecargamos los discos duros en lecturas y escrituras y por la noche se moverá todo y consolidará en el array
  3. _Only_: solo se escribirá en la caché. Es peligroso por que si te quedas sin espacio cualquier creacion o modificación de archivos no se ejecutará
  4. _Prefer_: los archivos vivirán en la caché siempre que haya espacio. En caso de que se llene y se escriba en el array, el _mover_ lo pasará de nuevo a la caché en cuanto haya espacio libre. Este es el modo habitual para nuestra carpeta appdata, ya que las aplicaciones requieren lecturas y escrituras rápidas y constantes. Evitaremos usar el array y así mejoraremos el rendimiento del sistema. Aquí tendremos en cuenta que corremos ciertos riesgos, en caso de fallo de nuestra SSD perderíamos la información. Para que esto no pase o bien podemos añadir un segundo disco SSD o bien podremos hacer backups periodicos al array de discos como explicaré más abajo

![smb_settings](/images/posts/unraid_config/unraid_config_5.png)

En el apartado __*SMB security settings*__ podemos configurar cómo queremos compartir en la red esta carpeta. Compartir las carpetas por SMB es muy útil para acceder a ellas desde el PC de forma remota, pero tiene ciertos riesgos si se deja completamente libre el acceso. Lo ideal aquí es dejar todas las carpetas sin compartir por defecto y después crear usuarios en unraid y concederles permisos específicos.

- __*Export*__: si queremos que la carpeta esté disponible por SMB
- __*Security*__: aquí es donde configuramos quien y cómo puede acceder a dicha carpeta. Las opciones que tenemos son:
  1. _Public_: cualquier puede acceder con permisos de lectura y escritura. Mi consejo es evitar esta opción por los riesgos que conlleva
  2. _Secure_: cualquier usuario puede leer, pero solo los que definamos abajo podrán escribir
  3. _Private_: solo podran leer y/o escribir los usarios a los que les concedamos ese permiso

En mi caso, tengo exportados de forma privada las carpetas __*appdata*__, __*media*__ e __*isos*__. El resto de las carpetas las tengo todas sin compartir. Esta configuración me permite interactuar directamente con los archivos de configuración etc. de las aplicaciones, los archivos multimedia y las plantillas de VMs. En cualquier caso, si quisierais utilizar una carpeta compartida como nube local, podrías crear un usuario con acceso a dicha carpeta y mapearla en windows usando esas credenciales. En un futuro post veremos cómo crear una nubea privada en unraid usando __*nextcloud*__, algo que nos es más interesante por su versatilidad en comparación a un simple SMB.

# Users

Como acabamos de ver, crear usuarios en unraid sirve para que podamos adminsitrar los permisos de accesos a los distintos __*shares*__ por SMB. Para crear un usuario nuevo solo tenemos que darle a __*add user*__ y rellenar los siguientes campos:

![add_user](/images/posts/unraid_config/unraid_config_6.png)

Es importante entender que estos usuarios no tendrán acceso al portal web de unraid, estos usuarios solo sirven para la configuración de los accesos por SMB. 

Una vez creado un usuario, podremos modificar los permisos de acceso a las carpetas que tengamos compartidas por SMB directamente en la pantalla del usuario.

![edit_user](/images/posts/unraid_config/unraid_config_7.png)

# Settings

En este apartado no voy a entrar en detalle en cada uno de los subapartados posibles ya que, como decía, la mayoría de las configuraciones en Unraid vienen correctas por defecto y solo deberías cambiar cosas si tienes pleno conocimiento de lo que haces. Vamos a pasar por los grupos de subapartados para explicar qué puedes hacer en cada uno de ellos y nos detendremos solo en __*User Preferences*__. Más adelante en futuros posts sí editaremos alguna configuración del sistema.

### System Settings

![System Settings](/images/posts/unraid_config/unraid_config_8.png)

  - __*CPU Pinning*__: podremos configurar a nuestro gusto la dedicación de nuestros cores de la CPU a apps y tareas específicas. Para uso doméstico, lo normal es tener todos los cores disponibles para todo y es como viene por defecto
  - __*Date and Time*__: conviene que revises que la zona horaria está bien configurada. También puedes editar los servidores NTP de los que se va a coger la hora para tenerla siempre al día
  - __*Disk Settings*__: la configuración general de los discos se puede editar como, por ejemplo, tiempo inactivo para spindown, % máximo de ocupación para alerta, temperaturas máximas para alerta y también la configuracion del S.M.A.R.T.
  - __*Docker*__: podemos habilitarlo y deshabilitarlo, configurar el tamaño máximo de la imagen, etc.
  - __*Identification*__: si queremos tener un nombre de servidor diferente lo podemos cambiar aquí
  - __*Management Access*__: gestión de accesos por http, ssh, telnet etc.
  - __*Network Settings*__: definición de las redes internas y externas. Esto lo veremos en un futuro post en profundidad
  - __*Global Share Settings*__: definición global de los shares. Parte de lo comentado en el apartado [shares](#Shares) se puede configurar aquí a nivel global
  - __*UPS Settings*__: en caso de haber conectado o queres conectar una UPS al servidor lo haremos desde aquí
  - __*VM Manager*__: al igual que docker, podremos habilitar y deshabilitar esta función
 

### Network Services
![Network Services](/images/posts/unraid_config/unraid_config_9.png)

En este apartado podemos configurar los diferentes aspectos del acceso remoto según nuestra SO de escritorio que tengamos en nuestro PC, así como el acceso por FTP y el syslog.

### User Preferences
![User Preferences](/images/posts/unraid_config/unraid_config_10.png)

- __*Confirmations*__: podemos deshabilitar el pop-up de confirmación para los reinicios o paradas del array
- __*Display Settings*__: tenemos algunas opciones para editar la visualización del webUI de unraid
- __*Notification Settings*__: existen tres formas de tener notificaciones de los eventos que queramos tener controlados: 
  1. De escritorio, con un aviso en el navegador
  2. Por correo electrónico, vía SNMP
  3. Por algún sistema de mensajería como telegram, slack, pushbullet etc. En mi caso tengo un grupo de telegram donde dejo todas las notificaciones de los distintos sistemas que tengo en casa

- __*Scheduler*__: Aquí nos vamos a detener un poco ya que es una parte fundamental de nuestro sistema. Es el lugar donde vamos a configurar cúando queremos que se ejecuten tanto __*Parity check*__ como el __*mover*__:
  1. Como vimos en [los principios básicos](https://firstcommit.dev/2021/01/31/Unraid/#Principios-basicos), la paridad consiste en tener los datos asegurados ante un posible fallo de un disco. Esta seguridad se basa en unos algoritmos matemáticos que también pueden fallar, por lo que es necesario pasar un control periódico para asegurarnos que todo está bien. Yo os aconsejo habilitarlo con una periodicidad de un mes, ya que consume muchos recursos del sistema y los discos sufren muchas lecturas. Además, también es importante habilitarlo para los casos de apagado no seguro del servidor (se va la luz).
  2.  El __*mover*__ sin embargo no consume tantos recursos, por lo que depende un poco del uso que le vayas a dar a tu servidor y el tamaño de tu caché. Lo más fácil es que empieces por una periodicidad diaria, por ejemplo de noche, para cuando no estés haciendo uso del servidor y haya recursos libres. Si ves que tu caché se llena de forma habitual puedes ir aumentando la frecuencia. En mi caso, lo tengo activo cada 8 horas.

    ![Scheduler](/images/posts/unraid_config/unraid_config_11.png)

# Plugins

Las configuraciones de Unraid que hemos visto hasta ahora se basan en funcionalidades nativas del SO. Estas funcionalidades son correctas, pero en algunos casos se quedan un poco cortas para tener el sistema controlado y limpio. Para cubrir estas necesidades en Unraid tenemos una comunidad muy activa que crea y publica de forma gratuita plugins que nos permiten llevar a cabo acciones que no están incluidas de serie en el SO. La instalación de estos plugins se hacen vía la pestaña __*APPS*__. Estos son los plugins que considero muy útiles y sus correspondientes funciones.

![Plugins](/images/posts/unraid_config/unraid_config_12.png)

- __*CA Backup / Restore*__: como hemos comentado en el apartado [Shares](#Shares), si solo tenemos un disco SSD como caché, toda la información que esté en ese disco no tiene protección de paridad. Sin embargo nos interesa mucho que las APPS que vayamos instalado vivan en este disco ya que aumenta mucho el rendimiento del sistema y no acortamos la vida de los HDD ya que no vamos a estar leyendo/escribiendo continuamente de ellos. Con este plugin podremos hacer backups de la carpeta __*APPDATA*__ de forma muy granular a una carpeta en el array y también restaurar esas backups a nuestra caché
- __*CA Cleanup Appdata*__: nos permitirá en el futuro borrar carpetas de apps que hayamos desinstalado. Muy útil, aunque parezca algo sencillo de hacer los permisos de linux pueden llegar a ser muy puñeteros.
- __*Community Applications*__: esta es la que instalamos en los [primeros pasos](https://firstcommit.dev/2021/01/31/Unraid/#Primeros-pasos), la propia _tienda_ de apps que estamos usando para instalar estos plugins
- __*Dynamix caché Directories*__: este plugin guarda el directorio de carpetas en RAM de forma para minimizar los encendidos de discos innecesarios.
- __*Dynamix SSD TRIM*__: como nuestro disco caché es un SSD, es conveniente realizar un [trim](https://es.wikipedia.org/wiki/TRIM) de forma periódica para mantener su velocidad durante toda su vida útil.
- __*Dynamix System Temperature*__: muy conveniente para controlar las temperaturas. Le daremos uso en un futuro post donde crearemos un panel de monitorización usando __*telegraf*__, __*influxdb*__ y __*grafana*__
- __*Fix Common Problems*__: es un plugin que nos ayuda a mantener el sistema limpio y corregir posibles errores de seguridad o configuración que hayamos cometido.


---

Espero que estas explicaciones puedan servirte como base para empezar a enredar por tu cuenta en Unraid. 

---
<a href="https://t.me/firstcommit" class="btn btn-primary text-white py-2">Suscríbete, que es gratis</a>

*Nota: algunos de los enlaces a productos o servicios pueden ser enlaces referidos con los que podemos obtener una comisión de venta.*