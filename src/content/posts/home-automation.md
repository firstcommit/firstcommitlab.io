---

title: Home automation y dashboarding en Home Assistant
authors: [yayitazale]
date: 2021-04-05T21:00:00Z
image: /images/posts/home-automation/banner2.png
categories:
  - selfhosted
tags:
  - selfhosted
  - home assistant
  - guia
  - domotica
  - iot

---
Anteriormente hemos llevado a cabo un gran trabajo para [preparar nuestro sistema operativo doméstico](https://firstcommit.dev/2021/01/31/Unraid/), [hemos hablado de IoT + MQTT](https://firstcommit.dev/2021/03/14/mqtt/) y tenemos lista una plataforma centralizada llamada [Home Assistant](https://firstcommit.dev/2021/03/11/Hass/). Ahora bien, te preguntarás, ¿todo esto para qué? En este artículo os voy a enseñar a __crear automatizaciones simples__ para que vuestra casa se convierta en una algo más inteligente. Vamos a poner a nuestros dispositivos IoT a trabajar para nosotros, haciendo nuestra vida un poco más sencilla y divertida. Además os voy a dar unos tips sobre __cómo crear dashboards en Home Assistant__ para que sea visualmente más atractivo y fácil de usar.

#### TOC

# Automatizaciones

Las automatizaciones en HASS se realizan desde la propia interfaz. Esta interfaz nos ofrece siempre la misma estructura o __lógica de automatización__:

- __Desencadenante__: podrá ser un valor de un sensor, un evento, un cambio de estado, un cambio en la geolocalización de un dispositivo, la puesta del sol, etc. Además podemos hacer que los desencadenantes sean varios de estos por separado o a la vez (lógica OR y AND),
- __Condiciones__: los usaremos para que la acción se ejecute tras el desencadenante solo si cumple estas condiciones. De la misma forma que los desencadenantes, pueden combinarse varias con lógica OR y AND.
- __Acción__: dispararemos eventos, llamadas MQTT, cambios en dispositivos etc.

Además de esto podremos elegir el Modo en el que se ejecuta esta automatización:

- __Único__: la automatización sólo se podrá ejecutar una vez por vez, esto es, si durante la ejecución se activan de nuevo los desencadenantes no se la hará caso.
- __Reiniciar__: si durante la ejecución de la automatización se dispara el desencadenante de nuevo, se reiniciará y empezará de nuevo desde cero.
- __En cola__: se irán guardando los disparos de los desencadenantes en una cola y se irán ejecutando uno tras otro. 
- __Paralelo__: se podrá ejecutar en paralelo tantas veces se disparen los desencadenantes sin importar si ya se está ejecutando

Para entender una automatización, vamos a realizar algo simple utilizando el __sensor de presencia de Xiaomi__ y una __bombilla de Ikea__ conectados por __Zigbee2MQTT__ a HASS [como vimos en el anterior post](https://firstcommit.dev/2021/03/19/zigbee/). La lógica será la siguiente:

<img src="/images/posts/home-automation/Auto_1.svg" alt="Lógica_1" class="sombra" />

Para hacer esto, iremos a nuestro hass a *configuración* > *automatizaciones*, pulsaremos el botón de *+ Añadir nueva automatización* y seleccionaremos *Empezar con una automatización vacía*.

- Cambiamos el nombre a Luz 1 ON
- El modo lo dejamos en único.
- Como desencadenante seleccionamos el dispositivo sensor que vayamos a usar y el virtual sensor concreto es el de cuando haya empezado a detectar movimiento.
- En acciones, seleccionamos la bombilla, cambiamos a la acción encender la bombilla y lo ponemos con un 100% de porcentaje de brillo.
- Guardamos

<img src="/images/posts/home-automation/crear1.png" alt="Crear1" class="sombra" />

Ahora si nos movemos físicamente delante del sensor la luz debería encenderse.

Vamos a crear un automatización igual pero para que la luz se apague. Los sensores de Xiaomi se quedan con la __detección activada durante un minuto__, tiempo suficiente para detectarnos de nuevo si estamos moviéndonos en casa. Sin embargo si estamos sentados delante del ordenador puede que la luz se esté encendiendo y apagando por lo que vamos a hacer que se mantenga encendida al menos 5 minutos. La lógica es la siguiente:

<img src="/images/posts/home-automation/Auto_2.svg" alt="Lógica_2" class="sombra" />

Para esto lo que vamos a hacer es, estando en esta misma automatización ir a los tres puntos superiores y __Duplicar la automatización__:

- Ahora cambiamos el nombre a Luz 1 OFF.
- El desencadenante lo cambiamos a dejó de detectar movimiento pero añadimos en el campo de tiempo 00:04:00 (hh:mm:ss) para que la automatización solo salte cuando al menos hayan pasado 5 minutos (1 minuto del sensor y 4 minutos de delay) desde la activación. Esto permitirá al sensor detectarnos de nuevo dentro de esos cinco minutos si no nos movemos mucho.
- Bajamos a acciones y cambiamos la acción a apagar la bombilla y guardamos.

<img src="/images/posts/home-automation/crear2.png" alt="Crear2" class="sombra" />

Ya tenemos nuestra luz automatizada pero, desde el punto de vista de __eficiencia energética__ estamos malgastando energía encendiendo la luz a cualquier hora del día. Ahora vamos a __implementar un condicionante__, la cantidad de luz en la sala, para activar o no la luz. Para ello volvemos atrás, buscamos la automatización “Luz 1 ON” y hacemos click en el lápiz para editar.

<img src="/images/posts/home-automation/Auto_3.svg" alt="Lógica_3" class="sombra" />

Vamos a la condición, seleccionamos el mismo sensor que el desencadenante pero en condición seleccionamos Luminosidad actual y ponemos que se active por debajo de 80. Este valor puede no ser el indicado para ti, por lo que tendrás que ir ajustándolo en cada estancia para que se ajuste a tus necesidades, pero lo normal es que esté entre 50 y 100.

<img src="/images/posts/home-automation/crear3.png" alt="Crear3" class="sombra" />

Guardamos y ahora cuando el sensor detecta presencia y si es de día, no debería de encenderse la luz, y si es de noche, encenderse.

Como ves, con esta sencilla lógica ya queda en nuestra manos y nuestra capacidad creativa las automatizaciones que podamos hacer. 

# Blueprints

Si estamos un poco vagos o no somos muy creativos, desde Home Assistant se ha añadido la funcionalidad de utilizar __blueprints o planos__ (¿en realidad deberían ser plantillas?). Estas plantillas las puede crear otra persona como base para que nosotros podamos usarla y adaptarla a nuestros sensores. Como ejemplo, Hass ya trae una plantilla para automatizar una luz con un sensor de movimiento.

Si vamos a *configuración* > *planos*, vemos que existe uno llamado *Motion-activated light*. Seleccionamos *Crear automatización* y nos llevará a una pantalla.

<img src="/images/posts/home-automation/blueprint.png" alt="Blueprint" class="sombra" />

Aquí como ves, tenemos una versión reducida pre-programada en la que definiremos los mismos elementos que hemos usado en las dos automatizaciones anteriores:

- Sensor de movimiento
- Dispositivo, área o entidad
- Tiempo desde que el sensor deje de detectar para apagar la luz

Esta forma de añadir automatizaciones __nos facilita bastante el trabajo__ ya que existe un [foro específico de planos creados por la comunidad](https://community.home-assistant.io/c/blueprints-exchange/) que puedes utilizar para empezar tus automatizaciones.

De todas formas, tal y como vemos, la lógica actual no nos permite hacer automatizaciones complejas de forma sencilla por lo que en un futuro os explicaré cómo integrar __Node-Red como plataforma de flujos__, una herramienta mucho más visual para crear automatizaciones.

# Ayudantes

Para explicar la función de los ayudantes voy a poner un sencillo ejemplo: no queremos que las luces automáticas se enciendan si estamos dormidos, pero no dispongo de ningún sensor que sea capaz de saber si me encuentro despierto o dormido. Para poder afrontar este problema, vamos a crear un ayudante o sensor virtual al que cambiarle el estado según necesidad:

<img src="/images/posts/home-automation/Auto_4.svg" alt="Lógica_4" class="sombra" />

Vamos a *Configuración* > *ayudantes* y vamos a crear uno de tipo *alternar* (como ves, podemos crear temporizadores, contadores, calendarios, etc) y le llamaremos __Despierto__.

<img src="/images/posts/home-automation/ayudante1.png" alt="Ayudante1" class="sombra-inline" />
<img src="/images/posts/home-automation/ayudante2.png" alt="Ayudante2" class="sombra" />

Ahora vamos a la automatización __Luz 1 ON__ que hemos creado antes y en condiciones, seleccionamos estado, elegimos el ayudante __Despierto__ que acabamos de crear y en Estado escribimos “on”.

<img src="/images/posts/home-automation/ayudante3.png" alt="Ayudante3" class="sombra" />

Guardamos. Ahora, la automatización solo funcionará de noche y si estamos despiertos, esto es, si el ayudante está activado. 

# Dashboards

Y te preguntarás, ¿cómo activo el ayudante? Para eso están los dashboards. Ya te habrás fijado que en Home Assistant puedes crear paneles en los que mostrar los estados de los sensores, añadir botones de ayudantes etc. Para empezar a entender este sistema, vamos a añadir a nuestra pantalla principal algunos elementos. Vamos al *Resumen*, hacemos click arriba a la derecha en los tres puntos y hacemos click en *editar panel de control*.

Ahora, hacemos click en *añadir tarjeta*. Para añadir el ayudante de antes como un __botón grande__, seleccionamos *botón* y una vez dentro seleccionamos nuestro ayudante. Si queremos, podemos editar las opciones como nombre, tamaño incluso el icono. 

<img src="/images/posts/home-automation/dash1.png" alt="dash1" class="sombra" />

<img src="/images/posts/home-automation/dash2.png" alt="dash2" class="sombra" />

*Si queremos cambiar el icono, que sepas que existen dos opciones, los iconos integrados de Hass (y los usarás escribiendo hass:nombre_del_icono), o los iconos de Material Design [que puedes encontrar aquí](https://materialdesignicons.com/) (y que usarás escribiendo mdi:nombre_del_icono).*

Le damos a guardar y ya tenemos nuestra primera tarjeta que nos permite activar o desactivar el modo despierto.

<img src="/images/posts/home-automation/dash3.png" alt="dash3" class="sombra" />

Ahora, vamos a añadir una nueva pestaña donde meteremos directamente __las luces de toda la casa__, para tenerlas a mano por si necesitamos encenderlos o apagarlos manualmente de forma rápida. Hacemos click en +.

<img src="/images/posts/home-automation/dash4.png" alt="dash4" class="sombra" />

Le damos un nombre e icono, y además puedes ver que podemos dar o no el permiso de verlo a los distintos usuarios de Hass (también podemos añadir insignias, pero no soy muy fan de ellas).

<img src="/images/posts/home-automation/dash5.png" alt="dash5" class="sombra" />

<img src="/images/posts/home-automation/dash6.png" alt="dash6" class="sombra" />

Ahora repetimos el proceso anterior, añadiremos una tarjeta. Esta vez elegimos la tarjeta tipo Luz. 

<img src="/images/posts/home-automation/dash7.png" alt="dash7" class="sombra" />

Seleccionamos nuestra bombilla, icono etc y aceptamos. Ahora podemos activar la luz y/o regular su intensidad de forma rápida.

<img src="/images/posts/home-automation/dash8.png" alt="dash8" class="sombra" />

Finalmente, vamos a crear un nuevo dashboard específico para tener separados de la vista principal los datos de los sensores. Para ello vamos a *Configuración* > *Paneles de control* y hacemos clic en añadir. Aquí le damos un nombre y seleccionamos si queremos que aparezca en el lateral, si solo será visible por el administrador y guardamos.

<img src="/images/posts/home-automation/dash9.png" alt="dash9" class="sombra" />

Vemos que en nuestro lateral aparece una nueva entrada llamada __Sensores__. Si vamos a ella vemos que está vacía por lo que vamos a añadir dos tarjetas. La primera, será de tipo entidades: esto es una especie de lista donde podremos añadir muchas entidades seguidas con un pequeño icono al lado para cada uno de ellos.

<img src="/images/posts/home-automation/dash10.png" alt="dash10" class="sombra" />

<img src="/images/posts/home-automation/dash11.png" alt="dash11" class="sombra" />

El segundo panel que vamos a añadir es de tipo sensor. Seleccionaremos el sensor de iluminación de un sensor de presencia de Xiaomi. 

<img src="/images/posts/home-automation/dash12.png" alt="dash12" class="sombra" />

Si elegimos que salga con un gráfico de las últimas 24 horas, podremos además del ver el valor actual, revisar la tendencia de forma muy visual.

<img src="/images/posts/home-automation/dash13.png" alt="dash13" class="sombra" />

Con estos pasos hemos aprendido a añadir __diferentes paneles, pestañas y vistas__. Como has visto, existen muchísimos paneles estándar que vienen ya por defecto dentro del sistema Hass y a partir de aquí ya __es cuestión de jugar y tocar__ para dejar las pantallas de la interfaz a tu gusto.

# HACS y Themes

Además de los paneles estándar que encontramos en Hass, existen muchísimos __paneles no oficiales__ creados por la comunidad que nos puede interesar añadir a nuestros dashboards para hacerlos más atractivos o funcionales. Podriamos instalarlos de forma manual añadiendo los archivos correspondientes a una de las carpetas del sistema, pero para facilitarnos el trabajo __existe un plugin que para mí es indispensable llamado [HACS](https://hacs.xyz/)__. Además, en Hacs también encontraremos un montón de __themes__ que cambiarán la apariencia superficial de nuestros dashboards con colores y formatos nuevos, así como integración con dispositivos de terceros que no están soportados oficialmente.

Su instalación y configuración [está muy bien documentada en su web](https://hacs.xyz/docs/installation/prerequisites) por lo que no voy a comentar todos los pasos a seguir, si os encontráis con algún problema o tenéis dudas podéis dejar un comentario y os ayudaré encantado.

Una vez instalado tendremos dos pestañas en las que buscar integraciones extraoficiales y tarjetas o themes. De todas formas es importante tener en cuenta que estos elementos que vayamos a añadir no serán oficiales y por tanto no han pasado los filtros de estandarización y seguridad que pasan los componentes e integraciones oficiales, por lo que su instalación será siempre bajo nuestra cuenta y riesgo.

El theme que tengo instalado es el [Google Dark Theme](https://github.com/JuanMTech/google_dark_theme) que encontrarás en Hacs, por si te interesa.

<a href="https://github.com/JuanMTech/google_dark_theme"><img src="/images/posts/home-automation/dash14.png" alt="dash14" class="zoom" /></a>

En el [próximo post](https://firstcommit.dev/2021/04/11/infraestructura/) aprenderemos a publicar SaS para que estén disponibles desde el exterior de __forma segura__ para poder acceder a nuestros dashboards de Home Assistant desde el móvil en cualquier parte del mundo.

Os dejo unos pantallazos de mis dashboards:

<img src="/images/posts/home-automation/dash15.png" alt="dash15" class="sombra" />
<img src="/images/posts/home-automation/dash16.png" alt="dash16" class="sombra" />
<img src="/images/posts/home-automation/dash17.png" alt="dash17" class="sombra" />
<img src="/images/posts/home-automation/dash18.png" alt="dash18" class="sombra" />
<img src="/images/posts/home-automation/dash19.png" alt="dash19" class="sombra" />
<img src="/images/posts/home-automation/dash20.png" alt="dash12" class="sombra" />

---
<a href="https://t.me/firstcommit" class="btn btn-primary text-white py-2">Suscríbete, que es gratis</a>

*Nota: algunos de los enlaces a productos o servicios pueden ser enlaces referidos con los que podemos obtener una comisión de venta.*

<style> 
img.sombra {
  display: block;  
  margin-left: auto;  
  margin-right: auto; 
  box-shadow: 5px 5px 8px grey;
}
img.sombra-inline {
  display: inline-block; 
  vertical-align: middle; 
  margin-right: 0px; 
  width:auto;
  box-shadow: 3px 3px 8px grey;
}
img.zoom {
  transition: transform .2s;
}
img.zoom:hover {
  -ms-transform: scale(1.2); /* IE 9 */
  -webkit-transform: scale(1.2); /* Safari 3-8 */
  transform: scale(1.2); 
}
</style>