---
title: "Jellyfin, tu Netflix casero DiY de software libre"
authors: [yayitazale]
date: 2023-04-23T14:00:00Z
image: /images/posts/netflix/intro.jpg
categories:
  - selfhosted
tags:
  - guia
  - diy
  - media center
  - video
  - torrent
  - p2p
  - jellyfin
  - jellyseerr
  - radarr
  - sonarr
  - transmission
  - flood
  - prowlarr
  - telegram
  - saas
  - selfhosted
  - servidores
  - reverse proxy

---
Si eres usuario de Netflix, es posible que hayas notado que el servicio de streaming [ha estado aumentando sus precios](https://cincodias.elpais.com/cincodias/2021/10/10/companias/1633891189_813971.html) y, en algunos casos, [anunciando restricciones en el uso compartido de cuentas](https://cincodias.elpais.com/cincodias/2023/04/19/lifestyle/1681922212_230652.html). Estos cambios han dejado a algunos __usuarios insatisfechos y buscando alternativas__.
Afortunadamente, existen opciones de __software libre__ que permiten crear tu propio servicio de streaming en casa, sin tener que pagar altas tarifas mensuales por querer tener cuentas en distintas plataformas (HBO, Amazon, Netflix etc) de forma simultanea, y/o preocuparte por compartir tu cuenta con otros. En este post, te mostraré cómo crear __tu propio media center en casa con software libre__.

Es importante señalar que __descargar o compartir contenido protegido por derechos de autor es ilegal en muchos países__, por lo que te recomiendo verificar las leyes de tu país antes de hacerlo. 

__Este post en ningún caso invita a nadie a descargar este tipo de contenido y se publica con intenciones meramente formativas__.

Te dejo también estos enlaces de interés sobre la legislación actual al respecto en España:
- [Canon por copia privada](https://es.wikipedia.org/wiki/Canon_por_copia_privada_(Espa%C3%B1a))
- [Derecho de copia privada](https://es.wikipedia.org/wiki/Derecho_de_copia_privada)
- [FACUA advierte que la copia y descarga de música y películas sin ánimo de lucro no están tipificadas como delito en la reforma del Código Penal](https://web.archive.org/web/20160304104418/http://www.facua.org/facuainforma/2004/20septiembre2004.htm)
- [Las redes P2P son legales en España: sentencia a favor de Pablo Soto](https://www.eldiario.es/turing/propiedad_intelectual/justicia-victoria-pablo-soto-discograficas_1_4941387.html)
- [¿Es legal descargar torrents en España? Abogados responden](https://www.genbeta.com/actualidad/es-legal-descargar-torrents-en-espana-abogados-responden)


Finalmente, debes tener en cuenta que como siempre, voy a basar mis explicaciones sobre el sistema operativo unraid, [que es el utilizamos en nuestro home server](https://firstcommit.dev/2021/01/31/unraid/). En cualquier caso, si tienes un servidor linux con docker y/o docker-compose, tendrás que adaptar las explicaciones a comandos/fichero docker para poder lanzar los contenedores, pero el resto de configuraciones serán exactamente las mismas.

Si quieres navegar de forma rápida, este es el TOC:

(Foto de portada<a href="https://unsplash.com/es/@jakobowens1?utm_content=creditCopyText&utm_medium=referral&utm_source=unsplash">Jakob Owens</a> en <a href="https://unsplash.com/es/fotos/clap-board-en-la-carretera-jakob-y-ryan-CiUR8zISX60?utm_content=creditCopyText&utm_medium=referral&utm_source=unsplash">Unsplash</a>)

#### TOC

# Shares en unraid

Como vimos en su momento, [unraid gestiona el pool de discos duros en forma de raid](https://firstcommit.dev/2021/01/31/unraid/#Principios-basicos) por lo que necesitaremos tener ya creado un share llamado *Media*. Lo lógico en este caso es utilizar la siguiente configuración, de forma que los contenidos recién descargados vayan a la *cache* (evitamos el uso de los discos duros) y que el *Mover* los mueva durante la noche cada cierto tiempo al array:

<img src="/images/posts/netflix/shares1.png" alt="shares" class="sombra" />

En mi caso además tengo compartida la ruta vía SMB ya que de esta manera puedo añadir copias privadas de contenido que tengo en otros formatos como DVD, CD etc. desde el ordenador de uso personal.

Dentro de esta share necesitaremos crear las carpetas *movies*, *TV show* y *download* para poder tener separadas las pelis, las series y las descargas respectivamente.

# Clientes P2P

Para empezar, voy a hacer una pequeña introducción a las redes P2P ya que es la base sobre la que voy a vamos a trabajar nuestro media center.

Una [red peer-to-peer](https://es.wikipedia.org/wiki/Peer-to-peer), red de pares, red entre iguales o red entre pares (P2P, por sus siglas en inglés) es una red de ordenadores en la que todos o algunos aspectos funcionan sin clientes ni servidores fijos, sino una serie de nodos que se comportan como iguales entre sí. Es más, actúan simultáneamente como clientes y servidores respecto a los demás nodos de la red. Las redes P2P permiten el intercambio directo de información, en cualquier formato, entre los ordenadores interconectados. 

<img src="/images/posts/netflix/p2p.jpg" alt="p2p" class="sombra" />

Para poder hacer uso de este tipo de redes por tanto __necesitamos un cliente/servidor P2P__. Los más conocidos son [rTorrent](https://github.com/rakshasa/rtorrent), [qBittorrent](https://www.qbittorrent.org/), [Deluge](https://deluge-torrent.org/) y [Transmission](https://transmissionbt.com/) entre muchos otros. Yo me he decantado por este último ya que me parece muy simple de configurar y hacerlo funcionar, es muy liviano y consume muy pocos recursos. Además, [existe una versión que integra un cliente VPN](https://github.com/haugene/docker-transmission-openvpn) por si fuera necesario (en caso de que quieras salir a Internet por otro país, como Suiza, país en el que la [leyes de privacidad del usuario prohiben a los ISP recolectar información de los usuarios de redes P2P](https://protonvpn.com/blog/p2p-in-switzerland/), necesitarías una cuenta en alguna VPN como [protonVPN](https://protonvpn.com/) y configurar la salida por un servidor de dicho país).

En este caso, vamos a instalar la versión normal sin VPN. Para ello, desde la [CA store](https://forums.unraid.net/topic/38582-plug-in-community-applications/) de unraid buscamos __Transmission__. Usaremos la plantilla de *linuxservers*, que viene con muchos parámetros a rellenar.

<img src="/images/posts/netflix/transmission0.png" alt="transmission" class="sombra" />

Nosotros vamos a modificar alguna cosa para poder operar de forma óptima (lo veremos en el apartado de hardlinks):

- Cambiar los puertos a los que nosotros queremos
- Eliminar la entrada de la ruta */download*
- Añadir una nueva entrada de tipo ruta */media* y mapearla con la ruta */mnt/user/Media* de unraid
- Mapear la ruta */watch* a la ruta */mnt/user/Media/download/watch/*

El resto de configuraciones de usuario, contraseña etc las podemos borrar ya que no vamos a hacer uso de las mismas, por lo que te debe quedar algo como esto:

<img src="/images/posts/netflix/transmission2.png" alt="transmission2" class="sombra" />

Lo lanzamos y esperamos a que se haya levantado el contenedor. Una vez levantado, entraremos al la webui y vamos a la configuración para cambiar las rutas a la carpeta de descargas que hemos creado:

- descarga: lo cambiamos al valor */Media/download/complete*
- (opcional) incompletos: lo cambiamos al valor */Media/download/incomplete*

<img src="/images/posts/netflix/transmission3.png" alt="transmission3" class="sombra" style="max-width: 50%" />

Además, en caso de que vayas a tener acceso a indexers (también llamados trackers) privados, deberás modificar las opciones de *Seeding* para cumplir con las normas que tenga el tracker en cuestión. En cualquier caso, por la naturaleza del P2P, el objetivo es siempre compartir el máximo posible todo nuestro contenido por lo que lo ideal es deshabilitar todo y __compartir todo siempre__. 

El siguiente paso es abrir el puerto TCP/UDP que hayamos definido en la plantilla en nuestro router. Si seguiste mi guía de [Infraestructura de red domestica](https://firstcommit.dev/2021/04/11/infraestructura/) y tienes un router con [OpenWRT](https://openwrt.org/start) (para el resto, deberéis buscar cómo hacerlo con vuestro modelo de router), tienes que ir a *network > firewall > port forward* y crear una regla con:

- Protocolos: TCP y UDP
- Origen: wan
- Puerto externo: el definido en la plantilla, por defecto 51413
- Destino: lan
- IP interno: el servidor
- Puerto interno: el definido en la plantilla, por defecto 51413

Te debe quedar así, guardamos y aplicamos lo cambios:

<img src="/images/posts/netflix/transmission4.png" alt="transmission4" class="sombra" />

Ahora de nuevo en la interfaz de Transmission, en la pestaña *Network* de la configuración deberás ver que el puerto está abierto:

<img src="/images/posts/netflix/transmission5.png" alt="transmission5" class="sombra" style="max-width: 50%" />

Ahora ya tenemos nuestro cliente P2P listo. Opcionalmente, voy a instalar otro micro servicio para tener una interfaz más amigable que se llama [Flood](https://flood.js.org/), pero esto te lo puedes saltar y seguir con los siguientes pasos.

Para instalar Flood, vamos a la [CA store](https://forums.unraid.net/topic/38582-plug-in-community-applications/) de unraid y buscamos por __Flood-UI__ y lo instalamos tal cual usando la plantilla de *Figro* (como mucho necesitarás cambiar el puerto 3000 si lo tienes ocupado). 

<img src="/images/posts/netflix/flood0.png" alt="flood" class="sombra" />

Una vez lanzado, abrimos la interfaz web y nos saltará un diálogo de primer inicio de sesión para configurar:

- Usuario y contraseña
- Cliente: Transmission
- Enlace: como pone en el ejemplo, deberá ser algo tipo *http://192.168.1.20:9091/Transmission/rpc* cambiando la IP y el puerto por el de Transmission.
- Usuario y contraseña de Transmission, si los has configurado en la plantilla.

Una vez configurado, nos logeamos y veremos una interfaz mucho más actual y del que podremos además configurar Transmission por lo que este será nuestro frontend P2P, mucho más bonito y además responsive si entramos desde un móvil o tablet.

<img src="/images/posts/netflix/flood1.png" alt="flood" class="sombra" />

 # Indexers

Ahora que ya sabemos lo que son las redes P2P, seguramente te estés preguntado, al tratarse de una red distribuida, cómo vamos a poder buscar el contenido disponible en todos los nodos de la red. Esto se hace a través de [indexers](https://es.wikipedia.org/wiki/Peer-to-peer#Redes_P2P_h%C3%ADbridas,_semicentralizadas_o_mixtas) (también llamados trackers), que básicamente son que listan quién dispone de qué contenido. 

Como puede entender, esto es uno de los elementos más importantes a la hora de crear nuestro sistema de media center ya que dependerá de la calidad de estos indexers (tipo y cantidad de contenido disponible). Existen indexers de dos tipos: públicos y privados. Los públicos son los que podremos utilizar sin restricción pero, como es lógico, no hay mucho control sobre el contenido subido. Los indexers privados, como dice su nombre, son privados por lo que para poder utilizarlos necesitaremos ser parte de la comunidad que los gestiona.

Los sistemas públicos son normalmente más masivos en cuanto a número de usuarios, pero al no tener un control sobre el contenido, será más complicado encontrar lo que buscamos y es más probable que estos indexers desaparezcan en el tiempo.

Los privados suelen tener normas estrictas sobre cómo compartir el contenido y, normalmente, hace falta una invitación para poder entrar (algo complicado). 

En cualquier caso, para gestionar los indexers vamos a instalar un servicio en el que podremos configurarlos. Los más conocidos son [Jackett](https://github.com/Jackett/Jackett) y [Prowlarr](https://github.com/Prowlarr/Prowlarr), siendo este último más actual y mejor integrado con el stack que vamos a montar. Para ello, de nuevo desde la [CA store](https://forums.unraid.net/topic/38582-plug-in-community-applications/) de unraid, buscamos e instalamos la plantilla de *linuxserver* de __Prowlarr__ (solo tenemos que tener en cuenta el puerto).

<img src="/images/posts/netflix/prowlarr0.png" alt="prowlarr" class="sombra" />

Una vez instalado, lo primero que vamos a hacer es ponerlo conectarlo a Transmission. Para ello vamos a *Settings > Download clients*, hacemos click en *+* y configuramos la conexión añadiendola IP, puerto, username y pass:

<img src="/images/posts/netflix/prowlarr1.png" alt="prowlarr1" class="sombra" style="max-width: 70%"/>

Después vamos a la pestaña principal *Indexers* y hacemos click en *Add Indexer*. Como podrás ver, en la cabecera tenemos unos filtros que podremos usar para buscar indexers que se ajusten a nuestras necesidades. Si vamos a usar un indexer privado, buscaremos su nombre y configuraremos la conexión con las instrucciones que nos hayan dado (url, API key, user pass, etc)

Si por contra, quieres usar trackers públicos, podemos hacer uso de estos filtros. Por ejemplo, si deseamos descargar contenido en castellano, en el filtrado ponemos:
- protocolo: torrent
- idioma: es-ES
- privacidad: público

Y vemos que aparecen algunos resultados:

<img src="/images/posts/netflix/prowlarr2.png" alt="prowlarr2" class="sombra" />

Si queremos agregarlo, hacemos click en ellos y le damos a añadir. Aquí, si sois novatos, os recomiendo [buscar en Internet referencias sobre los mejores indexers](https://duckduckgo.com/?q=p2p+indexer&t=newext&atb=v288-1&ia=web) (también llamados trackers).

Después volveremos a Prowlarr cuando hayamos configurado los gestores de contenido automatizados para conectarlos entre sí y que de esta manera, puedan encontrar contenido haciendo solicitudes a los indexers que tengamos configurados.

# Gestores de contenido automatizados

El objetivo final de este post es disponer de un sistema similar a la de Netflix, donde podamos ver __contenido multimedia a demanda__. Como es lógico, inicialmente no vamos a tener todo el contenido de Internet del mundo ya en la biblioteca de medios, por lo que vamos a poner en marcha dos servicios dedicados a automatizar las descargas, una para películas y otra para series.

## Películas

Para gestionar las descargas de películas vamos a instalar la plantilla de *linuxservers* de la app __[Radarr](https://radarr.video/)__ desde la [CA store](https://forums.unraid.net/topic/38582-plug-in-community-applications/) de unraid. 

<img src="/images/posts/netflix/radarr0.png" alt="radarr" class="sombra" />

Al lanzar el contenedor tendremos que revisar el puerto y además, eliminar las dos entradas de rutas */download* y */movies* y sustituirla por */Media* como hemos hecho con el cliente P2P. Esto es muy importante para poder después habilitar los hardlink que es fundamental para temas de almacenamiento que luego veremos y para ello, la ruta raíz debe empezar por */Media* para que todos los servicios vean los ficheros y carpetas con la misma base.

La plantilla debe quedar así:

<img src="/images/posts/netflix/radarr1.png" alt="radarr1" class="sombra" />

Una vez lanzado, entramos a su interfaz web y vamos a configurarlo. Lo primero es añadir la ruta como ruta raíz, para ello, vamos a *Settings > Media Management* y en la parte inferior le damos a *Add Root Folder* y seleccionamos la carpeta */Media* (probablemente ya os aparezca):

<img src="/images/posts/netflix/radarr2.png" alt="radarr2" class="sombra" />

Después, podremos configurar el resto de parámetros a nuestro gusto. Yo os recomiendo esta configuración:

- Movie naming:

<img src="/images/posts/netflix/radarr3.png" alt="radarr3" class="sombra" />

- Folders:

<img src="/images/posts/netflix/radarr4.png" alt="radarr4" class="sombra" />

- Importing (muy importante activar el uso de hardlinks):

<img src="/images/posts/netflix/radarr5.png" alt="radarr5" class="sombra" />

Ahora vamos a configurar perfiles de descarga. Los perfiles nos van a permitir decidir la calidad y formato deseados, idiomas, etc. En mi caso, como mi objetivo es descargar todo en castellano y a ser posible con una segunda pista de audio en ingles o VO, he creado solo un perfil (ya que todo es más sencillo con un único perfil):

<img src="/images/posts/netflix/radarr6.png" alt="radarr6" class="sombra"  />

Como puedes ver, he añadido un *custom format* multi-idioma que añade un punto, por lo que si para una película en concreto hubiera dos ficheros descargables, uno solo en un idioma y otro con varias pistas de audio, se descargará el fichero multi-idioma al tener mayor puntuación. Además, he definido las calidades mínimas y máximas a las que quiero que se descarguen los ficheros y cual es el corte preferido. De esta manera, si una película se descarga pro primera vez a una calidad inferior a la de corte, seguirá monitorizandola hasta que encuentre un fichero en calidades superiores hasta llegar al corte. Si lo descarga a un nivel equivalente al corte o superior, y no seguirá monitorizandola nunca más. Esto es muy útil ya que en muchos casos, los estrenos aparecen inicialmente en calidades medias y a posteriori pueden aparecer versiones superiores.

Finalmente vamos a configurar el cliente torrent para que de esta manera, cuando se tenga que descargar ficheros, los envía al cliente torrent. Para esto, vamos a *Settings > Download clients* y añadimos nuestro Transmission de la misma forma que hemos hecho en Prowlarr. Yo por preferencia, añado el campo categoría el valor *radarr*, ya que de esta forma Radarr solo monitorizará la actividad de ficheros creados por si mismo (con esa categoría) en Tranmission y no hará caso a las descargas de series que llevarán la categoría *sonarr*. Además es importante deshabilitar el campo *Remove Completed* para que no elimine el torrent del cliente una vez finalizada la descarga:

<img src="/images/posts/netflix/radarr7.png" alt="radarr7" class="sombra" style="max-width: 70%" />

No voy a entrar en detalles más avanzados pero no esta mal que sepas que en *Settings > Quality* podrás configurar el espacio mínimo y máximo en disco que quieres que ocupen los ficheros dependiendo el tipo de formato. Esto solo debes modificarlo si sabes lo que haces.

## Series

La instalación es prácticamente idéntica a la instalación de Radarr, pero esta vez el servicio se llama __[Sonarr](https://sonarr.tv/)__. De nuevo, desde la [CA store](https://forums.unraid.net/topic/38582-plug-in-community-applications/) de unraid instalaremos usando la plantilla de *linuxservers*.

<img src="/images/posts/netflix/sonarr0.png" alt="sonarr" class="sombra" />

Modificamos la plantilla de nuevo con la misma idea, dejar como ruta base la carpeta */Media* de nuestro server:

<img src="/images/posts/netflix/sonarr1.png" alt="sonarr1" class="sombra" />

Una vez instalado, los pasos a seguir son prácticamente los mismos:

- Configuramos la ruta raiz a */Media/TV Show*
- Configuramos el renombramiento de ficheros y los hardlinks
- Añadimos el cliente torrent (con la categoria *sonarr*)
- Configuramos los perfiles de descarga. Aquí es donde encontramos la única diferencia, ya que Sonarr los perfiles de idioma y calidad están separados. Estas son las dos configuraciones de perfil que yo uso para descargar si es posible siempre en castellano:

<img src="/images/posts/netflix/sonarr2.png" alt="sonarr2" class="sombra" style="max-width: 70%" />
<img src="/images/posts/netflix/sonarr3.png" alt="sonarr3" class="sombra" style="max-width: 70%" />

## Conectar indexers y gestores

Ahora que tenemos los dos gestores de contenido funcionando, vamos a volver a Prowlarr y vamos a ir a *Settings > Apps*

Aquí vamos a añadir tanto Radarr como Sonarr. Para poder hacer esta configuración, necesitamos sacar la API Key tanto de Radarr como de Sonarr (lo encontrarán en *Settings > General*) y las direcciones IP, de forma que te quede algo como esto:

<img src="/images/posts/netflix/prowlarr3.png" alt="prowlarr3" class="sombra" style="max-width: 70%" />
<img src="/images/posts/netflix/prowlarr4.png" alt="prowlarr4" class="sombra" style="max-width: 70%" />

Una vez configurados, si vamos a Radarr o Sonarr, en *Settings > Indexers* veremos que ya nos aparecen todos los indexers que tengamos configurados en Prowlarr. Con esta forma de configurar los indexers desde Prowlarr, cualquier modificación que hagamos (añadir, quitar, modificar) sobre los indexers será automáticamente sincronizado en los dos gestores (si, me encanta hacer que las cosas sea más cómodas gracias a la automatización).

# Minimizar el uso del espacio con hardlinks

Esto es probablemente el punto más importante de este post. El principio de los sistemas P2P es que cuanto más compartamos todos, más contenido habrá disponible para todos. Para que este contenido pueda ser correctamente indexado, normalmente se utilizan tanto los metadatos del fichero como el propio nombre del fichero, por lo que lo habitual es que cuando queramos bajar una película como por ejemplo, *Gattaca*, nos descarguemos un fichero con un nombre tipo:

```Gattaca.1997.HDR10.TrueHD.5.1.HEVC.[es][en][it].mkv```

Sin embargo, para mantener un poco de orden y limpieza en nuestra biblioteca y que nuestro media center identifique correctamente las películas y series lo que nos interesa es poder renombrar el fichero a algo como por ejemplo:

```Gattaca 1998 HDTV-2160p.mkv```

O incluso incluirlo en una carpeta para que se almacenen en ella los subtítulos, crear una carpeta para colecciones como *Star Wars* donde se almacenen las carpetas de cada película etc.

Si modificamos el fichero descargado en la ruta de descargas, el cliente P2P perderá la referencia y por tanto dejará de compartirlo. La opción sencilla es realizar una copia y trabajar sobre este nuevo fichero, pero esto hace que cada fichero descargado ocupe el doble de espacio, pasando por ejemplo de ocupar 30GB a 60GB. Esto en la práctica es inviable ya que vamos a llenar nuestros discos duros muy rápido con su correspondiente coste en discos más con mayor almacenamiento etc.

Para evitar esto, en los sistemas de almacenamiento linux [existen formas de realizar *copias* que en el fondo no lo son](https://www.junosnotes.com/linux/understanding-hard-and-soft-links-on-linux/). La primera forma, que en este caso no nos vale, es crear [enlaces simbólicos o soft-links](https://en.wikipedia.org/wiki/Symbolic_link). Esto básicamente es crear un nuevo fichero de tipo acceso directo que nos permite abrir desde una ruta como un escritorio, un fichero alojado en otra ruta (muy usado para crear accesos a programas desde el escritorio etc). La segunda forma, que es la que nos atañe, son los [hardlinks](https://en.wikipedia.org/wiki/Hard_link). En esta forma de enlace, lo que haremos es crear accesos directos al fichero pero con un formato especial en el que el programa que los utilice crea que se trata del original. Ningún programa accedera realmente al fichero real, sino que todos lo harán a través de un acceso directo. 

<img src="/images/posts/netflix/hardlink1.png" alt="hardlink1" class="sombra" />

En nuestro caso, esto hará que Transmission vea un fichero en su ruta de descargas completadas con el nombre original y con 30GB:

```/Media/download/complete/Gattaca.1997.HDR10.TrueHD.5.1.HEVC.[es][en][it].mkv```

Pero Radarr, Sonarr y el media center verán un segundo fichero con otro nombre en otra ruta, también de 30GB:

```/Media/movie/Gattaca (1998)/Gattaca 1998 HDTV-2160p.mkv```

 Ambos creen tratar con el original pero en realidad solo ven un *acceso directo* y en realidad solo existe un único fichero de 30GB. Si eliminamos uno de los dos, el fichero seguirá existiendo en el sistema y ocupando 30GB, y solo se eliminará por completo si eliminamos todos los hardlink de ese fichero del sistema.

Tal y como hemos configurado las rutas, la raíz del sistema siempre será */Media* por lo que la gestión de la creación de ficheros hardlinkeados entre si lo hará de forma automática Radarr y Sonarr. Si no te ha quedado del todo claro [te dejo este video](https://www.youtube.com/watch?v=FapBQOuASdo) en la que explica el funcionamiento de los enlaces en linux. 

En cualquier caso, para asegurarnos que esto funciona, cuando tengamos descargada la primera serie o película, desde la consola de unraid podremos ir a la ruta de descarga haciendo:

```bash
cd /mnt/user/Media/download/complete/radar/
```

Y sobre esta ruda, si hacemos un ```ls -l elnombredelfichero.mkv```, si existe una copia hardlink en otra ruta veremos un 2 tras el listado de permisos del fichero. Aquí puedes ver un ejemplo:

<img src="/images/posts/netflix/hardlink2.png" alt="hardlink2" class="sombra" />

Si hago lo mismo desde la ruta de películas, veremos lo mismo (en este caso los ficheros tienen el mismo nombre, pero si has activado el renombramiento pueden ser diferentes):

<img src="/images/posts/netflix/hardlink3.png" alt="hardlink3" class="sombra" />

Como ves, es un único fichero que ocupa 69133012962 bytes en el sistema (unos 64GB), y es exactamente lo que veo en Radarr y en Transmission:

<img src="/images/posts/netflix/hardlink4.png" alt="hardlink4" class="sombra" />
<img src="/images/posts/netflix/hardlink5.png" alt="hardlink5" class="sombra" />

# Media center y acceso remoto

Ahora que ya tenemos gestores de contenido, indexadores y un cliente torrent P2P, ya podemos descargar, por lo que nos toca poner en marcha el media center como tal. Aquí existen múltiples alternativas como [Emby](https://emby.media/), [Plex](https://www.plex.tv/) o [Kodi](https://kodi.tv/), pero yo me he decantado por [Jellyfin](https://jellyfin.org/) después de unos cuantos años usando Plex, ya que JF es una herramienta completamente libre y su comunidad está creciendo de forma increíble. Además estéticamente me parece mucho más moderno (aunque si vienes de Plex inicialmente puede parecerte algo más caótico en cuanto a los menús).

Para instalarlo, desde la [CA store](https://forums.unraid.net/topic/38582-plug-in-community-applications/) de unraid buscaremos __Jellyfin__ y usaremos la plantilla de *SmartPhoneLover* ya que este apunta directamente al repositorio oficial:

<img src="/images/posts/netflix/jellyfin0.png" alt="jellyfin" class="sombra" />

Tendremos que mapear los puertos y las rutas de las carpetas de pelis y series que queremos. En este caso, no es necesario tocar nada ya que al media center no le importa todo el tema de los hardlinks, por lo que simplemente mapearemos las carpetas de pelis y series tal cual. También podremos mapear las rutas de música si queremos o podemos eliminar dicha entrada de la plantilla.

Finalmente, en caso de que tengamos una tarjeta gráfica de Nvidia en el servidor y queramos activar la aceleración por hardware, tendremos que hacer los siguientes cambios (previamente instalando el plugin [Nvidia Driver](https://forums.unraid.net/topic/98978-plugin-nvidia-driver/) desde la [CA store](https://forums.unraid.net/topic/38582-plug-in-community-applications/)):

- Activar la vista avanzada y añadir el parámetro extra ```--runtime=nvidia```
- Añadir una entrada de tipo Variable con el KEY  ```NVIDIA_VISIBLE_DEVICES``` y en Value deberemos poner el GPUID que nos aparezca en el plugin de nvidia
- Añadir una entrada de tipo Variable con el KEY ```NVIDIA_DRIVER_CAPABILITIES``` y el value ```all```
- Opcional: en algunos casos se debe activar el modo privilegiado para que funcionen bien los permisos de las carpetas y/o la tarjeta gráfica de nvidia, pero primero prueba en modo normal.

De forma que nos quede algo así:

<img src="/images/posts/netflix/jellyfin1.png" alt="jellyfin1" class="sombra" />

(En caso de querer usar la gráfica integrada intel de tu CPU, deberás añadir una entrada de tipo Device y Value pondremos ```/dev/dri/renderD128``` sin necesidad de añadir el parámetro extra del runtime)

Una vez lanzado entraremos en su webui y seguimos los pasos de la configuración inicial en el que nos pedirá seleccionar el idioma y crear un usuario y contraseña del administrador. Después nos dará la opción de añadir las rutas de las bibliotecas de pelis y series. Aquí tendremos que elegir, para las pelis, la ruta */JF_MOVIES* y para las series */JF_SERIES*, que son las que hemos mapeado. Además tendremos que configurar a nuestro gusto el idioma de visualizado, país en el que estamos, de donde sacará la información de las sinopsis, etc. Aquí es complicado proponerte una configuración concreta ya que dependerá de cómo quieras organizar tu media center, pero en cualquier caso podrás modificar todos estos parámetros más adelante si cambias de idea (no te preocupes si no estas del todo seguro de que elegir en alguna opción). Una vez configurados empezará el descubrimiento de pelis y series de forma automática de fondo.

Finalmente te preguntará sobre el idioma de meta-datos y país (importante si quieres aplicar reglas de control parental ya que usará los que se apliquen al país seleccionado) y la configuración de acceso remoto en el que deberás dejarlo como está sin cambiar nada (solo seleccionado *Permitir conexiones remotas a este servidor*).

Ahora que ya lo tenemos listo, tendremos que entrar a la pantalla de configuración del servidor desde la barra izquierda y haciendo clic en *Panel de control* debajo de *Administración*.

<img src="/images/posts/netflix/jellyfin2.png" alt="jellyfin2" class="sombra" />

Aquí puedes recorrer cada parte para dejarlo a tu gusto pero por ahora debes conocer:

- *Servidor > Usuarios*: donde vamos controlar los usuarios, generar nuevos etc. 
- *Servidor > Biblioteca*: aquí podrás ver las bibliotecas que has añadido, cambiar su configuración, añadir nuevas etc.
- *Servidor > Reproducción*: es donde puedes activar la aceleración por hardware en la transcodificación. Si has añadido una tarjeta gráfica de Nvidia, debes seleccionar *Nvidia NVENC*, si es una integrada de intel *QSV* o *VAAPI*. Para más info puedes revisar la documentación de Jellyfin y ver qué opciones debes activar, o bien ir probando.
- *Avanzado > Complementos*: puedes instalar plugins para mejorar o añadir nuevas funcionalidades. Yo te recomiendo que añadas *Open Subtitles* para que los subtitulos estén disponibles de forma automática, *TheTVDB* para descargar mejores metadatos y opcionalmente también *Fanart*  para descargar caratulas y posters creados por fans (tendrás que crearte una cuenta para poder usarlos pero es gratis).

Ahora, como ya explicamos en su momento en el post sobre proxy inverso, usando nuestro [Nginx Proxy Manager](https://firstcommit.dev/2021/04/19/saas/) crearemos una entrada proxy host (por ejemplo *media.midominio.com*) que apunte a la IP del servidor donde hemos lanzado Jellyfin y al puerto (por defecto, 8096) con su certificado SSL:

<img src="/images/posts/netflix/jellyfin3.png" alt="jellyfin3" class="sombra"  style="max-width: 50%"/>

Con esto, ya podremos acceder desde Internet a nuestro media center. Para probar que funciona, puedes probar a instalar la [app móvil de Android o IOS](https://jellyfin.org/downloads/clients/), donde tendrás que definir como servidor la URL que acabamos de crear.

El último paso a dar para terminar con la configuración es ir al Panel de control de *Jellyfin > Avanzado > Claves API* y generar dos nuevas keys, una para Radarr y otra para Sonarr:

<img src="/images/posts/netflix/jellyfin4.png" alt="jellyfin4" class="sombra" />

Entramos a Radarr y Sonarr en su menú, en *Settings > Connect* añadimos la conexión a *Emby / Jellyfin* uisando la API key, la IP de unraid, el puerto de Jellyfin y seleccionando las siguientes opciones:

Triggers
- On import
- On upgrade
- On rename
- On Movie/Serier Delete

Y también seleccionamos *Update Library*. De esta manera, si hay cualquier nueva descarga o cambio en los gestores de medios, Jellyfin se actualizara automaticamente escaneando las librerías:

<img src="/images/posts/netflix/jellyfin5.png" alt="jellyfin5" class="sombra" style="max-width: 70%"/>

# Portal de peticiones

Con todo el sistema que hemos montado, ya podríamos utilizar el stack automático, accediendo a Radarr para buscar películas, a Sonarr para buscar series a través de los indexers definidos en Prowlarr, solicitar su descarga, esperar a que el cliente P2P se los descargue, se cree automáticamente el hardlink y Jellyfin lo importe. Esto es factible si vamos a ser los únicos usuarios del sistema, pero es verdad que Radarr y Sonarr están creados para utilizarlos como gestores y además no están diseñados para exponerlos a Internet a través de un proxy reverso, por lo que hay herramientas más específicas que pueden cumplir con esta función.

Las más conocidas son [Ombi](https://ombi.io/), [Overseerr](https://overseerr.dev/), [Reqestrr](https://github.com/darkalfx/requestrr), [Jellyseerr](https://github.com/Fallenbagel/jellyseerr) o [Botdarr](https://github.com/shayaantx/botdarr). Todos salvo el último de esta lista son servicios web y el último es un chatbot que podemos integrar con Discord, Telegram etc.

En mi caso __mi preferencia es Jellyseerr__ ya que me parece muy intuitivo, con un aspecto impecable, muy liviano y que se integra a la perfección con los servicios que estamos usando. Además, al ser una [PWA (aplicación web progresiva)](https://es.wikipedia.org/wiki/Aplicaci%C3%B3n_web_progresiva), se puede utilizar vía navegador en el PC o bien [instalar como APP nativa desde el navegador del móvil](https://www.tuexperto.com/2021/05/03/como-instalar-una-pagina-web-como-aplicacion-web-progresiva-pwa/). 

Para instalar Jellyseerr, desde la [CA store](https://forums.unraid.net/topic/38582-plug-in-community-applications/) de unraid buscamos e instalamos la plantilla de *fallenbagel*. Tendremos que eliminar la entrada de *Emby support* ya que no nos hace falta y como siempre, revisar que el puerto no está ocupado, por lo demás solo nos queda lanzarlo:

<img src="/images/posts/netflix/jellyseerr0.png" alt="jellyseerr" class="sombra" />

Aquí tendremos de nuevo un configurador de bienvenida. En el primer punto tendremos que decir que vamos a usar una cuenta de Jellyfin y rellenaremos los campos de URL, email, usuario y contraseña con los que hemos generado hasta ahora.

<img src="/images/posts/netflix/jellyseerr1.png" alt="jellyseerr1" class="sombra" />

En el segundo paso, al hacer clic en *Sync Libraries* nos aparecerán las librerias que hayamos configurado (Películas y series), las activamos.

<img src="/images/posts/netflix/jellyseerr2.png" alt="jellyseerr2" class="sombra" />

En el último paso nos pedirá añadir la conexión a los servicios de Radarr y Sonarr. Para ello, necesitaremos la API key (igual que cuando los hemos conectado a Prowlarr). Aquí nos aparecerán los famosos perfiles de los que tendremos que elegir uno (por eso hemos creado solo uno):

<img src="/images/posts/netflix/jellyseerr3.png" alt="jellyseerr3" class="sombra" />

<img src="/images/posts/netflix/jellyseerr4.png" alt="jellyseerr4" class="sombra" />

(en cuando a seleccionar la opción 4K: Jellyseerr permite separar las descargar en formato 4K para habilitar ese permiso solo a ciertos usuarios. En mi caso, no distingo entre usuarios de 4K o no por lo que con un único servidor y perfil cualquier podrás descargar en 4K y por tanto no me hace falta seleccionar esa opción).

Ahora, antes de seguir adelante, debemos volver a nuestro [Nginx Proxy Manager](https://firstcommit.dev/2021/04/19/saas/) y crear un nuevo host (por ejemplo *request.midominio.com*), en este caso apuntando a la IP del servidor de unraid y el puerto de Jellyseerr (por defecto 5055).

La configuración del sistema es bastante simple, entrando a la interfaz web con la ip y el puerto:

- *Ajustes > General*: Aquí podremos ponerle un título a la web y en el campo *Application URL* añadiremos la URL https que acabamos de crear en el Nginx. De esta forma, el servicio ya es accesible desde Internet.
- *Ajustes > Usuarios*: podremos definir los permisos por defecto de los usuarios, muy importante repasarlo para dejarlo a nuestro gusto
- *Usuarios*: podremos gestionar los usuarios manualmente, importar los usuarios de Jellyfin, crear usuarios locales etc. En principio esto es todo automático, pero inicialmente puedes probar a importar manualmente los usuarios desde Jellyfin si ya tienes algunos creados.

<img src="/images/posts/netflix/jellyseerr5.png" alt="jellyseerr5" class="sombra" />

# Gestión de usuarios

Ya estamos terminando. Como he comentado antes, con todo este stack ya podemos empezar a usar nuestro sistema, pero como es lógico, todo este trabajo que nos hemos pegado lo podemos compartir (si queremos) con otros usuarios, como amigos y familiares. Para esto, podemos bien crear usuarios manualmente en Jellyfin y enviarles las credenciales a estos usuarios a mano, o bien podemos utilizar un sistema automatizado para que ellos mismos sean capaces de crear su propio usuario (si, me encanta que todo se pueda automatizar).

Para esto vamos a instalar el servicio [JFA-GO](https://jfa-go.com/) desde la [CA store](https://forums.unraid.net/topic/38582-plug-in-community-applications/) de unraid, usando la plantilla de *ndetar*. En esta plantilla debemos mirar como siempre si el puerto lo tenemos libre y también debemos revisar, en caso de que hayamos cambiado la ruta de instalación por defecto de Jellyfin ya que debemos definir en la entrada *Jellfyfin Config Directory* y viene la usada por defecto */mnt/user/appdata/Jellyfin/*.

<img src="/images/posts/netflix/jfa-go0.png" alt="jfa-go" class="sombra" />

Una vez tengamos esto revisado, instalamos y de nuevo, tendremos un configurador de bienvenida. Antes de seguir adelante con el configurador conviene que creemos dos cosas:

- Una nueva entrada host en [Nginx Proxy Manager](https://firstcommit.dev/2021/04/19/saas/), por ejemplo *accounts.midominio.com*
- Un usuario con permisos básicos de lectura de las librerías de pelis y series en Jellyfin. Esto lo hacemos desde el *Panel de control > Usuarios*. Este usuario lo vamos a utilizar como plantilla para que cuando los nuevos usuarios se registren hereden los mismos permisos. Opcionalmente, al crear el usuario podemos deshabilitar la opción *Ocultar este usuario en las pantallas de inicio de sesión* para que los usuarios hagan login de una forma más sencilla. Otra opciones que podemos aplicar son horarios de acceso o un control parental basado en la clasificación de edad del país con el que hayas configurado Jellyfin:

<img src="/images/posts/netflix/jfa-go1.png" alt="jfa-go1" class="sombra" />

Ahora si, seguimos con el configurador de bienvenida de JFA-GO donde tendremos que seleccionar:

- Idioma
- IP y puerto de la red local (podemos dejar la IP en 0.0.0.0 ya que vamos a usar un proxy reverso)
- Permisos de login: debemos seleccionar la opción 1 (*Authorize with Jellyfin/Emby*) pero solo para Admins.
- IP local, puerto, URL externa, usuario y pass de administrador de Jellyfin
- La conexión con Ombi la dejamos sin configurar ya que nosotros usamos Jellyseerr
- Mensajes: lo dejamos habilitado ya que usaremos un bot de Telegram para comunicarnos con los usuarios
- Notificaciones, esto ya a vuestro gusto
- Mensajes de invitación, lo podemos habilitar pero yo al menos no lo voy a usar
- Resets de password: a nuestro gusto, yo lo tengo habilitado para que si a un usuario se le olvida la contraseña pueda modificarla
- Validación de passwords: aquí puedes ser todo lo tikismikis que quieras
- Mensajes de ayuda, si no los tocas saldrán en inglés

<img src="/images/posts/netflix/jfa-go2.png" alt="jfa-go2" class="sombra" />

Una vez terminado con el configurador, vamos a preparar un par de cosas más en la pestaña de *Ajustes* en las siguientes secciones:

- *Perfiles de usuario*: debes crear un perfil de usuario usando como base el usuario demo que hemos creado en Jellyfin.

<img src="/images/posts/netflix/jfa-go3.png" alt="jfa-go3" class="sombra" style="max-width: 50%"/>

- *Jellyfin*: revisamos que en Public Address aparece correctamente la URL de nuestro servidor Jellyfin
- *Captcha*: si queremos evitar que si nuestro link de invitación se publica en algún sitio en internet y un robot empiece a crear cuentas, podemos activar esto o usar Telegram (o ambas)
- *Telegram*: opcionalmente podemos añadir un bot para que los usuarios deban hacer una autenticación de doble factor usando un código generado por el bot. En el siguiente apartado explicaré cómo crear un bot en Telegram para utilizarlo en un canal de notificaciones, por lo que solo debes seguir los mismos pasos para crear otro bot para JFA-GO.

Finalmente vamos a la pestaña *Invitaciones* en la parte superior y creamos una nueva. Podemos seleccionar la duración que queramos pero en mi caso, como quiero que me dure lo máximo posible y que pueda usarse infinitas veces ya que el enlace estará dentro de un mensaje de un canal privado de Telegram, lo he creado con la siguiente configuración:

<img src="/images/posts/netflix/jfa-go4.png" alt="jfa-go4" class="sombra" style="max-width: 70%"/>

# Notificaciones y canal de Telegram

Por fin el paso final. Ya tenemos todo perfectamente configurado para que nuestros familiares y amigos, que junto a nosotros, puedan realizar solicitudes y que se descarguen los contenidos de forma automática. Ahora vamos a crear un canal de Telegram donde vamos a inviar a los usuarios para que reciban notificaciones cada vez que se agregue nuevo contenido. Además, como he dicho en el anterior apartado, podemos anclar un mensaje con las instrucciones de cómo y donde crear un usuario, a través de que URL realizar las solicitudes etc. 

Crear un [bot de Telegram](https://core.telegram.org/bots) es muy sencillo. Primero, debes abrir un chat con [@BotFather](https://t.me/BotFather) (y te ayudará a crear bots). Con el menú le damos a crear bot, le damos un nombre y un alias (este último debe ser único y terminar por *_bot*) y nos dará una __API key__. Después podemos cambiar su foto, descripción etc pero es opcional. Una vez creado el bot vamos a *Bot Settings -> Group Privacy* y le damos a *Turn Off*:

<img src="/images/posts/netflix/notificaciones2.png" alt="notificaciones2" class="sombra" style="max-width: 70%" />

Ahora, desde el panel de tu usuario de Telegram creamos un nuevo canal privado y agregamos al bot como subscritor. Una vez añadido al bot al canal, vamos de nuevo a bot father y activamos de nuevo el *group privacy*.

<img src="/images/posts/netflix/notificaciones3.png" alt="notificaciones3" class="sombra" style="max-width: 70%" />

Añadimos también como suscriptor a [@chat_id_echo_bot](https://t.me/chat_id_echo_bot) y lanzamos el comando ```/my_id``` para que nos diga el __chat ID__ del canal. Lo copiamos y eliminamos al bot del canal.

<img src="/images/posts/netflix/notificaciones4.png" alt="notificaciones4" class="sombra" style="max-width: 70%" />

Ahora, teniendo a mano la API key y el Chat ID, vamos a Jellyseerr y en el apartado *Ajustes > Notificaciones -> Telegram* podremos habilitar el agente añadiendo los dos códigos mencionados. Podremos definir los triggers de notificaciones por defecto del canal como por ejemplo, cuando haya nuevo contenido disponible. Además, Jellyseerr hace que el bot también sea interactivo por lo que cualquiera de los usuarios podrá abrir un chat con el bot y configurarse sus propias notificaciones privadas en la conversación con el bot creado (parece magia pero no, es Telegram).

<img src="/images/posts/netflix/notificaciones5.png" alt="notificaciones5" class="sombra" />

---

Ha sido largo, pero esto es todo. Espero que el post te sirva de ayuda para montar tu sistema de contenido a demanda en tu servidor. Si tienes cualquier problema o duda no dudes en dejarme un comentario y trataré de ayudarte lo antes posible.

Un saludo y nos vemos en el próximo post.

---
<a href="https://t.me/firstcommit" class="btn btn-primary text-white py-2">Suscríbete, que es gratis</a>


*Nota: algunos de los enlaces a productos o servicios pueden ser enlaces referidos con los que podemos obtener una comisión de venta.*


<style> 
img.sombra {
  display: block;  
  margin-left: auto;  
  margin-right: auto; 
  box-shadow: 5px 5px 8px grey;
}
img.sombra-small {
  display: block;  
  margin-left: auto;  
  margin-right: auto; 
  box-shadow: 5px 5px 8px grey;
  max-width: 20%;
}
img.sombra-inline {
  display: inline-block; 
  vertical-align: middle; 
  margin-right: 0px; 
  box-shadow: 3px 3px 8px grey;
}
img.zoom {
  transition: transform .2s;
}
img.zoom:hover {
  -ms-transform: scale(1.2); /* IE 9 */
  -webkit-transform: scale(1.2); /* Safari 3-8 */
  transform: scale(1.2); 
}
</style>
