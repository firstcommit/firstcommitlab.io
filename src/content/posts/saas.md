---
title: Selfhosted SaaS seguro con reverse proxy y TLS
authors: [yayitazale]
date: 2021-04-19T21:30:00Z
image: /images/posts/saas/banner.png
categories:
  - selfhosted
tags:
  - selfhosted
  - openwrt
  - firewall
  - guia
  - router
  - saas
  - tls
  - reverse proxy
  - home assistant

---
En el [último post](https://firstcommit.dev/2021/04/11/infraestructura/) preparamos nuestra __red doméstica local__ para tener una mayor seguridad implementada utilizando [OpenWRT](https://openwrt.org/start) en nuestro router. Ahora ya estamos listos para que los servicios de nuestro servidor pasen a estar __accesibles desde internet__, de forma que podamos acceder a estos servicios desde __cualquier parte del mundo__.

El esquema de [SaaS](https://es.wikipedia.org/wiki/Software_como_servicio) que os voy a proponer es el siguiente:

<img src="/images/posts/saas/esquema.jpg" alt="Esquema" class="sombra" />

#### TOC

---
# DDNS

En internet la forma que tenemos para acceder a los diferentes sitios web es a través del [direccionamiento IP](https://es.wikipedia.org/wiki/Direcci%C3%B3n_IP). Este sistema de direccionamiento es muy sencillo de interpretar por una máquina, pero no es que sea muy amigable para nosotros los humanos, por lo que lo habitual es que los sitios web se publiquen bajo un [nombre de dominio](https://es.wikipedia.org/wiki/Dominio_de_Internet), como por ejemplo [firstcommit.dev](https://firstcommit.dev/) o [www.wikipedia.org](https://www.wikipedia.org/). 

Existen a nivel mundial servidores dedicados a guardar esta relación entre nombres de dominio e IPs públicas de los servicios web, de manera que cuando en tu navegador introduces un nombre de dominio, este hará una solicitud a un [servidor DNS](https://es.wikipedia.org/wiki/Sistema_de_nombres_de_dominio) y este le entregará a tu navegador la dirección IP a la que debe llegar. Si no me crees:

1. Abre el menú de inicio de Windows
2. Escribe CMD y vuelve a pulsar Enter
3. En la ventana negra que se acaba de abrir escribre *ping www.wikipedia.org* y pulsa Enter

Verás que aparece una dirección IPv4 como a donde resuelve la llamada de [ping](https://es.wikipedia.org/wiki/Ping):

<img src="/images/posts/saas/ping.png" alt="Ping" class="sombra" />

Normalmente estas IPs públicas __son fijas__, esto es, tu adquieres una IP pública como una matrícula de coche y será tuya exclusivamente. Esto es muy útil, pero como sabrás, [el rango de direccionamiento IPv4 está agotado desde 2011](https://es.wikipedia.org/wiki/Agotamiento_de_las_direcciones_IPv4), por lo que el precio de estas direcciones ha subido mucho. Nuestro operador de internet, para ahorrar costes, lo que hace es utilizar un pool de IPs fijas que se van asignando a sus clientes de forma dinámica según necesidad, por lo que __tu IP pública variará en el tiempo__. En algunas operadoras existe la posibilidad de alquilar una de estas IPs públicas, pero con un coste. A nivel mundial, el [protocolo IPv6](https://es.wikipedia.org/wiki/IPv6) se está implementando como nuevo estándar con, teóricamente, direcciones IP ilimitadas, pero en España (y en otros países) todavía es algo que [no está muy extendido](https://www.google.com/intl/es/ipv6/statistics.html#tab=per-country-ipv6-adoption).

__¿Cómo vamos entonces a conectar un dominio a nuestra casa?__ Usando un sistema dinámico de DNS o [DDNS](https://es.wikipedia.org/wiki/DNS_din%C3%A1mico): un servicio local dentro de nuestra casa llamará cada poco tiempo a un servicio de DNS externo, y éste actualizará nuestra IP en la tabla de relación de nombre de dominio <-> IP pública cada vez que ésta cambie.

Existe un servicio gratuito que podemos instalar de forma muy sencilla en unraid llamado __DuckDNS__. Lo primero que vamos a hacer es [entrar a su web](https://www.duckdns.org/) y crearnos una cuenta utilizando alguna de sus posibles formas de inicio de sesión (github, google, twitter…)

Una vez creada la cuenta, nos aparecerá la siguiente ventana. En ella lo que debemos hacer es añadir un __nombre de subdominio__ que queramos (podemos crear hasta 5 de forma gratuita), de forma que el nombre web que tendrá nuestra casa sea algo como __firstcommit.duckdns.org__. Nos quedamos con la página abierta ya que necesitaremos el token en el siguiente paso.

<img src="/images/posts/saas/duckdns1.png" alt="Duckdns 1" class="sombra" />

Ahora vamos a unraid y buscamos la app DuckDNS:

<img src="/images/posts/saas/duckdns2.png" alt="Duckdns 2" class="sombra" />

En la pantalla de configuración del docker rellenamos el __subdominio que hemos elegido antes__ y el __token que nos ha aparecido en DuckDNS__:

<img src="/images/posts/saas/duckdns3.png" alt="Duckdns 3" class="sombra" />

Aceptamos, y una vez arrancado vamos hacemos click en el el logo del contenedor (en este caso, el pato) y __vamos al log__ para ver si el servicio se comunica con los servidores de DuckDNS y actualiza nuestra IP pública:

<img src="/images/posts/saas/duckdns4.png" alt="Duckdns 4" class="sombra" />

<img src="/images/posts/saas/duckdns5.png" alt="Duckdns 5" class="sombra" />

Ahora, si accedemos desde el navegador a nuestro subdominio de duckdns la solicitud llegará hasta nuestro firewall, que como tenemos [bien configurado](https://firstcommit.dev/2021/04/11/infraestructura/#Reglas-en-el-firewall) bloqueará la solicitud y no podremos ver nada.

---
# Dominio propio

Es posible que no te guste mucho tener que acceder a tus servicios utilizando un subdominio de duckdns. Es algo un poco feo, pero gratis. Si estás dispuesto a __gastar unos pocos euros al año__, de forma opcional podemos hacer que este acceso sea a través de un __dominio propio__, sin el *.duckdns.org* detrás. Si de ta igual, puedes saltarte este paso e ir directamente al [siguiente](#Port-Forwarding-en-OpenWRT).

Para comprar un dominio, puedes hacerlo en cualquier revendedor conocido como [Dinahosting](https://dinahosting.com/dominios), [Arsys](https://www.arsys.es/dominios), [Godaddy](https://www.godaddy.com/es-es/dominios) o incluso [Google](https://domains.google/intl/es_es/), pero __mi recomendación por calidad/precio__ es [Namecheap](https://www.shareasale.com/r.cfm?u=2804077&m=46483&b=518798). Los pasos para comprar y configurar un dominio serán diferentes en cada plataforma, por lo que en caso de no hacerlo con Namecheap y tengas dudas no dudes en dejar un comentario.

Entramos en [Namecheap](https://www.shareasale.com/r.cfm?u=2804077&m=46483&b=518798) y buscamos por el dominio que más nos guste:

<img src="/images/posts/saas/namecheap1.png" alt="Namecheap 1" class="sombra" />

Como puedes ver, el precio anual varía mucho, por lo que aquí ya queda en tus manos cuál quieres comprar. 

<img src="/images/posts/saas/namecheap2.png" alt="Namecheap 2" class="sombra" />

Una vez adquirido el dominio, vamos a *Manage* -> *Advanced DNS* y creamos un registro *HOST* de tipo *[CNAME](https://es.wikipedia.org/wiki/Registro_CNAME)* como este (con tu subdominio de duckdns):

<img src="/images/posts/saas/namecheap3.png" alt="Namecheap 3" class="sombra" />

Esto hará que cualquier intento de acceso a nuestro dominio o subdominios que crearemos después vaya al servidor DNS de duckdns y este a su vez lo redirija a nuestro firewall.

---
# Port Forwarding en OpenWRT

Ya tenemos nuestro dominio propio o subdominio duckdns apuntando a nuestra casa y actualizado siempre gracias a DuckDNS. Ahora lo que debemos hacer es __redirigir los accesos necesarios desde nuestro router a nuestro servidor__. 

La idea es que todos nuestros servicios sean de tipo web y seguros con un [cifrado TLS](https://es.wikipedia.org/wiki/Seguridad_de_la_capa_de_transporte). Este tipo de navegación es el conocido __[https](https://es.wikipedia.org/wiki/Protocolo_seguro_de_transferencia_de_hipertexto)__ y siempre será a través del __puerto 443__. En caso de que queramos publicar servicios no seguros sin TLS, la navegación será __[http](https://en.wikipedia.org/wiki/Hypertext_Transfer_Protocol)__ por el __puerto 80__, pero no os lo recomiendo ya que toda la información viajaría sin cifrado y cualquier persona podría ver la información. En cualquier caso, lo vamos a necesitar igualmente por lo que vamos a nuestro router/firewall OpenWRT, accedemos con nuestro usuario root/contraseña y vamos *Network* > *Firewall* > *Port Forwards* y añadimos dos reglas con la siguientes configuraciones:

<img src="/images/posts/saas/firewall1.png" alt="Firewall 1" class="sombra" />

<img src="/images/posts/saas/firewall2.png" alt="Firewall 2" class="sombra" />

Debemos usar un __puerto interno diferente al 80 y 443__ ya que estos puertos lo usa el propio portal de administración de unraid. Por ejemplo, el 1880 y 18443 nos podrían servir. 

Lo que estamos haciendo con esta regla es __redirigir todas las peticiones http y https__ que lleguen a nuestro router vía DuckDNS por el puerto 80/443 __a la IP local y un puerto específico de nuestro servidor__. Este puerto no será directamente el de un servicio concreto (como el 8123 de Home Assistant) ya que nos deberá servir para acceder a cualquiera de los servicios que queramos. ¿Cómo? ¿Es posible navegar a diferentes puertos de nuestro servidor utilizando como entrada y salida en el firewall solo los puertos http y https? 

---
# Proxy reverso

Para poder hacer este puente entre diferentes subdominios o sub-subdominios a diferentes puertos de cada servicio utilizaremos un proxy inverso o [reverse proxy](https://es.wikipedia.org/wiki/Proxy_inverso). Un [proxy](https://es.wikipedia.org/wiki/Servidor_proxy) es un servicio que redirige las solicitudes de acceso __de un cliente a un servidor__. Normalmente se usan para hacer de puente entre una red de tipo local sin acceso a internet y un servicio de internet. En nuestro caso, __le damos la vuelta a la tortilla__ y hacemos que sea el servicio que se aloja en nuestro servidor y que no es accesible desde internet, se convierta un servicio accesible a través de nuestro dominio:

<img src="/images/posts/saas/esquema.jpg" alt="Esquema" class="sombra" />

Para hacer esto existen diferentes plataformas, las más utilizadas son [Traefic](https://traefik.io/) y [Ngix Proxy Manager](https://nginxproxymanager.com/). En mi caso, me decanto por la segunda ya que la primera, además de ser más compleja de configurar está muy ligada a docker-compose, un sistema docker que nuestro unraid no tiene de forma nativa. Lo primero que vamos a hacer es instalar la App en unraid:

<img src="/images/posts/saas/proxy2.png" alt="Proxy 2" class="sombra" />

En la configuración, añadimos los puertos 1880 y 18443 que hemos configurado en nuestro OpenWRT:

<img src="/images/posts/saas/proxy3.png" alt="Proxy 3" class="sombra" />

Una vez arrancado vamos al webserver del propio reverse proxy y entramos con el siguiente user/pass:

<img src="/images/posts/saas/proxy4.png" alt="Proxy 4" class="sombra" />

- Email:    admin@example.com
- Password: changeme

Lo primero que vamos a hacer es __cambiar el email de usuario y la contraseña a algo seguro__. Lo lógico de todas formas es no exponer este panel de administración al exterior, pero en cualquier caso como siempre es mejor tener nuestros servicios securizados y sin contraseñas por defecto.

<img src="/images/posts/saas/proxy5.png" alt="Proxy 5" class="sombra" />

---
# SaaS seguro con TSL

Ya tenemos todo listo, solo nos queda publicar un servicio y securizar su acceso a https con SSL. En este momento debo volver a recordar que en el momento en el que tu servicio esté público a internet, __cualquier persona podrá acceder a él__. Para no tener sustos vamos a listar las __buenas prácticas__ que deberías seguir siempre con servicios publicados al exterior:

- Nunca publiques servicios críticos como el propio panel de unraid, el portal de administración del firewall o el propio portal de Nginx Proxy Manager
- Nunca publiques servicios que no tengan un portal de identificación con al menos usuario y contraseña
- Utiliza siempre securización SSL (navegación con https)
- Utiliza siempre contraseñas seguras y no las reutilices en diferentes servicios
- Si es posible, activa el OTP para tener una capa de seguridad mayor
- Mantén tus sistemas actualizados, tanto el firewall, el sistema de unraid como los propios docker de tus servicios.

El servicio que vamos a publicar es el de [Home Assistant](https://firstcommit.dev/2021/03/11/Hass/). Aplicamos actualizaciones en caso de que tengamos algo pendiente y mientras se descargan e instalan, en nuestro teléfono nos bajamos una aplicación de [TOTP](https://en.wikipedia.org/wiki/Time-based_One-Time_Password). Os recomiendo [Authy](https://authy.com/download/) para escapar de las garras de [Google](https://play.google.com/store/apps/details?id=com.google.android.apps.authenticator2&hl=es&gl=US). Una vez instalada y creada la cuenta en Authy, vamos a Home Assistant y entramos en nuestro perfil de usuario. En *Módulos de autenticación multifactor* le damos a habilitar y nos saldrá un QR:

<img src="/images/posts/saas/auth.png" alt="Auth" class="sombra" />

En Authy añadimos un acceso leyendo el QR con la cámara, y el código de 6 dígitos que nos aparece en la pantalla la metemos en Home Assistant. Con esto activado, en caso de acceder a Home Assistant necesitaremos este código a parte de nuestro usuario y contraseña. Como estos códigos caducan cada minuto, es prácticamente imposible que alguien pueda robarnos la cuenta y acceder a nuestro sistema domótico. Además, si alguien con un robot intentase un [ataque por fuerza bruta](https://es.wikipedia.org/wiki/Ataque_de_fuerza_bruta) contra Hass, seguiría teniendo siempre este paso intermedio que no podrá llevar a cabo sin tener acceso a nuestra terminal móvil.

Ahora en Hass vamos a *configuración* > *Configuración General* y bajamos a la zona donde se definen las URL. Vamos a añadir una *URL externa* basada en nuestro dominio como por ejemplo:

- __ha.midominio.duckdns.org__ en caso de que no tengas un dominio propio
- __ha.midominio.com__ en caso de que tengas un dominio propio

En la URL interna ponemos la IP y puerto al que estamos accediendo ahora mismo en la red local.

<img src="/images/posts/saas/publish5.png" alt="Publish 5" class="sombra" />

Por fin, después de todos estos pasos __estamos listos__ para efectivamente publicar nuestro servicio en internet. Vamos a Nginx Proxy Manager, nos logeamos y vamos a *Proxy Host* y hacemos click en *Add host*. Los datos a rellenar en la primera pantalla son estos:

<img src="/images/posts/saas/publish1.png" alt="Publish 1" class="sombra" />

<img src="/images/posts/saas/publish2.png" alt="Publish 2" class="sombra" />

- __Domain Name__: el subdominio al que llamaremos, sea con el dominio de duckdns o el nuestro propio, en este caso el mismo que hayamos puesto en la configuración de URL externa de hass.
- __Scheme__: el protocolo de acceso desde Nginx al servicio. Normalmente será http ya que una vez que llegamos al Nginx la navegación es en local.
- __Forward hostname o IP__: ponemos la ip local de nuestro servidor
- __Forward port__: el puerto web del servicio, en este caso el 8123
- __Cache Assets__: hará que la los accesos vayan más rápidos
- __Block common exploits__: añade una capa de seguridad, como por ejemplo, evitar ataques DDOS.
- __Websocket support__: en este caso Hass usa websockets por lo que lo activamos

Ahora vamos a la pestaña de SLL para añadir un certificado y que la navegación entre el cliente y el Nginx sea por https, de forma que nadie pueda espiar nuestras comunicaciones.

Para entenderlo de forma simple, podemos explicar el cifrado TLS o SSL como un sistema de correos: la comunicación entre el cliente y el servidor va cerrada dentro de un sobre (cifrado). Para poder cerrar o abrir el sobre, antes de empezar a comunicarse ambas partes pactan una clave que solo conocen ellos, por lo que nadie de correos podrá abrir esa carta durante el transporte.

Para poder hacer esto, debemos añadir un [certificado digital](https://es.wikipedia.org/wiki/Certificado_digital) a nuestro servicio y para ello normalmente hay que pagar, ya que la entidad que emite los certificados cobra por sus servicios. Por suerte existe un servicio gratuito y libre de __auto-certificación__ llamado [Let’s Encrypt](https://letsencrypt.org/getting-started/), al cual podemos solicitar un certificado digital para nuestros servicios. Esta funcionalidad ya está integrada en Nginx Proxy Manager por lo que los pasos a seguir son muy sencillos.

Seleccionamos en el desplegable *Requets a new SSL Certificate* y rellenaremos los siguientes datos:

<img src="/images/posts/saas/publish3.png" alt="Publish 3" class="sombra" />

- __Force SSL__: para obligar a que la comunicación sea https
- __HTTP/2 support__: activado
- __HSTS__: activado
- __HSTS__ subdomains activado
- __Email__: ponemos un email (que sea nuestro) y aceptamos los términos y condiciones de Let’s Encrypt.

Ya podemos guardar el Proxy Host. Tarda un poco en finalizar el proceso ya que automáticamente enviará una petición a Let’s encrypt, que para poder certificar nuestro subdominio, intentará acceder a él haciendo una __petición http__. (es por esto que debemos pasar el puerto 80 desde el firewall al Nginx). El certificado generado tiene una caducidad de 3 meses, pero Nginx Proxy Manager se encargará de actualizarlos por nosotros (aunque no está de más echarle un vistazo de vez en cuando para actualizarlos manualmente si hace falta).

Finalmente, vamos al navegador y escribimos __la url externa de Hass que hayamos configurado__ y si hemos hecho todo bien, nos llevará a la página de inicio de sesión donde deberemos introducir nuestro usuario, contraseña y el código TOTP que nos dará Authy.

<img src="/images/posts/saas/publish4.png" alt="Publish 4" class="sombra" />

---

En el [siguiente post](https://firstcommit.dev/2021/05/09/vaultwarden) vamos a dar un paso más en la seguridad de nuestros sistemas publicando un gestor de contraseñas como servicio para nosotros mismos y hacernos la vida un poco más fácil con contraseñas seguras y el TOTP integrado en una única plataforma.

---
<a href="https://t.me/firstcommit" class="btn btn-primary text-white py-2">Suscríbete, que es gratis</a>

*Nota: algunos de los enlaces a productos o servicios pueden ser enlaces referidos con los que podemos obtener una comisión de venta.*

<style> 
img.sombra {
  display: block;  
  margin-left: auto;  
  margin-right: auto; 
  box-shadow: 5px 5px 8px grey;
}
img.sombra-inline {
  display: inline-block; 
  vertical-align: middle; 
  margin-right: 0px; 
  width:auto;
  box-shadow: 3px 3px 8px grey;
}
img.zoom {
  transition: transform .2s;
}
img.zoom:hover {
  -ms-transform: scale(1.2); /* IE 9 */
  -webkit-transform: scale(1.2); /* Safari 3-8 */
  transform: scale(1.2); 
}
</style>
