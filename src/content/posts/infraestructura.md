---
title: Infraestructura de red doméstica
authors: [yayitazale]
date: 2021-04-11T21:00:00Z
image: /images/posts/infraestructura/banner.jpg
categories:
  - networking
tags:
  - openwrt
  - firewall
  - guia
  - redes
  - IoT
  - router
  - vlan
  - wifi
  - mesh
---

Ahora que [ya tenemos nuestro sistema domótico en marcha](https://firstcommit.dev/2021/04/05/home-automation/) podemos empezar a usarlo en _productivo_. ¿Cuál es el problema? Que no podemos acceder a él de forma remota, por lo que perdemos una gran funcionalidad del sistema y es el de operar nuestra casa sin estar físicamente en ella.

Antes de activar esta función, debemos tener en cuenta que, desde el punto de vista de la seguridad, el mero hecho de __publicar un servicio hacia internet puede ser inseguro__. Por tanto, para evitar disgustos o al menos, minimizar el riesgo, voy a dividir el trabajo en varios post en los que trataremos de preparar nuestra red local, firewall, router, servidor y sistemas de acceso.

En este primer post, vamos a ver un __posible esquema de infraestructura de red__ y vamos a __sustituir el router/firewall__ que nos suministra nuestro proveedor de internet por uno neutro con un __sistema operativo de software libre__ que nos permitirá gestionar la seguridad y la red con  mayor facilidad.

#### TOC

---
# Infraestructura lan doméstica

El primer punto a tener en cuenta a la hora de diseñar una red lan doméstica es si __disponemos de una red cableada__ y si esa red cableada tiene al menos [cables de categoría 6](https://es.wikipedia.org/wiki/Cable_de_categor%C3%ADa_6) para darnos velocidades de 10 Gbps (Gigabit). Si nuestros cables son de categoría 5e la velocidad máxima que vamos a conseguir es 1Gbps, nos podría servir para un uso normal de navegación pero si en un futuro ponemos en marcha un media center y queremos ver películas o series en calidad 4K en streaming desde nuestro servidor, es recomendable que sustituyas estos cables por unos de categoría 6.

Teniendo en cuenta esto, os voy a dar una posible solución tanto para redes cableadas Gigabit como para una red completamente inalámbrica.

De todas formas quiero dejar claro que la infraestructura de red que vamos a implementar en ambos casos es algo simple, no vamos a utilizar [segmentación por VLAN](https://es.wikipedia.org/wiki/VLAN) al menos por ahora, que aunque siendo un esquema más seguro, nos complica bastante la infraestructura, la instalación y encarece los equipos necesarios a utilizar. Si te interesa mucho este aspecto ya que vas a conectar muchos equipos IoT WiFi a tu red, te recomiendo que eches un ojo [este post](https://wiki.fortu.io/es/Ubiquity-EdgeRouterX/crear-vlan) de mi amigo __@FoRTu__ donde explica como crear segmentos de red sobre un router [Ubiquiti EdgeRouter X](https://amzn.to/3wHUoPH).

De la misma forma, en nuestro servidor de unraid no vamos a implementar una segmentación de las [redes internas de docker](https://docs.docker.com/network/host/) más allá de usar una __red bridge__ en el mismo segmento de red que nuestra red LAN. Aunque esa segmentación es algo aconsejable, para un servidor doméstico es algo que podemos obviar para simplificar los accesos. 

Estas dos decisiones aumentan el riesgo a sufrir algún tipo de ataque desde el exterior, por lo que será importante mantener nuestros servicios actualizados, utilizar __contraseñas de acceso seguras__ (y a ser posible OTPs), y como vimos en la [configuración básica de UnrRaid](https://firstcommit.dev/2021/02/20/Unraid_config/), no exponer los shares del servidor sin protección en nuestra red. Teniendo en cuenta que nuestros equipos IoT se encuentran en una red ZigBee aislada de nuestra red local y si seguimos estos consejos será algo más complicado que podamos sufrir ataques externos o que nuestros aparatos puedan ser usados en [ataques DDoS mundiales coordinados](https://www.iotforall.com/iot-botnets-ddos-attack-architecture).

También quiero comentar (antes de entrar a ver las posibles infraestructuras de red) que algunas operadoras están instalando equipos que integran el modem ONT fibra y el firewall/router en un solo aparato. En estos casos (por ejemplo, Movistar), si queremos hacer uso de un router neutro os recomiendo poner el __equipo de la operadora en modo brigde__ ya que los protocolos de conexión ONT suelen variar según operadora y estas suelen ser bastante opacas en ofrecer información sobre estos protocolos y su autenticación.

## Infraestructura con red cableada

Si ya tenemos una red cableada de cat6 ya tenemos una gran parte del trabajo hecho. En este caso vamos a entender la red en tres capas:

- Red cableada PoE
- Red cableada no PoE
- Red inalámbrica

Dado que tenemos una red cableada, como veremos, vamos a aprovecharla para no solo llevar comunicación a los puntos de red de la casa, sino que vamos a llevar también la corriente eléctrica necesaria a algunos puntos de la casa usado [PoE](https://es.wikipedia.org/wiki/Power_over_Ethernet), donde colocaremos elementos de red que funcionaran con un solo cable.

El esquema de infraestructura que podríamos plantear es el siguiente:

<img src="/images/posts/infraestructura/esquema1.png" alt="Esquema 1" class="sombra" />

En este caso he dotado a la red de un [Sistema de Alimentación Ininterrumpida](https://es.wikipedia.org/wiki/Sistema_de_alimentaci%C3%B3n_ininterrumpida) (cables rojos), de forma que en caso de que se vaya la luz en casa nuestra infraestructura siga funcionando y sirviendo los servicios que para nosotros sean __críticos__.

Si, como en mi caso particular, tus equipos de red (módem de fibra, router, etc) están en una __ubicación física diferente al servidor__, puedes optar por añadir un segundo UPS de menor tamaño y adecuado al consumo de estos aparatos, con esquema tal que así:

<img src="/images/posts/infraestructura/esquema2.png" alt="Esquema 2" class="sombra" />

Como podéis ver, yo he optado por __puntos de acceso WiFi externos al router__ distribuidos por casa en diferentes puntos de red para tener la mejor cobertura posible tanto en la banda de 5Ghz como en la de 2,4Ghz.

Este es el listado de equipos que os propongo:

- __Router__: [MikroTik Hex RB750Gr3](https://amzn.to/3dXBSKe)
- __Switch__: [D-Link DGS-1008P 4+4](https://amzn.to/3tbXeKi)
- __Antenas__: [D-Link DAP-2610](https://amzn.to/3g1yTTT)
- __UPS principal__: [APC BX700U-GR](https://amzn.to/2QjB4Ht)
- __UPS para equipos de red__: [Mini UPS 10000mAH 5V/9V/12V/24V/48V PoE](https://amzn.to/3s3lKw0)

## Infraestructura con red inalambrica

Si no disponemos de una red cableada que nos permita distribuir los puntos de acceso en casa usando esa red cableada, la opción más interesante que tenemos es la de crear un red [Mesh WiFi](https://en.wikipedia.org/wiki/Wireless_mesh_network) similar a la [red ZigBee para nuestros equipos IoT](https://firstcommit.dev/2021/03/19/zigbee/). 

Una red Mesh funciona con una antena maestra conectada al router y una serie de antenas distribuidas por casa que funcionarán como esclavos, conectados entre si y desde los más cercanos también al maestro. Esta configuración nos complica el uso de UPS ya que tendríamos que colocar uno por cada repetidor mesh. Mi propuesta como solución es la de conectar una de los esclavos mesh al UPS principal donde tenemos el servidor y la antena maestra conectarla al router, pero esto solo nos será útil si estas dos antenas se ven entre ellas (están relativamente cerca):

<img src="/images/posts/infraestructura/esquema3.png" alt="Esquema 3" class="sombra" />

En este caso el listado de equipos necesario sería:

- __Router__: [MikroTik Hex RB750Gr3](https://amzn.to/3dXBSKe)
- __Antenas MESH__: [D-Link COVR-1103](https://amzn.to/3dVQudc)
- __UPS principal__: [APC BX700U-GR](https://amzn.to/2QjB4Ht)
- __UPS para equipos de red__: [Mini UPS 10000mAH 5V/9V/12V/24V/48V PoE](https://amzn.to/3s3lKw0)

---
# Instalar OpenWRT

Lo primero que vamos a hacer cuando nos llegue a casa el Router MikroTik Hex es instalar [OpenWRT](https://openwrt.org/start) ya que su sistema operativo __RouterOS__ es bastante desagradable a la vista. Además OpenWRT es un [software libre](https://en.wikipedia.org/wiki/OpenWrt) que podemos instalar en [muchísimos tipos de routers](https://openwrt.org/supported_devices), por lo que puedes usar otro router diferente al que os propongo que te guste más y/o en un futuro migrar el sistema a otro equipo nuevo importando la configuración.

Los pasos para instalar OpenWRT varían según el router elegido, por lo que lo mejor es seguir los [pasos específicos para nuestro equipo](https://openwrt.org/toh/mikrotik/rb750gr3). De forma resumida:

0. Descargamos el archivo de instalación y el de upgrade ya que usaremos los dos.
1. Encendemos el router, nos enchufamos con un cable de red desde el PC al puerto eth1, y utilizando el navegador entramos al 192.168.1.1
2. Hacemos una copia del __RouterOS key__ por si en un futuro queremos volver a instalarlo.
3. Apagamos el router.
4. Descargamos el [tinyPXE server](http://reboot.pro/files/file/303-tiny-pxe-server/).
5. Cambiamos la IP de nuestro PC a 192.168.1.10
3. Abrimos el tinyPXE y aceptamos cualquier aviso del firewall de Windows.
4. En boot file quitamos el check y en el file seleccionamos el archivo de instalación “intramfs-kernel.bin”
5. Hacemos clic en Online.
6. Enchufamos el cable de red en el puerto “Internet” o 1.
7. Mientras mantenemos pulsado el botón de Reset con un palito, encendemos el router. Mantenemos pulsado el Reset hasta que escuches el primer beep. En ese momento veremos que en la consola del tinyPXE empezará a haber actividad y soltamos el botón.
8. Esperamos __SIN APAGAR EL EQUIPO NI TOCAR NADA__ hasta que los leds dejen de parpadear. En ese momento veremos que el equipo se reinicia.
9. Cambiamos el cable de ethernet al puerto 2.
10. Abrimos el navegador y entramos en 192.168.1.1
11. Vamos a _system > backup / flash software_ y en la zona de _Flash new firmware image_ seleccionamos el archivo de update sysupgrade.bin y le damos a OK. Esto hará que la instalación ya sea permanente. 
12. Esperamos a que finalice el proceso __SIN APAGAR EL EQUIPO NI TOCAR NADA__ y cuando se reinicie ya tendremos el router con OpenWRT listo.

Os dejo un video en el que se llevan a cabo los mismos pasos para otro modelo de router de Miktrotik (salvo el paso 11 y 12):

<iframe width="560" height="460" src="https://www.youtube.com/embed/8-jB30m8NmA" title="OpenWRT install on Mikrotik" frameborder="0" allow="accelerometer; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Una vez finalizada la instalación procederemos a poner __una contraseña segura al usuario Root__.

---
# Conectar nuestro router al operador

Antes de seguir adelante, tendremos que buscar en internet los protocolos de conexión para que podamos configurar el puerto WAN de nuestro mikrotik [acorde a nuestra operadora](https://wiki.bandaancha.st/Categor%C3%ADa:Operadores).

Una vez hecho esto procederemos a quitar el router de nuestro operador y conectar el modem ONT de fibra(o el router del operador en modo brigde) al puerto 1 del mikrotik y encenderlo. 

Como ejemplo, vamos a configurar __una conexión de O2__ que es la operadora que tengo contratada(los pasos para movistar son exactamente los mismos). Lo primero, es crear la VLAN en la que trabaja O2. En el router vamos a _Network > Switch_ y lo dejamos así:

<img src="/images/posts/infraestructura/switch_vlan.png" alt="VLAN" class="sombra" />

Ahora vamos a _Network > Interfaces_ y en _WAN_ seleccionamos _EDIT_. Introducimos los siguientes datos:

- Protocolo: PPPoE
- PPP Username / nombre de usuario: adslppp@telefonicanetpa
- PPP Password / contraseña: adslppp

En las pestañas Physical Settings y Firewall settings elegimos la VLAN 6 de la siguiente forma:

<img src="/images/posts/infraestructura/ppoe1.png" alt="ppoe1" class="sombra" />
<img src="/images/posts/infraestructura/ppoe2.png" alt="ppoe2" class="sombra" />

Guardamos y al activar, si hemos hecho todo bien, veremos que recibimos una IP pública y empezamos a tener paquetes en subida y bajada:

<img src="/images/posts/infraestructura/ppoe3.png" alt="ppoe3" class="sombra" />

_Por cierto, en caso de que nuestra operadora utilice el protocolo **CGI-NAT** como por ejemplo pepephone, es el momento de solicitar a la operadora que nos lo desactive (o de cambiarte de operadora) ya que en cuando vayamos a publicar los servicios para acceder desde el exterior necesitaremos una IP pública propia y no compartida._

---
# Reglas en el firewall

Para finalizar nuestra configuración vamos a ir a _Network > Firewall_ y vamos a dejar la conexión de esta forma:

<img src="/images/posts/infraestructura/firewall.png" alt="firewall" class="sombra" />

Con esta configuración tendremos la __salida abierta__ pero la __entrada bloqueada__ hasta que lo configuremos expresamente en el siguiente post.

---
# DHCP y Static Leases

En este paso lo que vamos a hacer es familiarizarnos con la parte del router donde vamos a configurar el pool DHCP y las reservas de IPs estáticas para equipos concretos a los que queramos o necesitemos ponerles una IP fija. 

Lo primero es configurar la red LAN, que aunque ya estará con una configuración inicial activa, la vamos a repasar. Para ello vamos a _Network > Switch_ y editamos la red _LAN_. Vamos a la columna _DHCP Server_ y lo configuramos así:

<img src="/images/posts/infraestructura/dhcp1.png" alt="dhcp1" class="sombra" />

En este caso, la IP más pequeña del pool será 192.168.1.100 hasta el 192.168.1.250. Lo hago así para tener el rango desde 192.168.1.1 hasta el 192.168.1.100 libre para poder asignarlos como IPs estáticas.

En la pestaña _Advanced Settings_ lo único que tendremos que activar es el _Dynamic DHCP_ tal que así:

<img src="/images/posts/infraestructura/dhcp2.png" alt="dhcp2" class="sombra" />

Guardamos y aplicamos. Ahora vamos a _Network > DHCP and DNS_ y a la pestaña _Static Leases_ donde podremos añadir una __reserva de IP estática para nuestro servidor unraid__. Hacemos click en _add_ y seleccionamos nuestro servidor en _MAC address_, le ponemos un nombre de Host y escribimos la IP deseada:

<img src="/images/posts/infraestructura/static.png" alt="static" class="sombra" />

Guardamos y aplicamos. Para que el servidor coja la nueva IP tendremos que o bien quitarle el cable y volver a conectarlo o bien reiniciarlo.

---

Con esto ya tendríamos la red preparada para poder empezar a publicar servicios hacia el exterior. Lo único que queda pendiente es crear los Port Forwarding necesarios, algo que veremos en el [siguiente post](https://firstcommit.dev/2021/04/19/saas/) junto a la instalación y configuración de un proxy reverso y un servicio de DDNS. 

Más adelante profundizaremos en las opciones de OpenWRT instalando paquetes como un QoS para priorizar el tráfico de algunos servicios sobre otros, etc.


---
<a href="https://t.me/firstcommit" class="btn btn-primary text-white py-2">Suscríbete, que es gratis</a>

*Nota: algunos de los enlaces a productos o servicios pueden ser enlaces referidos con los que podemos obtener una comisión de venta.*

<style> 
img.sombra {
  display: block;  
  margin-left: auto;  
  margin-right: auto; 
  box-shadow: 5px 5px 8px grey;
}
img.sombra-inline {
  display: inline-block; 
  vertical-align: middle; 
  margin-right: 0px; 
  width:auto;
  box-shadow: 3px 3px 8px grey;
}
img.zoom {
  transition: transform .2s;
}
img.zoom:hover {
  -ms-transform: scale(1.2); /* IE 9 */
  -webkit-transform: scale(1.2); /* Safari 3-8 */
  transform: scale(1.2); 
}
</style>
