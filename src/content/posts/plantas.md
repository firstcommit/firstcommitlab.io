---
title: Autoriego inteligente de plantas mediante IoT, HomeAssistant, OpenWeatherMap y NodeRed
authors: [yayitazale]
date: 2021-07-14T22:10:00Z
image: /images/posts/plantas/banner.jpg
categories:
  - diy
tags:
  - monitoring
  - timeseries
  - influx
  - grafana
  - selfhosted
  - home assistant
  - guia
  - domotica
  - iot
  - zigbee
  - mqtt
  - bluetooth
  - plantas
  - node-red
  - openweathermap

---

Verano, sinónimo de vacaciones… Como habréis notado por la baja de frecuencia de posts, ya estamos en época estival. 

Las vacaciones que tanto nos gustan se convierten en un quebradero de cabeza para los que, como a mí, __nos encantan las plantas__. Una buena forma de solucionar cualquier problema es que algún vecino, amigo o familiar pase por tu casa de forma frecuente para regar las plantas que más sed tengan, pero si quieres evitar comprar una __botella de vino__ como agradecimiento, sigue leyendo.

Antes de empezar este post, voy a dar por supuesto que lees de forma habitual nuestro blog, [estás suscrito a nuestro canal de telegram](https://firstcommit.dev/Subscribe/) y que ya tienes [Home Assistant instalado](https://firstcommit.dev/2021/03/11/Hass/), así como un [broker MQTT](https://firstcommit.dev/2021/03/14/mqtt/), un [gateway ZigBee](https://firstcommit.dev/2021/03/19/zigbee/), una base de datos [InfluxDB](https://firstcommit.dev/2021/06/07/monitorizacion-servidores/#Bases-de-datos-de-tipo-timeseries-con-InfluxDB) y [Grafana](https://firstcommit.dev/2021/06/07/monitorizacion-servidores/#Graficas-y-mas-graficas-con-Grafana) en nuestro servidor [unraid](https://firstcommit.dev/2021/01/31/Unraid/).

(Foto de portada<a href="https://unsplash.com/es/@anniespratt?utm_content=creditCopyText&utm_medium=referral&utm_source=unsplash">Annie Spratt</a> en <a href="https://unsplash.com/es/fotos/planta-verde-en-maceta-de-ceramica-blanca-S7viz8JWxwY?utm_content=creditCopyText&utm_medium=referral&utm_source=unsplash">Unsplash</a>)

#### TOC
---
# Plantas sensorizadas

Me encantaría poder ofreceros cientos de alternativas pero la realidad actual es que __apenas existen sensores__ de plantas que sean __económicos, duraderos y fiables__. Existe la opción de que te los fabriques tú mismo usando unos [ESP](https://amzn.to/3BnByzD) y [sensores de humedad](https://amzn.to/3BskqZw), pero no es nada fiable y menos para su uso en exteriores.

Mi recomendación es que utilices los [sensores](https://amzn.to/3kKu5o7) o [macetas sensorizadas](https://amzn.to/3itD7TW) de __Xiaomi__, ya que los puedes encontrar __muy baratos__ si tienes paciencia y esperas a ofertas, son bastante __fiables__ y son __robustos__.

<img src="/images/posts/plantas/sensores1.jpg" alt="Sensores 1" />

Las __características principales__ de estos sensores son:

- __Pila de botón 2032__: con unas baterías desechables CR2032 podrás aguantar no más de 6 meses, ya que vamos a realizar lecturas periódicas cada hora. Te recomiendo que te hagas con unas [pilas recargables de litio LIR2032 y un cargador](https://es.aliexpress.com/item/32819533703.html?spm=a2g0o.detail.1000060.1.283a5c19oshLSE&gps-id=pcDetailBottomMoreThisSeller&scm=1007.13339.169870.0&scm_id=1007.13339.169870.0&scm-url=1007.13339.169870.0&pvid=0e8850e9-33d6-4cc6-80ef-d70ff036df30&_t=gps-id:pcDetailBottomMoreThisSeller,scm-url:1007.13339.169870.0,pvid:0e8850e9-33d6-4cc6-80ef-d70ff036df30,tpp_buckets:668%230%23131923%230_668%230%23131923%230_668%23888%233325%233_668%23888%233325%233_668%232846%238114%231999_668%235811%2327186%2372_668%236421%2330825%23436_668%232717%237558%23170_668%231000022185%231000066059%230_668%233468%2315607%2388_668%232846%238114%231999_668%235811%2327186%2372_668%236421%2330825%23436_668%232717%237558%23170_668%233164%239976%23157_668%233468%2315607%2388&&pdp_ext_f=%257B%2522scene%2522%253A%25223339%2522%257D), que aunque su duración es menor (unos 3 meses) ya que su densidad energética es menor, ecológicamente es más sostenible.
- __Bluetooth BLE 4.1__: consumos bajos y alto rango.
- __Sensor de humedad__: mide la conductividad de la tierra. Bastante preciso, aunque conviene mover el sensor de vez en cuando (cada vez que cambias la pila, por ejemplo) para que los valores sean más exactos ya que a veces se queda algo de tierra pegada a las bornas.
- __Sensor de fertilidad__: muy poco útil ya que depende mucho de la humedad de la tierra. Cuanta más humedad, mayor concentración de sales y por tanto mayor fertilidad. Yo personalmente no le hago caso, el abonado de las plantas lo hago siguiendo los ciclos de cada planta o árbol.
- __Sensor de temperatura__: podríamos utilizarlo para crear alertas en las plantas que no aguanten bien el frío, como los cítricos.
- __Sensor de luz__: podríamos usarlo para medir la intensidad de luz que reciben, pero en mi caso prefiero utilizar los pronósticos de tiempo junto a la humedad de la tierra para decidir si riego o no.
- __Sensor de batería__: nos dice cuando un sensor se queda sin batería (bueno, si está sin batería no nos podrás decir nada…no es muy útil)

Para poder comunicarnos con estos sensores, necesitaremos saber su [número MAC](https://es.wikipedia.org/wiki/Direcci%C3%B3n_MAC) bluetooth. Para poder conocerlo utilizaremos la aplicación [BLE Scanner](https://play.google.com/store/apps/details?id=com.macdom.ble.blescanner&hl=en_US&gl=US) en nuestro teléfono. 

Introducimos la pila en el sensor, lo colocamos al lado del teléfono y en el listado buscando por el _*Flower Care*_ con la señal más fuerte (el -XXdB más bajo) o el único que haya :) :

<img src="/images/posts/plantas/sensores2.png" alt="Sensores 2" class="sombra" />

Anotamos en un block de notas la planta en la vamos a colocar el sensor y la MAC correspondiente (y colocamos ya el sensor para no liarnos si vamos a configurar más de uno). Hacemos esto con todos los sensores que vayamos a colocar.

# MiFlora Daemon en Raspberry Pi

Casi seguro que tu servidor con unraid con pincho Bluetooth no tenga suficiente cobertura para cubrir la totalidad de tu casa/piso y tu terraza o jardín. Mi recomendación es que distribuyas una serie de [Raspberry Pi Zero W](https://amzn.to/3eH3lkA) por las diferentes estancias y cerca de las ventanas o puertas en caso de que quieras obtener datos de plantas del exterior.

<img src="/images/posts/plantas/daemon2.jpg" alt="Daemon 2" class="sombra" />

Estos microordenadores son de muy bajo consumo y puedes disimularlos fácilmente con 
unas pequeñas [cajas impresas en 3D](https://www.thingiverse.com/thing:1167846). 

Podrías incluso meterlos en una caja estanca y alimentarlos con una placa solar y una batería. Si haces esto último __por favor déjalo en los comentarios__ para que lo podamos replicar.

Bueno, vamos a ello. Para obtener los datos de los sensores vamos a instalar en cada una de las Raspberry un pequeño [script de Python](https://elpythonista.com/script-en-python) que irá preguntando a cada planta su estado con el intervalo que nosotros definamos y lo publicará por MQTT contra nuestro Broker. Esta publicación será recogida automáticamente por Home Assistant ya que configuraremos el topic de forma que la función __autodiscovery__ de Hass capte estos dispositivos sin intervención por nuestra parte.

Para poner en marcha la primera Raspberry, vamos a descargarnos la [imagen oficial de la distro de Raspberry Pi OS Lite](https://www.raspberrypi.org/software/operating-systems/). Una vez descargado, utilizando [Balena Etcher](https://www.balena.io/etcher/) vamos a “quemar” la imagen en una [tarjeta microSD](https://amzn.to/3BtBaj2) (te recomiendo que utilices algo decente y de unos 16gb):

<img src="/images/posts/plantas/daemon1.gif" alt="Daemon 1" class="sombra" />

Al finalizar la instalación, en la __carpeta boot__ de la tarjeta microSD añadimos estos dos archivos. 

- [Este](/images/posts/plantas/ssh) sirve para habilitar la conexión remota por SSH.

- [Este](/images/posts/plantas/wpa_supplicant.conf) archivo sirve para que la Raspberry se conecte de forma automática a nuestra WiFi. Para ello debes cambiar la SSID y contraseña con las tuyas:

<img src="/images/posts/plantas/daemon3.png" alt="Daemon 3" class="sombra" />

Introducimos la tarjeta microSD en la Raspberry y enchufamos el microordenador a la corriente con un adaptador micro USB.

Ahora vamos a [nuestro router](https://firstcommit.dev/2021/04/11/infraestructura/#Instalar-OpenWRT) y vamos a buscar la IP de la Raspberry que se acabará de unir a la red.

Abrimos [nuestro editor de código VsCode](https://firstcommit.dev/2021/03/29/VScode-01-instalacion/) e instalamos la extensión [SSH Client](https://marketplace.visualstudio.com/items?itemName=cweijan.vscode-ssh), así podremos conectarnos de forma remota y modificar los archivos de la Raspberry sin salir de nuestro editor preferido:

<img src="/images/posts/plantas/daemon4.png" alt="Daemon 4" class="sombra" />

Añadimos la configuración de SSH y nos conectamos:

<img src="/images/posts/plantas/daemon5.png" alt="Daemon 5" class="sombra" />

- Name: el nombre para saber que raspi es
- Host: la IP de la raspi
- Port: el puerto ssh, el 22 por defecto
- Username: pi
- Password: raspberry

Si hacemos click en el nombre accederemos al explorador de archivos remoto de la raspberry. Si hacemos click en el icono de la consola, abrimos una sesión remota. 

<img src="/images/posts/plantas/daemon6.png" alt="Daemon 6" class="sombra" />

Lo primero que vamos a hacer en la consola SSH es actualizar la raspberry mediante los siguientes comandos:

```bash
sudo apt update
sudo apt upgrade
```

También es conveniente que cambies la clave SSH, para ello hacemos:

```bash
sudo raspi-config
```

Y en el menú vamos a *System Options* > *Password*, tecleamos la contraseña por defecto “raspberry” y la cambiamos por la que más nos guste, usando por ejemplo el generador de contraseñas de nuestro gestor de contraseñas (y aprovechamos para guardar los datos de acceso):

<img src="/images/posts/plantas/daemon7.png" alt="Daemon 7" class="sombra" />
<img src="/images/posts/plantas/daemon8.png" alt="Daemon 8" class="sombra" />

Reiniciamos con:

```bash
sudo reboot
```

Y en VsCode, en la configuración de conexión de esta Raspberry __cambiamos la contraseña__ de SSH que habíamos puesto por la nueva que acabamos de meter.

Ahora sí, __ya podemos instalar Miflora Daemon__. Los pasos son los que aparece en la [documentación de GitHub del proyecto](https://github.com/ThomDietrich/miflora-mqtt-daemon):

Instalamos las dependencias y librerías necesarias y descargamos el proyecto:

```bash
sudo apt install git python3 python3-pip bluetooth bluez

git clone https://github.com/ThomDietrich/miflora-mqtt-daemon.git /opt/miflora-mqtt-daemon

cd /opt/miflora-mqtt-daemon
sudo pip3 install -r requirements.txt
```

Creamos un archivo de configuración copiando el de por defecto:

```bash
cp /opt/miflora-mqtt-daemon/config.{ini.dist,ini}
```

Ahora, en el explorador de archivos remoto de VsCode vamos a la carpeta _*/opt/miflora-mqtt-daemon*_ de nuestra raspi y con doble click abrimos el archivo para editarlo:

<img src="/images/posts/plantas/daemon9.png" alt="Daemon 9" class="sombra" />

Los cambios a realizar son:

El metodo de reporte lo ponemos a homeassistant mqtt, así funcionará el autodiscovery:

```ini
[General]
reporting_method = homeassistant-mqtt 
```

Hacemos que el script funcione de forma ininterrumpida con una frecuencia de lectura de los sensores de 3600s (1 hora) por ejemplo:

```ini
[Daemon]
enabled = true
period = 3600
```

Añadimos los datos de nuestro servidor MQTT y el juego de user/pass que hayamos creado para este acceso:

```ini
[MQTT]
hostname = la ip del broker MQTT
port = puerto del broker MQTT, por defecto 1883
username = usuario creado en el Broker para este equipo
password = el password para este usuario en el Broker MQTT
```

Y finalmente añadimos los sensores de las plantas que tenga que leer esta raspberry, añadiendo como nombre el que queramos que aparezca en Home Assistant y a su lado el número MAC del sensor:

```ini
[Sensors]

cerezo = C4:7C:8D:6A:XX:XX 
manzano = C4:7C:8D:6A:XX:XX 
dracaena = C4:7C:8D:67:XX:XX 
orquidea = C4:7C:8D:66:XX:XX 
hedera = C4:7C:8D:6A:XX:XX
```

Guardamos el archivo y vamos a probar que todo funcione. Para eso vamos a lanzar los comandos:

```bash
python3 /opt/miflora-mqtt-daemon/miflora-mqtt-daemon.py
```
<img src="/images/posts/plantas/daemon10.png" alt="Daemon 10" class="sombra" />

Y ahora esperamos que no arroje errores. Si todo va bien, iremos viendo cómo el script hace lo que se supone que debe hacer, ir preguntando a cada planta su estado y publicar esos datos por MQTT. SI todo va bien, podremos ir a Home Assistant y en la lista de entidades veremos que se han creado las entidades correspondientes a cada planta y que los datos de cada sensor han llegado:

<img src="/images/posts/plantas/daemon11.png" alt="Daemon 11" class="sombra" />

De nuevo en la consola, paramos el script con ctrl+c y ahora convertimos el script en un servicio que funcione solo y que arranque solo al reiniciar el aparato haciendo lo siguiente:

```bash
sudo cp /opt/miflora-mqtt-daemon/template.service /etc/systemd/system/miflora.service

sudo systemctl daemon-reload

sudo systemctl start miflora.service
sudo systemctl status miflora.service

sudo systemctl enable miflora.service
```

# InfluxDB y Grafana en Home Assistant

En este momento los datos generados por las plantas son suficientes para pasar directos a la automatización del [siguiente paso](#Pronosticos-de-lluvia-con-IA-con-OpenWeatherMap), pero ya que tenemos instalada la base de datos de Influxdb y Grafana, con muy poco esfuerzo vamos a aprovecharlo y visualizar de forma automática los valores de estos sensores en una gráficas más chulas que las que nos ofrece Hass.

Vamos a nuestra interfaz de [nuestro Chronograf](https://firstcommit.dev/2021/06/07/monitorizacion-servidores/#Bases-de-datos-de-tipo-timeseries-con-InfluxDB) y creamos un __bucket__ llamado _*hass*_ con la política de retención que queramos (en mi caso, 7 días) y creamos un __token con acceso__ en escritura a este bucket:

<img src="/images/posts/plantas/influx1.png" alt="Influx 1" class="sombra" />

Para empezar a historizar los datos de las plantas y el resto de sensores de hass en nuestro influxDB, solo debes añadir lo siguiente a tu archivo de configuración de home assistant:

```yml
influxdb:
  api_version: 2
  ssl: false
  host: ip del servidor
  port: 8086
  token: token_creado
  organization: main
  bucket: hass
  tags:
    source: HA
  tags_attributes:
    - friendly_name
  default_measurement: units
  include:
    domains:
      - sensor
```

Ahora en grafana, con otro token en lectura, deberás añadir este bucket como nuevo origen de datos. Para ayudarte un poco, he creado [este dashboard](/images/posts/plantas/Plants.json) para ti que te permitirá ver los valores de todos los sensores de todas las plantas que tengas configuradas de forma automática:

<img src="/images/posts/plantas/influx2.png" alt="Influx 2" class="sombra" />

# Pronósticos de lluvia con IA con OpenWeatherMap

Utilizando únicamente los datos de los sensores de las plantas nos puede ocurrir que realicemos un riego cuando ya está lloviendo o está a punto de hacerlo, corriendo el riesgo de __ahogar nuestras plantas__ (de hecho, mi experiencia es que la mayoría de plantas toleran mejor la falta que el exceso de riego).

Para tener unos pronósticos de lluvias fiables vamos a implementar el plugin the [OpenWeatherMap](https://openweathermap.org/) en Hass, que es un servicio gratuito(con limitaciones) con [API pública](https://openweathermap.org/api) que utiliza modelos de Inteligencia Artificial para, en base a las mediciones de estaciones de tiempo profesionales de cada zona, ofrecer un pronóstico muy certero del tiempo en las siguientes horas.

<img src="/images/posts/plantas/weather1.png" alt="Weather 1" class="sombra" />

Para poder empezar a utilizar este servicio debemos crearnos una cuenta en su página web:

<img src="/images/posts/plantas/weather2.png" alt="Weather 2" class="sombra" />

En el apartado de API Keys, creamos una:

<img src="/images/posts/plantas/weather3.png" alt="Weather 3" class="sombra" />

Ahora en Home Assistant vamos a Configuración > Integraciones y añadimos la integración de OpenWeatherMap:

<img src="/images/posts/plantas/weather4.png" alt="Weather 4" class="sombra" />

Añadimos nuestra API token ya que la latitud y longitud las cogerá automáticamente de nuestra configuración de Hass y dejamos la llamada en **onecall_daily** ya que nuestra cuenta/token es gratuita y tiene la cantidad de llamadas diarias limitada si no queremos pagar:

<img src="/images/posts/plantas/weather5.png" alt="Weather 5" class="sombra" />

Esto nos creará una serie de sensores con el tiempo actual y los pronósticos:

<img src="/images/posts/plantas/weather6.png" alt="Weather 6" class="sombra" />

y una entidad de tipo Weather que podremos usar para poner bonito nuestro dashboard de Home Assistant:

<img src="/images/posts/plantas/weather7.png" alt="Weather 7" class="sombra" />

# Riego automático con ZigBee

De la misma forma que podemos [automatizar las luces de casa con Zigbee2MQTT](https://firstcommit.dev/2021/03/19/zigbee/) vamos a automatizar el riego de nuestras plantas creando un [control avanzado de lazo cerrado](https://es.wikipedia.org/wiki/Sistema_de_control#Sistema_de_control_de_lazo_cerrado). 

Para hacerlo de forma segura y barata lo mejor es adquirir un aparato como los que vende [Lidl](https://www.lidl.es/es/programador-de-riego-inteligente-smart-home/p21580?searchTrackingQuery=riego+parkside&searchTrackingId=Product.21580&searchTrackingPos=12&searchTrackingOrigPos=10&searchTrackingRelevancy=87.28&searchTrackingPage=1&searchTrackingPageSize=36&searchTrackingOrigPageSize=36) (agotadísimo en todas partes hasta el verano que viene) o este de [Woox](https://amzn.to/3zlOtAa) (que vale el doble...). En ambos casos __funcionarán automáticamente__ si los emparejamos con nuestro Zigbee2MQTT, exactamente igual que como funciona cualquier enchufe o bombilla ZigBee.

<img src="/images/posts/plantas/riego1.webp" alt="Riego 1"/>

Para montar el circuito de riego no necesitas conocimientos avanzados de hidráulica, solo un poco de maña y de lógica. Yo he optado por pasar por la tienda de la cadena __Leroy Merlin__ más cercana ya que tienen una sección dedicada al riego por goteo en el que encontrarás todo lo que necesitas. 

Mi sistema consiste en un [tubo principal de distribución de 16mm de diámetro](https://www.leroymerlin.es/fp/17063333/tuberia-riego-por-goteo-16-mm-50-m) (tubo gordo, mucho caudal) del cual cuelgan pequeños [tubos de 4mm](https://www.leroymerlin.es/fp/14115514/microtubo-de-25-metros) (tubos finos, poco caudal y más presión) que llegan a cada maceta y expulsan agua con un [difusor/gotero regulable](https://www.leroymerlin.es/fp/82324398/gotero-regulable-estaca). De esta manera puedo regular en cada riego la cantidad de agua dependiendo del tamaño de la maceta y las necesidades de agua generales de cada planta.

Esto es importante tenerlo en cuenta ya que el riego que vamos a implementar será de un __único canal__, esto es, cuando alguna de las plantas necesite agua __tendremos que regarlas todas__. 

# Node-Red, la aplicación definitiva para automatizaciones

Como vimos en el [post inicial de Home Assistant](https://firstcommit.dev/2021/03/11/Hass/), crear automatizaciones simples es fácil con su interfaz web, pero si la cosa se complica con muchos triggers, condicionantes, etc deja de ser amigable ya que la propia interfaz nos complica mucho las cosas.

Por suerte para nosotros, existe una herramienta de programación visual por bloques llamada [Node-Red](https://nodered.org/) con el cual podremos hacer prácticamente cualquier cosa que se nos ocurra:

<img src="/images/posts/plantas/nodered1.png" alt="Node red 1" class="sombra" />

Esta herramienta se está poniendo cada vez más de moda en el mundo del IoT ya que es muy flexible e intuitivo para usar y además es muy robusto en su funcionamiento.

Para instalar Node-Red en nuestro unraid debemos ir como siempre a la pestaña _*apps*_ y buscamos la aplicación con el mismo nombre:

<img src="/images/posts/plantas/nodered2.png" alt="Node red 2" class="sombra" />

En la plantilla a priori no hace falta cambiar nada, pero ojo, ya que si seguiste los pasos del [tutorial de instalación del Proxy Reverso](https://firstcommit.dev/2021/04/19/saas/), el puerto 1880 lo tendrás ya ocupado por dicho mircroservicio por lo que deberás cambiarlo a otro, como por ejemplo, el 2880:

<img src="/images/posts/plantas/nodered3.png" alt="Node red 3" class="sombra" />

Una vez instalado vamos a la interfaz web http://ip-de-unraid:2880 y veremos esto:

<img src="/images/posts/plantas/nodered4.png" alt="Node red 4" class="sombra" />

En la parte izquierda vemos los bloques que podemos añadir, a la derecha el entorno de configuración y en el centro el lienzo donde crear nuestros flujos.

Para que entienda cómo funciona Node-Red lo mejor es hacer primero un pequeño flujo muy simple. Selecciona un bloque _*Inject*_ y arrastralo al lienzo. Después hacemos lo mismo con un bloque _*debug*_. Ahora conectamos los dos extremos con un “cable” y hacemos clic en _*Deploy*_ arriba a la derecha:

<img src="/images/posts/plantas/nodered5.png" alt="Node red 5" class="sombra" />

Para ponernos en modo debug, hacemos click en el icono del _*bug*_ en el lado derecho. Ahora, si hacemos click en el botón de _*inject*_. Veremos que en el _*debugger*_ aparece esto:

<img src="/images/posts/plantas/nodered6.png" alt="Node red 6" class="sombra" />

Como ves, Node-Red funciona haciendo viajar la información en forma de mensajes desde el bloque que inicia el flujo hasta el último de forma dirigida, esto es, cada bloque con salidas emitirá un topic/payload, y los bloques con entradas harán caso a topics específicos de entrada. 

Vamos a crear un pequeño flujo que nos permita leer el valor de un sensor de una de las plantas que tenemos ya en Home Assistant y dependiendo de su valor activaremos el riego. Lo vamos a hacer en cuatro pasos:

## Instalar el palette

Lo primero que vamos a hacer es instalar los bloques de Home Assistant ya que no vienen instalados en Node-Red por defecto. Para ello vamos a _*manage palette*_:

<img src="/images/posts/plantas/nodered7.png" alt="Node red 7" class="sombra" />

Pestaña _*install*_, buscamos “node-red-contrib-home-assistant-websocket” y lo instalamos:

<img src="/images/posts/plantas/nodered8.png" alt="Node red 8" class="sombra" />

Ahora ya aparecerán los bloques azules de Hass en el lado izquierdo:

<img src="/images/posts/plantas/nodered9.png" alt="Node red 9" class="sombra" />

## Creamos un token de acceso a la API de home assistant

En Hass, accedemos a nuestro perfil de usuario y abajo del todo generamos un token:

<img src="/images/posts/plantas/nodered10.png" alt="Node red 10" class="sombra" />

Guardamos el token en un bloc de notas para el siguiente paso.

## Señal de la planta

Cogemos un bloque _*Poll state*_ que lo que hará es iniciar un flujo con una frecuencia específica que nosotros indiquemos emitiendo en el mensaje el valor del sensor. Hacemos doble click sobre el bloque para que nos abra el configurador y donde pone _*Add new server*_ hacemos click en el lápiz y rellenamos los datos con la IP local de hass y puerto, el token que acabamos de generar:

<img src="/images/posts/plantas/nodered11.png" alt="Node red 11" class="sombra" />

Aceptamos y ahora nos volverá a mostrar la configuración del bloque _*poll state*_. Aquí vamos a buscar el sensor de humedad de nuestra planta, cambiamos la frecuencia a una hora, indicamos que solo nos devuelva el valor del sensor si es menor al valor numérico mínimo que queramos y seleccionamos que nos devuelva un valor al hacer deploy.

<img src="/images/posts/plantas/nodered12.png" alt="Node red 12" class="sombra" />

## Activación del riego

Para el aparato de riego, vamos a añadir un bloque de _*call service*_ y lo rellenamos con los datos del switch que nos ha generado Zigbee2MQTT al emparejar el aparato de riego:

<img src="/images/posts/plantas/nodered13.png" alt="Node red 13" class="sombra" />

Juntamos el extremo del _*Poll*_ con el del _*Call*_ y hacemos _*Deploy*_. En ese momento y cada hora, si se cumplen las condiciones de que el valor del sensor sea menor que el que hayamos fijado, se iniciará el riego.

De acuerdo, esto no tiene nada de inteligente. No estamos usando ni OpenWeatherMaps ni nada de lo que hemos preparado en los pasos anteriores. Bien, para el siguiente paso vas a tener que instalar el palettes “node-red-contrib-combine” que nos servirá para hacer operaciones lógicas booleanas de tipo AND, OR, XOR etc. además de poder sacar deltas y tener un gang-bang (histéresis).

Una vez instalado el palette, ya puedes importar el [flow que yo te he preparado](/images/posts/plantas/flows.json) para facilitarte el trabajo:

<img src="/images/posts/plantas/nodered14.png" alt="Node red 14" class="sombra" />

Una vez importado, tendrás que modificar el servidor de Hass haciendo click en alguno de los bloques azules, y revisar que cada uno de los sensores indicados coincide con alguno tuyo. El flujo como ves, se activa tres veces al día y si no hay previsiones de lluvia y alguno de los sensores indica falta de agua, se activa el riego. Además he incluido un control de temperatura para evitar activar el riego si la temperatura es inferior a 0 grados ya que corremos el riesgo de romper nuestro circuito de riego si se ha congelado.

---

A partir de aquí ya es cuestión de que juegues un poco con Node-Red para dejar el flujo a tu gusto, y si te atreves, ir migrando las automatizaciones de luces etc que tengas hechas a esta nueva herramienta que es mucho más amigable.

Aqui te dejo algunos ejemplos de mis automatizaciones actuales:

<img src="/images/posts/plantas/nodered15.png" alt="Node red 15" class="sombra" />
<img src="/images/posts/plantas/nodered16.png" alt="Node red 16" class="sombra" />
<img src="/images/posts/plantas/nodered17.png" alt="Node red 17" class="sombra" />

---
<a href="https://t.me/firstcommit" class="btn btn-primary text-white py-2">Suscríbete, que es gratis</a>

*Nota: algunos de los enlaces a productos o servicios pueden ser enlaces referidos con los que podemos obtener una comisión de venta.*

<style> 
img.sombra {
  display: block;  
  margin-left: auto;  
  margin-right: auto; 
  box-shadow: 5px 5px 8px grey;
}
img.sombra-inline {
  display: inline-block; 
  vertical-align: middle; 
  margin-right: 0px; 
  width:auto;
  box-shadow: 3px 3px 8px grey;
}
img.zoom {
  transition: transform .2s;
}
img.zoom:hover {
  -ms-transform: scale(1.2); /* IE 9 */
  -webkit-transform: scale(1.2); /* Safari 3-8 */
  transform: scale(1.2); 
}
</style>
