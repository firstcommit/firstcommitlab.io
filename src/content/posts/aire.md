---
title: "Seguimiento de la calidad del aire con WAQI y software libre"
authors: [yayitazale]
date: 2022-07-17T21:00:00Z
image: /images/posts/aire/intro.jpg
categories:
  - selfhosted
tags:
  - guia
  - waqi
  - air
  - quality
  - monitoring

---
Con esta ola de calor que estamos viviendo en toda Europa, la calidad del aire que respiramos está empeorando de forma notable. En este artículo os voy a explicar cómo podemos monitorizar la calidad de aire de nuestro entorno sin una sola línea de código, utilizando software libre y la plataforma [WAQI](https://waqi.info/) (World's Air Pollution: Real-time Air Quality Index), una plataforma abierta que se nutre de los datos públicos de millones de estaciones de medición de calidad del aire por todo el planeta, y software libre (NodeRed, InfluxDB y Grafana).

En mi caso, el ejemplo se centra en la estaciones de Euskadi, ya que es donde vivo yo, pero es aplicable a cualquier región y país del mundo.

(Foto de portada <a href="https://unsplash.com/es/@pwphotography?utm_content=creditCopyText&)utm_medium=referral&utm_source=unsplash">Peter Werkman</a> en <a href="https://unsplash.com/es/fotos/una-chimenea-emite-desde-la-parte-superior-de-un-edificio-QNOWmmOi4es?utm_content=creditCopyText&utm_medium=referral&utm_source=unsplash">Unsplash</a>)

#### TOC

# Calima 

Lo primero que vamos a entender antes de empezar a instalar y configurar servicios es por qué ocurre la calima:

1. Una masa de aire caliente en diferentes capas atmosféricas procedente normalmente de los desiertos africanos se desplaza hacia el norte llegando a Europa. En esta ocasión esto ocurre debido a la presencia de un [anticiclón](https://es.wikipedia.org/wiki/Anticicl%C3%B3n) potente a la altura de las Azores que rota en sentido contrario a las agujas del reloj, unido a una borrasca en el norte que vuelve a empujar el aire caliente hacia el centro de Europa donde tenemos otro anticiclón absorbiendo esta masa de aire caliente. 


<div id="banner" style="overflow: hidden;justify-content:space-around;">
    <div class="" style="max-width: 49%;max-height: 50%;display: inline-block;">
        <img src="/images/posts/aire/satelite_aire.jpg">
    </div>
    <div class="" style="max-width: 49%;max-height: 50%;display: inline-block;">
        <img src="/images/posts/aire/satelite_isobaras.png">
    </div>
</div>


2. Esta masa de aire producida por el trío anticiclón/borrasca/anticiclón queda bloqueado por las [corrientes de chorro polar y subtropical](https://es.wikipedia.org/wiki/Corriente_en_chorro).
3. La falta de circulación de aire atrapa en este bucle los [gases de efecto invernadero](https://es.wikipedia.org/wiki/Efecto_invernadero) creando así un efecto invernadero (efectivamente, es el efecto que generan) 
4. El calor absorbido por la capa superficial de la tierra por la [luz visible del sol](https://es.wikipedia.org/wiki/Espectro_visible) que es capaz de atravesar esta capa de gases en su dirección a la tierra es devuelta hacia la [troposfera](https://es.wikipedia.org/wiki/Troposfera) en forma de [radiación térmica](https://es.wikipedia.org/wiki/Radiaci%C3%B3n_t%C3%A9rmica).
5. Está longitud de onda no visible no es capaz de atravesar la capa de gases, rebotando y volviendo de nuevo a la superficie de la tierra y haciendo que la temperatura aumente aún más.

<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/5/59/Sun_climate_system_alternative_%28Spanish%29_2008.svg/1920px-Sun_climate_system_alternative_%28Spanish%29_2008.svg.png" alt="efecto invernadero" class="sombra" />

6. Además a esto debemos sumar que por las noches ocurre una [inversión térmica](https://www.youtube.com/watch?v=-HXt_BIoNRk), dado que la capa superficial de la tierra se enfría más rápido que la parte alta de la troposfera y esto hace que la contaminación emitida durante el día no pueda disiparse a la estratosfera de noche ya que queda atrapada por debajo de una capa de aire más caliente y por tanto menos densa que la de abajo.
7. A todo esto hay que sumar que con este aumento de temperatura le sigue un riesgo extremo de incendios forestales que de ocurrir, aumentan aún más la cantidad de gases de efecto invernadero que se acumulan en la capa alta de la troposfera.
8. Finalmente, el aumento de las temperaturas trae además un aumento de l consumo eléctrico por el uso de los aires acondicionados, lo cual suele  generar un aumento de las emisiones por quema de gas y otro combustibles fósiles para abastecer la demanda electrica. 

<a href="https://www.ree.es/es/datos/demanda/evolucion?start_date=2022-07-04T00:00&end_date=2022-07-17T23:59&time_trunc=day&systemElectric=peninsular"><img src="/images/posts/aire/demanda_electrica.png" alt="pwoned" class="sombra" /></a>

# Requisitos

Ahora que hemos entendido cómo se genera esta situación vamos a ver los requisitos mínimos para seguir este post. 

Lo primero es contar con un servidor doméstico e idealmente [el sistema operativo Unraid](https://firstcommit.dev/2021/01/31/Unraid/). También podéis montar esto sobre una placa tipo Raspberry PI pero no quiero extenderme demasiado por lo que no voy a entrar a explicar los detalles de cómo instalar y utilizar Docker, ni los servicios que vamos a utilizar sobre RPI. En Unraid tenemos todo preparado, por lo que los pasos a seguir son mucho más sencillos. 

En segundo lugar necesitaremos tener instalado el stack de software libre con el que podemos monitorizar prácticamente lo que se nos ocurra. Como ya he explicado esto en anteriores posts, os dejo aquí en enlace para que aprendáis a instalar y configurar los tres elementos que necesitamos (como decía también puedes instalar esto por tu cuenta en una RPI):

- [NodeRed](https://firstcommit.dev/2021/07/14/plantas/#Node-Red-la-aplicacion-definitiva-para-automatizaciones)
- [InfluxDB](https://firstcommit.dev/2021/06/07/monitorizacion-servidores/#Bases-de-datos-de-tipo-timeseries-con-InfluxDB)
- [Grafana](https://firstcommit.dev/2021/06/07/monitorizacion-servidores/#Graficas-y-mas-graficas-con-Grafana)

# Base de datos TimeSeries en InfluxDB

Para este proyecto vamos a necesitar crear dos bases de datos en [InfluxDB](https://www.influxdata.com/), uno para el almacenamiento de los datos en bruto (horarios) que obtendremos de WAQI y otro para el almacenamiento de datos agregados (diarios). De esta manera podremos realizar una visualización detallada de los últimos 15 días y al mismo tiempo una visualización de tendencias a más largo plazo de forma dinámica y sin perder apenas rendimiento.

Para ello entramos a nuestro chronograf y vamos a Buckets:

<img src="/images/posts/aire/influxdb_bucket_create.png" alt="influxdb bucket create" class="sombra" />

Aquí creamos uno llamado _*Air*_ con la siguiente configuración, con la que los datos de más de 14 días serán eliminados automaticamente:

<img src="/images/posts/aire/influx_bucket_create_1.png" alt="influxdb create bucket 1" class="sombra" />

Y otro con la una configuración diferente llamada *Air_1d* donde no tendremos restricción de almacenamiento por fecha:

<img src="/images/posts/aire/influx_bucket_create_2.png" alt="influxdb create bucket 2" class="sombra" />

Ahora vamos a crear un token de acceso para Node-Red:

<img src="/images/posts/aire/influx_nodered_token.png" alt="influx nodered token create" class="sombra" />

Y lo anotamos en un bloc de notas para poder usarlo posteriormente.

Para terminar con la configuración de la base de datos, vamos a crear un proceso repetitivo donde cogeremos los datos de la base de datos _*Air*_ que tendrán una resolución horaria, les bajaremos la resolución a un día y los almacenaremos en la base de datos *Air_1d*. Para ello vamos al icono del calendario y hacemos clic en create task:

<img src="/images/posts/aire/influx_task_create.png" alt="influx task create" class="sombra" />

Le ponemos un nombre y metemos el siguiente código, que se ejecutará cada 7 días, cogerá los datos horarios de _*Air*_ desde hace 15 días a hace 7 días, les aplicará una reducción de la medía diaria y los almacenará en *Air_1d*:
```
option task = {name: "Air downsample", every: 7d}

// Defines a data source
data =
    from(bucket: "air")
        |> range(start: -duration(v: int(v: task.every) * 2))
        |> filter(fn: (r) => r._measurement == "iaqi")
        |> filter(
            fn: (r) =>
                r["_field"] == "aqi" or r["_field"] == "co" or r["_field"] == "h" or r["_field"] == "lat" or r["_field"]
                    ==
                    "lon" or r["_field"] == "no2" or r["_field"] == "o3" or r["_field"] == "p" or r["_field"] == "pm10"
                    or
                    r["_field"] == "pm25" or r["_field"] == "so2" or r["_field"] == "t" or r["_field"] == "w"
                    or
                    r["_field"] == "wg",
        )

data
    // Windows and aggregates the data in to 1d averages
    |> aggregateWindow(fn: mean, every: 1d)
    // Stores the aggregated data in a new bucket
    |> to(bucket: "air_1d", org: "main")
```
Guardamos y activamos la tarea:

<img src="/images/posts/aire/influx_task_activate.png" alt="influx task activate" class="sombra" />

# Datos de calidad del aire desde WAQI con NodeRed

WAQI es un servicio que nace del proyecto [Air Quality Index China](https://aqicn.org) que después de varios años monitorizando la calidad del aire del país asiático, se abrió al mundo. La calidad de aire de esta plataforma se basa en valores indexados de las diferentes estaciones de medición que puede haber en el mundo. Esto quiere decir que los valores medidos en bruto por los sensores pueden tener diferentes escalas y/o unidades de medida, por lo que se convierte la medición de cada estación a [un valor numérico estandar sin unidad de medida](https://aqicn.org/scale/):

<img src="/images/posts/aire/waqi_index.png" alt="waqi index" class="sombra" />

En el caso de Euskadi, los datos de las estaciones de calidad de aire los obtienen desde la [API publica de OpenData Euskadi](https://www.opendata.euskadi.eus/catalogo/-/calidad-aire-en-euskadi-2022/), la plataforma de datos públicos y transparencia del Gobierno Vasco.

Esta plataforma integra datos de todos los [medidores de calidad del aire instalador por el Gobierno Vasco por toda la geografía vasca](https://www.euskadi.eus/web01-a2ingair/es/contenidos/informacion/red_calidad_aire/es_def/index.shtml) de los que se ofrecen medias horarias y diarias. Podríamos [utilizar directamente los datos de estos medidores](https://www.opendata.euskadi.eus/contenidos/ds_informes_estudios/calidad_aire_2022/es_def/adjuntos/index.json) pero la idea es que este artículo sea replicable por cualquiera, por lo que utilizaremos la API publica de WAQI en el cual podremos encontrar los datos de la mayoría de estaciones de calidad del aire de cualquier punto de país o del mundo.

(Por apuntar un detalle, estas estaciones son mantenidas y gestionadas por mi actual empresa, [MSI Grupo](https://www.msigrupo.com/), el cual recolecta los datos de forma automática y los envía al Gobierno Vasco en tiempo real)

Para poder capturar datos desde la API de WAQI tenemos [toda la documentación publica](https://aqicn.org/json-api/doc/#api-_), pero yo ya he creado unos flujos para ayudaros. Lo primero es [crearnos una cuenta](https://aqicn.org/data-platform/token/) para generar un token de acceso a la API:

<img src="/images/posts/aire/WAQI_API.png" alt="WAQI API" class="sombra" />

Copiamos el token en el bloc de notas para no perderlo y ahora debes descargarte los siguientes ficheros:

- [Flujo de NodeRed](https://gitlab.com/yayitazale/air-monitoring-with-waqi-nodered-influx-grafana/-/raw/main/Node-Red%20Files/flows.json?inline=false)
- [Fichero de configuración](https://gitlab.com/yayitazale/air-monitoring-with-waqi-nodered-influx-grafana/-/raw/main/Node-Red%20Files/air-config.json?inline=false)

En NodeRed, vamos a instalar la liberia _*node-red-contrib-influxdb*_ para poder subir los datos directamente a  las bases de datos que hemos creado, para ello vamos al _*menu principal > Manage Pallete > Install*_ y buscamos la librería e instalamos:

<img src="/images/posts/aire/nodered_menu_pallete.png" alt="nodered menu pallete" class="sombra" />
<img src="/images/posts/aire/nodered_influx.png" alt="nodered influx" class="sombra" />

Cuando termine importamos el flow que he creado haciendo clic en el _*menú > Import > Select a file to import*_, os cargara el JSON y lo importais:

<img src="/images/posts/aire/nodered_menu_import.png" alt="nodered menu import" class="sombra" />
<img src="/images/posts/aire/nodered_import.png" alt="nodered import" class="sombra" />

En este flow tendremos que modificar dos cosas:

<img src="/images/posts/aire/nodered_flow.png" alt="nodered flow" class="sombra" />

- La caja IAQI es el conector a influx, hacemos doble click y donde poner Air Bucket, hacemos clic en el lapiz y configuramos tanto la IP/Puerto de nuestro influx, y metemos el token que hemos generado:

<img src="/images/posts/aire/nodered_influx_config_1.png" alt="nodered influx config 1" class="sombra" />
<img src="/images/posts/aire/nodered_influx_config_2.png" alt="nodered influx config 1" class="sombra" />

- En la caja Set URLs and Token tendremos que meter el token que nos ha proporcionado WAQI al hacer la inscripción.

<img src="/images/posts/aire/nodered_waqi_API.png" alt="nodered waqi API" class="sombra" />

Ahora, antes de terminar, tendremos que configurar usando el fichero de configuración, las estaciones de las que queremos coger información.

Si abres el fichero con un editor de código, podrás ver que se conforma de la siguiente manera: se añade un bloque por cada región, y dentro de cada bloque de región las estaciones concretas de las que queremos sacar datos. Los nombres de las regiones y las estaciones las utilizaremos después en Grafana como filtros dinámicos ya que serán las etiquetas de las variables que vamos a guardar en Influx. Para conocer los nombres de las estaciones lo más sencillo es utilizar el [buscador web de WAQI](https://waqi.info/) y copiar y pegar el nombre directamente. Ojo, es importante que mantengas la estructura del JSON respetando las tabulaciones, corchetes, comas y llaves:

<img src="/images/posts/aire/config_file.png" alt="config file" class="sombra" />

Una vez tengas tu fichero de configuración a tu gusto, debes dejarlo en la ruta _*\files*_ del contenedor, para lo cual en el navegador de ficheros de tu ordenador vamos a *IPUNRAID\appdata\nodered\files* (o donde hayas mapeado tu carpeta de nodered):

<img src="/images/posts/aire/nodered_config_file.png" alt="nodered config file" class="sombra" />

Ahora volvemos a NodeRed y hacemos deploy, y ya tenemos nuestro sistema de captura de datos en marcha.

# Dashboarding en Grafana

Para ayudarnos con la visualización vamos a utilizar Grafana, tal y como hicimos en el [post sobre monitorización de servidores](https://firstcommit.dev/2021/06/07/monitorizacion-servidores/). De nuevo como aquella vez, tendremos que crear un token de acceso en lectura a las bases de datos Air y Air_1d en Chronograf para Grafana (ya te explicado como hacerlo).

Ahora vamos a crear un nuevo Dashboard en Grafana:

<img src="/images/posts/aire/grafana_nuevo_dashboard.png" alt="grafana nuevo dashboard" class="sombra" />

Y antes de dibujar nada, vamos a crear las variables sobre las etiquetas de región y estación que vamos a tener en influx. Para ellos vamos a _*configuración > Variables > Create Variable*_:

<img src="/images/posts/aire/grafana_variables_1.png" alt="grafana variables 1" class="sombra" />

La primera variable que vamos a crear es el de región, al que yo he llamado Provincia, usando la siguiente query de exploración de etiquetas de InfluxDB:

<img src="/images/posts/aire/grafana_variable_1.png" alt="grafana variable 1" class="sombra" />

```flux
import "influxdata/influxdb/schema"

schema.tagValues(bucket: "air", tag: "reg")
```

Guardamos y creamos otra, a la que yo he llamado Municipio:

<img src="/images/posts/aire/grafana_variable_2.png" alt="grafana variables 2" class="sombra" />

```flux
from(bucket: "air")
  |> range(start: v.timeRangeStart, stop: v.timeRangeStop)
  |> filter(fn: (r) => contains(value: r["reg"], set: ${Probintzia:json}))
  |> keyValues(keyColumns: ["key"])
  |> group()
  |> keep(columns: ["key"])
  |> distinct(column: "key")
```

Como puedes observar en el código de esta query, vamos a prefiltrar los valores de municipios a las regiones seleccionadas en la variable de la región, que en mi caso es “Probintzia”, por lo que si tu le has llamado de otra manera, deberás cambiarlo por el nombre de la variable que hayas usado.

Finalmente vamos a crear una tercera variable pero esta vez, cambiamos el tipo de _*query_* a _*interval*_. Esta variable la dejaremos oculta y la utilizaremos para controlar de forma más óptima el número de puntos que se visualizan simultáneamente por cada serie en las gráficas. Esto lo hacemos ya que vamos a manejar una gran cantidad de series, una por cada juego de región/estación y debemos cuidar el rendimiento para que renderizado sea óptimo y no tarde una eternidad:

<img src="/images/posts/aire/grafana_variable_3.png" alt="grafana variable 3" class="sombra" />

Una vez creadas las variables, podemos ponernos a jugar con las gráficas. La complejidad a la hora de crear los cuadros de mando con dos bases de datos es que tenemos que realizar dos _*querys*_ simultaneas y juntar los datos respetando las etiquetas de los filtros. El ejemplo más sencillo que podemos hacer tendrías dos _*querys*_ en una:

```flux
t1 = from(bucket: "air")
  |> range(start: v.timeRangeStart, stop: v.timeRangeStop)
  |> filter(fn: (r) => r["_measurement"] == "iaqi")
  |> filter(fn: (r) => r["_field"] == "aqi")
  |> filter(fn: (r) => contains(value: r["key"], set: ${Udalerria:json}))

t2 = from(bucket: "air_1d")
  |> range(start: v.timeRangeStart, stop: v.timeRangeStop)
  |> filter(fn: (r) => r["_measurement"] == "iaqi")
  |> filter(fn: (r) => r["_field"] == "aqi")
  |> filter(fn: (r) => contains(value: r["key"], set: ${Udalerria:json}))

union(tables: [t1, t2])
  |> aggregateWindow(every: ${min_interval}, fn: mean, createEmpty: false)
  |> map(fn: (r) => ({_value:r._value, _time:r._time, _field:r.key}))
  |> yield(name: "mean")
```

Aquí lo que estoy haciendo es por un lado, crear una tabla temporal t1 con los datos de la tabla _*Air*_ (últimos 14 días con datos horarios), otra tabla temporal t2 con datos diarios de la tabla *Air_1d*, y uniendo estas tablas. Dinalmente a la tabla unida le aplico la reducción de intervalo que venga de la variable automática de _*min_interval*_ reducido calculando la media, y cambio el nombre de cada serie por la etiqueta del nombre de la estación.

De esta manera podremos utilizar las variables para seleccionar la región como filtro:
<img src="/images/posts/aire/grafana_ejemplo_1.png" alt="grafana ejemplo 1" class="sombra" />

Solo una estación como filtro:
<img src="/images/posts/aire/grafana_ejemplo_2.png" alt="grafana ejemplo 2" class="sombra" />

Y ademas cambiando el el periodo a visualizar, el intervalo entre puntos cambiara y el valor se calculará automáticamente realizando una media por intervalo:
<img src="/images/posts/aire/grafana_ejemplo_3.png" alt="grafana ejemplo 3" class="sombra" />

Para ayudaros os dejo el enlace a los Dashboards que he creado y publicado jugando con los formatos de visualización que nos permite Grafana:

- [Sumario](https://data.yayitazale.cc/d/MN6V-zT7k/summary?orgId=4): visualización tanto del AQI como de los contaminantes con la media, máximo y mínimo de cada región en formato sombreado.

- [Históricos por estación](https://data.yayitazale.cc/d/UAPrXck7z/historical-data-by-municipality?orgId=4): el filtrado también se puede hacer por estación, para tener los registros históricos y poder analizar la evolución por cada región y municipio.

- [Vista de mapa](https://data.yayitazale.cc/d/HHfl2hk7z/map-view?orgId=4&refresh=5m): utilizando las coordenadas GPS de posicionamiento de las estaciones y un sombreado de color en función de la calidad del aire, un mapa donde poder ver la situación actual

Si queréis reutilizar estos dashboards simplemente tenéis que descargarlos desde el [repositorio que he creado](https://gitlab.com/yayitazale/air-monitoring-with-waqi-nodered-influx-grafana/-/tree/main/Grafana%20Dashboards) y después importarlos haciendo clic en el menú e importar:

<img src="/images/posts/aire/grafana_import.png" alt="grafana import" class="sombra" />

---

Esto ha sido todo, os dejo en enlace directo anclado en el menú del blog por si más adelante os interesa entrar a ver los cuadros de mando sobre la calidad del aire de Euskadi. Espero que sea interesante, si tienes cualquier duda o complicación no dudes en dejarlo en los comentarios.

Un saludo y nos vemos en el siguiente post.

---
<a href="https://t.me/firstcommit" class="btn btn-primary text-white py-2">Suscríbete, que es gratis</a>

*Nota: algunos de los enlaces a productos o servicios pueden ser enlaces referidos con los que podemos obtener una comisión de venta.*

<style> 
img.sombra {
  display: block;  
  margin-left: auto;  
  margin-right: auto; 
  box-shadow: 5px 5px 8px grey;
}
img.sombra-small {
  display: block;  
  margin-left: auto;  
  margin-right: auto; 
  box-shadow: 5px 5px 8px grey;
  max-width: 20%;
}
img.sombra-inline {
  display: inline-block; 
  vertical-align: middle; 
  margin-right: 0px; 
  box-shadow: 3px 3px 8px grey;
}
img.zoom {
  transition: transform .2s;
}
img.zoom:hover {
  -ms-transform: scale(1.2); /* IE 9 */
  -webkit-transform: scale(1.2); /* Safari 3-8 */
  transform: scale(1.2); 
}
</style>
