---
title: Vaultwarden, un gestor de contraseñas selfhosted segura y práctica
authors: [yayitazale]
date: 2021-05-09T19:00:00Z
image: /images/posts/vaultwarden/banner.jpg
categories:
  - security
tags:
  - password
  - guia
  - bitwarden
  - accesos
  - seguridad
  - contraseñas
---
__¿Has olvidado la contraseña? Haz click aquí para restaurarla.__

__Perdona, tu contraseña debe tener al menos 50 caracteres, una letra minúscula, dos mayúsculas, cinco números, dos caracteres especiales, un carácter cirílico, un carácter chino, dos espacios, no debe contener ni tu nombre, ni tu fecha de nacimiento, ni tu dirección.__

Si estás cansado de que estas frases sean tu día a día pero no quieres usar contraseñas tipo __12345__, __0000__, __contraseña__ etc deberías plantearte utilizar algún sistema que te permita recordar todas tus contraseñas de forma sencilla. 

Algunos te dirán que lo mejor es una libreta de papel, pero el inconveniente de una libreta es que la deberás llevar contigo a todas partes y si la pierdes o te la roban, todas tus contraseñas estarán expuestas a quien la encuentre.

Por suerte, hoy en día existen una “libretas digitales”, aplicaciones creadas para gestionar contraseñas de forma segura y que nos hace la vida un poquito más fácil y un poquito más segura.

#### TOC

---
# Cómo funciona un gestor de contraseñas

Un [gestor de contraseñas](https://es.wikipedia.org/wiki/Gestor_de_contrase%C3%B1as) es un programa que se utiliza para __almacenar__ una gran cantidad de __pares de usuario/contraseña__.​ La base de datos donde se guarda esta información está cifrada mediante una única clave o contraseña maestra, de forma que el usuario __solo tenga que memorizar esa contraseña__ para acceder a todas las demás. Esto facilita la administración de contraseñas y fomenta que los usuarios escojan claves complejas sin miedo a no recordarlas posteriormente. 

Siguiendo la analogía que he hecho en la introducción, un gestor de contraseñas no deja de ser __una especie de libreta digital__, pero a diferencia de uno físico, lo dejaremos siempre en un sitio al que accederemos de forma remota. Esta libreta, llamada __caja fuerte__ o vault, tendrá un candado con una contraseña que solo nosotros conocemos.

Como estarás pensando, el mero hecho de utilizar un gestor de contraseñas hace que corramos el riesgo de ser atacados y que alguien pueda acceder a todas nuestras contraseñas. Sin embargo, la alternativa habitual suele ser aún peor, la mayoría de las personas utilizan [la misma contraseña para todo](https://es.statista.com/grafico/23636/contrasenas-mas-usadas-en-el-mundo/), o tiene dos o tres contraseñas según la complejidad requerida por cada página web o app. 

Estas contraseñas que usamos en las páginas webs o apps también [suelen ser objetivo de ataques](https://edition.cnn.com/2021/04/06/tech/facebook-data-leaked-what-to-do/index.html) y en muchos casos estas contraseñas acaban siendo vendidas en la red. Si no me crees, existe una web específica donde están almacenadas todos los ataques conocidos hasta la fecha a las páginas web más usadas a nivel mundial y donde, introduciendo solamente nuestro correo electrónico de registro, nos dirá si nuestra contraseña ha sido expuesta alguna vez:

<a href="https://haveibeenpwned.com/"><img src="/images/posts/vaultwarden/pwned.png" alt="pwoned" class="zoom" /></a>

Con un gestor de contraseñas __ganaremos en seguridad__, sobre todo si lo utilizamos siendo conscientes del riesgo inherente que tiene y seguimos unas pautas de __sentido común__. Además, ganaremos mucho en __calidad de vida__ ya que nuestra operativa diaria será muy similar, solo deberemos recordar una contraseña para poder acceder a cualquier web o app.

---
## Cloud vs Selfhosted

Es posible que a estas alturas ya te haya aparecido publicidad de alguno de los servicios gratuitos o de pago que existen en la web como [lastpass](https://www.lastpass.com/es/), [1password](https://1password.com/es/), [dashlane](https://www.dashlane.com/es/), [enpass](https://www.enpass.io/). También es posible que estés utilizando ya un gestor de contraseñas integrado en tu navegador, como el de [Google](https://passwords.google.com/) o [Firefox](https://www.mozilla.org/en-GB/firefox/lockwise/). 

En todos estos casos, el denominador común es que la __base de datos__ donde se almacenan tus contraseñas está __alojada en el cloud__, que no deja de ser el servidor de otro. Además, al ser un servicio compartido por miles o millones de usuarios, [los hace muy atractivos para intentar ataques](https://www.upguard.com/blog/lastpass-vulnerability-and-future-of-password-security). 

Dependemos siempre del proveedor del servicio para atajar y parchear estos problemas. Y no solo eso, sino que en muchos casos (sobre todo en los servicios gratuitos), el proveedor suele utilizar tus metadatos para venderlos a terceros y hacer negocio con ellos, por lo que tu privacidad queda expuesta.

En cualquier caso, la alternativa selfhosted también implica que somos completamente responsables de la seguridad y nivel de parcheo de nuestra aplicación, el nivel de seguridad que tengamos en nuestra red y servidor etc, por lo que tampoco estaremos 100% seguros nunca. Si eres de los que no se fía y prefiere tenerlo en un servidor de un proveedor, __mi recomendación es usar la herramienta nativa de Firefox (y también firefox como navegador)__.

En este post, vamos a instalar y configurar una herramienta open source basada en [Bitwarden, un gestor de contraseñas open source](https://bitwarden.com/), y lo haremos sobre [nuestro servidor unraid](https://firstcommit.dev/2021/01/31/Unraid/) en el que ya tendremos funcionando nuestro [proxy reverso](https://firstcommit.dev/2021/04/19/saas/) para poder publicar el microservicio al exterior.

---
# Instalar Vaultwarden

El gestor de contraseñas que vamos a instalar es [Vaultwarden](https://github.com/dani-garcia/vaultwarden), un servidor liviano para entornos domésticos que es __compatible con los clientes de Bitwarden__. Podríamos instalar el Bitwarden original, pero es muy exigente a nivel de recursos, por lo que si nuestra intención no es un uso empresarial(para un pyme si podría servir) y no requerimos de todas las funciones que tiene el original, con esta implementación liviana será más que suficiente. 

Su instalación en Unraid es muy sencilla. Vamos a la tienda de apps y buscamos por “Vaultwarden” y le damos a instalar:

<img src="/images/posts/vaultwarden/install1.png" alt="Install 1" class="sombra" />

Los datos a rellenar son los siguientes:

<img src="/images/posts/vaultwarden/install2.png" alt="Install 2" class="sombra" />

- Puerto: podemos poner lo que queremos, siempre deberá ser un puerto libre y no el 80.
- Email: será el usuario administrador
- Dejaremos deshabilitados las invitaciones por ahora, habilitamos los websockets y las altas de usuarios.

Para crear un token de administración desde donde configuraremos los distintos aspectos del backend, vamos a la terminal de Unraid y tecleamos 

```bash
openssl rand -base64 48
```

<img src="/images/posts/vaultwarden/install3.png" alt="Install 3" class="sombra" />
<img src="/images/posts/vaultwarden/install4.png" alt="Install 4" class="sombra" />

Copiamos el token que sale y lo añadimos en la variable, aceptamos, y tendremos el servidor funcionando.

---
# Configurar Vaultwarden

Ahora, vamos a la dirección http://IPDELSERVIDOR:8082/admin, introducimos el Token que acabamos de añadir y vamos a configurar las tripas del servidor. 

<img src="/images/posts/vaultwarden/config1.png" alt="Config 1" class="sombra" />

Vamos bloque por bloque y solo tocando lo que describo:

## General settings

Aquí tenemos las configuraciones más generales:

- Domain URL: escribiremos el dominio del que vayamos a acceder. Esto lo usaremos luego para crear el proxy reverso. Por ejemplo: __https://vw.example.com__
- Trash auto-delete: los elementos eliminados desaparecerán de la papelera en el tiempo que pongamos, por ejemplo, 30 días.
- Require email verification on signups = true. Esto hará que los usuarios utilicen correos electrónicos reales

## SMTP Email Settings

El correo SMTP servirá para que el servidor pueda comunicarse automáticamente por correo con los usuarios. Esto creo que es vital por lo que deberás utilizar algún proveedor de correo que permita hacer uso del SMTP. El ejemplo lo haré con un email de gmail:

- Enabled = true
- Host = smtp.gmail.com
- Enable Secure SMTP = true
- Port = 587
- From Address = tu email de gmail
- From name = por ejemplo Vaultwarden
- Username = tu email de gmail
- Password = tu password de gmail
- SMTP Auth mechanism = “Login”

Para probar si funciona, escribe una dirección de correo (puede ser la misma que usas tu) y le das a __Send test email__. Si recibes un correo es que todo está OK.

## Email 2FA Settings

Esto hace que los accesos de los usuarios desde dispositivos nuevos vayan siempre al menos con una autentificación de doble factor, esto es, con un token de uso único que le llegará por correo además de su usuario y contraseña maestra. Esto es muy importante ya que nos aseguramos que por mucho que un hacker consiga nuestra contraseña maestra, no podrá acceder a nuestra caja fuerte de contraseñas. Lo ideal de todas formas, es activar al 2FA pero con un autenticador móvil, como [Authy](https://authy.com/) que [comentamos anteriormente](https://firstcommit.dev/2021/04/19/saas/#SaaS-seguro-con-TSL), ya que es posible que alguien consiga suplantar nuestra identidad en nuestro proveedor de correo electrónico.

# Publicar el microservicio en Nginx Proxy Manager

Ahora, vamos a configurar el Nginx Proxy Manager. Como ya te acordarás, accedemos al panel de administración y añadimos un nuevo host con los datos de nuestra instalación y configuración:

<img src="/images/posts/vaultwarden/config2.png" alt="Config 2" class="sombra" />

<img src="/images/posts/vaultwarden/config3.png" alt="Config 3" class="sombra" />

Aceptamos, y una vez creado el registro, volvemos a la pantalla de admin de vaultwarden y vamos a la pestaña de users. Aquí, enviamos una invitación al email que hemos configurado como administrador.

En el correo llegará un enlace a https://vw.example.com que hemos configurado, y deberemos crear la cuenta usando una contraseña maestra. Esta contraseña maestra deberá ser lo suficientemente complicada para que sea segura, pero fácil de recordar. Por ejemplo: *I&l0v3&pizza*.

<img src="/images/posts/vaultwarden/config4.png" alt="Config 4" class="sombra" />

Nos pedirá activar el TOTP, para ello usaremos [Authy](https://authy.com/) como he comentado antes.

# Configurar clientes Bitwarden

Los clientes de vaultwarden son los mismos que los de bitwarden y los podremos descargar de la [página oficial del proyecto](https://bitwarden.com/#download). En cualquier dispositivo los pasos para conectar el cliente a nuestra caja fuerte serán idénticos. Una vez instalado o añadida la extensión del navegador vamos a ajustes:

<img src="/images/posts/vaultwarden/client1.png" alt="Client 1" class="sombra" />

Introducimos la URL que hayamos utilizado para publicar el servicio (https://vw.example.com):

<img src="/images/posts/vaultwarden/client2.png" alt="Client 2" class="sombra" />

Y una vez añadido, procederemos a hacer login con nuestra contraseña maestra y el TOTP de authy:

<img src="/images/posts/vaultwarden/client3.png" alt="Client 3" class="sombra" />

Ahora que ya tenemos el cliente configurado, funcionará tal y como lo hace el gestor de contraseñas de chrome o firefox, cada vez que introducimos un usuario y contraseña en una página de login nos notificará si queremos guardar dichas credenciales. Mi recomendación para hacer que el proceso de migración sea lo más llevadero posible es ir guardando esas credenciales e inmediatamente después modificar la contraseña de cada sitio, creando una nueva con el motor de creación de contraseñas aleatoria que integra.

Además, por comodidad, podríamos ir activando la 2FA en todas las páginas web que lo permitan e ir añadiendo el código en cada registro de usuario y clave.

Existen muchas opciones a configurar a gusto de cada uno, por lo que os invito a pasar por el [manual de usuario de Vaultwaren](https://github.com/dani-garcia/vaultwarden/wiki) para repasar todas esas opciones y dejar los clientes a tu gusto.

Y como siempre, si tienes cualquier problema o duda en la instalación, publicación o uso de este servicio, no dudes en dejar tu consulta en los comentarios y te ayudaré a resolverlo.

---
<a href="https://t.me/firstcommit" class="btn btn-primary text-white py-2">Suscríbete, que es gratis</a>

*Nota: algunos de los enlaces a productos o servicios pueden ser enlaces referidos con los que podemos obtener una comisión de venta.*

<style> 
img.sombra {
  display: block;  
  margin-left: auto;  
  margin-right: auto; 
  box-shadow: 5px 5px 8px grey;
}
img.sombra-inline {
  display: inline-block; 
  vertical-align: middle; 
  margin-right: 0px; 
  width:auto;
  box-shadow: 3px 3px 8px grey;
}
img.zoom {
  transition: transform .2s;
}
img.zoom:hover {
  -ms-transform: scale(1.2); /* IE 9 */
  -webkit-transform: scale(1.2); /* Safari 3-8 */
  transform: scale(1.2); 
}
</style>
