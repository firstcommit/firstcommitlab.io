---
title: Frigate, videovigilancia DIY con inteligencia artificial
authors: [yayitazale]
date: 2022-01-09T19:30:00Z
image: /images/posts/frigate/intro.jpg
categories:
  - selfhosted
tags:
  - cloud
  - selfhosted
  - guia
  - servidores
  - nvr
  - cctv
  - ia
  - domotica
  - mqtt
  - frigate

---
Para empezar el año os traigo un post que tenía muchas ganas de publicar y que los suscriptores también pidieron en la [encuesta que realizamos en el canal de telegram hace tiempo](https://t.me/firstcommit/29).

En los últimos años ha habido un crecimiento de ventas de __cámaras IP__ con cada vez una mayor cantidad de opciones de marcas que nos atan a su vez a sistemas propietarios en la nube. Con este sistema, podremos integrar en nuestra plataforma [Home Assistant](https://firstcommit.dev/2021/03/11/Hass/) todas las cámaras y tener __notificaciones utilizando inteligencia artificial__ para detectar objetos (personas, gatos, pájaros, coches, etc).

#### TOC

# Funcionamiento

Lo primero que demos hacer es entender el funcionamiento del sistema que vamos a implementar. El flujo de nuestras imágenes será el del siguiente esquema, donde podéis ver el papel que juega Frigate por si solo en todo el camino desde la cámara hasta la notificación inteligente con Telegram:

<img src="/images/posts/frigate/esquema.png" alt="esquema" class="sombra" />

Y esto es lo que hace cada uno de los elementos del esquema en cada paso:

-	__Cámaras__: emitirán la imagen de vídeo comprimida y a unos 25 [FPS](https://es.wikipedia.org/wiki/Fotogramas_por_segundo).
-	__[FFmpeg](https://es.wikipedia.org/wiki/FFmpeg)__: descomprimirá el vídeo en tiempo real y reducirá los FPS a lo que nosotros configuremos (por ejemplo, 10 FPS).
-	__[OpenCV](https://es.wikipedia.org/wiki/OpenCV)__: recibirá los fotogramas y nos dirá si hay zonas en la imagen donde hay movimiento. La salida serán las zonas recortadas donde ha encontrado movimiento.
-	__[TensorFlow](https://es.wikipedia.org/wiki/TensorFlow)__: recibirá los recortes de los fotogramas donde haya movimiento y usando un modelo de inteligencia artificial nos dirá si ha detectado alguno de los objetos que busquemos y con que confianza (de 0 a 100%). A partir de cierta confianza se iniciará un evento de detección y se guardará todo el vídeo desde el primer fotograma con una detección hasta el último cuando el objeto salga de la escena.
-	__[MQTT](https://firstcommit.dev/2021/03/14/mqtt/)__: el evento generará avisos (inicio y fin de un evento) que se enviarán a nuestro broker MQTT para que otros puedan utilizarlo como input 
-	__[Node-Red](https://firstcommit.dev/2021/07/14/plantas/#Node-Red-la-aplicacion-definitiva-para-automatizaciones)__: conectaremos el MQTT para saber cuando tenemos un final de evento y crearemos un flujo lógico para coger el _clip_ o _snapshot_ del evento y enviarlo a telegram.
-	__[Telegram](https://es.wikipedia.org/wiki/Telegram)__: crearemos un grupo para recibir los avisos con el video/foto del evento.

# Requisitos

Antes de empezar con las instalación vamos a hacer caso a la [documentación](https://docs.frigate.video/hardware) publicada por los autores del proyecto y vamos a revisar los requisitos para poner en marcha el sistema.

El primer requisito y el más importante es que debemos tener alguna cámara IP que permita el [streaming en RTSP](https://es.wikipedia.org/wiki/Protocolo_de_transmisi%C3%B3n_en_tiempo_real) y emita dicho [streaming de vídeo en formato H.264](https://es.wikipedia.org/wiki/H.264/MPEG-4_AVC).

Existen muchas marcas con diferentes precios y especificaciones pero mi recomendación es que si necesitas cámaras nuevas, compres las de la marca [Tapo](https://www.tapo.com/es/product/smart-camera/), la línea de IoT de TP-Link. 

Estas cámaras tienen una API (local) que nos permite activar y desactivar el __modo privacidad__, que consiste en un apagado del sensor. Además, si adquirimos las cámaras tipo PTZ, a través de la API podremos mover físicamente la cámara para apuntar al techo o pared cuando estemos en casa y queramos __privacidad físicamente asegurada__. 

En cualquier caso existen muchas otras marcas reconocidas como [Hikvision](https://www.hikvision.com/es/products/IP-Products/Network-Cameras/) o [Amcrest](https://amcrest.com/security-cameras-home.html) que nos permitirán activar el streaming RTSP sin dificultad.

En mi caso tengo:

-	[Tapo C100](https://amzn.to/33lSl9O): para zonas en interior en las que la privacidad no es tan importante y quiero una cámara pequeña y compacta. 
-	[Tapo C200](https://amzn.to/3zGiD2v): para interior, cuando la privacidad es importante prefiero poder hacer que la cámara mire al techo cuando estoy en casa
-	[Tapo C310](https://amzn.to/3GanMSV): para exteriores

<img src="/images/posts/frigate/tapocam.jpg" alt="tapoc100" class="sombra" />

Una vez que tengamos las cámaras tendremos que configurarlas y habilitar el streaming RTSP. Como depende mucho de cada marca, os dejo [este enlace para hacerlo con las Tapo](https://www.tp-link.com/es/support/faq/2680/). Es importante que cuando configures dicho stream anotes el enlace y la resolución ya que necesitaremos esa información para crear el archivo de configuración de Frigate.

En cuanto al servidor, como en todos los post hasta la fecha, voy a dar por supuesto que tienes un [servidor funcionando con unraid](https://firstcommit.dev/2021/01/31/Unraid/). De todas formas, si es tu primera vez aquí y no tienes un servidor en casa, quiero que sepas que [existe la posibilidad de instalarlo sobre una RPI](https://docs.frigate.video/hardware#server). Los pasos a seguir serán parecidos y si necesitas ayuda puedes pasarte pedirla en los comentarios o en nuestro [canal de Discord](https://discord.gg/QK8Ta5kq).

En cualquier caso, en el servidor podremos hacer que todo funcione sobre la CPU sin gastar un solo euro, por ejemplo para probar, pero es recomendable equipar con un par de elementos de hardware específicos que eviten el uso continuado de CPU. 

Si te ha picado la curiosidad y has leído lo que pone en el enlace a [streaming de vídeo en formato H.264](https://es.wikipedia.org/wiki/H.264/MPEG-4_AVC), te habrás dado cuenta de que se trata de una emisión de vídeo comprimida (para que el tráfico en la red sea la menor posible). Esto hace que nuestra aplicación tenga que descomprimir/decodificar el vídeo que le llegue de cada cámara, algo que no le gusta mucho a nuestra CPU. Para que el proceso sea más eficiente podemos utilizar la [aceleración por hardware](https://docs.frigate.video/configuration/hardware_acceleration) que han implementando los desarroladores. Dependiendo del tipo de hardware que tengamos en el servidor podremos usar la gráfica integrada de nuestra CPU Intel o AMD, o bien una gráfica dedicada de Nvidia. Para quede más sencillo vamos a instalar la versión que utiliza la gráfica integrada de la CPU ya que es bastante eficiente y no nos va a costar dinero ya que es un elemento que ya tenemos.

<img src="/images/posts/frigate/tensor.png" alt="tensor" class="sombra" />

En cuanto a la inteligencia artificial para detectar objetos, este sistema utiliza modelos pre-entrenados con la librería [TensorFlow](https://es.wikipedia.org/wiki/TensorFlow), para el cual es conveniente de nuevo no utilizar la CPU ya que es poco eficiente. El modelo a ejecutar concretamente es de tipo TF-lite, para lo cual lo ideal es utilizar placas [aceleradoras de IA](https://es.wikipedia.org/wiki/Acelerador_de_IA) de tipo [TPU](https://es.wikipedia.org/wiki/Unidad_de_procesamiento_tensorial). Esto nos ofrecerá un rendimiento mucho mayor a coste energético mucho más bajo y además con mucha más velocidad. Esto quiere decir que cuando le pasemos un fotograma del vídeo al modelo de inteligencia artificial tardará mucho menos tiempo en responder si ha detectado el tipo de objeto que buscamos. 

Las más conocidas son las [Coral Edge](https://www.coral.ai/products/#production-products), de las cuales con este sistema funcionan los modelos de __PCIe__ (tanto el mini-PCI como los M.2) o __USB__. 

<img src="/images/posts/frigate/coral.webp" alt="tensor" class="sombra" />

Con esto ya tendríamos cubiertos los requisitos de hardware. En cuanto al software, como hemos visto en [Funcionamiento](#Funcionamiento), necesitas tener previamente instalado el broker de MQTT, Node-Red y Home Assistant.


# Instalando Frigate sobre unraid con Docker

Antes de instalar el contenedor de frigate debemos crear un archivo de configuración mínimo en la ruta del servidor _/appdata/frigate_ (tendrás que crear la ruta también). Simplemente puedes descargar [este archivo](https://gitlab.com/firstcommit/firstcommit.gitlab.io/-/raw/master/source/images/posts/frigate/files/initial/config.yml?inline=false) y editarlo con la configuración que corresponda con tu instalación o [seguir los pasos de la documentación oficial para crear uno](https://docs.frigate.video/guides/getting_started). Más adelante, cuando  tengamos todo funcionando podremos alterar este archivo para dejarlo a nuestro gusto.

Además, en caso de que nuestro TPU sea de tipo PCIe, demos instalar el driver correspondiente (en caso de que sea USB no es necesario). Para ello, vamos a la _app store_ de unraid y buscamos por _Coral_:

<img src="/images/posts/frigate/install0.png" alt="install0" class="sombra" />

Lo instalamos y cuando termine aceptamos:

<img src="/images/posts/frigate/install0.1.png" alt="install01" class="sombra" />

Para proceder a la instalación de Frigate, como siempre, vamos a la _app store_ de unraid y buscamos por __Frigate__. Como puedes ver, tenemos dos versiones, la primera para instalaciones con la aceleración de hardware usando la gráfica integrada de la CPU, y la segunda para instalaciones con una gráfica Nvidia para dicha tarea:

<img src="/images/posts/frigate/install1.png" alt="install1" class="sombra" />

Si te fijas bien y haces click en el logo del programa, puedes ver que la persona que mantiene estas plantillas de unraid es, _oh wait_, soy yo! 

Elegimos el primero (sin nvidia) y le damos a install y nos llevará a la pantalla de configuración del contenedor:

<img src="/images/posts/frigate/install2.png" alt="install2" class="sombra" />

Aquí solo deberemos editar la entrada _TPU Mapping_ de nuevo de acuerdo a si tenemos un TPU PCI o uno USB. Si es PCI debemos sustituir _/dev/bus/usb_ por */dev/apex_0*. También como siempre conviene que revises que no tienes los puertos 1935 y 5000 ocupados por otra APP. En caso de que así sea deberás cambiarlos por otros.

<img src="/images/posts/frigate/install3.png" alt="install3" class="sombra" />

Aceptamos y esperamos a que arranque, si hemos hecho todo bien deberíamos pode acceder a http://IP-DE-UNRAID:5000 y deberemos ver la interfaz web:

<img src="/images/posts/frigate/install4.png" alt="install4" class="sombra" />

# Configurar Frigate para detección de personas y gatos

Ahora que ya tenemos el sistema funcionando vamos a seguir los pasos de la documentación para crear un [archivo de configuración completo](https://docs.frigate.video/configuration/index). 

Como es bastante extenso, voy a comentar por encima los elementos básicos que debes configurar en un [archivo de ejemplo ](https://gitlab.com/firstcommit/firstcommit.gitlab.io/-/raw/master/source/images/posts/frigate/files/complete/config.yml?inline=false). 

Los elementos que podemos rastrear dependen del banco de imagenes o datatset con el que se haya entrenado el modelo. El modelo que usa frigate está entrenado con el dataset [COCO](https://cocodataset.org/#home) y la lista de objetos que podemos rastrear la tienes [aquí](https://docs.frigate.video/configuration/objects). Por ejemplo:

- person
- bicycle
- car
- motorcycle
- airplane
- bus
- train
- car
- boat
- traffic light
- fire hydrant
- stop sign
- parking meter
- bench
- bird
- cat
- dog
- horse
- sheep
- cow
- elephant
- bear
- zebra
- giraffe
- ...

Además es importante que entiendas cómo se lanzan los eventos en base a la configuración de *min_score* y *threshold*. Por cada fotograma que le pasemos al modelo de TensorFlow nos devolverá una puntuación (confianza) si encuentra algún objeto de los que hemos configurado para rastrear. Cualquier puntuación por debajo de *min_score* será ignorada como un falso positivo. Si obtenemos puntuaciones por encima de ese valor empieza el rastreo del objeto. El *threshold* o umbral se basa en la media del historial de puntuaciones (3 valores) para un objeto rastreado. Tienes todo mejor explicado [aquí](https://docs.frigate.video/guides/false_positives).

<img src="/images/posts/frigate/tabla.png" alt="Tabla" class="sombra" />

Si la media de 3 puntuaciones está por encima del umbral configurado se considera que hemos detectado el objeto y se inicia un evento en Frigate. Cuando de nuevo, la media caiga por debajo del umbral consideraremos que el evento ha finalizado. 

Estos eventos generarán un pequeño clip de vídeo y un snapshot con el fotograma con mayor puntuación de todo el evento. 

# Notificaciones de Telegram

Ahora que hemos configurado nuestra cámara para rastrear personas y gatos, vamos a crear un flujo en Node-Red para enviar notifiaciones de Telegram. Lo primero es crear un bot de telegram y un grupo al que vamos a invitar al bot.

En Telegram buscamos a [BotFather](https://t.me/BotFather) y usando el comando */newbot* sigues los pasos para crearlo,. Guardamos el TOKEN. Después creas un grupo de telegram y añades al bot a dicho grupo. Escribes un mensaje en grupo para iniciarlo. En el navegador abres este enlace rellenando el token que te haya dado para el BotFather: https://api.telegram.org/bot+Token/getUpdates y copias el chatID.

En node red instalamos el _pallete_ [node-red-contrib-telegrambot](https://flows.nodered.org/node/node-red-contrib-telegrambot) y después importamos este [flow](https://gitlab.com/firstcommit/firstcommit.gitlab.io/-/raw/master/source/images/posts/frigate/files/flows.json?inline=false).

<img src="/images/posts/frigate/nodered.png" alt="nodered" class="sombra" />

Tendrás que configurar el nodo de MQTT con la configuración de tu broker, el nodo de Home Assistant con tu instancia de HASS y el nodo de Telegram con los datos de tu bot. Además, en el nodo *Create Message* debes añadir el chatID del grupo que hayas creado en telegram.

Con esto tendremos notificaciones para cuando el gato se detecte estemos o no en casa y de personas si no estamos en casa. Nos llegará en el final del evento un vídeo y una foto con el *bounding box* y el porcentaje de la puntuación obtenida tipo:

<img src="/images/posts/frigate/bird.jpg" alt="bird" class="sombra" />
<img src="/images/posts/frigate/cat.jpg" alt="cat" class="sombra" />
<img src="/images/posts/frigate/person.jpg" alt="person" class="sombra" />

# Integración de Frigate en Home Assistant

Frigate tiene un componente que puedes instalar vía [Hacs](https://firstcommit.dev/2021/04/05/home-automation/#HACS-y-Themes). Si buscas por *Frigate* podrás instalarlo. Luego si vas a integraciones y configuras el elemento Frigate tendremos no creará sensores y cámaras para poder añadirlas en un dashboard:

<div id="banner" style="overflow: hidden;justify-content:space-around;">
    <div class="" style="max-width: 49%;max-height: 55%;display: inline-block;">
        <img src="/images/posts/frigate/integracion2.png">
    </div>
    <div class="" style="max-width: 49%;max-height: 55%;display: inline-block;">
        <img src="/images/posts/frigate/integracion3.png">
    </div>
</div>

<img src="/images/posts/frigate/integracion4.png" alt="integracion4" class="sombra" />

# Control de Privacidad con la API de TAPO

En este caso también en Hacs tendremos la posibilidad de instalar el elemento Tapo:

Tendremos que configurar cada una de las cámaras ya que lo que hace este componente es enviar las peticiones al endpoint de cada una de las cámaras en vez de al cloud.

<div id="banner" style="overflow: hidden;justify-content:space-around;">
    <div class="" style="max-width: 49%;max-height: 50%;display: inline-block;">
        <img src="/images/posts/frigate/tapo1.png">
    </div>
    <div class="" style="max-width: 49%;max-height: 50%;display: inline-block;">
        <img src="/images/posts/frigate/tapo2.png">
    </div>
</div>

<img src="/images/posts/frigate/tapo3.png" alt="tapo3" class="sombra" />

El elemento nos creará servicios en Home assitant para llamar al modo privacidad o a los movimientos PTZ de las cámaras que lo soporten. Con esto podremos crear automatizaciones por ejemplo en Node-Red para que el moto privacidad salte solo:

<img src="/images/posts/frigate/tapo4.png" alt="tapo4" class="sombra" />


Nos vemos en el siguiente post.

---
<a href="https://t.me/firstcommit" class="btn btn-primary text-white py-2">Suscríbete, que es gratis</a>

*Nota: algunos de los enlaces a productos o servicios pueden ser enlaces referidos con los que podemos obtener una comisión de venta.*

<style> 
img.sombra {
  display: block;  
  margin-left: auto;  
  margin-right: auto; 
  box-shadow: 5px 5px 8px grey;
}
img.sombra-small {
  display: block;  
  margin-left: auto;  
  margin-right: auto; 
  box-shadow: 5px 5px 8px grey;
  max-width: 20%;
}
img.sombra-inline {
  display: inline-block; 
  vertical-align: middle; 
  margin-right: 0px; 
  box-shadow: 3px 3px 8px grey;
}
img.zoom {
  transition: transform .2s;
}
img.zoom:hover {
  -ms-transform: scale(1.2); /* IE 9 */
  -webkit-transform: scale(1.2); /* Safari 3-8 */
  transform: scale(1.2); 
}
</style>
