---
title: yayitazale
image: /images/authors/yayitazale.jpg
description: aka Joseba Egia.
social:
  telegram: https://t.me/yayitazale
  gitlab: https://gitlab.com/yayitazale
  linkedin: https://www.linkedin.com/in/joseba-egia-larrinaga
  instagram: https://www.instagram.com/yayitazale/
  website: https://josebaegia.me/
---

aka Joseba Egia. Soy un Apasionado del DiY en general pero sobreotodo lo que me gusta es el automatizar cosas. Me encanta hacer que las cosas funcionen solas y a eso dedico parte de mi tiempo actual, para ahorrarme tiempo futuro.

En lo laboral, actualmente soy Project Manager IIoT y MES/MOM y profesor universitario agregado en la UPV/EHU.