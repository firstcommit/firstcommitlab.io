---
title: mifulapirus
image: /images/authors/mifulapirus.jpg
description: aka Ángel Hernández.
social:
  telegram: https://t.me/mifulapirus
  twitter: https://twitter.com/angelrobots
  linkedin: https://www.linkedin.com/in/angelhernandezmejias
  instagram: https://www.instagram.com/mifulapirus/
---

AKA Ángel. Ingeniero desviado al mundo de los negocios internacionales y zarandeado por la robótica de un lugar del mundo a otro, pero de momento viviendo en Costa Rica entre perezosos y tucanes y disfrutando del cacharreo en público con mi amigo Yayitazale.
