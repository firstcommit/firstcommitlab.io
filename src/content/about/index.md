---
title: "About"
meta_title: "About"
image: "/images/author.jpg"
draft: false

what_i_do:
  title: "De qué creemos que sabemos"
  items:
  - title: "IoT y domótica"
    description: "¿Automatizar cosas para que? Para que mi casa trabaje por mi."
  
  - title: "Selfhosting"
    description: "No hay nada mejor que un buen proyecto de software libre que pueda alojar en mi propio servidor. Recuerda que el cloud no deja de ser el servidor de otro."
  
  - title: "Impresión 3D"
    description: "Podría pasarme horas mirando máquinas creando cosas de forma automática. No existe proyecto DiY que se precie sin una buena cajita impresa en 3D."

---

Hola mundo!

```bash
git commit -m *First Commit*
```

Si esta línea te da un poco de gustete, seguramente en este blog encuentres información que te será útil. Si no tienes ni idea de lo que es, pero has llegado hasta aquí, entonces… 👋¡Hola Mamá!👋

Este es un blog donde que comenzó como un proyecto de un par de amigos que querían ir publicando sus proyectos de forma libre y gratuita con un par de objetivos:

  1. Ayudar a gente que, como nosotros, también desarrolla proyectos interesantes o inventa cacharros curiosos, y puede que necesite alguna pista durante su aventura creativa.
    
  2. ¡Ayudarnos a nosotros mismos! Somos como los catalanes, que hacemos cosas (Si has entendido esta referencia seguramente estés en la siguiente ronda de vacunación de Coronavirus), así que la mayoría de nuestros proyectos quedan perdidos en un mar de líneas de código, diseños 3D y esquemas de los que ya ni nos acordamos.

> Ah! Así se resuelve este 🐛bug🐛  (Tú cuando te encuentras tu propia respuesta en un foro)

En First Commit vas a encontrar información sobre sistemas y proyectos que podrás replicar en su totalidad, pero es muy posible que también encuentres ideas nuevas sobre las que trabajar y, con un poco de suerte, alguna respuesta a ese problema que te trae por mal camino.

Aunque no tenemos una temática concreta, solemos escribir de temas relacionados con la informática, domótica, electrónica, automatización y… bueno, cacharros en general… el caso es que es poco probable que encuentres recetas para hacer cupcakes, a menos que Joseba se monte esa impresora 3D de chocolate que todo el mundo quiere.

En cualquier caso, si quieres que escribamos sobre algo en concreto, ¡aceptamos sugerencias!

---

Nuestra web se encuentra alojada en gitlab y [este es nuestro repositorio público](https://gitlab.com/firstcommit/firstcommit.gitlab.io).

---