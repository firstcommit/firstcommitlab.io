import image from "@astrojs/image";
import mdx from "@astrojs/mdx";
import react from "@astrojs/react";
import sitemap from "@astrojs/sitemap";
import tailwind from "@astrojs/tailwind";
import { defineConfig } from "astro/config";
import config from "./src/config/config.json";
import jopSoftwarematomo from "@jop-software/astro-matomo";
import jopSoftwarecookieconsent from "@jop-software/astro-cookieconsent";
import remarkToc from 'remark-toc';
import { rehypeAccessibleEmojis } from 'rehype-accessible-emojis';
import {readFileSync} from "fs"

const fluxGrammar = JSON.parse(readFileSync("./static/flux.tmLanguage.json"))

const flux = {
  id: "flux",
  scopeName: 'flux',
  grammar: fluxGrammar,
  aliases: ['flux', 'flux lang'],
}


// https://astro.build/config
export default defineConfig({
  site: config.site.base_url ? config.site.base_url : "https://firstcommit.dev",
  base: config.site.base_path ? config.site.base_path : "/",
  outDir: 'public',
  publicDir: 'static',
  trailingSlash: config.site.trailing_slash ? "always" : "never",
  integrations: [react(), sitemap(), tailwind({
    config: {
      applyBaseStyles: false
    }
  }), image({
    serviceEntryPoint: "@astrojs/image/sharp"
  }), mdx(), jopSoftwarematomo({
    baseUrl: "https://mm.yayitazale.cc/",
    siteId: 1
  }), jopSoftwarecookieconsent({
    current_lang: 'es',
    autoclear_cookies: true,
    // default: false
    page_scripts: true,
    // default: false

    onFirstAction: function (user_preferences, cookie) {
      // callback triggered only once
      const analyticsEnabled = window.CC.allowedCategory('analytics');
      console.log(`analytics ${analyticsEnabled ? 'enabled' : 'disabled'}`);
    },
    onAccept: function (cookie) {
      // ...
    },
    onChange: function (cookie, changed_preferences) {
      // ...
    },
    guiOptions: {
      consentModal: {
        layout: "box inline",
        position: "bottom right",
        equalWeightButtons: false,
        flipButtons: false
      },
      preferencesModal: {
        layout: "box",
        position: "right",
        equalWeightButtons: false,
        flipButtons: true
      }
    },
    categories: {
      necessary: {
        readOnly: true
      },
      analytics: {}
    },
    languages: {
      es: {
        consent_modal: {
          title: 'Hola viajero, ¡es la hora de las galletas!',
          description: 'Esta página utiliza cookies de análisis de terceros para mejorar tu experiencia de uso y medir las estadísticas de navegación. <button type="button" data-cc="c-settings" class="cc-link">Gestionar preferencias</button>',
          primary_btn: {
            text: 'Aceptar todo',
            role: 'accept_all' // 'accept_selected' or 'accept_all'
          },

          secondary_btn: {
            text: 'Rechazar todo',
            role: 'accept_necessary' // 'settings' or 'accept_necessary'
          }
        },

        settings_modal: {
          title: 'Preferencias de Consentimiento',
          save_settings_btn: 'Guardar preferencias',
          accept_all_btn: 'Aceptar todo',
          reject_all_btn: 'Rechazar todo',
          close_btn_label: 'Cerrar',
          cookie_table_headers: [{
            col1: 'Name'
          }, {
            col2: 'Domain'
          }, {
            col3: 'Expiration'
          }, {
            col4: 'Description'
          }],
          blocks: [{
            title: 'Uso de Cookies 📢',
            description: 'Esta página web utiliza Matomo, un servicio de analítica web, que permite la medición y análisis de la navegación en las páginas web. En su navegador podrá observar cookies de este servicio. Según la tipología anterior se trata de cookies de sesión (estríctamente necesarias) y de análisis.'
          }, {
            title: 'Cookies estrictamente necesarias',
            description: 'Estas cookies son esenciales para el correcto funcionamiento de este sitio web. Sin estas cookies, el sitio web no funcionaría correctamente.',
            toggle: {
              value: 'necessary',
              enabled: true,
              readonly: true // cookie categories with readonly=true are all treated as "necessary cookies"
            },

            cookie_table: [
            // list of all expected cookies
            {
              col1: 'matomo_sessid',
              col2: 'yayitazale.cc',
              col3: '14 días',
              col4: 'Cookien esencial que no guardad ningún dato de usuario',
              is_regex: true
            }]
          }, {
            title: 'Cookies Analíticas',
            description: 'A través de la analítica web se obtiene información relativa al número de usuarios que acceden a la web, el número de páginas vistas, la frecuencia y repetición de las visitas, su duración, el navegador utilizado, el operador que presta el servicio, el idioma, el terminal que utiliza y la ciudad a la que está asignada su dirección IP. Información que posibilita un mejor y más apropiado servicio por parte de este portal.',
            toggle: {
              value: 'analytics',
              // your cookie category
              enabled: true,
              readonly: false
            },
            cookie_table: [
            // list of all expected cookies
            {
              col1: '_pk_id',
              col2: 'yayitazale.cc',
              col3: '13 meses',
              col4: 'Cookies de identificación del visitante',
              is_regex: true
            }, {
              col1: '_pk_ref',
              col2: 'yayitazale.cc',
              col3: '6 meses',
              col4: 'Cookies de origen del visitante',
              is_regex: true
            }, {
              col1: '_pk_ses',
              col2: 'yayitazale.cc',
              col3: '30 minutos',
              col4: 'Cookies de sesión',
              is_regex: true
            }, {
              col1: '_pk_cvar',
              col2: 'yayitazale.cc',
              col3: '30 minutos',
              col4: 'Cookies de sesión',
              is_regex: true
            }, {
              col1: '_pk_hsr',
              col2: 'yayitazale.cc',
              col3: '30 minutos',
              col4: 'Cookies de sesión',
              is_regex: true
            }, {
              col1: '^mtm',
              // match all cookies starting with "_mtm"
              col2: 'yayitazale.cc',
              col3: 'never',
              col4: 'Cookies de consentiemiento',
              is_regex: true
            }]
          }, {
            title: 'Más información',
            description: 'Para más información visite nuestro página de <a href=\"/privacy-policy\">Política de privacidad</a>'
          }]
        }
      }
    },
    disablePageInteraction: false
  })],
  markdown: {
    remarkPlugins: [remarkToc],
    shikiConfig: {
      theme: "one-dark-pro",
      wrap: true,
      langs: [flux]
    },
    extendDefaultPlugins: true
  }
});